<?php

declare(strict_types=1);

namespace App\Command;

use App\Model\Setting;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * @Command
 */
class CheckVersion extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('checkversion:enable');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Hyperf Demo Command');
    }

    public function handle()
    {
        $status = $this->input->getArgument('status');
        $setting = Setting::where('key', '=', 'show_app_version')->first();

        if(is_null($setting)) {
            Setting::create([
                'name' => 'show_app_version',
                'key' => 'show_app_version',
                'value' => $status,
                'type' => 0,
                'is_hidden' => 1,
                'sort' => 0,
                'remark' => 'show_app_version',
            ]);
        } else {
            Setting::where('key', '=', 'show_app_version')->update([
                'value' => $status,
            ]);
        }
        $this->line('updated status to ' . $status, 'info');
    }

    protected function getArguments()
    {
        return [
            ['status', InputArgument::REQUIRED, 'Payment status, 0 for disable, 1 for enable'],
        ];
    }
}
