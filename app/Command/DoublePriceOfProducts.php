<?php

declare(strict_types=1);

namespace App\Command;

use App\Model\Goods;
use App\Model\GoodsOption;
use App\Model\MulGoodsCategory;
use App\Model\Navigation;
use App\Model\Setting;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;

/**
 * @Command
 */
class DoublePriceOfProducts extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('update:product-prices');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Set price of product as x2 NOTE: NO LONGER IN USER AFTER RESCRAP GOOD COMMAND, DONT NEED TO RUN IT');
    }

    public function handle()
    {
        ini_set('memory_limit', '128M');
        $categories = Setting::where('name', 'like', '%categories_price_%')->first()->pluck('key')->toArray();

        $this->line('Hello Hyperf!', 'info');
        $goods = Goods::select(['id', 'price', 'original_price', 'cost_price'])->get();

        foreach ($goods as $k => $v){
            $category = MulGoodsCategory::select(['id','goods_id', 'cid'])->where('goods_id', '=', $v->id)->first();
            if(in_array($category->cid, $categories)) {

                $nav = Navigation::where('url', 'like', $category->cid)->first();
                if(!is_null($nav)) {
                    $setting = Setting::where('name', '=', 'categories_price_' . $nav->id)->first();
                    $setting = json_decode($setting->value);
                    $orgPriceMarkup = $setting->original_price;
                    $costPriceMarkup = $setting->cost_price;

                    $orgPrice = $v->original_price;
                    $this->line('PRICE: ' . floor($orgPrice * $costPriceMarkup->value), 'info');

                    Goods::where('id', '=', $v->id)->update([
                        'cost_price' => $orgPrice,
                        'original_price' => floor($orgPrice * $orgPriceMarkup->vaule),
                        'price' => floor($orgPrice * $costPriceMarkup->value),
                    ]);
                    $optns = GoodsOption::where('goods_id', '=', $v->id)->get();
                    foreach ($optns as $k1 => $v1) {
                        GoodsOption::where('id', '=', $v1->id)->update([
                            'cost_price' => $orgPrice,
                            'original_price' => floor($orgPrice * $orgPriceMarkup->vaule),
                            'price' => floor($orgPrice * $costPriceMarkup->value),
                        ]);
                    }
                }
            }
        }
    }
}
