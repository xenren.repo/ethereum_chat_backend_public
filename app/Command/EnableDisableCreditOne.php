<?php

declare(strict_types=1);

namespace App\Command;

use App\Model\Setting;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * @Command
 */
class EnableDisableCreditOne extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('EnableDisableCreditOne:status');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Please pass status as true to enable and false to disbale');
    }

    public function handle()
    {
        $argument = $this->input->getArgument('status');
        $bit = $argument == "true" ? 1 :  0;
        Setting::where('key', '=', 'credit1_enabled')->update([
            'value' => $bit
        ]);
        $this->line('Set Credit 1 Status To ' . $argument);
    }

    protected function getArguments()
    {
        return [
            ['status', InputArgument::OPTIONAL, 'Here is an explanation of this parameter']
        ];
    }
}
