<?php

declare(strict_types=1);

namespace App\Command;

use App\Model\Module;
use Hyperf\Command\Annotation\Command;
use Hyperf\Command\Command as HyperfCommand;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * @Command
 */
class MenuLoader extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    protected $lists = [
        'cheatsheet',
    ];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('menu:load');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Menu loader, currently support: ' . implode(',', $this->lists));
    }

    public function handle()
    {
        $menu = strtolower($this->input->getArgument('menu'));
        $name = $this->input->getArgument('name');
        if (! in_array($menu, $this->lists)) {
            return $this->output->error('Menu Not on list');
        }
        $this->generateMenu($menu, $name);
    }

    public function generateMenu($menu, $name = null)
    {
        $pid = 0;
        if (is_null($name)) {
            $name = $menu;
        }
        $create = '';
        $name = implode(' ', explode('_', $name));

        if ($menu === 'cheatsheet') {
            $parentMenu = Module::query()->where('path', '/g_sys')->first();
            if ($parentMenu) {
                $pid = $parentMenu['id'];
            }
            $checkModule = Module::query()->where([
                'permission' => 'cheatsheet/index',
                'path' => '/cheatsheet',
            ]);
            if ($checkModule->first()) {
                $create = $checkModule->update([
                    'name' => $name,
                    'permission' => 'cheatsheet/index',
                    'path' => '/cheatsheet',
                    'rel' => 'cheatsheet/index',
                    'pid' => $pid,
                    'sort' => 0,
                    'type' => 1,
                    'role_permission' => 0,
                ]);
            } else {
                $create = Module::create([
                    'name' => $name,
                    'permission' => 'cheatsheet/index',
                    'path' => '/cheatsheet',
                    'rel' => 'cheatsheet/index',
                    'pid' => $pid,
                    'sort' => 0,
                    'type' => 1,
                    'role_permission' => 0,
                ]);
            }
        }
        if ($create !== '') {
            if ((bool) $create === true) {
                $this->info('Menu created');
            } else {
                $this->error('Menu failed to create');
            }
        }
        $this->info('Task done');
    }

    public function getArguments()
    {
        return [
            ['menu', InputArgument::REQUIRED, 'Menu want to loaded, list: ' . implode(',', $this->lists)],
            ['name', InputArgument::OPTIONAL, 'Name menu as'],
        ];
    }
}
