<?php

declare(strict_types=1);

namespace App\Command;

use App\Kernel\Common\Common;
use App\Model\Setting;
use Hyperf\Command\Annotation\Command;
use Hyperf\Command\Command as HyperfCommand;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * @Command
 */
class PaymentEnabler extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    protected $name = 'payment:enabler';

    protected $setting;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct();
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Hyperf Demo Command');
    }

    public function handle()
    {
        $payment = $this->input->getArgument('payment') ?? 'wechat';
        $status = (int) $this->input->getArgument('status') ?? 1;
        $this->getSetting();
        $this->setSetting($payment, $status);
        $commSettings = Common::getSetting('pay');
        $this->info(json_encode($commSettings));
        $this->info('Command complete');
    }

    public function getSetting(): void
    {
        $paymentSettings = Setting::query()->where('key', 'pay')->first()->toArray();
        $this->setting = json_decode($paymentSettings['value'], true);
    }

    public function setSetting($payment, $paymentValue)
    {
        $settings = [];
        $isExist = false;
        foreach ($this->setting as $key => $value) {
            if (strtolower($value['key']) === strtolower($payment)) {
                $value['value'] = (bool) $paymentValue;
                $isExist = true;
            }
            $settings[] = $value;
        }
        if (! $isExist) {
            $settings[] = [
                'key' => $payment,
                'value' => $paymentValue,
            ];
        }
        Setting::query()->where('key', 'pay')->update([
            'value' => json_encode($settings),
        ]);
        cache()->clearPrefix('setting');
    }

    protected function getArguments()
    {
        return [
            ['payment', InputArgument::REQUIRED, 'Payment type'],
            ['status', InputArgument::REQUIRED, 'Payment status, 0 for disable, 1 for enable'],
        ];
    }
}
