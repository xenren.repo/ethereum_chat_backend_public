<?php

declare(strict_types=1);

namespace App\Command;

use App\Model\Goods;
use App\Model\MulGoodsCategory;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;

/**
 * @Command
 */
class RescrapGoods extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $count = 0;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('rescrap:products');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Hyperf Demo Command');
    }

    public function handle()
    {
        $this->line('Hello Hyperf!', 'info');
        $failed = array();
        $goods = Goods::select(['id', 'price', 'original_price', 'cost_price'])->orderBy('id', 'desc')->get();
        foreach ($goods as $k => $v) {
            $category = MulGoodsCategory::select(['id', 'goods_id', 'cid'])->where('goods_id', '=', $v->id)->get();
            $arr = array();
            foreach ($category as $ck => $cv) {
                array_push($arr, $cv->cid);
            }
            if (in_array(392, $arr) || in_array(408, $arr) || in_array(413, $arr)) {

            } else {
                if(in_array(7, $arr)) {
                    $this->line($v->id, 'info');
                    self::scrap($v->id);
                    self::clearCache($v->id);
                }
            }
        }

        $this->line("FAILED:", 'info');
        $this->line(json_encode($failed));
    }

    /**
     * 缓存清除
     * @param int $id
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function clearCache($id = 0)
    {
        if ($id && is_numeric($id)) {
            cache()->delete('goods_goods_id_' . $id);
        } else {
            cache()->clearPrefix('goods');
        }
        cache()->delete('goods_today_recommend');
        cache()->delete('goods_hot_goods');
        cache()->clearPrefix('goods');
    }

    public function scrap($id)
    {
        $resp = self::callAPI($id);
        if ($this->count < 4) {
            if ($resp->code == -1) {
                self::incrementCount();
                self::scrap($id);
            }
            if ($resp->code == -3) {
                self::incrementCount();
                self::scrap($id);
            }
        } else {
            self::resetCount();
        }
        $this->line('OK', 'info');
    }

    public function incrementCount()
    {
        $this->count = $this->count + 1;
        $this->line('TRY # ' . $this->count, 'warn');
    }

    public function resetCount()
    {
        $this->count = 0;
        $this->line('TRY # ' . $this->count, 'warn');
    }

    public function callAPI($id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://shop.1688qia.com/user/goods/detail?id=" . $id . "&userName=fong7890",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $resp = json_decode($response);
        $this->line('CODE: ' . $resp->code);
        return json_decode($response);

    }
}
