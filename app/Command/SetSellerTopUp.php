<?php

declare(strict_types=1);

namespace App\Command;

use App\Logic\Seller\TopUpLogic;
use Hyperf\Command\Annotation\Command;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Di\Annotation\Inject;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * @Command
 */
class SetSellerTopUp extends HyperfCommand
{
    /**
     * @Inject
     * @var TopUpLogic
     */
    protected $logic;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('seller:topup');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Set Seller Top Up Data');
    }

    public function handle()
    {
        $currentRank = $this->input->getArgument('current_rank');
        $destinationRank = $this->input->getArgument('destination_rank');
        $amount = $this->input->getArgument('amount');
        $message = $this->input->getArgument('message') ?? '';
        try {
            $value = $this->logic->setTopUpValue((int) $currentRank, (int) $destinationRank, [
                'rank_id' => (int) $destinationRank,
                'message' => $message ?? '',
                'amount' => (int) $amount,
            ]);
            if (! $value) {
                $this->info(json_encode($value));
            }
            $this->info('Succes Change Top up value');
        } catch (\Exception $e) {
            $this->line("Error : {$e->getMessage()}");
        }
        $this->info(json_encode($value));
        $this->info('Command finish');
    }

    protected function getArguments()
    {
        return [
            ['current_rank', InputArgument::REQUIRED, 'Current Rank'],
            ['destination_rank', InputArgument::REQUIRED, 'Destination Rank'],
            ['amount', InputArgument::REQUIRED, 'Amount needed for top up'],
            ['message', InputArgument::OPTIONAL, 'Message when user top up'],
        ];
    }
}
