<?php

declare(strict_types=1);

namespace App\Command;

use App\Logic\Order\OrderLogic;
use App\Model\Credit1;
use Hyperf\Command\Annotation\Command;
use Hyperf\Command\Command as HyperfCommand;
use Psr\Container\ContainerInterface;

/**
 * @Command
 */
class TestQuery extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('test:query');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Hyperf Demo Command');
    }

    public function handle()
    {
        $credit1 = Credit1::with([
            'seller',
        ])->whereHas('seller')->whereHas('order', function ($query) {
            return $query->where('status', OrderLogic::FINISH);
        })->get();
        $this->line(count($credit1));
    }
}
