<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class ErrorCode extends AbstractConstants
{
    /**
     * @Message("system_error")
     */
    const SYSTEM = -1;


	/**
	 * @Message("params_invalid")
	 */
	const PARAMS_INVALID = -3;


	/**
	 * @Message("without_phone")
	 */
	const WITHOUT_PHONE = 1010;



	/**
	 * @Message("auth_fail")
	 */
	const AUTH_FAIL = 1000;


	/**
	 * @Message("permission_deny")
	 */
	const PERMISSION_DENY = 1001;




	/**
	 * 操作过于频繁
	 * @Message("throttle_submit")
	 */
	const THROTTLE_SUBMIT = 1009;



	/**
	 * @Message("unset_pay_password")
	 */
	const UNSET_PAY_PASSWORD = 1011;



	/**
	 * 乐观锁更新失败错误
	 * @Message("version_update")
	 */
	const VERSION_UPDATE = -4;

}
