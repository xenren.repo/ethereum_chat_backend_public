<?php
declare(strict_types=1);

namespace App\Controller\Admin\Ad;

use App\Controller\BaseController;
use App\Logic\Ad\AdLogic;
use App\Request\Ad\AdStoreRequest;
use App\Request\Ad\AdUpdateRequest;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;

/**
 * @Controller(prefix="ad")
 * @Middleware(AdminAuthMiddleware::class)
 */
class AdController extends BaseController
{

	/**
	 * @Inject()
	 * @var AdLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/ad")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="add")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param AdStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function store(AdStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="update")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param AdUpdateRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function update(AdUpdateRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->update($gpc);
		return $this->response->success();
	}


	/**
	 * @RequestMapping(path="delete")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param CommonDeleteRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function delete(CommonDeleteRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->delete($gpc);
		return $this->response->success();
	}
}
