<?php
declare(strict_types=1);

namespace App\Controller\Admin\Ad;

use App\Controller\BaseController;
use App\Logic\Ad\AdLogic;
use App\Logic\Ad\AdPositionLogic;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;

/**
 * @Controller(prefix="ad_position")
 * @Middleware(AdminAuthMiddleware::class)
 */
class AdPositionController extends BaseController
{

	/**
	 * @Inject()
	 * @var AdPositionLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/ad_position")
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index()
	{
		$res = $this->logic->json();
		return $this->response->success($res);
	}



}
