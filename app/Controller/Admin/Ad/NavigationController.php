<?php
declare(strict_types=1);

namespace App\Controller\Admin\Ad;

use App\Controller\BaseController;
use App\Logic\Ad\NavigationLogic;
use App\Request\Ad\NavigationStoreRequest;
use App\Request\Ad\NavigationUpdateRequest;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;

/**
 * @Controller(prefix="navigation")
 * @Middleware(AdminAuthMiddleware::class)
 */
class NavigationController extends BaseController
{

	/**
	 * @Inject()
	 * @var NavigationLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/navigation")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="add")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param NavigationStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function store(NavigationStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="update")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param NavigationUpdateRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function update(NavigationUpdateRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->update($gpc);
		return $this->response->success();
	}


	/**
	 * @RequestMapping(path="delete")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param CommonDeleteRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function delete(CommonDeleteRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->delete($gpc);
		return $this->response->success();
	}
}
