<?php

declare(strict_types=1);

namespace App\Controller\Admin\Admin;

use App\Controller\BaseController;
use App\Logic\Admin\AdminAuthLogic;
use App\Logic\Order\PayLogic;
use App\Middleware\AdminAuthMiddleware;
use App\Model\Admin;
use App\Model\User;
use App\Request\Admin\AdminChangePasswordRequest;
use App\Request\Admin\AdminLoginRequest;
use App\Request\IsSellerExistRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller
 */
class AdminAuthController extends BaseController
{
    /**
     * @Inject
     * @var AdminAuthLogic
     */
    protected $logic;

    /**
     * @RequestMapping(path="/admin/login")
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function login(AdminLoginRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->login($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="/admin/change_password")
     * @Middleware(AdminAuthMiddleware::class)
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function changePassword(AdminChangePasswordRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->changePassword($gpc);
        return $this->response->success($res);
    }

    /**
     * 注销登入.
     * @RequestMapping(path="/admin/logout")
     * @Middleware(AdminAuthMiddleware::class)
     * @return string
     */
    public function logout(RequestInterface $request)
    {
        $this->logic->logout();
        return $this->response->success();
    }


    /**
     * @RequestMapping(path="/admin/seller_validate")
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function isSellerExist(IsSellerExistRequest $request)
    {
        $gpc = $request->validated();
        $seller = Admin::with('reseller')
            ->where('name', '=', $request->input('username'))
            ->first();

        if ($request->input('is_existing') == true) {
            $user = User::where('name', '=', $request->input('username'))->first();
            if(is_null($user)){
                return $this->response->success([
                    'can_proceed' => false,
                    'message' => '用户不存在'
                ]);
            } else {
                $seller = Admin::with('reseller')
                    ->where('name', '=', $request->input('username'))
                    ->first();
                if(isset($seller) && isset($seller->reseller)) {
                    return $this->response->success([
                        'can_proceed' => false,
                        'message' => '卖家已存在'
                    ]);
                }
            }
        } else {
            $seller = Admin::with('reseller')
                ->where('name', '=', $request->input('username'))
                ->first();
            if(isset($seller) && isset($seller->reseller)) {
                return $this->response->success([
                    'can_proceed' => false,
                    'message' => '卖家已存在'
                ]);
            } else {
                $user = User::where('name', '=', $request->input('username'))->first();
                if(!is_null($user)){
                    return $this->response->success([
                        'can_proceed' => false,
                        'message' => '用户已存在'
                    ]);
                }
            }
        }

        return $this->response->success([
            'can_proceed' => true
        ]);

    }
}
