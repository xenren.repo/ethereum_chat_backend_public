<?php
declare(strict_types=1);

namespace App\Controller\Admin\Admin;

use App\Controller\BaseController;
use App\Logic\Admin\AdminLogic;
use App\Request\Admin\AdminStoreRequest;
use App\Request\Admin\AdminUpdateRequest;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\Admin\SellerRejectRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;

/**
 * @Controller()
 * @Middleware(AdminAuthMiddleware::class)
 */
class AdminController extends BaseController
{

	/**
	 * @Inject()
	 * @var AdminLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/admins")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="/admin/add")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param AdminStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function store(AdminStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="/admin/update")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param AdminUpdateRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function update(AdminUpdateRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->update($gpc);
		return $this->response->success();
	}


	/**
	 * @RequestMapping(path="/admin/delete")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param CommonDeleteRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function delete(CommonDeleteRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->delete($gpc);
		return $this->response->success();
	}

	/**
	 * @RequestMapping(path="/admin/seller/reject")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param SellerRejectRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function SellerReject(SellerRejectRequest $request)
	{
		$gpc = $request->validated();
		$res=$this->logic->sellerReject($gpc);
		return $this->response->success();
	}

}
