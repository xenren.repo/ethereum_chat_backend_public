<?php

declare(strict_types=1);

namespace App\Controller\Admin\Finance;

use App\Controller\BaseController;
use App\Logic\Finance\Credit1Logic;
use App\Logic\Order\OrderLogic;
use App\Middleware\AdminAuthMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Request\CommonIndexRequest;
use Carbon\Carbon;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * @Controller(prefix="finance/credit1")
 * @Middleware(AdminAuthMiddleware::class)
 */
class Credit1Controller extends BaseController
{
    /**
     * @Inject
     * @var Credit1Logic
     */
    protected $logic;

    /**
     * @RequestMapping(path="/finance/credit1")
     * @Middleware(AdminPermissionMiddleware::class)
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(CommonIndexRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->index($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="/finance/credit1/export")
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function export(CommonIndexRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->indexAllNew($gpc);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $headers = [
            'A' => 'Record No',
            'B' => 'Credit1 Type',
            'C' => 'Seller Name',
            'D' => 'Buyer Name',
            'E' => 'Payment Type',
            'F' => 'Amount',
            // 'G' => 'Balance',
            'G' => 'Remark',
            'H' => 'Order Time',
            'I' => 'Pay Time',
        ];
        foreach ($headers as $key => $value) {
            $sheet->setCellValue(sprintf('%s%s', $key, 1), $value);
        }
        // $sheet->setCellValue('A1', '流水号');
        // $sheet->setCellValue('B1', '账号');
        // $sheet->setCellValue('C1', '会员');
        // $sheet->setCellValue('D1', '类型');
        // $sheet->setCellValue('E1', '变动量');
        // $sheet->setCellValue('F1', '变动后');
        // $sheet->setCellValue('G1', '备注');
        // $sheet->setCellValue('H1', '时间');

        $index = 0;
        $indexInc = 2;
        foreach ($res as $k => $v) {
            $credit1TypeTrans = function ($credit1Type) {
                switch ($credit1Type) {
                    case Credit1Logic::SYSTEM:
                        return 'SYSTEM';
                    break;
                    case Credit1Logic::BUY:
                        return 'BUY';
                    break;
                    case Credit1Logic::SELL:
                        return 'SELL';
                    break;
                    case Credit1Logic::WITHDRAW:
                        return 'WITHDRAW';
                    break;
                    case Credit1Logic::REFUND:
                        return 'REFUND';
                    break;
                    case Credit1Logic::ORDER_PROFIT:
                        return 'ORDER PROFIT';
                    break;
                    default:
                        return 'OTHER';
                    break;
                }
            };
            $orderPayType = function ($val) {
                switch ($val) {
                    case OrderLogic::SYSTEM:
                        return 'SYSTEM';
                        break;
                    case OrderLogic::WECHAT:
                        return 'WECHAT';
                    break;
                    case OrderLogic::ALIPAY:
                        return 'ALIPAY';
                    break;
                    case OrderLogic::BALANCE:
                        return 'BALANCE';
                    break;
                    case OrderLogic::SPECIAL_PAY:
                        return 'SPECIAL PAY';
                    break;
                    case OrderLogic::CREDIT3_PAY:
                        return 'CREDIT3 PAY';
                    break;
                    default:
                        return 'OTHER';
                    break;
                }
            };

            if ((int) $v->credit1_type === Credit1Logic::SELL) {
                $buyersName = sprintf('%s %s', $v->buyers_name, $v->buyers_real_name ? "({$v->buyers_real_name})" : '');
            } else {
                $buyersName = sprintf('%s %s', $v->users_name, $v->users_real_name ? "({$v->users_real_name})" : '');
            }

            $sheet->setCellValue('A' . $indexInc, $v->record_no);
            $sheet->setCellValue('B' . $indexInc, $credit1TypeTrans($v->credit1_type));
            $sheet->setCellValue('C' . $indexInc, sprintf('%s %s', $v->admin_name, $v->admin_real_name ? "({$v->admin_real_name})" : ''));
            $sheet->setCellValue('D' . $indexInc, $buyersName);
            $sheet->setCellValue('E' . $indexInc, $orderPayType($v->order_pay_type));
            $sheet->setCellValue('F' . $indexInc, $v->credit1_amount);
            $sheet->setCellValue('G' . $indexInc, $v->remark);
            $sheet->setCellValue('H' . $indexInc, $v->order_time);
            $sheet->setCellValue('I' . $indexInc, $v->credit1_pay_time);

            $index = $index + 1;
            $indexInc = $indexInc + 1;
        }

        $writer = new Xlsx($spreadsheet);
        $stamp = Carbon::now()->timestamp;
        $path = BASE_PATH . '/public/app/' . $stamp . '.xlsx';
        $pathN = 'app/' . $stamp . '.xlsx';

        try {
            if (! file_exists($path)) {
                touch($path);
                chmod($path, 0777);
            }

            $writer->save($path);
            return $this->response->success('/' . $pathN);
        } catch (\Exception $e) {
            return $this->response->success('Unable to save file. Please close any other applications(s) that are using it:' . $e->getMessage());
        }
    }
}
