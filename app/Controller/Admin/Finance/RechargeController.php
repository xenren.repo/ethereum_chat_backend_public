<?php
declare(strict_types=1);

namespace App\Controller\Admin\Finance;

use App\Controller\BaseController;
use App\Logic\Finance\RechargeLogic;
use App\Request\CommonIndexRequest;
use App\Request\Finance\RechargeSetStatusRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;

/**
 * @Controller(prefix="recharge")
 * @Middleware(AdminAuthMiddleware::class)
 */
class RechargeController extends BaseController
{

	/**
	 * @Inject()
	 * @var RechargeLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/recharge")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="set_status")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param RechargeSetStatusRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function setStatus(RechargeSetStatusRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->setStatus($gpc);
		return $this->response->success($res);
	}

}
