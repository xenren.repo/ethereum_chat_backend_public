<?php
declare(strict_types=1);

namespace App\Controller\Admin\Finance;

use App\Controller\BaseController;
use App\Logic\Finance\Credit1Logic;
use App\Logic\Finance\WithdrawLogic;
use App\Request\CommonIndexRequest;
use App\Request\Finance\ApproveWithDrawRequest;
use App\Request\Finance\WithdrawNewRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;

/**
 * @Controller(prefix="finance/withdraw")
 * @Middleware(AdminAuthMiddleware::class)
 */
class WithdrawController extends BaseController
{

	/**
	 * @Inject()
	 * @var WithdrawLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/finance/withdraw")
	
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
    }
    
   /**
	 * @RequestMapping(path="/finance/withdraw/approve")
	
	 * @param ApproveWithDrawRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */

    public function approveWithDraw(ApproveWithDrawRequest $request) { 

        $gpc = $request->validated(); 
        $res = $this->logic->approveWithDraw($gpc); 
        return $this->response->success($res); 
    }

	/**
	 * @RequestMapping(path="/finance/withdraw/create")

	 * @param WithdrawNewRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */

    public function Withdraw(WithdrawNewRequest $request) { 

        $gpc = $request->validated(); 
        $res = $this->logic->Withdraw($gpc); 
        return $this->response->success($res); 
    }

	

}
