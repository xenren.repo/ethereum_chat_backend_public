<?php
declare(strict_types=1);

namespace App\Controller\Admin\Goods;

use App\Controller\BaseController;
use App\Logic\Goods\GoodsCommentLogic;
use App\Request\CommonIndexRequest;
use App\Request\Goods\SetCommentStatusRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use App\Middleware\NotForSellerMiddleware;
use App\Request\Goods\SetCommentReplyRequest;

/**
 * @Controller(prefix="/comment")
 * @Middleware(AdminAuthMiddleware::class)
 */
class CommentController extends BaseController
{
	/**
	 * @Inject()
	 * @var GoodsCommentLogic
	 */
	private $logic;

	/**
	 * @RequestMapping(path="/order_comment")
	 * @Middlewares({
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param CommonIndexRequest $request
	 * @return string
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$data = $this->logic->index($gpc);
		return $this->response->success($data);
	}

	/**
	 * @RequestMapping(path="/order_comment/set_status")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class),
	 * 			@Middleware(NotForSellerMiddleware::class)
	 * })
	 * @param SetCommentStatusRequest $request
	 * @return string
	 */
	public function setStatus(SetCommentStatusRequest $request)
	{
		$gpc = $request->validated();
		$data = $this->logic->setStatus($gpc);
		return $this->response->success($data);
	}

	/**
	 * @RequestMapping(path="/order_comment/reply")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class),
	 * })
	 * @param SetCommentReplyRequest $request
	 * @return string
	 */
	public function reply(SetCommentReplyRequest $request)
	{
		$gpc = $request->validated();
		$data = $this->logic->reply($gpc);
		return $this->response->success($data);
	}
}
