<?php
declare(strict_types=1);

namespace App\Controller\Admin\Goods;

use App\Controller\BaseController;
use App\Logic\Goods\GoodsCategoryLogic;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\Goods\GoodsCategoryStoreRequest;
use App\Request\Goods\GoodsCategoryUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;


/**
 * @Controller(prefix="/goods_category")
 * @Middleware(AdminAuthMiddleware::class)
 */
class GoodsCategoryController extends BaseController
{
	/**
	 * @Inject()
	 * @var GoodsCategoryLogic
	 */
	private $logic;

	/**
	 * 列表
	 * @RequestMapping(path="/goods_categories")
	 * @Middleware(AdminPermissionMiddleware::class)
	 */
	public function index()
	{
		$res = $this->logic->pcIndex();
		return $this->response->success($res);
	}

	/**
	 * 添加
	 * @RequestMapping(path="add")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param GoodsCategoryStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function store(GoodsCategoryStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}

	/**
	 * 编辑
	 * @RequestMapping(path="update")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param GoodsCategoryUpdateRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function update(GoodsCategoryUpdateRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->update($gpc);
		return $this->response->success();
	}

	/**
	 * 删除
	 * @RequestMapping(path="delete")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param CommonDeleteRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function delete(CommonDeleteRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->delete($gpc);
		return $this->response->success();
	}
}
