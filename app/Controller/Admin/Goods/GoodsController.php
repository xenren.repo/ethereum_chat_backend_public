<?php
declare(strict_types=1);

namespace App\Controller\Admin\Goods;

use App\Constants\ErrorCode;
use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Admin\AdminLogic;
use App\Logic\Goods\GoodsLogic;
use App\Model\Admin;
use App\Model\Goods;
use App\Model\Seller;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\Goods\FetchGoodsRequest;
use App\Request\Goods\GoodsIndexRequest;
use App\Request\Goods\GoodsSetStatusRequest;
use App\Request\Goods\GoodsStoreRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;


/**
 * @Controller(prefix="/goods")
 * @Middleware(AdminAuthMiddleware::class)
 */
class GoodsController extends BaseController
{
	/**
	 * @Inject()
	 * @var GoodsLogic
	 */
	private $logic;

	/**
	 * 列表
	 * @RequestMapping(path="/goods")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param GoodsIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(GoodsIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}

	/**
	 * 添加
	 * @RequestMapping(path="add")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param GoodsStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function store(GoodsStoreRequest $request)
	{
		$gpc = $request->validated();

        $admin = Admin::where('id', '=', Auth::id())->first();
        if($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) {
            $seller = Seller::where('admin_id', '=', $admin['id'])->first();
            $count = Goods::where('created_by','=', $admin['id'])->count();
            if($seller->rank_id == 0) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID,'请升级以添加产品');
            }
            if($seller->rank_id == 1 && $count >= 100 ) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID,'请升级以添加更多产品');
            }
            if($seller->rank_id == 2 && $count >= 5000 ) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID,'超出了产品添加限制。');
            }
        }
;
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}



	/**
	 * 添加
	 * @RequestMapping(path="set_status")
	 * @Middlewares({
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param GoodsSetStatusRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function setStatus(GoodsSetStatusRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->setStatus($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="show")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param RequestInterface $request
	 * @return
	 */
	public function show(RequestInterface $request)
	{
		$goods = $this->logic->show($request->input('id'));
		return $this->response->success($goods);
	}




	/**
	 * 编辑
	 * @RequestMapping(path="update")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param GoodsStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function update(GoodsStoreRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->update($gpc);
		return $this->response->success();
	}

	/**
	 * 删除
	 * @RequestMapping(path="delete")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param CommonDeleteRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function delete(CommonDeleteRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->delete($gpc);
		return $this->response->success();
	}


	/**
	 * @RequestMapping(path="fetch")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param FetchGoodsRequest $request
	 * @return string
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function fetchGoods(FetchGoodsRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->fetchGoods($gpc);
		return $this->response->success(['msg' =>$res]);
	}
}
