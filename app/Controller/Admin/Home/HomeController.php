<?php
declare(strict_types=1);

namespace App\Controller\Admin\Home;

use App\Controller\BaseController;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middleware(AdminAuthMiddleware::class)
 */
class HomeController extends BaseController
{




	/**
	 * @RequestMapping(path="/notice/new_info")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 */
	public function newInfo()
	{
		// 需要根据权限隔离
		$data['withdraw'] = 0;
		$data['member_auth'] = 0;
		return $this->response->success($data);
	}




}
