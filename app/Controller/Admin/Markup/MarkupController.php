<?php
declare(strict_types=1);

namespace App\Controller\Admin\Markup;

use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Logic\Medium\MediumLogic;
use App\Model\Goods;
use App\Model\GoodsOption;
use App\Model\MulGoodsCategory;
use App\Model\Navigation;
use App\Model\Setting;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\IdRequest;
use App\Request\MarkupRequest;
use App\Request\Medium\MediumStoreRequest;
use App\Request\NullRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;


/**
 * @Controller(prefix="markup")
 * @Middleware(AdminAuthMiddleware::class)
 */
class MarkupController extends BaseController
{

    /**
     * 列表
     * @RequestMapping(path="/markup")
     * @Middleware(AdminPermissionMiddleware::class)
     * @param NullRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(NullRequest $request)
    {
        $navigations = Navigation::get();
        $arr = array();

        foreach ($navigations as $k => $v) {
            $d = Setting::where('name', '=', 'categories_price_' . $v->id)->first();
            if (!is_null($d)) {
                $arr['categories_price_' . $v->id] = json_decode($d->value);
            }
        }

        return $this->response->success($arr);
    }


    /**
     * 添加
     * @RequestMapping(path="update")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     *      @Middleware(AdminPermissionMiddleware::class)
     * })
     * @param MarkupRequest $request
     * @return string
     */
    public function update(MarkupRequest $request)
    {
        $gpc = $request->validated();
//        $data0 = Setting::where('key', '=', 'original_price')->update(['value' => $gpc['original_price']]);
//        $data1 = Setting::Where('key', '=', 'cost_price')->update(['value' => $gpc['cost_price']]);
//        $data1 = Setting::Where('key', '=', 'categories_selected')->update(['value' => implode(',', $gpc['categories_selected'])]);

        $obj = array(
            'category_id' => $gpc['categories_selected'],
            'original_price' => $gpc['original_price'],
            'cost_price' => $gpc['cost_price']
        );


//        $params = [];
        $data = Setting::Where('name', '=', 'categories_price_' . $gpc['categories_selected'])->first();
        $nav = Navigation::where('id', '=', $gpc['categories_selected'])->first();

        $url = $nav->url;
        $url_components = parse_url($url);
        if (count($url_components) > 1) {
            parse_str($url_components['query'], $params);
            $cid = isset($params['cid']) ? $params['cid'] : 0;
        } else {
            $cid = 0;
        }

        if (!is_null($data)) {
            $data1 = Setting::Where('name', '=', 'categories_price_' . $gpc['categories_selected'])->update([
                'value' => json_encode($obj),
                'key' => $cid,
            ]);
        } else {
            Setting::create([
                'name' => 'categories_price_' . $gpc['categories_selected'],
                'key' => $cid,
                'value' => json_encode($obj)
            ]);
        }
        return $this->response->success($gpc);

    }

    /**
     * @RequestMapping(path="updatePrices")
     * @param NullRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function updatePrices(NullRequest $request)
    {
        echo var_dump('START');

        ini_set('memory_limit', '128M');
//        echo var_dump('Hello Hyperf!', 'info');
        $goods = Goods::select(['id', 'price', 'original_price', 'cost_price'])->where('created_by', '=', 0)->get();
        foreach ($goods as $k => $v) {
            $category = MulGoodsCategory::select(['id', 'goods_id', 'cid'])
                ->where('goods_id', '=', $v->id)
                ->whereNotIn('cid', [392, 408, 413])
                ->get()
                ->pluck('cid');

            if (!is_null($category) && count($category) > 0) {
                foreach ($category as $kCat => $vCat) {

                    $nav = Navigation::where('url', 'like', '%cid=' . $vCat . '&%')->first();
                    if ($v->id == 573518) {
                        echo var_dump('NAV:' . $nav);
                        echo var_dump('CAT:' . $vCat);
                    }
                    if (!is_null($nav)) {
                        $setting = Setting::where('name', '=', 'categories_price_'.$nav->id)->first();
                        if (!is_null($setting)) {
                            $setting = json_decode($setting->value);
                            echo var_dump($setting);
                            $orgPrice = $v->original_price;
                            $orgPriceMarkup = $setting->original_price;
                            $costPriceMarkup = $setting->cost_price;

                            echo var_dump('CAT:' . $vCat);
                            echo var_dump('Original Price ORG: ' . $orgPrice);
                            echo var_dump('Original Price: ' . floor($orgPrice * $orgPriceMarkup));
                            Goods::where('id', '=', $v->id)->update([
                                'cost_price' => $orgPrice,
                                'original_price' => $orgPrice + floor($orgPrice * $orgPriceMarkup),
                                'price' => $orgPrice + floor($orgPrice * $costPriceMarkup),
                            ]);
                            $optns = GoodsOption::where('goods_id', '=', $v->id)->get();
                            foreach ($optns as $k1 => $v1) {
                                GoodsOption::where('id', '=', $v1->id)->update([
                                    'cost_price' => $orgPrice,
                                    'original_price' => $orgPrice + floor($orgPrice * $orgPriceMarkup),
                                    'price' =>  $orgPrice + floor($orgPrice * $costPriceMarkup),
                                ]);
                            }
                        }
                    }
                }
            }
        }
        cache()->delete('goods_today_recommend');
        cache()->delete('goods_hot_goods');
        cache()->clearPrefix('goods');
        echo var_dump('DONE');
        return $this->response->success([]);
    }

}