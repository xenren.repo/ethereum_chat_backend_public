<?php
declare(strict_types=1);

namespace App\Controller\Admin\Medium;

use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Logic\Medium\MediumLogic;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\IdRequest;
use App\Request\Medium\MediumStoreRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;


/**
 * @Controller(prefix="medium")
 * @Middleware(AdminAuthMiddleware::class)
 */
class MediumController extends BaseController
{

	/**
	 * @Inject()
	 * @var MediumLogic
	 */
	private $logic;


	/**
	 * 列表
	 * @RequestMapping(path="/medium")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$data = $this->logic->index($gpc);
		return $this->response->success($data);
	}


	/**
	 * @RequestMapping(path="show")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param IdRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function show(IdRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->show($gpc['id']);
		return $this->response->success($res);
	}

	/**
	 * 添加
	 * @RequestMapping(path="add")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param MediumStoreRequest $request
	 * @return string
	 */
	public function store(MediumStoreRequest $request)
	{
		$gpc = $request->validated();
		if (isset($gpc['id'])) {
			unset($gpc['id']);
		}
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}


	/**
	 * 添加
	 * @RequestMapping(path="update")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param MediumStoreRequest $request
	 * @return string
	 */
	public function update(MediumStoreRequest $request)
	{
		$gpc = $request->validated();
		if (empty($gpc['id'])) {
			throw new BusinessException();
		}
		$res = $this->logic->update($gpc);
		return $this->response->success($res);
	}

	/**
	 * 删除
	 * @RequestMapping(path="delete")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param CommonDeleteRequest $request
	 * @return string
	 */
	public function destroy(CommonDeleteRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->delete( $gpc);
		return $this->response->success($res);
	}


}