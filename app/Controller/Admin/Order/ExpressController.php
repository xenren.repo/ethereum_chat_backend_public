<?php
declare(strict_types=1);

namespace App\Controller\Admin\Order;

use App\Controller\BaseController;
use App\Logic\Order\ExpressLogic;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middleware(AdminAuthMiddleware::class)
 */
class ExpressController extends BaseController
{


	/**
	 * @RequestMapping(path="/express_list")
	 * @return string
	 */
	public function expressList()
	{
		$res = make(ExpressLogic::class)->getExpressList();
		return $this->response->success($res);
	}


}
