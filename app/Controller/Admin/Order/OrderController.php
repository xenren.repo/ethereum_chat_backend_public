<?php
declare(strict_types=1);

namespace App\Controller\Admin\Order;

use App\Controller\BaseController;
use App\Logic\Finance\WithdrawLogic;
use App\Logic\Order\OrderLogic;
use App\Logic\Order\OrderRefundLogic;
use App\Logic\Order\PayLogic;
use App\Model\Admin;
use App\Model\Order;
use App\Model\OrderRefund;
use App\Model\Seller;
use App\Model\SellerOrderRefundRequest;
use App\Model\User;
use App\Model\Withdraw;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\IdRequest;
use App\Request\Order\DealRefundRequest;
use App\Request\Order\OrderIndexRequest;
use App\Request\Order\OrderPackageStoreRequest;
use App\Request\Order\SetOrderAddressRequest;
use App\Request\Seller\AliPaySellerHistoryRequest;
use App\Request\Seller\AliPaySellerRequest;
use App\Request\Seller\DowngradeRequest;
use App\Request\Seller\OrderRequest;
use App\Request\Seller\SellerOrderPaidRequest;
use App\Request\Seller\UpdateSellerRequest;
use App\Request\Order\ImportPendingRequest; 
use Carbon\Carbon;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @Controller(prefix="order")
 * @Middleware(AdminAuthMiddleware::class)
 */
class OrderController extends BaseController
{

    /**
     * @Inject()
     * @var OrderLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="/orders")
     * @Middlewares({
     *      @Middleware(AdminPermissionMiddleware::class)
     * })
     * @param OrderIndexRequest $request
     * @return string
     */
    public function index(OrderIndexRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->pcIndex($gpc);
        return $this->response->success($res);
    }


    /**
     * @RequestMapping(path="close")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     *      @Middleware(AdminPermissionMiddleware::class)
     * })
     * @param CommonDeleteRequest $request
     * @return string
     */
    public function close(CommonDeleteRequest $request)
    {
        $gpc = $request->validated();
        $this->logic->close($gpc['id']);
        return $this->response->success();
    }


    /**
     * @RequestMapping(path="system_pay")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     *      @Middleware(AdminPermissionMiddleware::class)
     * })
     * @param IdRequest $request
     * @return ResponseInterface
     */
    public function systemPay(IdRequest $request)
    {
        $gpc = $request->validated();
        $this->logic->systemPay($gpc['id']);
        return $this->response->success();
    }

    /**
     * @RequestMapping(path="total_amount/set")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     *      @Middleware(AdminPermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function setTotalAmount(RequestInterface $request)
    {
        $this->logic->setTotalAmount($request->input('id'), $request->input('total_amount'));
        return $this->response->success();
    }

    /**
     * @RequestMapping(path="set_address")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     *      @Middleware(AdminPermissionMiddleware::class)
     * })
     * @param SetOrderAddressRequest $request
     * @return string
     */
    public function setAddress(SetOrderAddressRequest $request)
    {
        $gpc = $request->validated();
        $this->logic->setAddress($gpc);
        return $this->response->success();
    }

    /**
     * @RequestMapping(path="set_express_cost")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     *      @Middleware(AdminPermissionMiddleware::class)
     * })
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function setExpressCost(RequestInterface $request)
    {
        $this->logic->setExpressCost($request->input('id'), $request->input('express_amount'));
        return $this->response->success();
    }


    /**
     * @RequestMapping(path="generate_order")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function generateOrderForSeller(OrderRequest $request)
    {
        $admin = Admin::where('id', '=', $request->input('user_id'))->first();
        $amount = $request->input('amount');
//        if ($admin->name == 'fong7890') {
//            $amount = 0.01;
//        }
        $order = Order::create([
            'order_no' => $this->logic->generateNo(),
            'package_num' => 0,
            'user_id' => $request->input('user_id'),
            'pid' => 0,
            'total_amount' => $amount,
            //'total_amount' => 0.01,
            'original_total' => $amount,
            'express_amount' => 0,
            'off_amount' => 0,
            'goods_amount' => 0,
            'pay_type' => OrderLogic::SELLER_PAID_TYPE
        ]);
        return $this->response->success($order);
    }


    /**
     * @RequestMapping(path="alipay_seller_order_paid")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function alipayOrderPaid(AliPaySellerHistoryRequest $request)
    {
        $order = Order::with('admin')
            ->with('user')
            ->where('status', '=', OrderLogic::HAS_PAY)
            ->where('created_at', '>=', Carbon::now()->startOfDay())
            ->where('created_at', '<=', Carbon::now()->endOfDay())
//            ->where('pay_type', '=', OrderLogic::SELLER_PAID_TYPE)
            ->get();

        foreach ($order as $k => $v) {
            if ($v->user) {
                $order[$k]['nn'] = $v->user->name;
            } else {
                $order[$k]['nn'] = $v->admin->name;
            }
        }
        return $this->response->success(collect([
            'data' => $order,
            'summary' => $order->sum('original_total')
        ]));
    }

    /**
     * @RequestMapping(path="alipay_seller_order")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function aliPayQrOrderPay(AliPaySellerRequest $request)
    {
        $payLogic = new PayLogic();
        $order = Order::where('id', '=', $request->input('order_id'))->first();
        $user = Admin::where('id', '=', $request->input('user_id'))->first();
        $pay_result = $payLogic->alipayQr($order, $user, true);
        return $this->response->success($pay_result);
    }


    /**
     * @RequestMapping(path="alipay_seller_order_history")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function aliPayQrOrderPayHistory(AliPaySellerHistoryRequest $request)
    {
        $orders = Order::with('sellerRefundRequest')->where('user_id', '=', $request->input('user_id'))
            ->where('pay_type', '=', OrderLogic::SELLER_PAID_TYPE)
            ->whereIn('status', [OrderLogic::HAS_PAY, OrderLogic::REFUNDING_IN_PROGRESS, OrderLogic::REFUNDING])
            ->get();
        return $this->response->success($orders);
    }


    /**
     * @RequestMapping(path="alipay_seller_order_history_all")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function aliPayQrOrderPayHistoryAll(AliPaySellerHistoryRequest $request)
    {
        $orders = Order::with('admin')
            ->with('sellerRefundRequest')
            ->where('pay_type', '=', OrderLogic::SELLER_PAID_TYPE)
            ->whereIn('status', [Order::status_seller_refunded, OrderLogic::REFUNDING_IN_PROGRESS])
            ->orderBy('id', 'desc')
            ->get();
        return $this->response->success($orders);
    }

    /**
     * @RequestMapping(path="update_sellers")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param UpdateSellerRequest $request
     * @return ResponseInterface
     */
    public function updateSeller(UpdateSellerRequest $request)
    {
        $sellerData = Seller::where('id', '=', $request->input('seller_id'))->first();
        $sellers = Seller::where('id', '=', $request->input('seller_id'))->update([
            'rank_id' => $request->input('rank'),
            'company_info' => $request->input('company_info'),
            'company_no' => $request->input('company_no'),
        ]);
        Admin::where('id', '=', $sellers->admin_id)->update([
            'name' => $request->input('name')
        ]);
        return $this->response->success($sellers);
    }


    /**
     * @RequestMapping(path="get_all_sellers")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function getAllSellers(AliPaySellerHistoryRequest $request)
    {
        $sellers = Seller::with('admin')->get();
        return $this->response->success($sellers);
    }

    /**
     * @RequestMapping(path="admin_refund_request")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function adminRefundRequest(AliPaySellerHistoryRequest $request)
    {
        $complete_deposit_request = $request->input('complete_deposit_request');
        if($complete_deposit_request == 0) {
            $ord = SellerOrderRefundRequest::create([
                'amount' => $request->input('amount'),
                'bank_name' => $request->input('bank_name'),
                'bank_account_no' => $request->input('bank_account_no'),
                'bank_account_holder_name' => $request->input('bank_account_holder_name'),
                'admin_id' => $request->input('user_id'),
                'img_url' => $request->input('img_url'),
                'status' => SellerOrderRefundRequest::STATUS_APPROVED
            ]);
        } else {
            SellerOrderRefundRequest::where('order_id', '=', $request->input('order_id'))->update([
                'img_url' => $request->input('img_url'),
                'status' => SellerOrderRefundRequest::STATUS_APPROVED
            ]);
        }

        $ord = Order::where('id', '=', $request->input('order_id'))->update([
            'status' => Order::status_seller_refunded
        ]);

        return $this->response->success($ord);
    }

    /**
     * @RequestMapping(path="seller_order_paid")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function sellerOrderPaid(SellerOrderPaidRequest $request)
    {
        $ord = Order::where('id', '=', $request->input('order_id'))->update([
            'status' => OrderLogic::HAS_PAY
        ]);
        Seller::where('admin_id', '=', $request->input('user_id'))->update([
            'rank_id' => $request->input('rank_id')
        ]);

        return $this->response->success($ord);
    }

    /**
     * @RequestMapping(path="downgrade")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function downgrade(DowngradeRequest $request)
    {
        $ord = Seller::where('admin_id', '=', $request->input('user_id'))->update([
            'rank_id' => $request->input('rank_id')
        ]);

        return $this->response->success($ord);
    }

    /**
     * @RequestMapping(path="seller_transactions_refunds")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function sellerTransactionRefunds(AliPaySellerHistoryRequest $request)
    {
        $ord = SellerOrderRefundRequest::where('admin_id', '=', $request->input('user_id'))->get();
        return $this->response->success($ord);
    }

    /**
     * @RequestMapping(path="deposit_withdraw_request")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     * })
     * @param OrderRequest $request
     * @return ResponseInterface
     */
    public function depositWithdrawRequest(AliPaySellerHistoryRequest $request)
    {

        $ord = SellerOrderRefundRequest::create([
            'amount' => $request->input('amount'),
            'bank_name' => $request->input('bank_name'),
            'bank_account_no' => $request->input('bank_account_no'),
            'bank_account_holder_name' => $request->input('bank_holder'),
            'admin_id' => $request->input('user_id'),
            'order_id' => $request->input('order_id'),
            'status' => SellerOrderRefundRequest::STATUS_IN_PROGRESS
        ]);

        Order::where('id', '=', $request->input('order_id'))->update([
            'status' => OrderLogic::REFUNDING_IN_PROGRESS
        ]);

//        $wl = new WithdrawLogic();
//        $invoice = $wl->generateNo();
//        $ord = Withdraw::create([
//            'user_id' => $request->input('user_id'),
//            'order_id' => $request->input('order_id'),
//            'amount' => $request->input('amount'),
//            'invoice' => $invoice,
//            'status' => 0,
//            'type' => Withdraw::type_deposit_refund,
//            'bank_name' => $request->input('bank_name'),
//            'bank_account_no' => $request->input('bank_account_no'),
//            'bank_no' => $request->input('bank_no'),
//            'bank_holder' => $request->input('bank_holder'),
//        ]);
//        Order::where('id', '=', $request->input('order_id'))->update([
//            'status' => OrderLogic::WITHDRAW_IN_PROGRESS
//        ]);
        return $this->response->success($ord);
    }

    /**
     * @RequestMapping(path="detail")
     * @Middleware(AdminPermissionMiddleware::class)
     * @param IdRequest $request
     * @return string
     */
    public function detail(IdRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->detail($gpc['id']);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="deliver")
     * @Middlewares({
     *      @Middleware(SubmitThrottleMiddleware::class),
     *      @Middleware(AdminPermissionMiddleware::class)
     * })
     * @param OrderPackageStoreRequest $request
     * @return string
     */
    public function deliver(OrderPackageStoreRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->deliver($gpc);
        return $this->response->success($res);
    }


    /**
     * @RequestMapping(path="confirm")
     * @Middleware(AdminPermissionMiddleware::class)
     * @param IdRequest $request
     * @return string
     */
    public function confirm(IdRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->confirm($gpc['id']);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="excel")
     * @Middlewares({     
     *   
     * })
     * @param OrderIndexRequest $request
     * @return ResponseInterface|null
     */
    public function excel(OrderIndexRequest $request)
    {
        $gpc = $request->validated();
        return $this->logic->excel($gpc);
    }

    /**
     * @RequestMapping(path="excel/pending")
     * @Middlewares({
     *      
     *      
     * })
     * @param OrderIndexRequest $request
     * @return ResponseInterface|null
     */
    public function excelPending(OrderIndexRequest $request)
    {
        $gpc = $request->validated();
        return $this->logic->excelPending($gpc);
    }


    /**
     * @RequestMapping(path="import-csv")
     * @Middlewares({
     *     
     * })
     * @param ImportPendingRequest $request
     * @return ResponseInterface
     */
    public function importExcelPending(ImportPendingRequest $request)
    {
        $gpc = $request->validated() ;
        
        $res= $this->logic->importExcelPending($gpc);
        return $this->response->success();
    }


}