<?php
declare(strict_types=1);

namespace App\Controller\Admin\Order;

use App\Controller\BaseController;
use App\Logic\Order\ExpressLogic;
use App\Logic\Order\OrderPackageLogic;
use App\Request\IdRequest;
use App\Request\ExpressRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middleware(AdminAuthMiddleware::class)
 */
class OrderPackageController extends BaseController
{
	/**
	 * @Inject()
	 * @var OrderPackageLogic
	 */
	private $logic;

	/**
	 * @RequestMapping(path="/order_package/detail")
	 * @param IdRequest $request
	 * @return string
	 */
	public function expressDetail(IdRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->getPackageExpress($gpc['id']);
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="/order_package/dev/trace")
	 * @param ExpressRequest $request
	 * @return string
	 */
	public function devTrace(ExpressRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->getDevTracer($gpc);
		return $this->response->success($res);
	}

}
