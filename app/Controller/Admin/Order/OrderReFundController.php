<?php
declare(strict_types=1);

namespace App\Controller\Admin\Order;

use App\Controller\BaseController;
use App\Logic\Order\OrderRefundLogic;
use App\Request\Order\DealRefundRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminAuthMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middleware(AdminAuthMiddleware::class)
 */
class OrderReFundController extends BaseController
{
	/**
	 * @Inject()
	 * @var OrderRefundLogic
	 */
	private $logic;

	/**
	 * @RequestMapping(path="/order/refund/set_status")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param DealRefundRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function dealRefund(DealRefundRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->dealRefund($gpc);
		return $this->response->success();
	}


	/**
	 * @RequestMapping(path="/order/refund/detail")
	 * @param RequestInterface $request
	 * @return string
	 */
	public function refundDetail(RequestInterface $request)
	{
		$res = $this->logic->refundDetail($request->input('refund_id'));
		return $this->response->success($res);
	}
}
