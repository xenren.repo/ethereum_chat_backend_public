<?php
declare(strict_types=1);

namespace App\Controller\Admin\Receipts;

use App\Controller\BaseController;
use App\Model\OrderReceipt;
use App\Model\Setting;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\IdRequest;
use App\Request\MarkupRequest;
use App\Request\Medium\MediumStoreRequest;
use App\Request\NullRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;


/**
 * @Controller(prefix="markup")
 * @Middleware(AdminAuthMiddleware::class)
 */
class ReceiptsController extends BaseController
{

	/**
	 * 列表
	 * @RequestMapping(path="/receipts")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param NullRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(NullRequest $request)
	{
		$data = OrderReceipt::get();
		return $this->response->success($data);
	}

}