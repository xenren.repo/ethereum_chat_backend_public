<?php

declare(strict_types=1);

namespace App\Controller\Admin\System;

use App\Controller\BaseController;
use App\Logic\System\CheatSheetLogic;
use App\Middleware\AdminAuthMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Request\System\CheatSheet\SetPaymentRequest;
use App\Request\System\CheatSheet\SetSellerSettingRequest;
use App\Request\System\CheatSheet\SetSellerTopUpRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller(prefix="cheatsheet")
 * @Middleware(AdminAuthMiddleware::class)
 */
class CheatSheetController extends BaseController
{
    /**
     * @Inject
     * @var CheatSheetLogic
     */
    protected $logic;

    /**
     * @RequestMapping(path="payment", method="GET")
     * @Middlewares({
     *     @Middleware(AdminPermissionMiddleware::class)
     * })
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getPayment(RequestInterface $request)
    {
        $gpc = $request->all();
        $res = $this->logic->getPayment($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="payment/set", method="POST")
     * @Middlewares({
     *     @Middleware(AdminPermissionMiddleware::class)
     * })
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function setPayment(SetPaymentRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->setPaymentSetting($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="seller", method="GET")
     * @Middlewares({
     *     @Middleware(AdminPermissionMiddleware::class)
     * })
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getSellerRank(RequestInterface $request)
    {
        $gpc = $request->all();
        $res = $this->logic->getSellerRank($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="seller/set", method="POST")
     * @Middlewares({
     *     @Middleware(AdminPermissionMiddleware::class)
     * })
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function setSellerRankSetting(SetSellerSettingRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->setSellerRankSetting($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="seller_topup", method="GET")
     * @Middlewares({
     *     @Middleware(AdminPermissionMiddleware::class)
     * })
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getSellerTopup(RequestInterface $request)
    {
        $gpc = $request->all();
        $res = $this->logic->getSellerTopup($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="seller_topup/set", method="POST")
     * @Middlewares({
     *     @Middleware(AdminPermissionMiddleware::class)
     * })
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function setSellerTopup(SetSellerTopUpRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->setSellerTopup($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="seller_topup/delete", method="POST")
     * @Middlewares({
     *     @Middleware(AdminPermissionMiddleware::class)
     * })
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function deleteSellerTopup(SetSellerTopUpRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->deleteSellerTopup($gpc);
        return $this->response->success($res);
    }
}
