<?php
declare(strict_types=1);

namespace App\Controller\Admin\System;

use App\Constants\ErrorCode;
use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Logic\System\ModuleLogic;
use App\Request\Module\ModuleDeleteRequest;
use App\Request\Module\ModuleStoreRequest;
use App\Request\Module\ModuleUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller(prefix="module")
 * @Middleware(AdminAuthMiddleware::class)
 */
class ModuleController extends BaseController
{

	/**
	 * @Inject()
	 * @var ModuleLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="add")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param ModuleStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function add(ModuleStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="update")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param ModuleUpdateRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function update(ModuleUpdateRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->update($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="delete")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param ModuleDeleteRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function delete(ModuleDeleteRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->delete($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="/modules")
	 * @param RequestInterface $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function getModuleTree(RequestInterface $request)
	{
		$client_type = $request->input('client_type');
		if (!in_array($client_type, [0,1])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID);
		}
		$res = $this->logic->getModuleTree($client_type);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="auth")
	 * @param RequestInterface $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function getAuthModules(RequestInterface $request)
	{
		$client_type = $request->input('client_type');
		if (!in_array($client_type, [0,1])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID);
		}
		$res = $this->logic->getAuthModules($client_type);
		return $this->response->success($res);
	}

}
