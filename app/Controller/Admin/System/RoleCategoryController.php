<?php
declare(strict_types=1);

namespace App\Controller\Admin\System;

use App\Controller\BaseController;
use App\Logic\System\RoleLogic;
use App\Request\System\RoleDeleteRequest;
use App\Request\System\RoleStoreRequest;
use App\Request\System\RoleUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller(prefix="role_category")
 * @Middleware(AdminAuthMiddleware::class)
 */
class RoleCategoryController extends BaseController
{

	/**
	 * @Inject()
	 * @var RoleLogic
	 */
	protected $logic;




	/**
	 * @RequestMapping(path="/role_categories")
	 * @param RequestInterface $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function json(RequestInterface $request)
	{
		$gpc = $request->all();
		$res = $this->logic->json($gpc);
		return $this->response->success($res);
	}


}
