<?php
declare(strict_types=1);

namespace App\Controller\Admin\System;

use App\Controller\BaseController;
use App\Logic\System\RoleLogic;
use App\Request\System\RoleDeleteRequest;
use App\Request\System\RoleStoreRequest;
use App\Request\System\RoleUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller(prefix="role")
 * @Middleware(AdminAuthMiddleware::class)
 */
class RoleController extends BaseController
{

	/**
	 * @Inject()
	 * @var RoleLogic
	 */
	protected $logic;



	/**
	 * @RequestMapping(path="/roles")
	 * @Middlewares({
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param RequestInterface $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(RequestInterface $request)
	{
		$gpc = $request->all();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="json")
	 * @param RequestInterface $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function json(RequestInterface $request)
	{
		$gpc = $request->all();
		$res = $this->logic->json($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="add")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param RoleStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function add(RoleStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="update")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param RoleUpdateRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function update(RoleUpdateRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->update($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="delete")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param RoleDeleteRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function delete(RoleDeleteRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->delete($gpc);
		return $this->response->success($res);
	}

}
