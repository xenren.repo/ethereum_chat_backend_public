<?php
declare(strict_types=1);

namespace App\Controller\Admin\System;

use App\Controller\BaseController;
use App\Logic\System\RolePermissionLogic;
use App\Request\System\RolePermissionGetRequest;
use App\Request\System\RolePermissionSetRequest;
use App\Request\System\RoleStoreRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;

/**
 * @Controller(prefix="role_permission")
 * @Middleware(AdminAuthMiddleware::class)
 */
class RolePermissionController extends BaseController
{

	/**
	 * @Inject()
	 * @var RolePermissionLogic
	 */
	protected $logic;



	/**
	 * @RequestMapping(path="set")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param RoleStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function set(RolePermissionSetRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->set($gpc);
		return $this->response->success($res ?? []);
	}


	/**
	 * @RequestMapping(path="get")
	 * @Middlewares({
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param RoleStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function get(RolePermissionGetRequest $request)
	{
	    $gpc = $request->validated();
		$res = $this->logic->getPermission($gpc);
		return $this->response->success($res);
	}



}
