<?php
declare(strict_types=1);

namespace App\Controller\Admin\System;

use App\Controller\BaseController;
use App\Logic\System\SettingLogic;
use App\Request\System\SettingSetRequest;
use App\Request\System\UserPermissionGetRequest;
use App\Request\System\UserPermissionSetRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middleware(AdminAuthMiddleware::class)
 */
class SettingController extends BaseController
{

	/**
	 * @Inject()
	 * @var SettingLogic
	 */
	protected $logic;



	/**
	 * @RequestMapping(path="/setting/update")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param UserPermissionSetRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function set(SettingSetRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->set($gpc['data']);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="/settings")
	 * @Middlewares({
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param RequestInterface $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function get(RequestInterface $request)
	{
		$res = $this->logic->index($request->input('type'));
		return $this->response->success($res);
	}


}
