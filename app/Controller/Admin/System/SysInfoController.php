<?php
declare(strict_types=1);

namespace App\Controller\Admin\System;

use App\Controller\BaseController;
use App\Logic\System\SysInfoLogic;
use App\Request\Ad\AdStoreRequest;
use App\Request\Ad\AdUpdateRequest;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\System\SysInfoStoreRequest;
use App\Request\System\SysInfoUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller(prefix="sys_info")
 * @Middleware(AdminAuthMiddleware::class)
 */
class SysInfoController extends BaseController
{

	/**
	 * @Inject()
	 * @var SysInfoLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/sys_info")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="add")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param SysInfoStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function store(SysInfoStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="update")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param SysInfoUpdateRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function update(SysInfoUpdateRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->update($gpc);
		return $this->response->success();
	}



	/**
	 * @RequestMapping(path="show")
	 * @Middlewares({
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param RequestInterface $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function show(RequestInterface $request)
	{
		$data = $this->logic->show($request->input('id'));
		return $this->response->success($data);
	}

	/**
	 * @RequestMapping(path="delete")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param CommonDeleteRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function delete(CommonDeleteRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->delete($gpc);
		return $this->response->success();
	}
}
