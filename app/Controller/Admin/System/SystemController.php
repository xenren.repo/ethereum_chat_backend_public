<?php
declare(strict_types=1);

namespace App\Controller\Admin\System;

use App\Controller\BaseController;
use App\Kernel\Auth\Auth;
use App\Logic\System\RoleLogic;
use App\Logic\System\SystemLogic;
use App\Logic\User\UserRelationLogic;
use App\Model\UserRelation;
use App\Request\System\RoleDeleteRequest;
use App\Request\System\RoleStoreRequest;
use App\Request\System\RoleUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middleware(AdminAuthMiddleware::class)
 */
class SystemController extends BaseController
{




	/**
	 * @RequestMapping(path="/cache_clear")
	 * @return string
	 */
	public function cacheClear()
	{
		if (!Auth::isSuperAdmin()) {
			return 'permission deny!';
		}

		make(SystemLogic::class)->clearCache();

		return $this->response->success('success');
	}



	/**
	 * @RequestMapping(path="/user_relation/rebuild")
	 * @return string
	 */
	public function rebuild()
	{
		if (!Auth::isSuperAdmin()) {
			return 'permission deny!';
		}

		make(UserRelationLogic::class)->rebuildRelation();

		return $this->response->success('success');
	}


}
