<?php
declare(strict_types=1);

namespace App\Controller\Admin\System;

use App\Controller\BaseController;
use App\Logic\System\UserPermissionLogic;
use App\Request\System\UserPermissionGetRequest;
use App\Request\System\UserPermissionSetRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;

/**
 * @Controller(prefix="user_permission")
 * @Middleware(AdminAuthMiddleware::class)
 */
class UserPermissionController extends BaseController
{

	/**
	 * @Inject()
	 * @var UserPermissionLogic
	 */
	protected $logic;



	/**
	 * @RequestMapping(path="set")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param UserPermissionSetRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function set(UserPermissionSetRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->set($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="/user_permissions")
	 * @Middlewares({
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param UserPermissionGetRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function get(UserPermissionGetRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->getPermission($gpc);
		return $this->response->success($res);
	}


}
