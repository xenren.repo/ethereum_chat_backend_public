<?php
declare(strict_types=1);

namespace App\Controller\Admin\System;

use App\Constants\ErrorCode;
use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Logic\System\UserRoleLogic;
use App\Request\System\UserRoleSetRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller(prefix="user_role")
 * @Middleware(AdminAuthMiddleware::class)
 */
class UserRoleController extends BaseController
{

	/**
	 * @Inject()
	 * @var UserRoleLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="set")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param UserRoleSetRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function set(UserRoleSetRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->set($gpc);
		return $this->response->success($res);
	}




	/**
	 * @RequestMapping(path="get")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param RequestInterface $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function get(RequestInterface $request)
	{
		$user_id = $request->input('user_id');
		if (empty($user_id) || !is_numeric($user_id)) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID);
		}
		$res = $this->logic->getRolesId($user_id);
		return $this->response->success($res);
	}

}
