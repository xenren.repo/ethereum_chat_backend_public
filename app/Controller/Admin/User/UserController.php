<?php
declare(strict_types=1);

namespace App\Controller\Admin\User;

use App\Controller\BaseController;
use App\Logic\User\UserLogic;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\User\SetCreditRequest;
use App\Request\User\SetStatusRequest;
use App\Request\User\UserStoreRequest;
use App\Request\User\UserUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\AdminAuthMiddleware;

/**
 * @Controller(prefix="user")
 * @Middleware(AdminAuthMiddleware::class)
 */
class UserController extends BaseController
{

	/**
	 * @Inject()
	 * @var UserLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/users")
	 * @Middleware(AdminPermissionMiddleware::class)
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="add")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param UserStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 * @throws \Exception
	 */
	public function store(UserStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}



	/**
	 * @RequestMapping(path="update")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param UserUpdateRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function update(UserUpdateRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->update($gpc);
		return $this->response->success();
	}


	/**
	 * @RequestMapping(path="delete")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param CommonDeleteRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function delete(CommonDeleteRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->delete($gpc);
		return $this->response->success();
	}


	/**
	 * @RequestMapping(path="set_status")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param SetStatusRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function setStatus(SetStatusRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->setStatus($gpc);
		return $this->response->success();
	}


	/**
	 * @RequestMapping(path="/account_record/set_credit")
	 * @Middlewares({
	 *     @Middleware(SubmitThrottleMiddleware::class),
	 *     @Middleware(AdminPermissionMiddleware::class)
	 * })
	 * @param SetCreditRequest $request
	 * @return string
	 */
	public function setCredit(SetCreditRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->setCredit($gpc);
		return $this->response->success();
	}

}
