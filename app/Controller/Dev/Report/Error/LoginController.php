<?php

declare(strict_types=1);

namespace App\Controller\Dev\Report\Error;

use App\Controller\BaseController;
use App\Logic\Dev\ReportLoginLogic;
use App\Logic\User\UserAddressLogic;
use App\Request\Dev\ReportLoginRequest;
use App\Request\External\ResendTypeRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;

/**
 * Class LoginController
 * @package App\Controller\Dev\Report\Error
 * @Controller(prefix="dev/report/error")
 */
class LoginController extends BaseController
{
    /**
     * @Inject()
     * @var ReportLoginLogic
     */
    private $logic;


    /**
     * @RequestMapping(path="login")
     * @param ReportLoginRequest $request
     * @return mixed
     */
    public function index(ReportLoginRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->report($gpc);
        return $this->response->success($res);
    }
}
