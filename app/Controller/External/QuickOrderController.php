<?php

declare(strict_types=1);

namespace App\Controller\External;

use App\Controller\BaseController;
use App\Logic\Order\OrderLogic;
use App\Request\External\OrderHistoryRequest;
use App\Request\External\QuickOrderRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller(prefix="api/external/order")
 */
class QuickOrderController extends BaseController
{
    /**
     * @Inject
     * @var OrderLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="quick", method="POST")
     * @return mixed
     */
    public function index(QuickOrderRequest $request)
    {
        $gpc = $request->validated();
        $response = $this->logic->quickOrder($gpc);
        return $this->response->success($response);
    }

    /**
     * @RequestMapping(path="history", method="POST")
     * @return mixed
     */
    public function orderHistory(OrderHistoryRequest $request)
    {
        $gpc = $request->validated();
        $response = $this->logic->orderHistory($gpc);
        return $this->response->success($response);
    }
}
