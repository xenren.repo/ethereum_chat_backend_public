<?php

declare(strict_types=1);

namespace App\Controller\External;

use App\Controller\BaseController;
use App\Logic\External\ResendRequestLogic;
use App\Request\External\ResendTypeRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller(prefix="api/external")
 */
class ResendRequestController extends BaseController
{
    /**
     * @Inject
     * @var ResendRequestLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="resend")
     * @return mixed
     */
    public function resendRequest(ResendTypeRequest $request)
    {
        $gpc = $request->validated();
        $response = $this->logic->resendRequest($gpc);
        return $this->response->success($response);
    }

    /**
     * @RequestMapping(path="get_resend_type", methods={"GET", "POST"})
     * @return mixed
     */
    public function getResendType()
    {
        $response = $this->logic->getResendType();
        return $this->response->success($response);
    }

    /**
     * @RequestMapping(path="get_failed_request", methods={"GET", "POST"})
     * @return mixed
     */
    public function getFailedRequest(ResendTypeRequest $request)
    {
        $gpc = $request->validated();
        $response = $this->logic->getFailedRequest($gpc);
        return $this->response->success($response);
    }
}
