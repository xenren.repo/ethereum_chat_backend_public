<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Controller\Home;

use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Kernel\Common\Common;
use App\Logic\Business\BusinessLogic;
use App\Logic\Order\PayLogic;
use App\Logic\System\DataDictionaryLogic;
use App\Logic\System\SysInfoLogic;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;

/**
 * @Controller()
 */
class HomeController extends BaseController
{
	/**
	 * @RequestMapping(path="/")
	 * @param RequestInterface $request
	 * @return mixed
	 */
	public function redirect(RequestInterface $request)
	{
		$url = $request->getUri()->getScheme().'://'.$request->getUri()->getHost().'/mobile.html';
		return $this->response->redirect($url);
	}

	/**
	 * @RequestMapping(path="/data_dictionary")
	 * @param RequestInterface $request
	 * @return mixed
	 */
	public function dataDictionary(RequestInterface $request)
	{
		validate($request->all(),['module'=>'sometimes|string|max:50','key_name'=>'sometimes|string|max:50']);
		$data = make(DataDictionaryLogic::class)->getJson($request->input('module'), $request->input('key_name'));
		return $this->response->success($data);
	}



	/**
	 * @RequestMapping(path="/app/info")
	 * @return mixed
	 * @throws BusinessException
	 */
	public function appInfo()
	{
		$data = [
			'site_email'=> Common::getSetting('base.site_email', ''),
		];
		return $this->response->success($data);
	}

	/**
	 * @RequestMapping(path="/site/info")
	 * @return mixed
	 * @throws BusinessException
	 */
	public function siteInfo()
	{
		$data = [
			'site_name'=> Common::getSetting('base.name', ''),
			'download_url'=> Common::getSetting('base.download_url', ''),
			'custom_phone'=> Common::getSetting('base.custom_phone', ''),
			'custom'=> Common::getSetting('base.custom', ''),
			'mobile_version'=> Common::getSetting('base.mobile_version', ''),
			'upload_type'=> Common::getSetting('upload.upload_type', 1),
			'app_id'=> Common::getSetting('base.app_id', ''),
			'h5_version'=> Common::getSetting('version.h5', 0),
			'logo'=> Common::getSetting('base.logo', ''),
			'bank'=> Common::getSetting('plat', []),
			'cny_to_credit2'=> BusinessLogic::getCnyToCredit2Ratio(),
			'sys_info'  => make(SysInfoLogic::class)->getLatestInfo()
		];
		return $this->response->success($data);
	}

	/**
	 * @RequestMapping(path="/alipay/notify")
	 * @param RequestInterface $request
	 * @return mixed
	 */
	public function alipayNotify(RequestInterface $request)
	{
		$gpc = $request->all();
		make(PayLogic::class)->alipayNotify($gpc);
		return 'success';
	}
	/**
	 * @RequestMapping(path="/alipay/notify-new")
	 * @param RequestInterface $request
	 * @return mixed
	 */
	public function alipayNotifyNew(RequestInterface $request)
	{
		$gpc = $request->all();
		make(PayLogic::class)->alipayNotifyNew($gpc);
		return 'success';
	}
}