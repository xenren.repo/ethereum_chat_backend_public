<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Controller\Home;

use App\Controller\BaseController;
use App\Logic\System\SmsLogic;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller()
 */
class SmsController extends BaseController
{
	/**
	 * @RequestMapping(path="/get_sms")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @param RequestInterface $request
	 * @return mixed
	 */
	public function getSms(RequestInterface $request)
	{
		make(SmsLogic::class)->getSms($request->all());
		return $this->response->success();
	}


}