<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Controller\Home;

use App\Controller\BaseController;
use App\Logic\System\SmsLogic;
use App\Logic\System\UploadLogic;
use App\Model\User;
use App\Request\System\ImagesUploadRequest;
use Hyperf\DbConnection\Db;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller()
 */
class SystemController extends BaseController
{
	/**
	 * @RequestMapping(path="/images/upload")
	 * @param ImagesUploadRequest $request
	 * @return mixed
	 */
	public function upload(ImagesUploadRequest $request)
	{
		$res = make(UploadLogic::class)->uploadImages($request->file('images'));
		return $this->response->success($res);
	}





}