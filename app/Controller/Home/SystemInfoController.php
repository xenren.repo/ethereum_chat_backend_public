<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Controller\Home;

use App\Controller\BaseController;
use App\Logic\System\SysInfoLogic;
use App\Request\CommonIndexRequest;
use App\Request\IdRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller()
 */
class SystemInfoController extends BaseController
{

	/**
	 * @Inject()
	 * @var SysInfoLogic
	 */
	private $logic;

	/**
	 * @RequestMapping(path="/user/sys_info")
	 * @param CommonIndexRequest $request
	 * @return string
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$data = $this->logic->index($gpc);
		return $this->response->success($data);
	}


	/**
	 * @RequestMapping(path="/user/sys_info/show")
	 * @param IdRequest $request
	 * @return string
	 */
	public function show(IdRequest $request)
	{
		$gpc = $request->validated();
		$data = $this->logic->show($gpc['id']);
		return $this->response->success($data);
	}

}