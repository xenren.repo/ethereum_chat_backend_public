<?php

declare(strict_types=1);

namespace App\Controller\Payment;

use App\Controller\BaseController;
use App\Lib\Payment\WeChatPay\Handler;
use App\Logic\Order\PayLogic;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller(prefix="notify")
 */
class NotificationController extends BaseController
{
    /**
     * @Inject
     * @var PayLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="wechat", methods={"GET", "POST"})
     * @return mixed
     */
    public function wechat(RequestInterface $request)
    {
        logger()->error('Notification is called');
        $gpc = $this->catchRequest($request);
        $res = $this->logic->notifyWechat($gpc);
        return $this->response->success($res);
    }

    private function catchRequest($request)
    {
        $contentType = $request->header('Content-Type');
        logger()->error($contentType);
        if (strpos(strtolower($contentType), 'xml') !== false) {
            $body = (string) $request->getBody()->getContents();
            logger()->error($body);
            return (new Handler([]))->xmlToarray($body);
        }
        logger()->error(json_encode($request->all()));
        return $request->all();
    }
}
