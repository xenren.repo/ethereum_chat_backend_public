<?php
declare(strict_types=1);

namespace App\Controller\Remote;

use App\Constants\ErrorCode;
use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Logic\Finance\Credit2Logic;
use App\Logic\Finance\Credit3Logic;
use App\Logic\User\UserAuthLogic;
use App\Model\Credit2;
use App\Model\Credit3;
use App\Model\User;
use App\Request\Remote\RemoteSetCreditRequest;
use App\Request\Remote\RemoteUserClearCacheRequest;
use App\Request\Remote\RemoteUserRegisterRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\RemoteApiMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middleware(RemoteApiMiddleware::class)
 */
class UserController extends BaseController
{

	/**
	 * @Inject()
	 * @var UserAuthLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/user/register_by_remote")
	 * @return mixed
	 */
	public function addUserByApi()
	{
		// request 验证比中间件验证更优先,故不第一时间自动注入
		$request = make(RemoteUserRegisterRequest::class);
		$gpc = $request->validated();
		$this->logic->addUserByApi($gpc);
		return $this->response->success('success');
	}

	/**
	 * @RequestMapping(path="/user/remote/info")
	 * @param RequestInterface $request
	 * @return mixed
	 */
	public function balance(RequestInterface $request)
	{
		$name = $request->input('name');
		if (empty($name)) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'用户名必须填写');
		}
		$user = User::where('name', $name)->firstOrFail();
		$data = [
			'name' => $user['name'],
			'credit1' => $user['credit1'],
			'credit2' => $user['credit2'],
		];
		return $this->response->success($data);
	}


	/**
	 * @RequestMapping(path="/user/remote/credit2")
	 * 实际使用的是credit3表
	 */
	public function setCredit()
	{
		$request = make(RemoteSetCreditRequest::class);
		$gpc = $request->validated();
		$user = User::where('name', $gpc['name'])->firstOrFail()->toArray();
		$exist = Credit3::where('sub_type', $gpc['record_no'])->value('id');
		if ($exist) {
			return $this->response->success('已存在该记录');
		}
		$data = [
			'user' => $user,
			'credit' => $gpc['amount'],
			'type' => $gpc['type'] ?? Credit3Logic::PRIZE,
			'foreign_id' => 0,
			'remark' => $gpc['remark'],
			'sub_type' => $gpc['record_no'],
		];
		make(Credit3Logic::class)->setCredit($data);
		return $this->response->success('success');
	}

	/**
	 * @RequestMapping(path="/user/remote/add-credit2")
	 * 实际使用的是credit3表
	 */
	public function addCreditTwo(RequestInterface $request)
	{
        $request = make(RemoteSetCreditRequest::class);
        $gpc = $request->validated();
        $user = User::where('name', $gpc['name'])->firstOrFail()->toArray();
        $exist = Credit2::where('sub_type', $gpc['record_no'])->value('id');
		if ($exist) {
			return $this->response->success('已存在该记录');
		}
		$data = [
			'user' => $user,
			'credit' => $gpc['amount'],
			'type' => $gpc['type'] ?? Credit2Logic::PRIZE,
			'foreign_id' => 0,
			'remark' => $gpc['remark'],
			'sub_type' => $gpc['record_no'],
		];
		make(Credit2Logic::class)->setCredit($data);
		return $this->response->success('success');
	}
	/**
     * @RequestMapping(path="/user/remote/clear-cache")
     */
    public function clearCache(RemoteUserClearCacheRequest $request)
    {
        $gpc = $request->validated();
        $this->logic->setGpc($gpc);
        $res = $this->logic->clearUserIm()->clearCache()->getResponse();
        return $this->response->success($res);
	}
}
