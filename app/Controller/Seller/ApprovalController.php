<?php

declare(strict_types=1);

namespace App\Controller\Seller;

use App\Controller\BaseController;
use App\Logic\Order\OrderLogic;
use App\Logic\Seller\SellerLogic;
use App\Request\Order\OrderIndexRequest;
use App\Request\Seller\UpdateSellerProfileRequest;
use App\Request\CommonIndexRequest;
use App\Request\Seller\ApprovalRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
/**
 * @Controller(prefix="seller/approval")
 */
class ApprovalController extends BaseController
{
    /**
     * @Inject()
     * @var SellerLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="list")
     * @param CommonIndexRequest $request
     * @return string
     */
    public function index(CommonIndexRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->list($gpc);
        return $this->response->success($res);
    }
    /**
     * @RequestMapping(path="status")
     * @param ApprovalRequest $request
     * @return string
     */
    public function changeStatus(ApprovalRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->changeStatus($gpc);
        return $this->response->success($res);
    }

     /**
     * @RequestMapping(path="update")
     * @param UpdateSellerProfileRequest $request
     * @return string
     */
    public function updateReject(UpdateSellerProfileRequest $request)
    {
        $gpc = $request->validated();
         $res= $this->logic->updateReject($gpc);
        return $this->response->success($res);
    }

    
    
    
}
