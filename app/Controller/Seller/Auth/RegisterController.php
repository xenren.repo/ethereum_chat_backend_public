<?php

declare(strict_types=1);

namespace App\Controller\Seller\Auth;

use App\Controller\BaseController;
use App\Logic\Order\OrderLogic;
use App\Logic\Seller\SellerRegisterLogic;
use App\Request\Order\OrderIndexRequest;
use App\Request\Seller\RegisterRequest;
use App\Request\Seller\MobileVerifyRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
/**
 * @Controller(prefix="seller/auth")
 */
class RegisterController extends BaseController
{
    /**
     * @Inject()
     * @var SellerRegisterLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="register")
     * @param RegisterRequest $request
     * @return string
     */
    public function register(RegisterRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->register($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="mobile-verify")
     * @param RegisterRequest $request
     * @return string
     */

    public function mobileVerify(MobileVerifyRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->SellerVerifPhone($gpc);
        return $this->response->success($res);
    }
}
