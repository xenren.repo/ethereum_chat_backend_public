<?php

declare(strict_types=1);

namespace App\Controller\Seller;

use App\Controller\BaseController;
use App\Logic\Seller\SellerLogic;
use App\Request\CommonIndexRequest;
use App\Request\Seller\ApprovalRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller(prefix="seller/report")
 */
class ReportController extends BaseController
{
    /**
     * @Inject
     * @var SellerLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="list")
     * @return string
     */
    public function index(CommonIndexRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->reportList($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="status")
     * @return string
     */
    public function changeStatus(ApprovalRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->changeStatus($gpc);
        return $this->response->success($res);
    }
}
