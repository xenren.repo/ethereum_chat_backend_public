<?php

declare(strict_types=1);

namespace App\Controller\Seller;

use App\Controller\BaseController;
use App\Logic\Seller\TopUpLogic;
use App\Request\Seller\TopupListRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller(prefix="seller/topup")
 */
class TopupController extends BaseController
{
    /**
     * @Inject
     * @var TopUpLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="list", method="POST")
     * @return array
     */
    public function getRank(TopupListRequest $request)
    {
        $gpc = $request->validated();
        $response = $this->logic->getTopUpLists((int) $gpc['rank_id']);
        return $this->response->success($response);
    }
}
