<?php

declare(strict_types=1);

namespace App\Controller\Test\Payment;

use App\Controller\BaseController;
use App\Logic\Finance\Payment\WechatPayLogic;
use App\Logic\Seller\RankSettingLogic;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;

/**
 * @Controller(prefix="test/payment/wechat")
 */
class WechatController extends BaseController
{
    /**
     * @Inject
     * @var WechatPayLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="pay", method="GET")
     * @return string
     */
    public function pay(RequestInterface $request, ResponseInterface $response)
    {
        $res = $this->logic->appPay('123', '123', 1);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="check_order", method="GET")
     * @return string
     */
    public function checkOrder(RequestInterface $request, ResponseInterface $response)
    {
        $res = $this->logic->checkOrder($request->all());
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="ok", method="GET")
     * @return string
     */
    public function ok(RequestInterface $request, ResponseInterface $response)
    {
        $res = (new RankSettingLogic())->getSettings([3, 4], 'auto_confirm_order');
        return $this->response->success($res);
    }
}
