<?php

declare(strict_types=1);

namespace App\Controller\User\Files;

use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Storage\UploadFileLogic;
use App\Model\UploadFileLog;
use App\Request\CommonIndexRequest;
use App\Request\IdRequest;
use App\Request\User\UploadFileRequest;
use App\Traits\UploadFileRule;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Middleware\UserAuthMiddleware;
use Hyperf\HttpServer\Contract\ResponseInterface;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

/**
 * @Controller(prefix="/api/user/upload-file")
 */
class UploadController extends BaseController
{
    use UploadFileRule;
    /**
     * @Inject()
     * @var UploadFileLogic
     */
    protected $logic;

    /**
     * @RequestMapping(path="upload")
     * @Middleware(UserAuthMiddleware::class)
     * @param UploadFileRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function upload(UploadFileRequest $request)
    {
        $request->validated();
        $res = $this->logic->uploadHandler($request);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="index")
     * @Middleware(UserAuthMiddleware::class)
     * @param CommonIndexRequest $request
     */
    public function index(CommonIndexRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->index($gpc);
        return $this->response->success($res);
    }
    /**
     * @RequestMapping(path="show")
     * @Middleware(UserAuthMiddleware::class)
     * @param IdRequest $request
     */
    public function show(IdRequest $request)
    {
        $gpc = $request->validated();
        $find = UploadFileLog::find($gpc['id']);
        if ($find)
        {
            $find->shareable_link = 'share/moment/'.$find->user_id.'/'.$find->file_name;
        }
        return $this->response->success($find);
    }
    /**
     * @RequestMapping(path="/share/moment/{user_id:\d+}/{uuid:.*?}",methods={"GET","POST"})
     * @param IdRequest $request
     */
    public function readFile(ResponseInterface $request,int $user_id,string $uuid,ResponseInterface $response)
    {
        $findData = UploadFileLog::where([
            'user_id' => $user_id,
            'file_name' => $uuid,
            'is_shareable' => '1'
        ])->first();
        if (!$findData)
        {
            throw new BusinessException(-3,'File not found');
        }
        $origName = implode('_',explode(' ',$findData->original_name));
        return $response->download(BASE_PATH.'/'.$findData->full_path,$origName);
    }
    /**
     * @RequestMapping(path="allowed-exts")
     */
    public function allowedExts()
    {
        $response = $this->getAllowedMimes();
        return $this->response->success($response);
    }
    /**
     * @RequestMapping(path="delete")
     * @Middleware(UserAuthMiddleware::class)
     * @param IdRequest $request
     */
    public function delete(IdRequest $request)
    {
        $gpc = $request->validated();
        $find = UploadFileLog::find($gpc['id']);
        $userId = Auth::id();
        if ($find && $userId == $find->user_id)
        {
            $fullPath = BASE_PATH.'/'.$find->full_path;
            if (file_exists($fullPath))
            {
                if (unlink($fullPath))
                {
                    $response = true;
                } else
                {
                    $response = false;
                }
            } else
            {
                $response = true;

            }
        } else
        {
            throw new BusinessException(-3,"Unauthorized Operation");
        }

 
        if ($response == true)
        {
            $response = $find->delete();
        }

        return $this->response->success($response);
    }

}
