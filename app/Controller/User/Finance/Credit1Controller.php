<?php
declare(strict_types=1);

namespace App\Controller\User\Finance;

use App\Controller\BaseController;
use App\Logic\Finance\Credit1Logic;
use App\Request\CommonIndexRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;

/**
 * @Controller(prefix="/user/finance")
 * @Middleware(UserAuthMiddleware::class)
 */
class Credit1Controller extends BaseController
{

	/**
	 * @Inject()
	 * @var Credit1Logic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/user/finance/credit1")
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}

}
