<?php
declare(strict_types=1);

namespace App\Controller\User\Finance;

use App\Controller\BaseController;
use App\Logic\Finance\Credit2Logic;
use App\Request\CommonIndexRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\AdminPermissionMiddleware;
use App\Middleware\UserAuthMiddleware;

/**
 * @Controller(prefix="user/finance/credit2")
 * @Middleware(UserAuthMiddleware::class)
 */
class Credit2Controller extends BaseController
{

	/**
	 * @Inject()
	 * @var Credit2Logic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/user/finance/credit2")
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}

}
