<?php
declare(strict_types=1);

namespace App\Controller\User\Finance;

use App\Controller\BaseController;
use App\Kernel\Auth\Auth;
use App\Logic\Finance\Credit2Logic;
use App\Logic\Finance\Credit3Logic;
use App\Model\Credit4;
use App\Request\CommonIndexRequest;
use App\Request\Finance\ExchangeRmbRequest;
use App\Request\NullRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\UserAuthMiddleware;

/**
 * @Controller(prefix="user/finance/credit3")
 * @Middleware(UserAuthMiddleware::class)
 */
class Credit3Controller extends BaseController
{

	/**
	 * @Inject()
	 * @var Credit3Logic
	 */
	protected $logic;


    /**
     * @RequestMapping(path="/user/finance/credit3")
     * @param CommonIndexRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(CommonIndexRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->index($gpc,true);
        return $this->response->success($res);
    }


    /**
     * @RequestMapping(path="/user/finance/credit4")
     * @param CommonIndexRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function credit4(CommonIndexRequest $request)
    {
        $gpc = $request->validated();
        $res = Credit4::where('user_id', '=', Auth::id())
                ->orderBy('created_at','desc')->get();

        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="/user/finance/credit3/convert-to-zone-coupon")
     * @param ExchangeRmbRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function convertToCoupon(ExchangeRmbRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->convert2Coupon($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="/user/finance/credit3/unredeemable_amount")
     * @param NullRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function unredeemableAmount(NullRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->getUnredeemableAmount($gpc);
        return $this->response->success($res);
    }

}
