<?php

declare(strict_types=1);

namespace App\Controller\User\Finance;

use App\Controller\BaseController;
use App\Logic\Finance\ExchangeLogic;
use App\Logic\Finance\RechargeLogic;
use App\Request\CommonIndexRequest;
use App\Request\Finance\ExchangeRmbRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use App\Middleware\UserAuthMiddleware;
/**
 * @Controller(prefix="user/exchange")
 * @Middleware(UserAuthMiddleware::class)
 */
class ExchangeController extends BaseController
{
    /**
     * @Inject()
     * @var ExchangeLogic
     */
    protected $logic;

    /**
     * @RequestMapping(path="/user/exchange")
     * @param ExchangeRmbRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function exchange(ExchangeRmbRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->exchange($gpc);
        return $this->response->success($res);
    }

}