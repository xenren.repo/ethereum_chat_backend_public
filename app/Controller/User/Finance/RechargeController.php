<?php
declare(strict_types=1);

namespace App\Controller\User\Finance;

use App\Controller\BaseController;
use App\Logic\Finance\RechargeLogic;
use App\Request\CommonIndexRequest;
use App\Request\Finance\RechargeSetStatusRequest;
use App\Request\Finance\RechargeStoreRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller(prefix="user/recharge")
 * @Middleware(UserAuthMiddleware::class)
 */
class RechargeController extends BaseController
{

	/**
	 * @Inject()
	 * @var RechargeLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/user/recharge")
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="add")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @param RechargeStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function store(RechargeStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->recharge($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="cancel")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @param RechargeSetStatusRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function setStatus(RechargeSetStatusRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->setStatus($gpc);
		return $this->response->success();
	}
}
