<?php
declare(strict_types=1);

namespace App\Controller\User\Goods;

use App\Constants\ErrorCode;
use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Logic\Goods\GoodsCategoryLogic;
use App\Logic\Goods\GoodsLogic;
use App\Logic\User\UserImFavoriteLogic;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\Goods\GoodsIndexRequest;
use App\Request\IdRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Middleware\SubmitThrottleMiddleware;
/**
 * @Controller(prefix="/user/goods_category")
 */
class GoodsCategoryController extends BaseController
{
	/**
	 * @Inject()
	 * @var GoodsCategoryLogic
	 */
	private $logic;


	/**
	 * @RequestMapping(path="index")
	 * @return array
	 */
	public function index()
	{
		$res = $this->logic->index();
		return $this->response->success($res);
	}





}
