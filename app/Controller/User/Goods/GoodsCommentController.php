<?php
declare(strict_types=1);

namespace App\Controller\User\Goods;

use App\Constants\ErrorCode;
use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Goods\GoodsCategoryLogic;
use App\Logic\Goods\GoodsCommentLogic;
use App\Logic\Goods\GoodsLogic;
use App\Logic\User\UserImFavoriteLogic;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\Goods\CommentStoreRequest;
use App\Request\Goods\GetGoodsCommentRequest;
use App\Request\Goods\GoodsIndexRequest;
use App\Request\IdRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller(prefix="/user/order_comment")
 */
class GoodsCommentController extends BaseController
{
	/**
	 * @Inject()
	 * @var GoodsCommentLogic
	 */
	private $logic;


//	/**
//	 * @RequestMapping(path="index")
//	 * @return array
//	 */
//	public function index()
//	{
//		$res = $this->logic->index();
//		return $this->response->success($res);
//	}
//
//
//
	/**
	 * @RequestMapping(path="comment")
	 * @Middlewares({
	 *      @Middleware(SubmitThrottleMiddleware::class),
	 *      @Middleware(UserAuthMiddleware::class)
	 * })
	 * @param CommentStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function comment(CommentStoreRequest $request)
	{
		$gpc = $request->validated();
		make(GoodsCommentLogic::class)->comment($gpc);
		return $this->response->success();
	}

	/**
	 * @RequestMapping(path="detail")
	 * @param GetGoodsCommentRequest $request
	 * @return void
	 */
	public function getComment(GetGoodsCommentRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->getComment($gpc);
		return $this->response->success($res);
	}


}
