<?php

declare(strict_types=1);

namespace App\Controller\User\Goods;

use App\Controller\BaseController;
use App\Logic\Goods\GoodsLogic;
use App\Request\Goods\GoodsIndexRequest;
use App\Request\IdRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Psr\Http\Message\ResponseInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * @Controller(prefix="/user/goods")
 */
class GoodsController extends BaseController
{
    /**
     * @Inject
     * @var GoodsLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="search")
     * @throws InvalidArgumentException
     * @return array
     */
    public function index(GoodsIndexRequest $request)
    {
        $gpc = $request->validated();
        cache()->delete('goods_today_recommend');
        cache()->delete('goods_hot_goods');
        cache()->clearPrefix('goods');
        $res = $this->logic->search($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="detail")
     * @throws InvalidArgumentException
     * @return ResponseInterface
     */
    public function detail(IdRequest $request)
    {
        cache()->delete('goods_today_recommend');
        cache()->delete('goods_hot_goods');
        cache()->clearPrefix('goods');
        $goods = $this->logic->userGoodsDetail($request->input('id'), $request->input('userName'));
        return $this->response->success($goods);
    }

    /**
     * @RequestMapping(path="current-price")
     * @throws InvalidArgumentException
     * @return ResponseInterface
     */
    public function currentPrice(IdRequest $request)
    {
        cache()->delete('goods_today_recommend');
        cache()->delete('goods_hot_goods');
        cache()->clearPrefix('goods');
        $goods = $this->logic->userGoodsDetailForPrice($request->input('id'), $request->input('userName'));
        return $this->response->success($goods);
    }
}
