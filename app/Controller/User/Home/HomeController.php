<?php
declare(strict_types=1);

namespace App\Controller\User\Home;

use App\Controller\BaseController;
use App\Kernel\Common\Common;
use App\Kernel\Common\Helper;
use App\Lib\Sms\Sms;
use App\Logic\Ad\AdLogic;
use App\Logic\Ad\NavigationLogic;
use App\Logic\Ad\ThirdAdLogic;
use App\Logic\Goods\GoodsLogic;
use App\Logic\User\UserAuthLogic;
use App\Request\User\ChangePasswordRequest;
use App\Request\User\ResetPasswordRequest;
use App\Request\User\UserLoginRequest;
use App\Request\User\UserRegisterRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\UserAuthMiddleware;

/**
 * @Controller()
 */
class HomeController extends BaseController
{

	/**
	 * @Inject()
	 * @var UserAuthLogic
	 */
	protected $logic;


	/**
	 * 商城基础信息
	 * @RequestMapping(path="/user/mall/info")
	 */
	public function mallInfo()
	{
		$data =  [
			'ad' => make(AdLogic::class)->json(),
			'navigation' => make(NavigationLogic::class)->json(),
			'third_ad' => make(ThirdAdLogic::class)->fetchAd(1),
			'today_recommend' => make(GoodsLogic::class)->todayRecommend(),
			'hot_goods' => make(GoodsLogic::class)->hotGoods()   //爆款
		];
		return $this->response->success($data);
	}




	/**
	 * @RequestMapping(path="/user/about_us")
	 */
	public function aboutUs()
	{
		$data =  Common::getSetting('about_us.doc','');
		return $this->response->success($data);
	}
}
