<?php
declare(strict_types=1);

namespace App\Controller\User\Medium;

use App\Controller\BaseController;
use App\Logic\Medium\MediumLogic;
use App\Request\CommonIndexRequest;
use App\Request\IdRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;

/**
 * @Controller(prefix="/user/medium")
 * @Middleware(UserAuthMiddleware::class)
 */
class MediumController extends BaseController
{

	/**
	 * @Inject()
	 * @var MediumLogic
	 */
	private $logic;


	/**
	 * 列表
	 * @RequestMapping(path="/user/medium")
	 * @param CommonIndexRequest $request
	 * @return array
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$data = $this->logic->index($gpc);
		return $this->response->success($data);
	}


	/**
	 * @RequestMapping(path="show")
	 * @param IdRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function show(IdRequest $request)
	{
		$gpc = $request->validated();
		$data = $this->logic->show($gpc['id']);
		return $this->response->success($data);
	}
}