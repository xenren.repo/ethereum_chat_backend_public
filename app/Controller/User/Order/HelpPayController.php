<?php

declare(strict_types=1);

namespace App\Controller\User\Order;

use App\Controller\BaseController;
use App\Logic\Order\PayLogic;
use App\Middleware\SubmitThrottleMiddleware;
use App\Request\Order\PayOrderRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpMessage\Server\Response;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * @Controller(prefix="help-pay")
 */
class HelpPayController extends BaseController
{
    /**
     * @Inject
     * @var PayLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="later/{service}/{no}")
     * @Middleware(SubmitThrottleMiddleware::class)
     * @param PayOrderRequest $request
     * @param mixed $service
     * @param mixed $no
     * @return Response|ResponseInterface
     */
    public function payLater($service, $no)
    {
        $res = $this->logic->payLaterHandler($service, $no);
        if (strtolower($service) === 'alipay') {
            return $this->response->redirect($res);
        }
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="later/{service}/{no}/{cart}")
     * @Middleware(SubmitThrottleMiddleware::class)
     * @param mixed $service
     * @param mixed $no
     * @param $cart
     * @return Response|ResponseInterface
     */
    public function payLaterJinQia($service, $no, $cart)
    {
        $res = $this->logic->payLaterHandler($service, $no, $cart);
        if (strtolower($service) === 'alipay') {
            return $this->response->redirect($res);
        }
        return $this->response->success($res);
    }
}
