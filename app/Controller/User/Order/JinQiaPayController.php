<?php

declare(strict_types=1);

namespace App\Controller\User\Order;

use App\Controller\BaseController;
use App\Logic\Order\PayLogic;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\UserAuthMiddleware;
use App\Request\Order\AllowListRequest;
use App\Request\Order\getRewardRequest;
use App\Request\Order\JinQiaPayOrderRequest;
use App\Request\Order\PayOrderRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller(prefix="user/jinqiapay")
 */
class JinQiaPayController extends BaseController
{
    /**
     * @Inject
     * @var PayLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="order")
     * @Middleware(SubmitThrottleMiddleware::class)
     * @param JinQiaPayOrderRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function order(JinQiaPayOrderRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->jinqiaPay($gpc);
        return $this->response->success($res);
    }
}
