<?php
declare(strict_types=1);

namespace App\Controller\User\Order;

use App\Controller\BaseController;
use App\Logic\Order\OrderLogic;
use App\Request\CommonIndexRequest;
use App\Request\IdRequest;
use App\Request\Order\OrderIndexRequest;
use App\Request\Order\OrderStoreRequest;
use App\Request\User\UserAddressStoreRequest;
use App\Request\User\UserAddressUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller(prefix="user/order")
 * @Middleware(UserAuthMiddleware::class)
 */
class OrderController extends BaseController
{

	/**
	 * @Inject()
	 * @var OrderLogic
	 */
	private $logic;

	/**
	 * @RequestMapping(path="/user/orders")
	 * @param OrderIndexRequest $request
	 * @return string
	 */
	public function index(OrderIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="add")

	 * @param OrderStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function store(OrderStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="pre_count")
	 * @param OrderStoreRequest $request
	 * @return array
	 */
	public function preCountOrder(OrderStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->preCountOrder($gpc);
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="close")
	 * @param IdRequest $request
	 * @return string
	 */
	public function close(IdRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->close($gpc['id']);
		return $this->response->success();
	}

	/**
	 * @RequestMapping(path="detail")
	 * @param IdRequest $request
	 * @return string
	 */
	public function detail(IdRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->userDetail($gpc['id']);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="confirm")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @param IdRequest $request
	 * @return string
	 */
	public function confirm(IdRequest $request)
	{
		$gpc = $request->validated();
		$this->logic->confirm($gpc['id']);
		return $this->response->success();
	}

}
