<?php
declare(strict_types=1);

namespace App\Controller\User\Order;

use App\Controller\BaseController;
use App\Logic\Order\ExpressLogic;
use App\Logic\Order\OrderPackageLogic;
use App\Logic\Order\UserCartLogic;
use App\Request\ExpressRequest;
use App\Request\IdRequest;
use App\Request\Order\UserCartStoreRequest;
use App\Request\Order\UserCartUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller(prefix="user/order_package")
 * @Middleware(UserAuthMiddleware::class)
 */
class OrderPackageController extends BaseController
{

	/**
	 * @Inject()
	 * @var OrderPackageLogic
	 */
	private $logic;


    /**
     * @RequestMapping(path="detail")
     * @param IdRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function expressDetail(IdRequest $request)
    {
        $gpc = $request->validated();
        $data = $this->logic->getPackageExpress($gpc['id']);
        return $this->response->success($data);
    }

    /**
     * @RequestMapping(path="trace")
     * @param ExpressRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function expressTrace(ExpressRequest $request)
    {
        $gpc = $request->validated();
        $data = $this->logic->getDevTracer($gpc);
        return $this->response->success($data);
    }
}
