<?php
declare(strict_types=1);

namespace App\Controller\User\Order;

use App\Controller\BaseController;
use App\Logic\Order\ExpressLogic;
use App\Logic\Order\OrderPackageLogic;
use App\Logic\Order\OrderRefundLogic;
use App\Logic\Order\UserCartLogic;
use App\Request\IdRequest;
use App\Request\Order\OrderRefundStoreRequest;
use App\Request\Order\UserCartStoreRequest;
use App\Request\Order\UserCartUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use App\Middleware\SubmitThrottleMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller(prefix="user/order")
 * @Middleware(UserAuthMiddleware::class)
 */
class OrderRefundController extends BaseController
{

	/**
	 * @Inject()
	 * @var OrderRefundLogic
	 */
	private $logic;


	/**
	 * @RequestMapping(path="refund")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @param OrderRefundStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function refund(OrderRefundStoreRequest $request)
	{
		$gpc =$request->validated();
		$res = $this->logic->refundApply($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="refund/detail")
	 * @param RequestInterface $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function refundDetail(RequestInterface $request)
	{
		$res = $this->logic->refundDetail($request->input('refund_id'));
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="cancel_refund")
	 * @param IdRequest $request
	 * @return string
	 */
	public function cancelRefund(IdRequest $request)
	{
		$gpc =$request->validated();
		$this->logic->cancelRefundApply($gpc['id']);
		return $this->response->success();
	}
}
