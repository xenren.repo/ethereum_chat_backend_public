<?php

declare(strict_types=1);

namespace App\Controller\User\Order;

use App\Controller\BaseController;
use App\Logic\Order\PayLogic;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\UserAuthMiddleware;
use App\Request\Order\AllowListRequest;
use App\Request\Order\getRewardRequest;
use App\Request\Order\PayOrderRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller(prefix="user/pay")
 * @Middleware(UserAuthMiddleware::class)
 */
class PayController extends BaseController
{
    /**
     * @Inject
     * @var PayLogic
     */
    private $logic;

    /**
     * @RequestMapping(path="allow_list")
     * @return string
     */
    public function index(AllowListRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->getAllowList($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="reward")
     * @return string
     */
    public function getReward(GetRewardRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->getRewardPoint($gpc['user_id']);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="order")
     * @Middleware(SubmitThrottleMiddleware::class)
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function order(PayOrderRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->pay($gpc);
        return $this->response->success($res);
    }
}
