<?php
declare(strict_types=1);

namespace App\Controller\User\Order;

use App\Controller\BaseController;
use App\Logic\Order\UserCartLogic;
use App\Request\IdRequest;
use App\Request\Order\UserCartStoreRequest;
use App\Request\Order\UserCartUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller(prefix="user/goods_cart")
 * @Middleware(UserAuthMiddleware::class)
 */
class UserCartController extends BaseController
{

	/**
	 * @Inject()
	 * @var UserCartLogic
	 */
	private $logic;


	/**
	 * @RequestMapping(path="add")
	 * @param UserCartStoreRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function store(UserCartStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="update")
	 * @param UserCartUpdateRequest $request
	 * @return string
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function update(UserCartUpdateRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->update($gpc);
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="delete")
	 * @param IdRequest $request
	 * @return string
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function delete(IdRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->delete($gpc);
		return $this->response->success($res);
	}




	/**
	 * @RequestMapping(path="flush")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function flush()
	{
		$this->logic->flush();
		return $this->response->success();
	}


	/**
	 * @RequestMapping(path="/user/goods_cart")
	 * @return string
	 */
	public function index()
	{
		$data = $this->logic->index([]);
		return $this->response->success($data);
	}
}
