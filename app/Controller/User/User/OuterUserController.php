<?php
declare(strict_types=1);

namespace App\Controller\User\User;

use App\Constants\ErrorCode;
use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Kernel\Common\Curl;
use App\Logic\User\UserImFavoriteLogic;
use App\Logic\User\UserLogic;
use App\Logic\User\UserPosterLogic;
use App\Logic\User\UserSignLogic;
use App\Model\Credit4;
use App\Model\Setting;
use App\Model\User;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\UpdateMudanCoinRequest;
use App\Request\User\SetUserInfoRequest;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller()
 */
class OuterUserController extends BaseController
{
	/**
	 * @RequestMapping(path="/user/insert/mudancoin")
	 * @return string
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function insertMudanCoin(UpdateMudanCoinRequest $request)
	{
        $gpc = $request->validated();
        $user = User::where('name', '=', $gpc['name'])->first();
        $old = $user->mudan_coin;
        $new = $gpc['mudan_coin'];
        User::where('name', '=', $gpc['name'])->update([
            'mudan_coin' => $user->mudan_coin + $gpc['mudan_coin']
        ]);
        $amount = $gpc['mudan_coin'];
        $ono = $this->generateRecordNo();
        $data = [
            'user_id'        => $user['id'],
            'foreign_id'     => $gpc['foreign_id'] ?? 0,
            'pay_time'       => $gpc['pay_time'] ?? date('Y-m-d H:i:s'),
            'original_amount'=> 0,
            'amount'         => $amount,
            'fee'            => 0,
            'record_no'      => $ono,
            'remark'         => '购买期权',
            'type'           => Credit4::TYPE_EARN_MD_COIN,
            'sub_type'       => $gpc['sub_type'] ?? '',
            'balance'        => $new,
        ];
        Credit4::create($data);
		return $this->response->success([
		    'status' => 'success'
        ]);
	}

    /**
     * @return string
     */
    public function generateRecordNo()
    {
        return 'C1'.time().mt_rand(100000, 999999);
    }
}
