<?php
declare(strict_types=1);

namespace App\Controller\User\User;

use App\Controller\BaseController;
use App\Logic\User\UserAddressLogic;
use App\Request\IdRequest;
use App\Request\User\UserAddressStoreRequest;
use App\Request\User\UserAddressUpdateRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller(prefix="user/address")
 * @Middleware(UserAuthMiddleware::class)
 */
class UserAddressController extends BaseController
{

	/**
	 * @Inject()
	 * @var UserAddressLogic
	 */
	private $logic;

	/**
	 * @RequestMapping(path="add")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @param UserAddressStoreRequest $request
	 * @return mixed
	 */
	public function store(UserAddressStoreRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="update")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @param UserAddressUpdateRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function update(UserAddressUpdateRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->update($gpc);
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="delete")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @param IdRequest $request
	 * @return string
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function delete(IdRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->delete($gpc);
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="list")
	 */
	public function getList()
	{
		$res = $this->logic->getList();
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="default_address")
	 * @return array
	 */
	public function getDefaultAddress()
	{
		$res = $this->logic->getDefaultAddress();
		return $this->response->success($res);
	}
}
