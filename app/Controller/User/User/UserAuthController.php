<?php

declare(strict_types=1);

namespace App\Controller\User\User;

use App\Controller\BaseController;
use App\Logic\System\SystemLogic;
use App\Logic\User\UserAuthLogic;
use App\Middleware\SubmitThrottleMiddleware;
use App\Middleware\UserAuthMiddleware;
use App\Model\Setting;
use App\Model\User;
use App\Request\User\ChangePasswordRequest;
use App\Request\User\ResetPasswordRequest;
use App\Request\User\UserAutoLoginRequest;
use App\Request\User\UserLoginRequest;
use App\Request\User\UserRegisterRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller
 */
class UserAuthController extends BaseController
{
    /**
     * @Inject
     * @var UserAuthLogic
     */
    protected $logic;

    /**
     * @RequestMapping(path="/user/login")
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function login(UserLoginRequest $request)
    {
        make(SystemLogic::class)->clearCache();
        $gpc = $request->validated();
        $res = $this->logic->login($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="/user/auto-login")
     * @param UserAutoLoginRequest $request
     * @return void
     */
    public function autoLogin(UserAutoLoginRequest $request)
    {
        make(SystemLogic::class)->clearCache();
        $setting = collect(json_decode(Setting::where('name', 'like', '%业务设置%')->first()->value));
        $enableLoginSetting = $setting->where('name', 'enable_login_api')->first();
        if(isset($enableLoginSetting) && !is_null($enableLoginSetting)) {
            if((int)$enableLoginSetting->value == 1) {
                $user = User::find($request->input('user_id'))->toArray();
                $res = $this->logic->autoLogin($user);
                return $this->response->success($res);
            }
        } else {
            return $this->response->fail([
                'message' => false
            ]);
        }
    }

    /**
     * @RequestMapping(path="/user/register")
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function register(UserRegisterRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->register($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="/user/change_password")
     * @Middleware(SubmitThrottleMiddleware::class)
     * @Middleware(UserAuthMiddleware::class)
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->changePassword($gpc);
        return $this->response->success();
    }

    /**
     * @RequestMapping(path="/user/reset_password")
     * @Middleware(SubmitThrottleMiddleware::class)
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->resetPassword($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="/user/logout")
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function logout()
    {
        $this->logic->logout();
        return $this->response->success();
    }
}
