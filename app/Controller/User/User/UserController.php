<?php
declare(strict_types=1);

namespace App\Controller\User\User;

use App\Constants\ErrorCode;
use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Kernel\Common\Curl;
use App\Logic\User\UserImFavoriteLogic;
use App\Logic\User\UserLogic;
use App\Logic\User\UserPosterLogic;
use App\Logic\User\UserSignLogic;
use App\Model\Setting;
use App\Model\User;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\UpdateMudanCoinRequest;
use App\Request\User\SetUserInfoRequest;
use App\Request\User\SellerReportRequest;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller()
 * @Middleware(UserAuthMiddleware::class)
 */
class UserController extends BaseController
{
	/**
	 * @Inject()
	 * @var UserLogic
	 */
	private $logic;

	/**
	 * @RequestMapping(path="/user/info")
	 * @return array
	 */
	public function info()
	{
		$res = $this->logic->getUserInfo();
		$res['credit1_enabled'] = (int)Setting::where('key', '=', 'credit1_enabled')->pluck('value')[0];
		$res['show_check_version'] = (int)Setting::where('key', '=', 'show_app_version')->pluck('value')[0];
        $res['level_name'] = self::getLevelName($res['level']);
		return $this->response->success($res);
	}

    public function getLevelName($level)
    {
        switch ($level) {
            case 0:
                $levelName = '会员';
                break;
            case 1:
                $levelName = 'VIP1';
                break;
            case 2:
                $levelName = 'VIP2';
                break;
            case 3:
                $levelName = 'VIP3';
                break;
            default:
                $levelName = '';
        }
        return $levelName;
    }


    /**
	 * @RequestMapping(path="/user/info/set")
	 * @param SetUserInfoRequest $request
	 * @return string
	 */
	public function setInfo(SetUserInfoRequest  $request)
	{
		$gpc = $request->validated();
		if ($gpc) {
			$this->logic->setInfo($gpc);
		}
		return $this->response->success();
	}

	/**
	 * @RequestMapping(path="/user_sign/sign")
	 * @return string
	 */
	public function sign()
	{
//		$message = make(UserSignLogic::class)->sign();
		return $this->response->success('签到额度已满！期待下个版本！');
	}

	/**
	 * @RequestMapping(path="/user/qrcode")
	 * @return string
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function getPoster()
	{
		$res = make(UserPosterLogic::class)->getPoster();
		return $this->response->success($res);
	}

	/**
	 * @RequestMapping(path="/user/report-seller")
	 * @return mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */

	public function reportSeller(SellerReportRequest $request) 
	{ 
		$gpc = $request->validated();
		
		$res= $this->logic->reportSeller($gpc);
		 
		return $this->response->success($res);
	}
}
