<?php
declare(strict_types=1);

namespace App\Controller\User\User;

use App\Controller\BaseController;
use App\Logic\User\UserImLogic;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use App\Middleware\SubmitThrottleMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @Controller()
 * @Middleware(UserAuthMiddleware::class)
 */
class UserImController extends BaseController
{

	/**
	 * @Inject()
	 * @var UserImLogic
	 */
	protected $logic;


	/**
	 * @RequestMapping(path="/user/im/token")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function getToken()
	{
		$res = $this->logic->getImToken();
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="/user/im/get_accid")
	 * @param RequestInterface $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function getImAccid(RequestInterface $request)
	{
		$res = $this->logic->getImAccid($request->input('name'));
		return $this->response->success($res);
	}


	/**
	 * @RequestMapping(path="/user/im/reset_token")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function resetToken()
	{
		$res = $this->logic->resetToken();
		return $this->response->success($res);
	}
}
