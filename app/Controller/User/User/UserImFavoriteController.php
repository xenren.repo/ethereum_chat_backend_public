<?php
declare(strict_types=1);

namespace App\Controller\User\User;

use App\Constants\ErrorCode;
use App\Controller\BaseController;
use App\Exception\BusinessException;
use App\Logic\User\UserImFavoriteLogic;
use App\Request\CommonDeleteRequest;
use App\Request\CommonIndexRequest;
use App\Request\User\UserImFavoriteRequest;
use App\Request\UuidDeleteRequest;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;
use App\Middleware\UserAuthMiddleware;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Middleware\SubmitThrottleMiddleware;

/**
 * @Controller(prefix="user/favorite")
 * @Middleware(UserAuthMiddleware::class)
 */
class UserImFavoriteController extends BaseController
{

	/**
	 * @Inject()
	 * @var UserImFavoriteLogic
	 */
	protected $logic;

	/**
	 * @RequestMapping(path="/user/favorite")
	 * @param CommonIndexRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function index(CommonIndexRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->index($gpc);
		return $this->response->success($res);
	}




	/**
	 * @RequestMapping(path="add")
	 * @Middleware(SubmitThrottleMiddleware::class)
	 * @param UserImFavoriteRequest $request
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function add(UserImFavoriteRequest $request)
	{
		$gpc = $request->validated();
		$res = $this->logic->store($gpc);
		return $this->response->success($res);
	}



    /**
     * @RequestMapping(path="delete")
     * @param CommonDeleteRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(CommonDeleteRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->delete($gpc);
        return $this->response->success($res);
    }

    /**
     * @RequestMapping(path="delete/uuid")
     * @param UuidDeleteRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function deleteByUuid(UuidDeleteRequest $request)
    {
        $gpc = $request->validated();
        $res = $this->logic->deleteByUuid($gpc);
        return $this->response->success($res);
    }

}
