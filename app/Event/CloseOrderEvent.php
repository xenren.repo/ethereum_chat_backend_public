<?php
namespace App\Event;

class CloseOrderEvent
{

	public $order;


	public function __construct($order)
	{
		$this->order = $order;
	}
}