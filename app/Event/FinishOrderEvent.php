<?php
namespace App\Event;

class FinishOrderEvent
{

	public $order;


	public function __construct($order)
	{
		$this->order = $order;
	}
}