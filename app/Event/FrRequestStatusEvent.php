<?php
namespace App\Event;

class FrRequestStatusEvent
{
    public $data;
    public $requestType;
    public $message;
    public $status;

	public const STATUS_FAILED_REQUEST = 0;
	public const STATUS_SUCCESS_REQUEST = 1;


	public const TYPE_SPEND_EVOUCHER = 1;


	public function __construct($data,$requestType,$message,$status = 0)
	{
		$this->data = $data;
		$this->requestType = $requestType;
		$this->message = $message;
		$this->status = $status;
	}
}