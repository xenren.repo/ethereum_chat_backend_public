<?php
namespace App\Event;

class LoginEvent
{

	public $user;
	public $type; // 用户类型 1:管理员 2:用户


	public function __construct($user,$type)
	{
		$this->user = $user;
		$this->type = $type;
	}
}