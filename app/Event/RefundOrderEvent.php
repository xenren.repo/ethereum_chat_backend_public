<?php
namespace App\Event;

class RefundOrderEvent
{

	public $order;

	public $order_refund;



	public function __construct($order,$order_refund)
	{
		$this->order = $order;
		$this->order_refund = $order_refund;
	}
}