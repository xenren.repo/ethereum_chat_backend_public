<?php


namespace App\Event;


class SpendingRewardCalculatorEvent
{
    public $user;
    public $order;
    public function __construct($user,$order)
    {
        $this->user = $user;
        $this->order = $order;
    }
}