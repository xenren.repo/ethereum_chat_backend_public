<?php


namespace App\Event;


class UserLoginEvent
{
    public $user;
    public $type;
    public $response;


    public function __construct($user,$type,$response = null)
    {
        $this->user = $user;
        $this->type = $type;
        $this->response = $response;
    }

}