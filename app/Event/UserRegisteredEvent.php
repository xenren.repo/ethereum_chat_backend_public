<?php
namespace App\Event;

class UserRegisteredEvent
{

	public $user;

	public $from;   // register 用户端自主注册   ,add  后台操作员添加

	public function __construct($user,$from = 'register')
	{
		$this->user = $user;
		$this->from = $from;
	}
}