<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Exception;

use App\Constants\ErrorCode;
use App\Kernel\Common\Common;
use Hyperf\Server\Exception\ServerException;
use Throwable;

class BusinessException extends ServerException
{
	/**
	 * BusinessException constructor.
	 * @param int $code
	 * @param string|null $message
	 * @param Throwable|null $previous
	 */
	public function __construct(int $code = -3, string $message = null, Throwable $previous = null)
    {
        if (is_null($message)) {
            $message = ErrorCode::getMessage($code);
	        $message = trans('messages.'.$message);
        }

        parent::__construct($message, $code, $previous);
    }
}
