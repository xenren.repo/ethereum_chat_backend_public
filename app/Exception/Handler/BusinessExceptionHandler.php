<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Exception\Handler;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Http\Response;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Database\Model\ModelNotFoundException;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\Validation\ValidationException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class BusinessExceptionHandler extends ExceptionHandler
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var StdoutLoggerInterface
     */
    protected $logger;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->response = $container->get(Response::class);
        $this->logger = $container->get(StdoutLoggerInterface::class);
    }

    public function handle(Throwable $throwable, ResponseInterface $response)
    {
    	$message = 'Server Error';

        if ($throwable instanceof BusinessException) {
	        $code = $throwable->getCode();
	        $message = $throwable->getMessage();
	        if ($code != -1) {
		        return $this->response->fail($code, $message);
	        }
        }

	    if ($throwable instanceof ModelNotFoundException) {
		    return $this->response->fail(ErrorCode::PARAMS_INVALID, '记录不存在');
	    }

	    if ($throwable instanceof ValidationException) {
		    $message = $throwable->validator->errors()->first();
		    return $this->response->fail(ErrorCode::PARAMS_INVALID, $message);
	    }


        $this->logger->error(format_throwable($throwable));

        return $this->response->fail(ErrorCode::SYSTEM, $message);
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }
}
