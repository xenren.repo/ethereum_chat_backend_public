<?php

declare(strict_types=1);

namespace App\Kernel\Auth;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Admin\AdminAuthLogic;
use App\Logic\Admin\AdminLogic;
use App\Model\Admin;
use App\Model\Model;
use Hyperf\Utils\Context;

class Auth
{
    public static function id()
    {
        $data = Context::get(AuthManager::CONTEXT_KEY);
        if (empty($data) || ! isset($data[config('jwt.sso_key')]) || empty($data[config('jwt.sso_key')])) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        }
        $info = explode('_', $data[config('jwt.sso_key')]);
        if (! isset($info[2]) || ! is_numeric($info[2])) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        }
        return $info[2];
    }

    public static function isLogin()
    {
        $data = Context::get(AuthManager::CONTEXT_KEY);
        if (empty($data) || ! isset($data[config('jwt.sso_key')]) || empty($data[config('jwt.sso_key')])) {
            return false;
        }
        $info = explode('_', $data[config('jwt.sso_key')]);
        if (! isset($info[2]) || ! is_numeric($info[2])) {
            return false;
        }
        return true;
    }

    public static function user(): Model
    {
        $type = self::parseUserType();
        $model = self::getUserModel($type);
        $user = $model->findFromCache(self::id());
        if (empty($user)) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        }
        return $user;
    }

    public static function parseUserType()
    {
        $data = Context::get(AuthManager::CONTEXT_KEY);
        if (empty($data) || ! isset($data['type']) || empty($data['type'])) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        }
        return $data['type'];
    }

    public static function getUserModel($type)
    {
        $model = config('auth.model');
        if (! isset($model[$type])) {
            throw new BusinessException(ErrorCode::SYSTEM, '用户模型错误');
        }
        return new $model[$type]();
    }

    public static function isAdmin(): bool
    {
        $type = self::parseUserType();
        return $type === AdminAuthLogic::AUTH_TYPE ? true : false;
    }

    public static function isSuperAdmin($user_id = 0): bool
    {
        if (empty($user_id)) {
            $user_id = self::id();
        }
        if (! self::isAdmin()) {
            return false;
        }
        return $user_id < 3;
    }

    public static function isSeller($user_id = 0): bool
    {
        if (empty($user_id)) {
            $user_id = self::id();
        }
        return cache()->remember('user_role_is_seller_' . $user_id, function () use ($user_id) {
            $user = Admin::find($user_id)->toArray();
            if ($user['group_id'] === AdminLogic::GROUP_ID_SELLER) {
                return true;
            }
            return false;
        });
    }
}
