<?php

declare(strict_types=1);

namespace App\Kernel\Auth;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use Carbon\Carbon;
use Exception;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;
use Phper666\JwtAuth\Jwt;
use Throwable;

class AuthManager
{
    const CONTEXT_KEY = 'auth_manager_jwt_token';

    /**
     * @Inject
     * @var Jwt
     */
    protected $jwt;

    /**
     * @param string $uid
     * @param string $type user 用户 ,admin 管理员
     */
    public function getToken($uid, string $type): array
    {
        if (! in_array($type, ['auth_user', 'auth_admin'])) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, 'token用户类型错误');
        }
        $uid = $type . '_' . $uid;
        $userData = ['uid' => $uid, 'type' => $type];
        $token = $this->jwt->getToken($userData);
        return [
            'token' => (string) $token,
            'expire' => Carbon::now()->addSeconds($this->jwt->getTTL())->timestamp,
        ];
    }

    /**
     * @param $type
     * @throws \Throwable
     * @return array
     */
    public function parseData($type)
    {
        try {
            $this->jwt->checkToken();
            $result = $this->jwt->getParserData();
        } catch (Exception $e) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        } catch (Throwable $e) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        }
        if ($result['type'] != $type) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        }
        $this->setUser($result);
        return $result;
    }

    public function logout(string $token = null)
    {
        try {
            $this->jwt->logout($token);
        } catch (Exception $e) {
        }
        return true;
    }

    public function getJwt()
    {
        return $this->jwt;
    }

    public function setUser($result)
    {
        Context::set(self::CONTEXT_KEY, $result);
    }

    public function internalLogin($user, $authType): void
    {
        $authInfo = $this->getToken($user['id'], $authType);
        try {
            $result = $this->jwt->getParserData($authInfo['token']);
        } catch (Exception $e) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        } catch (Throwable $e) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        }
        if ($result['type'] != $authType) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        }
        $this->setUser($result);
    }
}
