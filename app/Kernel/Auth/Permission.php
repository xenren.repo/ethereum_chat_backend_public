<?php

declare(strict_types=1);


namespace App\Kernel\Auth;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\System\UserPermissionLogic;


class Permission
{
	protected $user_id = '';


	/**
	 * 检测用户是否有某个节点权限
	 * @param $permission
	 * @param bool $throw_error 无权限时是否抛出错误
	 * @return bool
	 * @throws BusinessException
	 */
	public function check($permission, $throw_error = true)
	{
		$user_permissions = $this->getPermissions();
		$permission       = ltrim($permission, '/');
		$res              = in_array($permission, $user_permissions);
		if ($throw_error && !$res) {
			throw new BusinessException(ErrorCode::PERMISSION_DENY);
		}
		return $res;
	}


	/**
	 * 获取个人拥有的权限节点
	 * @return mixed
	 */
	public function getPermissions()
	{
		return make(UserPermissionLogic::class)->getAllNodePermissions();
	}


}