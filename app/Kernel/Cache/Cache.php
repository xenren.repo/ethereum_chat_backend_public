<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Kernel\Cache;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use Closure;
use Hyperf\Retry\Retry;


class Cache extends \Hyperf\Cache\Cache
{



	public function remember($key, Closure $callback, $ttl = null)
	{
		$value = $this->get($key);
		if ($value !== null) {
			return $value;
		}
		$locking_key = 'remember_locking_'.$key;
		$res = $this->setnx($locking_key,1);
		if ($res) {
			$this->expire($locking_key,10);
			$this->set($key,$value = $callback(),$ttl);
			$this->delete($locking_key);
			return $value;
		}
		$value = Retry::whenReturns('_retry')
		->max(5)
		->inSeconds(10)
		->sleep(300)
		->fallback(function(){
			throw new BusinessException(ErrorCode::VERSION_UPDATE);
		}) // fallback函数
		->call(function()use($locking_key,$key){
			$res = $this->get($key);
			if (!is_null($res)) {
				return $res;
			} else {
				return '_retry';
			}
		});
		return $value;
	}







}
