<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Kernel\Cache;

use Hyperf\Cache\Driver\RedisDriver;


class CustomRedisDriver extends RedisDriver
{

	public function getCacheKey($key)
	{
		return $this->prefix . $key;
	}

	public function expire( $key, $ttl )
	{
		return $this->redis->expire($this->getCacheKey($key),$ttl);
	}


	public function setnx(string $key, $value )
	{
		return $this->redis->setnx($this->getCacheKey($key),$value);
	}


	public function incr(string $key )
	{
		return $this->redis->incr($this->getCacheKey($key));
	}


	public function incrBy(string $key , $value)
	{
		return $this->redis->incrBy($this->getCacheKey($key), $value);
	}



	public function decr(string $key )
	{
		return $this->redis->decr($this->getCacheKey($key));
	}


	public function decrBy(string $key , $value)
	{
		return $this->redis->decrBy($this->getCacheKey($key), $value);
	}


	public function eval($script, $args = array(), $numKeys = 0)
	{
		return $this->redis->eval($script, $args, $numKeys);
	}
}
