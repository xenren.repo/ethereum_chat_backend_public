<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Kernel\Common;

use App\Logic\System\SettingLogic;
use Hyperf\Utils\Context;
use Psr\Http\Message\ServerRequestInterface;

class Common
{

	/**
	 * @return string
	 */
	public static function getRequestLang():string
	{
		$request = Context::get(ServerRequestInterface::class);
		$lang = $request->getHeader('lang');
		return $lang[0] ?? 'zh_CN';
	}


	public static function getSetting($key, $default = null)
	{
		return SettingLogic::getSetting($key,$default);
	}



	public static function host()
	{
		return env('HOST');
	}
}
