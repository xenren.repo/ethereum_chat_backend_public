<?php

namespace App\Kernel\Common;



use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use GuzzleHttp\Client;

/**
 * Trait HasHttpRequest.
 */
class Curl
{

	/**
	 * @param $endpoint
	 * @param array $query
	 * @param array $headers
	 * @return mixed|string
	 */
	public function get($endpoint, $query = [], $headers = [])
	{
		return $this->request('get', $endpoint, ['headers' => $headers, 'query' => $query,]);
	}


	/**
	 * @param $endpoint
	 * @param array $params
	 * @param array $headers
	 * @return mixed|string
	 */
	public function post($endpoint, $params = [], $headers = [])
	{
		return $this->request('post', $endpoint, ['headers' => $headers, 'form_params' => $params,]);
	}


	/**
	 * @param $endpoint
	 * @param array $params
	 * @param array $headers
	 * @return mixed|string
	 */
	public function jsonPost($endpoint, $params = [], $headers = [])
	{
		return $this->request('post', $endpoint, ['headers' => $headers, 'json' => $params,]);
	}

	/**
	 * Make a http request.
	 *
	 * @param string $method
	 * @param string $endpoint
	 * @param array $options http://docs.guzzlephp.org/en/latest/request-options.html
	 *
	 * @return mixed|string
	 */
	public function request($method, $endpoint, $options = [])
	{
		try {
			return $this->unwrapResponse($this->getHttpClient($this->getBaseOptions())->{$method}($endpoint, $options));
		} catch (\Exception $e) {
			logger()->warning($e->getMessage());
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'请求发生错误');
		}
	}

	/**
	 * Return base Guzzle options.
	 *
	 * @return array
	 */
	protected function getBaseOptions()
	{
		$options = ['timeout' => property_exists($this, 'timeout') ? $this->timeout : 10.0,];
		if (method_exists($this, 'getBaseUri') && $uri = $this->getBaseUri()) {
			$options['base_uri'] = $uri;
		}
		return $options;
	}

	/**
	 * Return http client.
	 *
	 * @param array $options
	 *
	 *
	 * @codeCoverageIgnore
	 * @return Client
	 */
	protected function getHttpClient(array $options = [])
	{
		return new Client($options);
	}

	/**
	 * @param $response
	 * @return mixed|string
	 */
	protected function unwrapResponse($response)
	{
		$contentType = $response->getHeaderLine('Content-Type');
		$contents    = $response->getBody()->getContents();
		if (false !== stripos($contentType, 'json') || false !== stripos($contentType, 'javascript') || false !== stripos($contentType, 'text/plain') || false !== stripos($contentType, 'text/html')) {
			return json_decode($contents, true, 512, JSON_BIGINT_AS_STRING);
		}
		elseif (false !== stripos($contentType, 'xml') || false !== stripos($contentType, 'text/xml')) {
			return json_decode(json_encode(simplexml_load_string($contents)), true);
		}
		return $response;
	}


}
