<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Kernel\Common;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use Hyperf\Utils\Context;
use Psr\Http\Message\ServerRequestInterface;

class Helper
{



	/**
	 * 增加字符串前后打*号处理
	 * @param $str
	 * @param int $forward
	 * @param int $last
	 * @return bool
	 */
	public static function maskStr($str,$forward = 2,$last = 2){
		//获取字符串长度
		$len = mb_strlen($str, 'utf-8');
		if($len< ($forward + $last)){
			return $str;
		}else{
			//mb_substr — 获取字符串的部分
			$firstStr = mb_substr($str, 0, $forward, 'utf-8');
			$lastStr = mb_substr($str, -$last, $last, 'utf-8');
			//str_repeat — 重复一个字符串
			return $len == ($forward + $last) ? $firstStr . str_repeat('*', mb_strlen($str, 'utf-8') - 1) : $firstStr . str_repeat("*", $len - $forward - $last) . $lastStr;
		}
	}


	public static  function isProd()
	{
		return env('APP_ENV') === 'prod';
	}



	public static  function isMobile(string $mobile)
	{
		$pattern = '/^1[0-9]{10}$/';
		$res = preg_match($pattern,$mobile);
		return $res;
	}


	/**
	 * 树形整理,子分类在父分类数组里
	 * @param $data
	 * @param int $pid
	 * @param int $level
	 * @param string $pid_key 父级键名
	 * @param string $level_key
	 * @return array
	 */
	public static function sortLevel($data, $pid = 0, $level = 1,$pid_key = 'pid',$level_key = 'level')
	{
		$tmp = array();
		foreach ($data as $v) {
			if ($v[$pid_key] == $pid) {
				$v[$level_key]    = $level;
				$v['children'] = self::sortLevel($data, $v['id'], $level + 1,$pid_key,$level_key);
				if (empty($v['children'])) {
					unset($v['children']);
				}
				$tmp[]         = $v;
			}
		}
		return $tmp;
	}


	/**
	 * 获取下级列表
	 * @param $data
	 * @param $id
	 * @param string $pid_field
	 * @param int $level 层级
	 * @param int $level_limit 获取层级
	 * @return array
	 */
	public static function getChildren($data, $id, $pid_field = 'pid',$level = 1,$level_limit = 100) {
		$arr = array();
		if ($level > $level_limit) {
			return $arr;
		}
		foreach ($data as $v) {
			if ($v[$pid_field] == $id) {
				$v['level'] = $level;
				$arr[]=$v;
				$arr=array_merge($arr,self::getChildren($data,$v['id'],$pid_field,$level+1,$level_limit));
			}
		}
		return $arr;
	}


	/**
	 * 找父级列表
	 * @param $data
	 * @param $id
	 * @param string $pid_field
	 * @return array
	 */
	public static function  getParents($data, $id, $pid_field = 'pid') {
		$arr=array();
		foreach ($data  as $v) {
			if ($v['id']==$id) {
				$arr[]=$v;
				$arr=array_merge($arr,self::getParents($data,$v[$pid_field],$pid_field));
			}
		}
		return $arr;
	}


	/**
	 * @param $url
	 * @return mixed
	 */
	public static function parseTaobaoId($url)
	{
		if (strpos($url, 'taobao.') === false && strpos($url, 'tmall.') === false) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'请输入正确的淘宝地址');
		}
		$pattern = '/item_id=(\d*)&?/';
		preg_match($pattern, $url, $gpc);
		if (empty($gpc)) {
			$pattern = '/[\?|&]id=(\d*)&?/';
			preg_match($pattern, $url, $gpc);
		}
		if (empty($gpc) || empty($gpc[1])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'无法识别该链接');
		}
		return $gpc[1];
	}
}
