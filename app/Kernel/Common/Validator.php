<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Kernel\Common;

use Hyperf\Utils\Context;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\Validation\ValidationException;
use Psr\Http\Message\ServerRequestInterface;

class Validator
{

	static public function make(array $params,array $rules,array $message = [])
	{
		$validator = make(ValidatorFactoryInterface::class)->make(
			$params,$rules,$message
		);
		if ($validator->fails()){
			throw new ValidationException($validator);
		}
		return true;
	}



}
