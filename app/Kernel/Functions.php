<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use App\Kernel\Common\Validator;
use Hyperf\Amqp\Message\ProducerMessageInterface;
use Hyperf\Amqp\Producer;
use Hyperf\AsyncQueue\Driver\DriverFactory;
use Hyperf\AsyncQueue\JobInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\ExceptionHandler\Formatter\FormatterInterface;
use Hyperf\Utils\ApplicationContext;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\SimpleCache\CacheInterface;

if (! function_exists('di')) {
    /**
     * Finds an entry of the container by its identifier and returns it.
     * @param null|mixed $id
     * @return mixed|\Psr\Container\ContainerInterface
     */
    function di($id = null)
    {
        $container = ApplicationContext::getContainer();
        if ($id) {
            return $container->get($id);
        }

        return $container;
    }
}

if (! function_exists('format_throwable')) {
    /**
     * Format a throwable to string.
     */
    function format_throwable(Throwable $throwable): string
    {
        return di()->get(FormatterInterface::class)->format($throwable);
    }
}

if (! function_exists('queue_push')) {
    /**
     * Push a job to async queue.
     */
    function queue_push(JobInterface $job, int $delay = 0, string $key = 'default'): bool
    {
        $driver = di()->get(DriverFactory::class)->get($key);
        return $driver->push($job, $delay);
    }
}

if (! function_exists('amqp_produce')) {
    /**
     * Produce a amqp message.
     */
    function amqp_produce(ProducerMessageInterface $message): bool
    {
        return di()->get(Producer::class)->produce($message, true);
    }
}

if (! function_exists('logger')) {
	/**
	 * @return StdoutLoggerInterface
	 */
	function logger(): StdoutLoggerInterface
	{
		return di()->get(StdoutLoggerInterface::class);
	}
}


if (! function_exists('cache')) {
	/**
	 * @return CacheInterface
	 */
	function cache(): CacheInterface
	{
		return di()->get(CacheInterface::class);
	}
}


if (! function_exists('curl')) {
	/**
	 * @return CacheInterface
	 */
	function curl()
	{
		return make(\App\Kernel\Common\Curl::class);
	}
}



if (! function_exists('validate')) {
	/**
	 * @param array $params
	 * @param array $rules
	 * @param array $message
	 * @return bool
	 */
	function validate(array $params,array $rules,array $message = [])
	{
		return Validator::make($params,$rules,$message);
	}
}



if (! function_exists('dispatch')) {
	/**
	 * @param object $event
	 * @return bool
	 */
	function dispatch(object $event)
	{
		return make(EventDispatcherInterface::class)->dispatch($event);
	}
}


if (! function_exists('ip')) {
	function ip()
	{
		$server_params = make(\Hyperf\HttpServer\Contract\RequestInterface::class)->getServerParams();
		return $server_params['remote_addr'] ?? '';
	}
}
