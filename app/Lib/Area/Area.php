<?php

namespace App\Lib\Area;



class Area
{
	public static function getProvince()
	{
	}

	public static function getCityCode($province_code)
	{
	}

	public static function getAreaCodes($city_code)
	{
	}

	/**
	 * @return bool|mixed|string
	 */
	public static function getAllData()
	{
		return cache()->remember('area_all_list', function () {
			$res = file_get_contents(BASE_PATH.'/app/Lib/Area/list.json');
			return json_decode($res, true);
		},86400);
	}

	/**
	 * @param $area_code
	 * @return string
	 */
	public static function getNameByCode($area_code)
	{
		$data = self::getAllData();
		return $data[$area_code] ?? '';
	}

	public static function getAddressText($province_code, $city_code, $area_code)
	{
		return self::getNameByCode($province_code). self::getNameByCode($city_code).self::getNameByCode($area_code);
	}
}
