<?php
namespace App\Lib\Express;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Common\Helper;

class Express
{

	private $api_key;

	private $app_id;

	const URL = 'http://api.kdniao.com/Ebusiness/EbusinessOrderHandle.aspx';

	const DEV_URL = 'http://api.kdniao.com/Ebusiness/EbusinessOrderHandle.aspx';

	private $url = '';

	public $data_type = 2; // 返回数据格式  (json)

	public function __construct()
	{
		$this->app_id = env('EXPRESS_ID');
		$this->api_key = env('EXPRESS_KEY');
		if (Helper::isProd()) {
			$this->url = self::URL;
		} else {
			$this->url = self::DEV_URL;
		}
	}

	/**
	 * @param $gpc
	 * @return mixed
	 */
	public function getTrace($gpc)
	{
		$rules = [
			'express_id' => 'required|string|max:20',
			'express_sn' => 'required|string|max:255',
		];
		validate($gpc, $rules);

		$express_info = [
			'CustomerName' => $gpc['custom_code'] ?? '',
			'LogisticCode' => $gpc['express_sn'],
			'ShipperCode' => $gpc['express_id'],
		];

		array_filter($express_info);
		$express_info['OrderCode'] = '';
		$express_info = json_encode($express_info);

		$data['RequestType'] = 8001;   // 即时查询码
		$data['DataType'] = $this->data_type;
		$data['RequestData'] = 2;
		$data['EBusinessID'] = $this->app_id;
		$data['DataSign'] = $this->sign($express_info);
		$data['RequestData'] = $express_info;

		$data = $this->go($this->url, $data);
		return $data['Traces'] ?? [];
	}

	/**
	 * @param string $gpc
	 * @return string
	 */
	public function sign(string $gpc)
	{
		return urlencode(base64_encode(md5($gpc.$this->api_key)));
	}


	/**
	 * @param $url
	 * @param $gpc
	 * @return mixed
	 */
	public function go($url, $gpc)
	{
		$res = curl()->post($url, $gpc);
		if (isset($res['Success']) && $res['Success']) {
			return $res;
		} else {
			logger()->error($res);
			throw new BusinessException(ErrorCode::SYSTEM,'请求'.$url.'出现错误');
		}
	}
}
