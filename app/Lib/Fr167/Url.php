<?php
declare(strict_types=1);

namespace App\Lib\Fr167;


class Url
{
    protected $host;

    const ENV_LOCAL = 'local';
    const ENV_PROD = 'prod';

    const URL_EXCHANGE_RMB = '/api/ecomm/gainrmb';


    public function __construct()
    {
        $this->resolveHost();
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return mixed
     */
    public static function getUrl($url)
    {
        return sprintf("%s%s", (new Url)->resolveHost(), $url);
    }

    private function resolveHost()
    {
        return env('REMOTE_SYSTEM_URL','https://fr167.com');
    }
}