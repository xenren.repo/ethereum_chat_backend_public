<?php

namespace App\Lib\Goods;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;

class DingDanXia
{

	private $api_key;

	public function __construct()
	{
		$this->api_key = env('DINGDANXIA_KEY');
	}

	/**
	 * @param $item_id
	 * @return mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function goodsDetail($item_id)
	{
		$data = $this->goodsDetailV2($item_id);
		return $data ?? [];
	}


	/**
	 * @param $item_id
	 * @return mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function goodsDetailV2($item_id)
	{
		if (empty($item_id)) {
			throw new BusinessException();
		}
		$url  = 'http://api.tbk.dingdanxia.com/shop/good_info?apikey=' . $this->api_key . '&id=' . $item_id;
		$data = [
			'apikey' => $this->api_key,
			'id'     => $item_id,
			'info'   => 'all'
		];
		$data = $this->go($url, $data);
		return $data['data'] ?? [];
	}

	/**
	 * @param $url
	 * @param array $data
	 * @return mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function go($url, $data = [])
	{
		if ($data) {
			$res = curl()->post($url, $data);
		} else {
			$res = curl()->get($url);
		}
		if (empty($res) || !isset($res['code']) || $res['code'] != 200) {
			logger()->error(json_encode($res));
			throw new BusinessException(ErrorCode::SYSTEM,'请求' . $url . '出现错误');
		}
		return $res['data'];
	}

	public function parsePriceInfo(array $goods_info)
	{
		if (!isset($goods_info['apiStack'][0]['value'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'价格数据解析错误');
		}
		return json_decode($goods_info['apiStack'][0]['value'],true);
	}


	public function parseSkuInfo(array $goods_info)
	{
		if (!isset($goods_info['mockData'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'规格数据包解析错误');
		}
		return json_decode($goods_info['mockData'],true);
	}

	public function getGoodsPrice(array $goods_info)
	{
		$price_info = $this->parsePriceInfo($goods_info);
		if (!isset($price_info['price']['price']['priceText'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'价格解析错误');
		}
		return $price_info['price']['price']['priceText'];
	}

	public function getGoodsTitle(array $goods_info)
	{
		if (!isset($goods_info['item']['title'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'标题解析错误');
		}
		return $goods_info['item']['title'];
	}

	public function getGoodsMobileContent(array $goods_info)
	{
		$content = '';
		$goods_info['item_info']['pics']['string'] = $this->getGoodsImages($goods_info);
		foreach ($goods_info['item_info']['pics']['string'] as $v) {
			$content .= '<p><img src="' . $v . '" style="max-width:100%;"><br></p>';
		}
		return $content;
	}

	/**
	 * @param array $goods_info
	 * @return mixed
	 */
	public function getGoodsImages(array $goods_info)
	{
		if (!isset($goods_info['item']['images'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'图片解析错误');
		}
		if (!is_array($goods_info['item']['images'])) {
			$goods_info['item']['images'] = [$goods_info['item']['images']];
		}
		if (count($goods_info['item']['images']) > 3) {
			array_shift($goods_info['item']['images']);
			array_shift($goods_info['item']['images']);
		}
		return $goods_info['item']['images'];
	}

	public function getOptionInfo(array $goods_info)
	{
		$mock_price_info = $this->parseSkuInfo($goods_info);
		$sku_price_info = $this->parsePriceInfo($goods_info);
		if (!isset($mock_price_info['feature']['hasSku']) || !$mock_price_info['feature']['hasSku']) {
			return [];
		}
		if (!isset($goods_info['skuBase']['props'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'规格解析错误');
		}
		if (!isset($goods_info['skuBase']['props'][0])) {
			$goods_info['skuBase']['props'] = [$goods_info['skuBase']['props']];
		}
		$option['spec'] = [];
		$id_name        = [];  // 规格项 与规格子项id 转换为名称
		$option_images  = [];  // 规格子项 id转换为 图片
		foreach ($goods_info['skuBase']['props'] as $sku_prop) {
			if (!isset($sku_prop['values'])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'规格子项解析错误');
			}
			// 单规格与多规格返回字段格式不一致
			if (!isset($sku_prop['values'][0])) {
				$sku_prop['values'] = [$sku_prop['values']];
			}
			$spec_item = [];
			foreach ($sku_prop['values'] as $v) {
				$spec_item[]             = [
					'title'    => $v['name'],
					'value_id' => $v['vid'],
					'img_url'  => $v['img_url'] ?? '',
					'sort'     => 0
				];
				$id_name[$v['vid']] = $v['name'];
				if (isset($v['img_url']) && $v['img_url']) {
					$option_images[$v['vid']] = $v['img_url'];
				}
			}
			$spec                          = [
				'title'     => $sku_prop['name'],
				'value_id'  => $sku_prop['pid'],
				'sort'      => 0,
				'spec_item' => $spec_item
			];
			$option['spec'][]              = $spec;
			$id_name[$sku_prop['pid']] = $sku_prop['name'];
		}

		$option['options'] = [];

		if (!isset($goods_info['skuBase']['skus']) || !is_array($goods_info['skuBase']['skus'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'规格集合解析错误');
		}

		if (!isset($goods_info['skuBase']['skus'][0])) {
			$goods_info['skuBase']['skus'] = [$goods_info['skuBase']['skus']];
		}

		$stock_info = [];
		if (!isset($sku_price_info['skuCore']['sku2info']) || !is_array($sku_price_info['skuCore']['sku2info'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'规格库存,价格解析错误');
		}

		if (!isset($sku_price_info['skuCore']['sku2info'][0])) {
			$sku_price_info['skuCore']['sku2info'] = [$sku_price_info['skuCore']['sku2info']];
		}

		foreach ($sku_price_info['skuCore']['sku2info'] as $key => $stock) {
			if ($key == 0) {
				continue;
			}
			$stock_info[$key] = $stock['quantity'];
		}

		$price_info = [];

		foreach ($sku_price_info['skuCore']['sku2info'] as $key => $price_item) {
			if ($key == 0) {
				continue;
			}
			$price_info[$key] = $price_item['price']['priceText'];
		}

		foreach ($goods_info['skuBase']['skus'] as $item) {
			$item['pv'] = explode(';', $item['propPath']);
			$specs      = [];
			$title      = '';
			$images     = '';
			foreach ($item['pv'] as $pv) {
				$pv      = explode(':', $pv);
				$specs[] = [
					'key'  => $id_name[$pv[0]] ?? '',
					'name' => $id_name[$pv[1]] ?? '',
				];
				$title   .= '_' . $id_name[$pv[1]] ?? '';

				if (empty($images) && isset($option_images[$pv[1]])) {
					$images = $option_images[$pv[1]];
				}
			}

			$title = trim($title, '_');

			$temp                   = [
				'title'    => $title,
				'goods_sn' => $item['skuId'],
				'sort'     => 0,
				'specs'    => $specs,
				'stock'    => $stock_info[$item['skuId']],
				'price'    => $price_info[$item['skuId']],
				'images'   => $images
			];
			$temp['cost_price']     = $temp['price'];
			$temp['original_price'] = $temp['price'];
			$option['options'][]    = $temp;
		}
		return $option;
	}

	/**
	 * @param $goods_info
	 * @return mixed
	 */
	public function getGoodsStock($goods_info)
	{
		//        if (!isset($goods_info['stock_info']['item_quantity'])) {
		//            throw new ParamsException('产品库存解析错误');
		//        }
		return 1000;
	}
}
