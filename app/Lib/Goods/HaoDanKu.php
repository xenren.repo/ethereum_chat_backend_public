<?php
namespace App\Lib\Goods;



use App\Constants\ErrorCode;
use App\Exception\BusinessException;

class HaoDanKu
{

	private $api_key;

	public function __construct()
	{
		$this->api_key = env('HAOKUDAN_KEY');
	}

	/**
	 * @return mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function getCategories()
	{
		$url = 'http://v2.api.haodanku.com/super_classify/apikey/'.$this->api_key;
		$data = $this->go($url);
		return $data['general_classify'] ?? [];
	}

	/**
	 * @param $name
	 * @param int $page
	 * @param int $limit
	 * @param array $search
	 * @return array
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function search($name, $page = 1, $limit = 20, $search = [])
	{
		$name = urlencode(urlencode($name));
		if (empty($search['sort'])) {
			$search['sort'] = 0;
		}

		$url = "http://v2.api.haodanku.com/get_keyword_items/apikey/{$this->api_key}/keyword/{$name}/back/{$limit}/sort/{$search['sort']}/min_id/{$page}";

		if (isset($search['price_min'])) {
			$url .= '/price_min/'.$search['price_min'];
		}

		if (isset($search['price_max'])) {
			$url .= '/price_max/'.$search['price_max'];
		}

		if (!empty($search['type'])) {
			$url .= '/type/'.$search['type'];
		}

		if (!empty($search['cid'])) {
			$url .= '/cid/'.$search['cid'];
		} else {
			$url .= '/cid/0';
		}

		$data = $this->go($url);
		return $data;
	}

	/**
	 * @param $item_id
	 * @return mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function detail($item_id)
	{
		$url = 'http://v2.api.haodanku.com/item_detail/apikey/'.$this->api_key.'/itemid/'.$item_id;
		$data = $this->go($url);
		return $data['data'] ?? [];
	}

	/**
	 * @param $url
	 * @return mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function go($url)
	{
		$res = curl()->get($url);
		if (empty($res) || !isset($res['code'])) {
			logger()->error(json_encode($res));
			throw new BusinessException(ErrorCode::SYSTEM,'请求'.$url.'出现错误');
		}
		return $res;
	}

	/**
	 * @return mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function todayRecommend()
	{
		$url = 'http://v2.api.haodanku.com/get_deserve_item/apikey/'.$this->api_key;

		return $this->go($url);
	}

	/**
	 * @param int $min_id
	 * @return array
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function getGoods($min_id = 1)
	{
		$page_size = 200;
		$res['min_id'] = 1;
		$data = [];
		$time =  0;
		do {
			$url = "http://v2.api.haodanku.com/itemlist/apikey/{$this->api_key}/nav/3/cid/0/back/{$page_size}/min_id/{$res['min_id']}";
			$res = $this->go($url);
			if ($res['code'] == 1 && isset($res['data'])) {
				$data = array_merge($data, $res['data']);
			} else {
				logger()->error(json_encode($res));
				logger()->error('拉取全量产品出现错误');
				break;
			}
			$time++;
		} while ($res['min_id'] != 0 && $time < 10);

		return [
			'data' => $data,
			'min_id' => $res['min_id']
		];
	}

	/**
	 * @return array|mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function getDisabledGoods()
	{
		$hour = date('H') + 0;
		if ($hour == 0) {
			$start = $hour;
			$end = 1;
		} else {
			$end = $start = $hour -1;
		}
		$url = "http://v2.api.haodanku.com/get_down_items/apikey/{$this->api_key}/start/{$start}/end/{$end}";
		$res = $this->go($url);
		return $res['data'] ?? [];
	}


	public function getUpdateGoods($min_id = 1,$limit = 1000)
	{
		$url = "http://v2.api.haodanku.com/update_item/apikey/{$this->api_key}/sort/1/back/{$limit}/min_id/{$min_id}";
		$res = $this->go($url);
		return $res ?? [];
	}
}
