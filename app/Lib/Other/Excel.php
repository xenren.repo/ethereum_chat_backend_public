<?php
namespace App\Lib\Other;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Http\Response;
use Hyperf\HttpMessage\Stream\SwooleStream;

class Excel
{


	/**
	 * @param $filename
	 * @param array $header
	 * @param array $data
	 * @return null|\Psr\Http\Message\ResponseInterface
	 */
	public function csvOutput($filename, array $header, array $data)
	{
		$str = '';
		foreach ($header as $v) {
			$str .= iconv('utf-8', 'gbk', $v) .',';
		}
		$str .= "\n";
		foreach ($data as $vo) {
			if (is_array($vo)) {
				foreach ($vo as $item) {
					$str .= iconv('utf-8', 'gbk', $item) .',';
				}
			}
			$str .= "\n";
		}
		return make(Response::class)->response()->withHeader('Content-Type', 'text/csv;charset=utf8')
			->withHeader('Content-Disposition', 'attachment;filename="'.$filename.date('Y-m-d').'".csv')
			->withHeader('Cache-Control', 'max-age=0')
			->withBody(new SwooleStream($str));
	}
}
