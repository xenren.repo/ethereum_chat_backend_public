<?php
namespace App\Lib\Other;

use App\Model\User;
use Grafika\Grafika;

/**
 * 图片处理库
 */
class Images
{

	/**
	 * @param string $qrcode
	 * @param int $user_id 用户ID
	 * @return string
	 * @throws \Exception
	 */
	public function blend( $qrcode , $user_id )
	{
		$imageName = 'poster_v1_'.$user_id.'.jpg';
		$path = $this->getBasePath().'/'.$imageName;
		if (!file_exists($path)) {
			$editor = Grafika::createEditor();
			$background = $this->getBackground();
			$editor->open($image1 , $background);
			$editor->open($image2 , $qrcode);
			$editor->blend ( $image1, $image2 , 'normal', 0.9, 'center',0,-80);
			$user = User::findFromCache($user_id);
			$editor->text($image1,$user['old_id'], '20',185,375);
			$editor->save($image1,$path);
		}
		return $imageName;
	}

	/**
	 * @return string
	 */
	public function getBasePath()
	{
		$path = BASE_PATH.'/public/poster';
		if (!file_exists($path)) {
			mkdir($path,0755,true);
		}
		return $path;
	}

	/**
	 * 获取背景图片地址
	 * @return string
	 */
	public function getBackground()
	{
		return BASE_PATH.'/resources/images/bg.png';
	}

}