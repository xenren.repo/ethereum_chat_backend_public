<?php

declare(strict_types=1);

namespace App\Lib\Payment\WeChatPay;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;

class Config
{
    protected $options;

    protected $mchId;

    protected $appId;

    protected $appSecret;

    protected $apiKey;

    protected $notifyUrl;

    protected $refundNotifyUrl;

    public function setOptions(?array $options = null)
    {
        $this->getDefaultOptions();
        if (! is_null($options)) {
            foreach ($options as $key => $value) {
                $this->options[$key] = $value;
            }
        }
        return $this;
    }

    public function getOptions()
    {
        $this->checkRequirements();
        return $this->options;
    }

    public function checkRequirements()
    {
        $requiredProps = ['mchId', 'appId', 'appSecret', 'apiKey', 'notifyUrl', 'refundNotifyUrl'];
        foreach ($requiredProps as $item) {
            if (is_null($this->{$item}) || empty($this->{$item})) {
                logger()->error("Missing `{$item}` value, please check again");
                throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Problem occurs, please contact administrator');
            }
        }
    }

    public function getDefaultOptions(): void
    {
        $basePath = BASE_PATH . '/config/cert';
        $this->options = [
            'mch_id' => $this->mchId, //商户号
            'app_id' => $this->appId, //APPID
            'app_secret' => $this->appSecret, //App Secret
            'api_key' => $this->apiKey, //支付密钥
            // 'ssl_cert_path' => $basePath.'/wechatpay_apiclient_cert.pem',
            // 'ssl_key_path' => $basePath.'/wechat_pay_apiclient_key.pem',
            'sign_type' => 'MD5',
            'notify_url' => $this->notifyUrl,
            'refund_notify_url' => $this->refundNotifyUrl,
            // 'h5_scene_info'     => [//required in H5
            //     'h5_info' => ['type' => 'Wap', 'wap_url' => 'http://wapurl', 'wap_name' => 'wapname']
            // ],
            // 'rsa_pubkey_path'   => __DIR__ .'/../cert/pubkey.pem',
            // 'jsapi_ticket'      => __DIR__ .'/jsapi_ticket.json' //jsticket的临时存放路径
        ];
    }

    /**
     * Get the value of appId.
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * Set the value of appId.
     *
     * @param mixed $appId
     * @return self
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
        $this->setOptions(['appId' => $appId]);
        return $this;
    }

    /**
     * Get the value of mchId.
     */
    public function getMchId()
    {
        return $this->mchId;
    }

    /**
     * Set the value of mchId.
     *
     * @param mixed $mchId
     * @return self
     */
    public function setMchId($mchId)
    {
        $this->mchId = $mchId;
        $this->setOptions(['mchId' => $mchId]);
        return $this;
    }

    /**
     * Get the value of appSecret.
     */
    public function getAppSecret()
    {
        return $this->appSecret;
    }

    /**
     * Set the value of appSecret.
     *
     * @param mixed $appSecret
     * @return self
     */
    public function setAppSecret($appSecret)
    {
        $this->appSecret = $appSecret;
        $this->setOptions(['appSecret' => $appSecret]);
        return $this;
    }

    /**
     * Get the value of apiKey.
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set the value of apiKey.
     *
     * @param mixed $apiKey
     * @return self
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        $this->setOptions(['apiKey' => $apiKey]);
        return $this;
    }

    /**
     * Get the value of notifyUrl.
     */
    public function getNotifyUrl()
    {
        return $this->notifyUrl;
    }

    /**
     * Set the value of notifyUrl.
     *
     * @param mixed $notifyUrl
     * @return self
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
        $this->setOptions(['notifyUrl' => $notifyUrl]);
        return $this;
    }

    /**
     * Get the value of refundNotifyUrl.
     */
    public function getRefundNotifyUrl()
    {
        return $this->refundNotifyUrl;
    }

    /**
     * Set the value of refundNotifyUrl.
     *
     * @param mixed $refundNotifyUrl
     * @return self
     */
    public function setRefundNotifyUrl($refundNotifyUrl)
    {
        $this->refundNotifyUrl = $refundNotifyUrl;
        $this->setOptions(['refundNotifyUrl' => $refundNotifyUrl]);
        return $this;
    }
}
