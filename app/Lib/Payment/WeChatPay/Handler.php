<?php

declare(strict_types=1);

namespace App\Lib\Payment\WeChatPay;

use App\Kernel\Common\Helper;
use zhangv\wechat\pay\WechatPay;

class Handler extends WechatPay
{
    public $sandbox = false;

    public function __construct(array $config = [])
    {
        if (Helper::isProd()) {
            $this->sandbox = false;
        }
        parent::__construct($config);
    }

    public function arrayToXml($array)
    {
        $xml = '<xml>' . PHP_EOL;
        foreach ($array as $k => $v) {
            if ($v && trim($v) != '') {
                $xml .= "<{$k}><![CDATA[{$v}]]></{$k}>" . PHP_EOL;
            }
        }
        $xml .= '</xml>';
        return $xml;
    }

    public function xmlToarray($xml): array
    {
        $array = [];
        $tmp = (array) simplexml_load_string($xml);
        foreach ($tmp as $k => $v) {
            $array[$k] = (string) $v;
        }
        return $array;
    }
}
