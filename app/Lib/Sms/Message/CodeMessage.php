<?php
declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Lib\Sms\Message;

use App\Exception\BusinessException;
use App\Kernel\Common\Helper;
use App\Lib\Sms\Sms;
use Overtrue\EasySms\Gateways\ChuanglanGateway;
use Overtrue\EasySms\Message;
use Overtrue\EasySms\Contracts\GatewayInterface;

class CodeMessage extends Message
{
	const EXPIRE = 300;

	protected $type;
	protected $mobile;

	public function __construct($type,$mobile)
	{
		$this->type = $type;
		$this->mobile = $mobile;
	}

	// 定义直接使用内容发送平台的内容
	public function getContent(GatewayInterface $gateway = null)
	{
		if (empty($gateway)) {
			throw new BusinessException();
		}
		if ($gateway instanceof ChuanglanGateway) {
			$data = $this->getData($gateway);
			return '尊敬的用户，您本次的验证码为'.$data['code'].',有效期'.$data['expire'].'分钟。';
		}
		return '';
	}

	// 定义使用模板发送方式平台所需要的模板 ID
	public function getTemplate(GatewayInterface $gateway = null)
	{
		return '';
	}

	// 模板参数
	public function getData(GatewayInterface $gateway = null)
	{
		$key = Sms::getCacheKey($this->type,$this->mobile);
		if (Helper::isProd()) {
			$code = mt_rand(100000,999999).'';
		} else {
			$code = '111111';
		}
		cache()->set($key,$code,self::EXPIRE);
		if ($gateway instanceof ChuanglanGateway) {
			return [
				'code'   => $code,
				'expire' => ceil(self::EXPIRE/60)
			];
		}
	}
}
