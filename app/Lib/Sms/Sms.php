<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Lib\Sms;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;

class Sms
{

	/**
	 * @param string $code
	 * @param string $type
	 * @param string $mobile
	 * @return bool
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public static function check(string $code,string $type,string $mobile):bool
	{
		$key = self::getCacheKey($type,$mobile);
		$verify_code = cache()->get($key);
		if (empty($verify_code) || $verify_code != $code) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'验证码错误');
		}
		return true;
	}


	public static function getCacheKey(string $type, string $mobile)
	{
		return 'sms_'.$mobile.'_'.$type.'_code';
	}


	/**
	 * @param string $type
	 * @param string $mobile
	 * @return bool
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public static function clear(string $type,string $mobile):bool
	{
		$key = self::getCacheKey($type,$mobile);
		cache()->delete($key);
		return true;
	}

}
