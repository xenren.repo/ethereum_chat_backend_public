<?php

declare(strict_types=1);

namespace App\Listener;

use App\Event\LoginEvent;
use App\Logic\Admin\AdminAuthLogic;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
class AdminOnlineListener implements ListenerInterface
{
    public function listen(): array
    {
        return [
            LoginEvent::class,
        ];
    }

    public function process(object $event)
    {
        $user = $event->user;
        $type = $event->type;
        if ($type === AdminAuthLogic::ADMIN_TYPE) {
            make(AdminAuthLogic::class)->adminIsOnline($user['id']);
        }
    }
}
