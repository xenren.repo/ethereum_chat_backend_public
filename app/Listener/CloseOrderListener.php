<?php
declare(strict_types=1);

namespace App\Listener;


use App\Event\CloseOrderEvent;
use App\Event\PayOrderEvent;
use App\Logic\Order\OrderLogic;
use App\Service\PayOrderService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener()
 */
class CloseOrderListener implements ListenerInterface
{
	/**
	 * @Inject
	 * @var PayOrderService
	 */
	protected $service;

	public function listen(): array
	{
		return [
			CloseOrderEvent::class,
		];
	}

	/**
	 * @param object $event
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function process(object $event)
	{
		$order = $event->order;
		make(OrderLogic::class)->recoverGoodsStock($order['id']);
	}
}
