<?php
declare(strict_types=1);

namespace App\Listener;


use App\Event\FinishOrderEvent;
use App\Logic\Order\OrderLogic;
use App\Service\PayOrderService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener()
 */
class FinishOrderListener implements ListenerInterface
{
	/**
	 * @Inject
	 * @var PayOrderService
	 */
	protected $service;

	public function listen(): array
	{
		return [
			FinishOrderEvent::class,
		];
	}

	public function process(object $event)
	{
		$order = $event->order;

		make(OrderLogic::class)->finishOrder($order);
	}
}
