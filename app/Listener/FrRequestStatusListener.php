<?php
declare(strict_types=1);

namespace App\Listener;


use App\Event\FrRequestStatusEvent;
use App\Logic\System\LoginLogLogic;
use App\Model\FrRequestStatus;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener()
 */
class FrRequestStatusListener implements ListenerInterface
{


	public function listen(): array
	{
		return [
			FrRequestStatusEvent::class,
		];
	}

	/**
	 * @param object $event
	 */
	public function process(object $event)
	{
        if($event instanceof FrRequestStatusEvent)
        {
            $data = $event->data;
            $requestType = $event->requestType;
            $message = $event->message;
            $status = $event->status;

            if ($status === FrRequestStatusEvent::STATUS_FAILED_REQUEST) 
            {
                FrRequestStatus::insert([
                    'uniqueid'  => $data['uniqueid'],
                    'type'      => $requestType,
                    'message'   => $message,
                    'status' => $status,
                    'url'       => $data['request']['url'],
                    'request_data' => $data['request']['data'],
                    'original_object' => $data['original_object'],
                    'error_line' => $data['error']['line']??'',
                    'error_message' => $data['error']['message']??'',
                    'error_raw' => $data['error']['raw']??'',
                    'response_data' => $data['response']['data']??'',
                ]);
            }

        }
	}
}
