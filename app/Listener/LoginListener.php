<?php

declare(strict_types=1);

namespace App\Listener;

use App\Event\LoginEvent;
use App\Logic\System\LoginLogLogic;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
class LoginListener implements ListenerInterface
{
    public function listen(): array
    {
        return [
            LoginEvent::class,
        ];
    }

    public function process(object $event)
    {
        $user = $event->user;
        $type = $event->type;
        $info = [
            'user_id' => $user['id'],
            'type' => $type,
            'ip' => ip2long(ip()),
            'remark' => '',
        ];
        make(LoginLogLogic::class)->log($info);
    }
}
