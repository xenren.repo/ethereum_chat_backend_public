<?php
declare(strict_types=1);

namespace App\Listener;


use App\Event\PayOrderEvent;
use App\Logic\Order\OrderLogic;
use App\Service\PayOrderService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener()
 */
class PayOrderListener implements ListenerInterface
{
	/**
	 * @Inject
	 * @var PayOrderService
	 */
	protected $service;

	public function listen(): array
	{
		return [
			PayOrderEvent::class,
		];
	}

	public function process(object $event)
	{
		$order = $event->order;
		if ($order['type'] !=  OrderLogic::SPECIAL_TYPE) {
			return true;
		}
		$this->service->notify($order);

	}
}
