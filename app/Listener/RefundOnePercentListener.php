<?php
declare(strict_types=1);

namespace App\Listener;


use App\Event\FinishOrderEvent;
use App\Event\RefundOrderEvent;
use App\Logic\Order\OrderRefundLogic;
use App\Logic\Order\PayLogic;
use App\Logic\Finance\Credit3Logic;
use App\Service\PayOrderService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\DbConnection\Db;

use App\Model\DataDictionary;
use App\Model\GoodsCategory;
use App\Model\User;
use App\Model\Credit3;
use App\Model\MulGoodsCategory;
use App\Model\OrderGoods;


/**
 * @Listener()
 */
class RefundOnePercentListener implements ListenerInterface
{
    const CATEGORY_ID = 408;

	protected $order;

	public function listen(): array
	{
		return [
			RefundOrderEvent::class,
		];
	}

	public function process(object $event)
	{
		logger()->error("[CALL REFUND ONE LISTENER]");
		$order = $event->order;
		$order_refund = $event->order_refund;

		$this->order = $event->order;
		$this->user = User::find($this->order['user_id']);
		logger()->error($this->user['name']);
		$this->refundOnePercent();
	}
	public function refundOnePercent()
	{
		logger()->error("[CALL REFUND ONE LISTENER 22]");
		$order = $this->order;
		$user = $this->user;


		$orderGoods = OrderGoods::where('order_id', $order['id'])->get();
        $goodsIds = $orderGoods->pluck('goods_id')->toArray();
        $pinpaiId = $this->getPinpaiId();
        $cat = MulGoodsCategory::where([
            'cid' => $pinpaiId
        ])->whereIn('goods_id',$goodsIds)->get()->pluck('goods_id')->toArray();
        $totalPinpai = 0;
        foreach ($orderGoods as $k => $v)
        {
            if (in_array($v['goods_id'],$cat))
            {
                $totalPinpai += $v['num']*$v['price'];
            }
		}
		logger()->error("TOTAL PINPAI = ".$totalPinpai);
		
        if ($totalPinpai > 0)
        {
            $this->refundCredit3($user['name'],$totalPinpai);
        }
	}
	public function getPinpaiId()
	{
        $dataDictionary = DataDictionary::where([
            'module' => 'goods',
            'key_name' => 'category',
            'key_name' => 'pinpai'
        ])->first();
        if ($dataDictionary)
        {
            $dataDictionary = $dataDictionary->toArray();
            return $dataDictionary['key_id'];
        } else
        {
            $goodCaategory = GoodsCategory::where('id',self::CATEGORY_ID)->first()->toArray();
            return $goodCaategory['id'];
        }
	}
	public function refundCredit3($username,$totalPinpai)
	{
		logger()->error("CALL refundCredit");
        $url = 'https://fr167.com/api/spending-reward-calculator-consumer-reward';

        $data = [
            'server_token' => 'Dhjj5-7042m@N52190Hctas#',
            'amount' => $totalPinpai,
            'username'   => $username
		];
		logger()->error(\json_encode($data));
        $res = curl()->post($url,$data);
        if (isset($res['code']) && $res['code'] == '1')
        {
            $data = $res['data'];
			logger()->error(\json_encode($res['data']));
            Db::transaction(function () use($data,$username) {
                $this->recalculateCredit3($username,$data['introducer'],$data['amount']);
                $this->recalculateCredit3($username,$data['business_center'],$data['amount']);
                return;
            });
        } else {
            throw new BusinessException(ErrorCode::PARAMS_INVALID,$res['message'] ?? '');
        }
	}
	public function recalculateCredit3($username,$parentUsername,$amount)
	{
		logger()->error("CALL recalculateCredit3");
		$user = User::where('name', $parentUsername)->firstOrFail()->toArray();
		if(!$user)
		{
			logger()->error("User not found");
		}
		logger()->error("BEFORE: ".$user['credit3']);
		// User::where('id',$user['id'])->update(['credit3' => $user['credit3']-$amount]);
		$testSearch = Credit3::where([
			'user_id' => $user['id'],
			'type' => Credit3Logic::REWARD_ONE_PERCENT_SPENT,
			'original_amount' => $amount
		])
			->orderBy(Db::raw("ABS(timestampdiff(second,created_at,'{$this->order['created_at']}'))"))
			->first();
		logger()->error("CREDIT3 ID: ".$testSearch->id);
		$update = Credit3::where('id',$testSearch->id)->update(['is_redeemable' => 0]);
		if($update)
		{
			logger()->error("DATA UPDATED");
		}
		$remark = trans('remark.exchange.refund_one_percent_spent');
        $info = [
            'user' => $user,
            'credit' => $amount,
            'type' => Credit3Logic::REFUND_REWARD_ONE_PERCENT_SPENT,
            'foreign_id' => 0,
            'remark' => $remark,
            'sub_type' => '',
		];
		logger()->error("Refund Credit3 info:");
		logger()->error(\json_encode($info));
        make(Credit3Logic::class)->deductCredit($info);
	}
}
