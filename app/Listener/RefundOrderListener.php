<?php
declare(strict_types=1);

namespace App\Listener;


use App\Event\FinishOrderEvent;
use App\Event\RefundOrderEvent;
use App\Logic\Order\OrderRefundLogic;
use App\Logic\Order\PayLogic;
use App\Service\PayOrderService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener()
 */
class RefundOrderListener implements ListenerInterface
{
	/**
	 * @Inject
	 * @var PayOrderService
	 */
	protected $service;

	public function listen(): array
	{
		return [
			RefundOrderEvent::class,
		];
	}

	public function process(object $event)
	{
		$order = $event->order;
		$order_refund = $event->order_refund;

		make(PayLogic::class)->refund($order,$order_refund);
	}
}
