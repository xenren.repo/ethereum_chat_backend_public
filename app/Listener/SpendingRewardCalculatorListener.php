<?php


namespace App\Listener;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Finance\Credit3Logic;
use App\Model\User;
use Hyperf\DbConnection\Db;
use Hyperf\Event\Annotation\Listener;
use App\Event\SpendingRewardCalculatorEvent;
use App\Model\DataDictionary;
use App\Model\GoodsCategory;
use App\Model\LoginLog;
use App\Model\MulGoodsCategory;
use App\Model\OrderGoods;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Logger\Logger;

/**
 * @Listener()
 */
class SpendingRewardCalculatorListener implements ListenerInterface
{
    const CATEGORY_ID = 408;

    /**
     * @inheritDoc
     */
    public function listen(): array
    {
        return [
            SpendingRewardCalculatorEvent::class
        ];
    }

    /**
     * @inheritDoc
     */
    public function process(object $event)
    {
        $user = $event->user;
        $order = $event->order;

        $orderGoods = OrderGoods::where('order_id', $order['id'])->get();
        $goodsIds = $orderGoods->pluck('goods_id')->toArray();

        $pinpaiId = $this->getPinpaiId();

        $cat = MulGoodsCategory::query();
        // $cat->where(['cid' => $pinpaiId]); // uncomment this part if you want pinpai condition again
        $cat = $cat->whereIn('goods_id',$goodsIds)->get()->pluck('goods_id')->toArray();


        $category= MulGoodsCategory::query();
        $category = $category->whereIn('goods_id',$goodsIds)->get()->pluck('cid')->toArray();

        $parent_category = GoodsCategory::query();
        $parent_category = $parent_category->whereIn('id', $category)->pluck('pid')->toArray();

        if(in_array(392, $parent_category)){

        }
        else {
          $totalPinpai = 0;
          foreach ($orderGoods as $k => $v)
          {
              if (in_array($v['goods_id'],$cat))
              {
                  $totalPinpai += $v['num']*$v['price'];
              }
          }
          if ($totalPinpai > 0)
          {
            if($pinpaiId !== 392 || $pinpaiId !== "392") {
              $this->addCredit3($user['name'],$totalPinpai);
            }

          }
        }
    }

    private function getPinpaiId()
    {
        $dataDictionary = DataDictionary::where([
            'module' => 'goods',
            'key_name' => 'category',
            'key_name' => 'pinpai'
        ])->first();
        if ($dataDictionary)
        {
            $dataDictionary = $dataDictionary->toArray();
            return $dataDictionary['key_id'];
        } else
        {
            $goodCaategory = GoodsCategory::where('id',self::CATEGORY_ID)->first()->toArray();
            return $goodCaategory['id'];
        }
    }

    private function addCredit3($username,$totalPinpai)
    {
        $url = 'https://fr167.com/api/spending-reward-calculator-consumer-reward';

        $data = [
            'server_token' => 'Dhjj5-7042m@N52190Hctas#',
            'amount' => $totalPinpai,
            'username'   => $username
        ];
        $res = curl()->post($url,$data);
        if (isset($res['code']) && $res['code'] == '1')
        {
            $data = $res['data'];

            Db::transaction(function () use($data,$username) {
              $data_amount = $data['amount'];

              $for_me = $data_amount;
              $for_upline = $data_amount / 2 ;
              $for_bc = $data_amount/5;

             $this->addCredit3Logs($username,$username,$for_me);
             $this->addCredit3Logs($username,$data['introducer'],$for_upline);
             $this->addCredit3Logs($username,$data['business_center'], $for_bc);
             return;

            });
        } else {
            throw new BusinessException(ErrorCode::PARAMS_INVALID,$res['message'] ?? '');
        }
    }

    private function addCredit3Logs($username, $parentUsername,$amount)
    {
        $user = User::where('name', $parentUsername)->firstOrFail()->toArray();
        $info = [
            'user' => $user,
            'credit' => $amount,
            'type' => Credit3Logic::REWARD_ONE_PERCENT_SPENT,
            'foreign_id' => 0,
            'remark' => "商城返佣1%品牌券，来自 [$username]",
            'sub_type' => '',
        ];
        make(Credit3Logic::class)->setCredit($info);
    }
}
