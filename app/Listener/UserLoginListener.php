<?php

declare(strict_types=1);

namespace App\Listener;

use App\Event\UserLoginEvent;
use App\Logic\System\LoginLogLogic;
use App\Model\UserLoginLog;
use Hyperf\Event\Annotation\Listener;
use Psr\Container\ContainerInterface;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
class UserLoginListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            UserLoginEvent::class
        ];
    }

    public function process(object $event)
    {
        logger()->error("[CALL USER LOGIN LISTENER]");
        $user = $event->user;
        $type = $event->type;
        $remark = '';
        if (!is_null($event->response))
        {
            logger()->error("[USER LOGIN LISTENER ERROR]");
            logger()->error(json_encode($event->response));
            $remark = json_encode($event->response);
        }
        $info = [
            'user_id' => $user['id'],
            'type'    => $type,
            'ip'      => ip2long(ip()),
            'remark'  => $remark
        ];
        UserLoginLog::insert($info);

    }
}
