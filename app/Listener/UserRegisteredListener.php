<?php
declare(strict_types=1);

namespace App\Listener;


use App\Event\UserRegisteredEvent;
use App\Kernel\Common\Helper;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\Validation\Event\ValidatorFactoryResolved;

/**
 * @Listener()
 */
class UserRegisteredListener implements ListenerInterface
{

	public function listen(): array
	{
		return [
			UserRegisteredEvent::class,
		];
	}

	public function process(object $event)
	{
		$user = $event->user;


	}
}
