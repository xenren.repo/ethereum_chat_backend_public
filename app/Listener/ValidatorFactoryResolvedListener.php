<?php
declare(strict_types=1);

namespace App\Listener;


use App\Kernel\Common\Helper;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\Validation\Event\ValidatorFactoryResolved;

/**
 * @Listener()
 */
class ValidatorFactoryResolvedListener implements ListenerInterface
{

	public function listen(): array
	{
		return [
			ValidatorFactoryResolved::class,
		];
	}

	public function process(object $event)
	{
		/**  @var ValidatorFactoryInterface $validatorFactory */
		$validatorFactory = $event->validatorFactory;

		$validatorFactory->extend('mobile', function ($attribute, $value, $parameters, $validator) {
			return Helper::isMobile($value) ? true  : false;
		});
		// 当创建一个自定义验证规则时，你可能有时候需要为错误信息定义自定义占位符这里扩展了 :foo 占位符
		$validatorFactory->replacer('mobile', function ($message, $attribute, $rule, $parameters) {
			return str_replace(':mobile', $attribute, $message);
		});
	}
}
