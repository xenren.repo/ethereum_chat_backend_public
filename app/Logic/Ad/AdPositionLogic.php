<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Ad;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use App\Logic\System\RoleLogic;
use App\Logic\System\UserRoleLogic;
use App\Model\Ad;
use App\Model\Admin;
use App\Model\AdPosition;
use App\Model\Role;
use App\Model\UserRole;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;

class AdPositionLogic extends Logic
{

	const ENABLE_STATUS = 0;
	const DISABLE_STATUS = 1;


	public function index($gpc)
	{
		$data = $this->query($gpc)
			->paginate((int)$gpc['limit'])
			->toArray();
		return $data;
	}


	public function json()
	{
		return cache()->remember('third_ad_position',function(){
			$data = AdPosition::selectRaw('id,name')->get()->toArray();
			return $data;
		},86400);
	}


	/**
	 * @param $gpc
	 * @return Role
	 */
	public function query($gpc)
	{
		$query = new AdPosition();
		if (!empty($gpc['search'])) {
			$search = $gpc['search'].'%';
			$query = $query->where(function ($query)use($search) {
				$query->where('name','like',$search);
			});
		}
		return $query;
	}


	/**
	 * @param $data
	 * @return mixed
	 */
	public function store($data)
	{
		$res = AdPosition::create($data);
		return $res;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function update($data)
	{
		$admin = AdPosition::findOrFailFromCache($data['id']);

		$admin->update($data);

		return null;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function delete($data)
	{

		$res = AdPosition::whereIn('id',$data['id'])->delete();

		ApplicationContext::getContainer()
			->get(Manager::class)
			->destroy($data['id'],AdPosition::class);


		return $res;
	}
}
