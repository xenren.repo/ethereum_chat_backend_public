<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Ad;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use App\Model\Ad;
use App\Model\Admin;
use App\Model\Navigation;
use App\Model\Role;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;

class NavigationLogic extends Logic
{

	const ENABLE_STATUS = 0;
	const DISABLE_STATUS = 1;


	public function index($gpc)
	{
		$data = $this->query($gpc)
			->paginate((int)$gpc['limit'])
			->toArray();
		return $data;
	}

	/**
	 * @return bool|mixed|string
	 */
	public function json()
	{
//		return cache()->remember('navigation_json_info', function ()  {
			$data = $this->query([])
				->selectRaw('id,name,img,url,remark,status')
				->whereIn('status', [0,2])
				->orderBy('sort', 'desc')
                ->where('name', '!=', '专区')
				->get()->toArray();
			return $data;
//		},86400);
	}

	/**
	 * @param $gpc
	 * @return Role
	 */
	public function query($gpc)
	{
		$query = new Navigation();
		if (!empty($gpc['search'])) {
			$search = $gpc['search'].'%';
			$query = $query->where(function ($query)use($search) {
				$query->where('name','like',$search);
			});
		}
		return $query;
	}


	/**
	 * @param $data
	 * @return mixed
	 */
	public function store($data)
	{
		$res = Navigation::create($data);
		$this->clearCache();
		return $res;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function update($data)
	{
		$admin = Navigation::findOrFailFromCache($data['id']);

		$admin->update($data);
		$this->clearCache();
		return null;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function delete($data)
	{

		$res = Navigation::whereIn('id',$data['id'])->delete();

		ApplicationContext::getContainer()
			->get(Manager::class)
			->destroy($data['id'],Navigation::class);

		$this->clearCache();
		return $res;
	}



	public function clearCache()
	{
		cache()->clearPrefix('navigation');
	}
}
