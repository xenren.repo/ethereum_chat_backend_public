<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Ad;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use App\Model\Role;
use App\Model\ThirdAd;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;

class ThirdAdLogic extends Logic
{

	const ENABLE_STATUS = 0;
	const DISABLE_STATUS = 1;


	public function index($gpc)
	{
		$data = $this->query($gpc)
			->paginate((int)$gpc['limit'])
			->toArray();
		foreach ($data['data'] as $k => $v) {
			$data['data'][$k]['img'] = json_decode($v['img'],true);
		}
		return $data;
	}



	/**
	 * @param $gpc
	 * @return Role
	 */
	public function query($gpc)
	{
		$query = new ThirdAd();
		if (!empty($gpc['search'])) {
			$search = $gpc['search'].'%';
			$query = $query->where(function ($query)use($search) {
				$query->where('name','like',$search);
			});
		}
		return $query;
	}


	/**
	 * @param $data
	 * @return mixed
	 */
	public function store($data)
	{
		$data['img'] = json_encode($data['img']);
		$res = ThirdAd::create($data);
		$this->clearCache();
		return $res;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function update($data)
	{
		$admin = ThirdAd::findOrFailFromCache($data['id']);
		$data['img'] = json_encode($data['img']);
		$admin->update($data);
		$this->clearCache();
		return null;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function delete($data)
	{

		$res = ThirdAd::whereIn('id',$data['id'])->delete();

		ApplicationContext::getContainer()
			->get(Manager::class)
			->destroy($data['id'],ThirdAd::class);

		$this->clearCache();
		return $res;
	}



	/**
	 * @param $pid
	 * @return array
	 */
	public function fetchAd($pid)
	{
		if (empty($pid)) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'广告位必须填写');
		}

		$data = cache()->remember('third_ad_fetch_list_'.$pid, function () use ($pid) {
			return ThirdAd::selectRaw('name,img,url')
				->where('pid', $pid)->where('status', 0)->get()->toArray();
		},86400);

		foreach ($data as $k => $v) {
			$data[$k]['img'] = json_decode($v['img'], true);
		}

		if ($data) {
			$key = mt_rand(0, count($data)-1);
			return $data[$key];
		}

		return [];
	}


	public function clearCache()
	{
		cache()->clearPrefix('third_ad');
	}
}
