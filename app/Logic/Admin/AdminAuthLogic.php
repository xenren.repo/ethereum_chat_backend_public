<?php

declare(strict_types=1);

namespace App\Logic\Admin;

use App\Constants\ErrorCode;
use App\Event\LoginEvent;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Kernel\Auth\AuthManager;
use App\Logic\Logic;
use App\Model\Admin;
use Carbon\Carbon;
use Hyperf\Di\Annotation\Inject;
use Psr\EventDispatcher\EventDispatcherInterface;

class AdminAuthLogic extends Logic
{
    const AUTH_TYPE = 'auth_admin';

    const ADMIN_TYPE = 1;

    /**
     * @Inject
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function login(array $gpc)
    {
        $user = $this->attempt($gpc);
        $auth_info = make(AuthManager::class)->getToken($user['id'], self::AUTH_TYPE);
        $auth_info['info'] = make(AdminLogic::class)->getUserInfo($user['id']);
        $this->eventDispatcher->dispatch(new LoginEvent($user, self::ADMIN_TYPE));
        return $auth_info;
    }

    public function logout()
    {
        $this->adminIsOffline();
        return make(AuthManager::class)->logout();
    }

    /**
     * 验证账号密码
     * @param $data
     * @return null|\Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object
     */
    public function attempt(array $data)
    {
        $user = Admin::query()->where('name', $data['name'])->first();

        if (empty($user) || ! password_verify($data['password'], $user['password'])) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '账号或密码错误');
        }
        // 检测是否其他限制无法登入
        $this->checkLogin($user);
        return $user;
    }

    /**
     * 登入前用户业务逻辑.
     * @param $user
     * @throws BusinessException
     */
    public function checkLogin($user)
    {
        if ($user['status'] != 0) {
            //throw new BusinessException(ErrorCode::PARAMS_INVALID,'该账号异常,请联系管理员');
        }
    }

    public function register($gpc)
    {
    }

    /**
     * 修改密码
     * @param $gpc
     * @return bool
     */
    public function changePassword($gpc)
    {
        $user = Auth::user();
        if (! password_verify($gpc['old_password'], $user['password'])) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '旧密码错误');
        }
        $user['password'] = password_hash($gpc['password'], PASSWORD_BCRYPT);
        return $user->save();
    }

    public function adminIsOnline(int $userId): void
    {
        Admin::where([
            'id' => $userId,
        ])->update([
            'is_online' => true,
        ]);
    }

    public function adminIsOffline(): void
    {
        $user = Auth::user();
        $user->update([
            'is_online' => false,
            'last_online_at' => Carbon::now(),
        ]);
    }
}
