<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Admin;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use App\Logic\System\RoleLogic;
use App\Logic\System\UserRoleLogic;
use App\Model\Admin;
use App\Model\Seller;
use App\Model\SellerReject;
use App\Model\Role;
use App\Model\UserRole;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;
use Hyperf\DbConnection\Db;

class AdminLogic extends Logic
{

	const ENABLE_STATUS = 0;
	const DISABLE_STATUS = 1;
	const DEFAULT_STATUS = 2; // when you first register
	const REJECT_REGISTER_STATUS = 3; 

	const GROUP_ID_ADMIN = 1;
	const GROUP_ID_SUB_ADMIN = 0;
	const GROUP_ID_SELLER = 2;

	// flag for rejection 

	const REJECT_PHOTO = 1; 
	const REJECT_COMPANY_NO= 2; 
	const REJECT_COMPANY_INFO = 3; 
	const REJECT_COMPANY_PHOTO = 4; 


	public function getUserInfo($user_id)
	{
        $seller = 0;
		$admin = Admin::findOrFail($user_id);
            if($admin['group_id'] == 2) {
                $seller_reject = SellerReject::where([
                    'seller_id' => $admin['id'],
                    'done' => 0
            ])->get()->toArray();
            $seller = Seller::where('admin_id', '=', $admin->id)->first();
		}
		return  [
			'id' => $admin['id'],
			'name' => $admin['name'],
			'mobile' => $admin['mobile'],
			'realname' => $admin['realname'],
			'credit1' => $admin['credit1'],
			'created_at' => $admin['created_at'],
			'group_id' => $admin['group_id'],
			'status' => $admin['status'],
            'seller' => $seller,
			'rejection' => $seller_reject ?? null,
		];
	}

	public function index($gpc)
	{
		$data = $this->query($gpc)
			->selectRaw('admin.id,name,mobile,realname,remark,status,admin.created_at,admin.updated_at, seller.approval_photo,seller.company_picture,seller.company_info,seller.company_no')
			->orderBy('admin.id','ASC')
			->paginate((int)$gpc['limit'])
		
			->toArray();
		$roles = make(RoleLogic::class);
		$user_role_logic = make(UserRoleLogic::class);
		foreach ($data['data'] as $k => $v) {
			$role_ids = $user_role_logic->getRolesId($v['id']);
			$data['data'][$k]['roles'] = '';
			if ($role_ids) {
				foreach ($role_ids as $role_id) {
					$data['data'][$k]['roles']  .= ($roles->getRoleName($role_id)) .' ';
				}
			}
			rtrim($data['data'][$k]['roles']);
		}
		return $data;
	}



	/**
	 * @param $gpc
	 * @return Role
	 */
	public function query($gpc)
	{
		$query = Admin::leftJoin('seller','seller.admin_id','=','admin.id');
		$query = $query->where('admin.id','>','2');
	
		if (!empty($gpc['search'])) {
			$search = $gpc['search'].'%';
			$query = $query->where(function ($query)use($search) {
				$query->where('name','like',$search)
					->orWhere('mobile', 'like', $search)
					->orWhere('realname', 'like', $search);
			});
		}
		return $query;
	}


	/**
	 * @param $data
	 * @return mixed
	 */
	public function store($data)
	{
		$data['password'] =  password_hash($data['password'],PASSWORD_BCRYPT);
		$res = Admin::create($data);
		unset($res['password']);
		return $res;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function update($data)
	{
		if (isset($data['password'])) {
			if ($data['password']) {
				$data['password'] = password_hash($data['password'],PASSWORD_BCRYPT);
			} else {
				unset($data['password']);
			}
		}

		$admin = Admin::findOrFailFromCache($data['id']);

		$admin->update($data);

		return null;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function delete($data)
	{

		$res = Admin::whereIn('id',$data['id'])->delete();

		ApplicationContext::getContainer()
			->get(Manager::class)
			->destroy($data['id'],Admin::class);


		return $res;
	}

	public function sellerReject($gpc) { 
		//return $gpc;
		
		$transaction = 	Db::transaction(function () use($gpc) {

			foreach ($gpc['seller_reject_reason'] as $reason) { 
				$data = [
				'seller_id' => $reason['seller_id'], 
				'action' => $reason['action'],
				'message' => $reason ['message']
				];

				$reject = SellerReject::create($data); 
			}
		}); 

	//   return $transaction;
	}
	  
}
