<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Business;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Common\Common;
use App\Logic\Logic;

class BusinessLogic extends Logic
{



	public static function getCnyToCredit2Ratio()
	{
		$ratio = Common::getSetting('business.cny_to_credit2');
		if (empty($ratio) || $ratio <= 0) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'兑换专区券比例设置错误');
		}
		return $ratio;
	}


	/**
	 * 默认全站分润信息
	 * @return array
	 */
	public static function defaultCommission()
	{
		$data = [
			'type' => 2, // 1:金额  2:比例
			'value' => [
				'1' => Common::getSetting('commission.level1_ratio', 0)/100,
				'2' => Common::getSetting('commission.level2_ratio', 0)/100,
			]
		];
		return $data;
	}


}
