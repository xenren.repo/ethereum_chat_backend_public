<?php


namespace App\Logic\Dev;


use App\Logic\Logic;
use App\Model\UserLoginLog;

class ReportLoginLogic extends Logic
{
    public function report($gpc)
    {
        $remark['req'] = $gpc['request'];
        $remark['res'] = $gpc['response'];
        if (isset($gpc['remark']))
        {
            $remark['remark'] = $gpc['remark'];
        }
        $remark = json_encode($remark);
        $info = [
            'type' => 1,
            'ip' => ip2long(ip()),
            'remark' => $remark
        ];
        UserLoginLog::insert($info);
    }
}