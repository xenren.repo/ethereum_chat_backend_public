<?php


namespace App\Logic\External;


use App\Logic\Logic;
use App\Model\FrRequestStatus;
use App\Traits\FR167;

class ResendRequestLogic extends Logic
{
    use FR167;
    public function resendRequest($gpc)
    {
        $failedData = $this->getFailedRequest($gpc);
        $temp = [];
        $uniqueIds = [];
        foreach ($failedData AS $item)
        {
            $data = json_decode($item['request_data']);
            if (!is_null($data))
            {
                $url = sprintf("%s%s",$this->getUrl(),$this->getResendType($gpc['type'],'url'));
                $send = curl()->post($url,$data);
                // if request failed
                if (!isset($send['code']) || $send['code'] != 0)
                {
                    $temp['failed'][] = [
                        'url' => $url,
                        'data' => $data
                    ];
                } else
                {
                    $uniqueIds[] = $item['uniqueid'];
                    $temp['success'][] = [
                            'url' => $url,
                            'data' => $data
                    ];
                }
            }
        }
        $update = FrRequestStatus::whereIn('uniqueid',$uniqueIds)->update(['status' => '1']);
        if ($update)
        {
            return [
                'status' => 1,
                'data' => $temp
            ];
        } else
        {
            return [
                'status' => 0,
                'data' => null
            ];
        }
    }
    public function getResendType($type = null,$keyName = null)
    {
        // add resend type definition here~
        $list = [
            1 => [
                'type' => 1,
                'name' => 'spend e-voucher',
                'url' => '/api/user/spend-evoucher',
            ],
        ];
        if (!is_null($type))
        {
            if (!is_null($keyName))
            {
                return $list[$type][$keyName];
            }
            return $list[$type];
        }
        return $list;
    }

    public function getFailedRequest($gpc)
    {
        $query = FrRequestStatus::query();


        if (isset($gpc['type']))
        {
            if (is_array($gpc['type']))
            {
                $query->whereIn('type',$gpc['type']);
            } else
            {
                $query->where('type',$gpc['type']);
            }
        }

        // predefined filtering
        $query->selectRaw('DISTINCT(uniqueid)');
        $query->select(['id','request_data','type','status','uniqueid']);
        $query->where('status','=','0'); // filter by failed status
        $query->where(function ($query) {
            $query->whereNotNull('request_data');
//            $query->orWhere('request_data','!=','');
        });
        // end of predefined filtering
        return $query->get()->toArray();
    }
}