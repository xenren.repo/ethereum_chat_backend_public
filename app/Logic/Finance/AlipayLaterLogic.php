<?php

declare(strict_types=1);

namespace App\Logic\Finance;

use Alipay\EasySDK\Kernel\Config;
use Alipay\EasySDK\Kernel\Factory;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RedirectMiddleware;
use Symfony\Component\DomCrawler\Crawler;

class AlipayLaterLogic extends Logic
{
    public function getNotifyUrl()
    {
        return env('HOST') . '/alipay/notify-new';
    }

    public function getQuitUrl()
    {
        return env('HOST');
    }

    /**
     * @param $gpc
     * @return string
     */
    public function webPay($gpc)
    {
        Factory::setOptions($this->getOptions());
        try {
            $result = Factory::payment()->wap()->pay(
                '商品支付',
                $gpc['system_no'],
                round($gpc['money'], 2) . '',
                $this->getQuitUrl(),
                $this->getNotifyUrl()
            );
            if ($result->body) {
                return $this->getLink($result->body);
            }
        } catch (Exception $e) {
            logger()->error($e->getMessage());
            throw new BusinessException(ErrorCode::SYSTEM, 'Failed request');
        }
    }

    /**
     * @param $gpc
     * @return string
     */
    public function webJinQiaPay($gpc, $cart)
    {
        Factory::setOptions($this->getOptions());
        try {
            $result = Factory::payment()->wap()->pay(
                '商品支付',
                $gpc['system_no'],
                round($gpc['money'], 2) . '',
                env('JINQIA_HOST').'/success-alipay-payment/'.$cart.'/',
                env('JINQIA_HOST').'/failure-alipay-payment/'.$cart.'/'
            );
            if ($result->body) {
                return $this->getLink($result->body);
            }
        } catch (Exception $e) {
            logger()->error($e->getMessage());
            throw new BusinessException(ErrorCode::SYSTEM, 'Failed request');
        }
    }

    /**
     * @param $gpc
     * @param null|mixed $returnUrl
     * @return string
     */
    public function qrPay($gpc, $returnUrl = null)
    {
        if (is_null($returnUrl)) {
            $returnUrl = $this->getQuitUrl();
        }
        Factory::setOptions($this->getOptions());
        try {
            $result = Factory::payment()->page()->pay(
                '商品支付',
                $gpc['system_no'],
                round($gpc['money'], 2) . '',
                $returnUrl
            );
            if ($result->body) {
                return $this->getLink($result->body);
            }
        } catch (Exception $e) {
            logger()->error($e->getMessage());
            throw new BusinessException(ErrorCode::SYSTEM, 'Failed request');
        }
    }

    public function newVerify($gpc)
    {
        Factory::setOptions($this->getOptions());
        try {
            return Factory::payment()->common()->verifyNotify($gpc);
        } catch (\Exception $e) {
            logger()->error('alipay sign verify fail : ' . $e->getMessage());
            throw new BusinessException(ErrorCode::PARAMS_INVALID, 'sign fail');
        }
    }

    public function getOptions()
    {
        // $private = file_get_contents('private.key');
        // $public = file_get_contents('pub.key');

        $base_path = BASE_PATH . '/config/cert';

        if (! file_exists($base_path . '/alipay_private.key') || ! file_exists($base_path . '/alipay_public.key')) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '证书未准备');
        }
        $private = file_get_contents($base_path . '/alipay_private.key');
        if ($private) {
            $raw = explode(
                "\n",
                str_replace(["\r\n", "\n\r", "\r"], "\n", $private)
            );
            $private = $raw[1];
        }
        $public = file_get_contents($base_path . '/alipay_public.key');

        $id = env('ALI_APP_ID', '2019101868434816');

        $options = new Config();
        $options->protocol = 'https';
        $options->gatewayHost = 'openapi.alipay.com';
        $options->signType = 'RSA2';

        $options->appId = $id;

        // 为避免私钥随源码泄露，推荐从文件中读取私钥字符串而不是写入源码中
        $options->merchantPrivateKey = $private;

        // $options->alipayCertPath = '<-- 请填写您的支付宝公钥证书文件路径，例如：/foo/alipayCertPublicKey_RSA2.crt -->';
        // $options->alipayRootCertPath = '<-- 请填写您的支付宝根证书文件路径，例如：/foo/alipayRootCert.crt" -->';
        // $options->merchantCertPath = '<-- 请填写您的应用公钥证书文件路径，例如：/foo/appCertPublicKey_2019051064521003.crt -->';

        //注：如果采用非证书模式，则无需赋值上面的三个证书路径，改为赋值如下的支付宝公钥字符串即可
        $options->alipayPublicKey = $public;

        //可设置异步通知接收服务地址（可选）
        $options->notifyUrl = $this->getNotifyUrl();

        //可设置AES密钥，调用AES加解密相关接口时需要（可选）
        // $options->encryptKey = "<-- 请填写您的AES密钥，例如：aa4BtZ4tspm2wnXLb1ThQA== -->";
        return $options;
    }

    public function getLink($html)
    {
        $domParser = new Crawler();
        $domParser->addHtmlContent($html);
        $form = $domParser->filter('form');
        $url = $form->attr('action');
        $method = $form->attr('method');
        $data = array_filter($form->filter('input')->each(function ($node) {
            if (! empty($node->attr('name')) && ! empty($node->attr('value'))) {
                return [
                    'name' => $node->attr('name'),
                    'value' => $node->attr('value'),
                ];
            }
        }));
        if (count($data) > 0) {
            foreach ($data as $key => $item) {
                $data[$item['name']] = $item['value'];
                unset($data[$key]);
            }
        }
        $client = new Client([
            'allow_redirects' => [
                'track_redirects' => true,
            ],
        ]);
        $response = $client->request($method, $url, [
            'query' => $data,
        ]);
        $headersRedirect = $response->getHeader(RedirectMiddleware::HISTORY_HEADER);
        if ($headersRedirect && isset($headersRedirect)) {
            return $headersRedirect[0];
        }
        throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Failed request');
    }
}
