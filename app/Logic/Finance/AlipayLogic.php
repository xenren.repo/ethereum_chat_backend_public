<?php

declare(strict_types=1);

namespace App\Logic\Finance;

use Alipay\AlipayRequestFactory;
use Alipay\AopClient;
use Alipay\Key\AlipayKeyPair;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;

class AlipayLogic extends Logic
{
    public function getNotifyUrl()
    {
        return env('HOST') . '/alipay/notify';
    }

    /**
     * @param $gpc
     * @return string
     */
    public function pay($gpc)
    {
        $rules = [
            'system_no' => 'required|string|max:50',
            'money' => 'required|numeric|min:0|max:999999',
        ];
        validate($gpc, $rules);
        $request = (new AlipayRequestFactory())->create('alipay.trade.app.pay', [
            'notify_url' => $this->getNotifyUrl(),
            'biz_content' => [
                'body' => '商品支付', // 对一笔交易的具体描述信息。如果是多种商品，请将商品描述字符串累加
                'subject' => '商品支付', // 商品的标题 / 交易标题 / 订单标题 / 订单关键字等
                'out_trade_no' => $gpc['system_no'], // 商户网站唯一订单号
                'total_amount' => round($gpc['money'], 2) . '', // 订单总金额，单位为元，精确到小数点后两位，取值范围 [0.01,100000000]
                'product_code' => 'QUICK_MSECURITY_PAY', // 销售产品码，商家和支付宝签约的产品码，为固定值 QUICK_MSECURITY_PAY
            ],
        ]);
        try {
            return $this->getAop()->sdkExecute($request);
        } catch (\Exception $ex) {
            logger()->error($ex->getMessage());
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '支付过程产生错误,请稍后尝试');
        }
    }

    /**
     * @return AlipayKeyPair
     */
    public function getKeyPair()
    {
        $base_path = BASE_PATH . '/config/cert';

        if (! file_exists($base_path . '/alipay_private.key') || ! file_exists($base_path . '/alipay_public.key')) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '证书未准备');
        }
        $private = file_get_contents($base_path . '/alipay_private.key');

        $public = file_get_contents($base_path . '/alipay_public.key');
        return AlipayKeyPair::create(
            $private,
            $public
        );
    }

    /**
     * @return AopClient
     */
    public function getAop()
    {
        $keyPair = $this->getKeyPair();
        return new AopClient(env('ALI_APP_ID'), $keyPair);
    }

    /**
     * @param $gpc
     * @return bool
     */
    public function verify($gpc)
    {
        $pass = $this->getAop()->verify($gpc);
        if (! $pass) {
            logger()->error('alipay sign verify fail');
            throw new BusinessException(ErrorCode::PARAMS_INVALID, 'sign fail');
        }
        return true;
    }
}
