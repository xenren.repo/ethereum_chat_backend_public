<?php

declare(strict_types=1);

namespace App\Logic\Finance;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Admin\AdminLogic;
use App\Logic\Logic;
use App\Logic\Order\OrderLogic;
use App\Logic\Order\PayLogic;
use App\Logic\Seller\RankSettingLogic;
use App\Model\Admin;
use App\Model\Credit1;
use App\Model\Goods;
use App\Model\Order;
use App\Model\OrderGoods;
use App\Model\User;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;

class Credit1Logic extends Logic
{
    const NAME = '余额';

    const SYSTEM = 0; // 系统设置

    const BUY = 1; // 购买

    const SELL = 10;

    const WITHDRAW = 11;

    const REFUND = 2; // 退款

    const ORDER_PROFIT = 3; // 奖励

    const OTHER = 9; // 其他

    /////// freeze & seller refund

    const UNFREEZE = 0;

    const FREEZE = 1;

    const REFUNDFREEZE = 2;

    /**
     * 增加/减少.
     * @param $gpc
     * @return mixed
     */
    public function setCredit($gpc)
    {
        if (round($gpc['credit'], 2) == 0) {
            return null;
        }
        $data = Db::transaction(function () use ($gpc) {
            if (is_numeric($gpc['user'])) {
                $user_id = $gpc['user'];
            } elseif (isset($gpc['user']['id']) && is_numeric($gpc['user']['id']) && $gpc['user']['id']) {
                $user_id = $gpc['user']['id'];
            } else {
                throw new BusinessException();
            }
            $user = User::findOrFail($user_id);
            if (empty($user)) {
                return null;
            }
            if (! isset($gpc['fee'])) {
                $gpc['fee'] = 0;
            } else {
                $gpc['fee'] = abs($gpc['fee']);
            }

            // 实际金额
            $amount = $this->getCredit($gpc['credit'], $gpc['fee']);

            $update = [
                'credit1' => $user['credit1'] + $amount,
                'version' => $user['version'] + 1,
            ];
            if ($gpc['credit'] < 0 && $update['credit1'] < 0) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, self::NAME . '不足');
            }
            $count = User::where('id', $user['id'])
                ->where('version', $user['version'])
                ->update($update);

            if ($count == 0) {
                throw new BusinessException(ErrorCode::VERSION_UPDATE);
            }

            $data = [
                'user_id' => $user['id'],
                'foreign_id' => $gpc['foreign_id'] ?? 0,
                'pay_time' => $gpc['pay_time'] ?? date('Y-m-d H:i:s'),
                'original_amount' => $gpc['credit'],
                'amount' => $amount,
                'fee' => -$gpc['fee'],
                'record_no' => $this->generateRecordNo(),
                'remark' => $gpc['remark'] ?? '',
                'type' => $gpc['type'],
                'sub_type' => $gpc['sub_type'] ?? '',
                'balance' => $update['credit1'],
            ];

            return Credit1::create($data);
        });

        ApplicationContext::getContainer()
            ->get(Manager::class)
            ->destroy([$data['user_id']], User::class);

        return $data;
    }

    /**
     * @param $order
     */
    public function sellerAddCredit1($order)
    {
        echo var_dump('************************* sellerAddCredit1 called *************************');
        echo var_dump('************************* sellerAddCredit1 called *************************');
        echo var_dump('************************* sellerAddCredit1 called *************************');
        echo var_dump('*************************ORDER ID *************************');
        echo var_dump($order->id);
        $data = Db::transaction(function () use ($order) {
            $orderGoods = OrderGoods::where('order_id', '=', $order->id)->get();
            echo var_dump(count($orderGoods));
            foreach ($orderGoods as $good) {
                //				if($good['created_by'] != 0 ) {
                $goodDetail = Goods::where('id', '=', $good['goods_id'])->first();
                $seller = Admin::find($goodDetail['created_by']);
                $balance = $seller['credit1'] - $good['price'];
                echo var_dump('new balance is');
                echo var_dump($balance);
                $seller_data = [
                    'user_id' => $goodDetail['created_by'],
                    'foreign_id' => $order['id'] ?? 0,
                    'pay_time' => $good['pay_time'] ?? date('Y-m-d H:i:s'),
                    'original_amount' => $good['total'],
                    'amount' => $good['price'],
                    'fee' => -$good['fee'],
                    'record_no' => $this->generateRecordNo(),
                    'remark' => $good['remark'] ?? '',
                    'type' => self::SELL,
                    'sub_type' => $good['sub_type'] ?? '',
                    'balance' => $balance,
                    'status' => self::FREEZE, // freeze
                ];
                $record = Credit1::create($seller_data);
                echo var_dump($record);
                //				}
            }
        });
    }

    /**
     * @return string
     */
    public function generateRecordNo()
    {
        return 'C1' . time() . mt_rand(100000, 999999);
    }

    /**
     * @param $gpc
     * @return mixed
     */
    public function index($gpc)
    {
        $data = $this->adminQuery($gpc);

        $admin = Admin::where('id', '=', Auth::id())->first();

        if ($gpc['searchSeller']) {
            $data->where('admin.name', 'like', '%' . $gpc['searchSeller'] . '%');
        }

        if ($gpc['seller_only']) {
            $sellers = self::getSellers();
            $data->whereIn('credit1.user_id', $sellers);
        }

        if ($gpc['admin_only']) {
            $sellers = self::getAdmins();
            $data->whereIn('credit1.user_id', $sellers);
        }

        $norecord = false;
        if ($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) {
            $goodsIds = Goods::where('created_by', '=', Auth::id())->pluck('id');
            $orders = OrderGoods::whereIn('goods_id', $goodsIds)->pluck('order_id');
            $order = Order::whereIn('id', $orders)->get()->pluck('id');
//            if(count($order) < 1) {
//                echo var_dump('no record');
//                $norecord = true;
//            } else {
            $data->whereIn('foreign_id', $order);
//            }
        }

        $newData = clone $data;

        $data->selectRaw(
            'credit1.record_no,
            credit1.record_no,
            credit1.original_amount,
            credit1.amount,
            credit1.type,
            credit1.balance,
            credit1.remark,
            credit1.created_at,
            credit1.pay_time,
            admin.name as adminName,
            admin.id as adminId,
            users.name as userName,
            users.id as usersId'
        )
            ->orderBy('credit1.id', 'desc');

        if ($norecord) {
            return collect();
        }
        $result = $data->paginate((int) $gpc['limit']);

        $totalAmount = $newData->selectRaw('ROUND(SUM(amount),2) AS total_amount')->first();

        return [
            'result' => $result,
            'total_amount' => $totalAmount['total_amount'] ?? 0,
        ];
    }

    /**
     * @param $gpc
     * @return mixed
     */
    public function indexAllNew($gpc)
    {
        $data = $this->adminQuery($gpc)
            ->leftJoin('third_pay AS tp', 'credit1.foreign_id', '=', 'tp.order_id')
            ->leftJoin('orders AS o', 'credit1.foreign_id', '=', 'o.id')
            ->leftJoin('users AS buyers', 'o.user_id', '=', 'buyers.id')
            ->select(
                [
                    'credit1.record_no',
                    'credit1.amount AS credit1_amount',
                    'credit1.type AS credit1_type',
                    'credit1.balance AS credit1_balance',
                    'credit1.pay_time AS credit1_pay_time',
                    'o.pay_type AS order_pay_type',
                    'o.created_at AS order_time',
                    'admin.name AS admin_name',
                    'admin.realname AS admin_real_name',
                    'admin.id AS admin_id',
                    'users.name AS users_name',
                    'users.id AS users_id',
                    'users.realname AS users_real_name',

                    'buyers.name AS buyers_name',
                    'buyers.id AS buyers_id',
                    'buyers.realname AS buyers_real_name',
                ]
                // 'credit1.record_no,
                // credit1.record_no,
                // credit1.original_amount,
                // credit1.amount,
                // credit1.type,
                // credit1.balance,
                // credit1.remark,
                // credit1.created_at,
                // credit1.pay_time,
                // admin.name as adminName,
                // admin.id as adminId,
                // users.name as userName,
                // users.id as usersId'
            )
            ->orderBy('credit1.id', 'desc');

        $admin = Admin::where('id', '=', Auth::id())->first();

        if ($gpc['searchSeller']) {
            $data->where('admin.name', 'like', '%' . $gpc['searchSeller'] . '%');
        }

        if ($gpc['seller_only']) {
            $sellers = self::getSellers();
            $data->whereIn('credit1.user_id', $sellers);
        }

        if ($gpc['admin_only']) {
            $sellers = self::getAdmins();
            $data->whereIn('credit1.user_id', $sellers);
        }

        $norecord = false;
        if ($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) {
            $goodsIds = Goods::where('created_by', '=', Auth::id())->pluck('id');
            $orders = OrderGoods::whereIn('goods_id', $goodsIds)->pluck('order_id');
            $order = Order::whereIn('id', $orders)->get()->pluck('id');
//            if(count($order) < 1) {
//                echo var_dump('no record');
//                $norecord = true;
//            } else {
            $data->whereIn('foreign_id', $order);
//            }
        }

        if ($norecord) {
            return collect();
        }
        return $data->get();
    }

    /**
     * @param $gpc
     * @return mixed
     */
    public function indexAll($gpc)
    {
        $data = $this->adminQuery($gpc)
            ->selectRaw(
                'credit1.record_no,
                credit1.record_no,
                credit1.original_amount,
                credit1.amount,
                credit1.type,
                credit1.balance,
                credit1.remark,
                credit1.created_at,
                credit1.pay_time,
                admin.name as adminName,
                admin.id as adminId,
                users.name as userName,
                users.id as usersId'
            )
            ->orderBy('credit1.id', 'desc');

        $admin = Admin::where('id', '=', Auth::id())->first();

        if ($gpc['searchSeller']) {
            $data->where('admin.name', 'like', '%' . $gpc['searchSeller'] . '%');
        }

        if ($gpc['seller_only']) {
            $sellers = self::getSellers();
            $data->whereIn('credit1.user_id', $sellers);
        }

        if ($gpc['admin_only']) {
            $sellers = self::getAdmins();
            $data->whereIn('credit1.user_id', $sellers);
        }

        $norecord = false;
        if ($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) {
            $goodsIds = Goods::where('created_by', '=', Auth::id())->pluck('id');
            $orders = OrderGoods::whereIn('goods_id', $goodsIds)->pluck('order_id');
            $order = Order::whereIn('id', $orders)->get()->pluck('id');
//            if(count($order) < 1) {
//                echo var_dump('no record');
//                $norecord = true;
//            } else {
            $data->whereIn('foreign_id', $order);
//            }
        }

        if ($norecord) {
            return collect();
        }
        return $data->get();
    }

    public function getSellers()
    {
        return Admin::where('group_id', '=', AdminLogic::GROUP_ID_SELLER)->pluck('id');
    }

    public function getAdmins()
    {
        return Admin::whereIn('group_id', [AdminLogic::GROUP_ID_ADMIN, AdminLogic::GROUP_ID_SUB_ADMIN])->pluck('id');
    }

    /**
     * @param $data
     * @return
     */
    public function query($data)
    {
        $query = Credit1::leftJoin('users', 'users.id', '=', 'credit1.user_id');
        if (! empty($data['search'])) {
            $search = $data['search'] . '%';
            $query = $query->where(function ($query) use ($search) {
                $query->where('users.name', 'like', $search)
                    ->orWhere('users.realname', 'like', $search)
                    ->orWhere('credit1.record_no', 'like', $search);
            });
        }

        if (isset($data['type']) && ($data['type'] != '') && ! is_null($data['type'])) {
            $query = $query->where('credit1.type', $data['type']);
        }

        if (isset($data['start_end']) && ! empty($data['start_end'])) {
            if (! isset($data['start_end'][0]) || ! isset($data['start_end'][1])) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, '时间格式错误');
            }
            $query = $query->whereBetween('credit1.created_at', [$data['start_end'][0], $data['start_end'][1]]);
        }

        if (! Auth::isAdmin()) {
            $query = $query->where('credit1.user_id', Auth::id());
        }
        return $query;
    }

    /**
     * @param $data
     * @return
     */
    public function adminQuery($data)
    {
        $query = Credit1::leftJoin('admin', 'admin.id', '=', 'credit1.user_id')->leftJoin('users', 'users.id', '=', 'credit1.user_id');
        if (! empty($data['search'])) {
            $search = $data['search'] . '%';
            $query = $query->where(function ($query) use ($search) {
                $query->where('admin.name', 'like', $search)
                    ->orWhere('credit1.record_no', 'like', $search);
            });
        }

        if (isset($data['type']) && ($data['type'] != '') && ! is_null($data['type'])) {
            if ($data['type'] == 'alipay') {
                $query->leftJoin('third_pay', 'third_pay.order_id', '=', 'credit1.foreign_id')
                    ->where('third_pay.pay_type', '=', OrderLogic::ALIPAY)
                    ->where('third_pay.status', '=', PayLogic::HAS_PAY);
            } elseif ($data['type'] == 'wechat') {
                $query->leftJoin('third_pay', 'third_pay.order_id', '=', 'credit1.foreign_id')
                    ->where('third_pay.pay_type', '=', OrderLogic::WECHAT)
                    ->where('third_pay.status', '=', PayLogic::HAS_PAY);
            } else {
                $query = $query->where('credit1.type', $data['type']);
            }
        }

        if (isset($data['start_end']) && ! empty($data['start_end'])) {
            if (! isset($data['start_end'][0]) || ! isset($data['start_end'][1])) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, '时间格式错误');
            }
            $query = $query->whereBetween('credit1.created_at', [$data['start_end'][0], $data['start_end'][1]]);
        }
        if (isset($data['amount']) && ! empty($data['amount'])) {
            if (isset($data['amount']['from'])) {
                $query->where('amount', '>=', $data['amount']['from']);
            }
            if (isset($data['amount']['to']) && $data['amount']['to'] > 0) {
                $query->where('amount', '<=', $data['amount']['to']);
            }
        }

        if (! Auth::isAdmin()) {
            $query = $query->where('credit1.user_id', Auth::id());
        }
        return $query;
    }

    public function autoUnfreeze()
    {
        $credit1 = Credit1::with('seller')->whereHas('seller')->whereHas('order', function ($query) {
            return $query->where('status', OrderLogic::FINISH);
        })->where('status', self::FREEZE)->get();
        $count = 0;
        Db::transaction(function () use ($credit1, $count) {
            foreach ($credit1 as $credit) {
                echo var_dump($credit->toJson());
                $rank_id = $credit->seller->rank_id;
                $payTime = Carbon::parse($credit->pay_time);
                $now = Carbon::now();
                $diff = $payTime->diffInMinutes($now);
                $minutes = 200 * 60;

                $minutes = (new RankSettingLogic())->getSettings((int) $rank_id, RankSettingLogic::KEY_AUTO_RELEASE_PAYMENT);

                echo var_dump('rank_id');
                echo var_dump($rank_id);
                echo var_dump('now');
                echo var_dump($now->toDateTimeString());
                echo var_dump('paytime');
                echo var_dump($payTime->toDateTimeString());
                echo var_dump('diff');
                echo var_dump($diff);

                if ($diff >= $minutes) {
                    echo var_dump('******************** GOING TO UPDATE ******************** ');
                    $creditsearch = Credit1::where('id', $credit['id'])->update([
                        'status' => self::UNFREEZE,
                    ]);

                    $credit_data = Credit1::where('id', $credit['id'])->first();
                    $seller = $credit_data['user_id'];

                    $credit1_seller = Admin::where('id', $seller)->first();
                    $credit1_total = $credit1_seller['credit1'];

                    $data_seller = Admin::where('id', $seller)->update([
                        'credit1' => $credit1_total + $credit_data['original_amount'],
                    ]);
                    ++$count;
                }
            }
        });
        return $count;
    }

    public function xautoUnfreeze()
    {
        $release_day = 1;
        //$out_time = Carbon::now()->subMinutes($auto_shutdown_minutes)->format('Y-m-d H:i:s');
        $out_time = Carbon::now()->startOfDay()->addDays(1)->format('Y-m-d H:i:s');

        $credit1 = Credit1::where('status', self::FREEZE)
            ->where('pay_time', '<=', $out_time)
            ->get()->toArray();
        $count = 0;
        $data = Db::transaction(function () use ($credit1, $count) {
            foreach ($credit1 as $credit) {
                $creditsearch = Credit1::where('id', $credit['id'])->update([
                    'status' => self::UNFREEZE,
                ]);

                $credit_data = Credit1::where('id', $credit['id'])->first();
                $seller = $credit_data['user_id'];

                $credit1_seller = Admin::where('id', $seller)->first();
                $credit1_total = $credit1_seller['credit1'];

                $data_seller = Admin::where('id', $seller)->update([
                    'credit1' => $credit1_total + $credit_data['original_amount'],
                ]);
                ++$count;
            }
        });
        return $count;
    }

    public function refundFreezeSeller($order_id)
    {
        $data = Db::transaction(function () use ($order_id) {
            $getCredit = Credit1::where([
                'status' => self::FREEZE,
                'foreign_id' => $order_id,
            ])->get()->toArray();

            foreach ($getCredit as $credit1) {
                $update = Credit1::where('id', $credit1['id'])->update([
                    'status' => self::REFUNDFREEZE,
                ]);
            }
        });
    }

    /**
     * 获取与用户当前余额可直接相加减的金额.
     * @param $total_amount
     * @param $fee
     * @return float|int
     */
    private function getCredit($total_amount, $fee)
    {
        if (abs($total_amount) < $fee) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '手续费不能大于申请数目');
        }
        if ($total_amount > 0) {
            $total_amount = $total_amount - $fee;
        } else {
            $total_amount = $total_amount + $fee;
        }
        return $total_amount;
    }
}
