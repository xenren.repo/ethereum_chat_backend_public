<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Finance;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\Credit2;
use App\Model\User;
use Hyperf\DbConnection\Db;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;

class Credit2Logic extends Logic
{

	const NAME = '专用券';

	const SYSTEM   = 0; // 系统设置

	const RECHARGE = 1;  // 充值

	const ORDER = 2;  // 商城购买

    const PRIZE = 3;  // 购买奖励

    const BRAND_COUPON_TO_ZONE_COUPON = 4;  // 增加专区券，来自品牌券转换 // will convert credit3 to credit2

	/**
	 * 增加/减少
	 * @param $gpc
	 * @return mixed
	 */
	public function setCredit($gpc)
	{
		if (round($gpc['credit'], 2) == 0) {
			return null;
		}
		$data =  Db::transaction(function () use ($gpc) {
			if (is_numeric($gpc['user'])) {
				$user_id = $gpc['user'];
			} elseif (isset($gpc['user']['id']) && is_numeric($gpc['user']['id']) && $gpc['user']['id']) {
				$user_id = $gpc['user']['id'];
			} else {
				throw new BusinessException();
			}
			$user = User::findOrFail($user_id);
			if (empty($user)) {
				return null;
			}
			if (!isset($gpc['fee'])) {
				$gpc['fee'] = 0;
			} else {
				$gpc['fee'] = abs($gpc['fee']);
			}

			// 实际金额
			$amount = $this->getCredit($gpc['credit'], $gpc['fee']);

			$update = [
				'credit2' => $user['credit2'] + $amount,
				'version' => $user['version'] + 1
			];
			if ($gpc['credit'] < 0  && $update['credit2'] < 0) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,self::NAME.'不足');
			}
			$count = User::where('id', $user['id'])
				->where('version', $user['version'])
				->update($update);

			if ($count == 0) {
				throw new BusinessException(ErrorCode::VERSION_UPDATE);
			}



			$data = [
				'user_id'        => $user['id'],
				'foreign_id'     => $gpc['foreign_id'] ?? 0,
				'pay_time'       => $gpc['pay_time'] ?? date('Y-m-d H:i:s'),
				'original_amount'=> $gpc['credit'],
				'amount'         => $amount,
				'fee'            => -$gpc['fee'],
				'record_no'      => $this->generateRecordNo(),
				'remark'         => $gpc['remark'] ?? '',
				'type'           => $gpc['type'],
				'sub_type'       => $gpc['sub_type'] ?? '',
				'balance'        => $update['credit2'],
			];
			return Credit2::create($data);
		});

		ApplicationContext::getContainer()
			->get(Manager::class)
			->destroy([$data['user_id']],User::class);

		return $data;
	}

	/**
	 * @return string
	 */
	public function generateRecordNo()
	{
		return 'C1'.time().mt_rand(100000, 999999);
	}

	/**
	 * @param $gpc
	 * @return mixed
	 */
	public function index($gpc)
	{
		$data = $this->query($gpc)
			->selectRaw('credit2.record_no,credit2.original_amount,credit2.amount,credit2.type,credit2.balance,credit2.remark,credit2.created_at,credit2.pay_time,users.name,users.realname')
			->orderBy('credit2.id', 'desc')
			->paginate((int)$gpc['limit']);
		return $data;
	}

	/**
	 * @param $data
	 * @return
	 */
	public function query($data)
	{
		$query = Credit2::leftJoin('users', 'users.id','=','credit2.user_id');
		if (!empty($data['search'])) {
			$search = $data['search'].'%';
			$query = $query->where(function ($query)use($search) {
				$query->where('users.name','like',$search)
					->orWhere('users.realname', 'like', $search)
					->orWhere('credit2.record_no', 'like', $search);
			});
		}

		if (isset($data['type']) && ($data['type'] != '') && !is_null($data['type'])) {
			$query = $query->where('credit2.type', $data['type']);
		}

		if (isset($data['start_end']) && !empty($data['start_end'])) {
			if (!isset($data['start_end'][0]) || !isset($data['start_end'][1])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'时间格式错误');
			}
			$query = $query->whereBetween('credit2.created_at', [$data['start_end'][0], $data['start_end'][1]]);
		}

		if (!Auth::isAdmin()) {
			$query = $query->where('credit2.user_id', Auth::id());
		}
		return $query;
	}



	/**
	 * 获取与用户当前余额可直接相加减的金额
	 * @param $total_amount
	 * @param $fee
	 * @return float|int
	 */
	private function getCredit($total_amount, $fee)
	{
		if (abs($total_amount) < $fee) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'手续费不能大于申请数目');
		}
		if ($total_amount > 0) {
			$total_amount = $total_amount - $fee;
		} else {
			$total_amount = $total_amount + $fee;
		}
		return $total_amount;
	}


}
