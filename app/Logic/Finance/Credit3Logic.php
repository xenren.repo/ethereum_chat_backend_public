<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Finance;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\Credit3;
use App\Model\User;
use Hyperf\DbConnection\Db;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;
use Exception;
use Carbon\Carbon;
class Credit3Logic extends Logic
{

	const NAME = '品牌券';

	const MIN_REDEEM_DAY = 14;

	const SYSTEM   = 0; // 系统设置

	const PRIZE = 1;  // 消费返佣

	const SIGN = 2;  // 签到

	const TRANS_CREDIT2 = 3;  // 转换

	const SETTLE = 4;  // 结算

    const REWARD_ONE_PERCENT_SPENT = 5; //百分之一商城返佣

    const CONVERT_CASH_COUPON = 6;  //  转换成现金券 // this will send to FR167 ??

    const CONVERT_ZONE_COUPON = 7;  // 转换成专区券 // this will convert to credit2 ??

    const REFUND_REWARD_ONE_PERCENT_SPENT = 8; //Refund 1% reward


    const ORDER = 9; // When buy item

    const REFUND = 10; // When refund order


    function deductCredit($gpc)
    {
        if (round($gpc['credit'], 2) == 0) {
            return null;
        }
        try {
            $data = Db::transaction(function () use ($gpc) {
                if (is_numeric($gpc['user'])) {
                    $user_id = $gpc['user'];
                } elseif (isset($gpc['user']['id']) && is_numeric($gpc['user']['id']) && $gpc['user']['id']) {
                    $user_id = $gpc['user']['id'];
                } else {
                    throw new BusinessException();
                }
                $user = User::findOrFail($user_id);
                if (empty($user)) {
                    return null;
                }
                if (!isset($gpc['fee'])) {
                    $gpc['fee'] = 0;
                } else {
                    $gpc['fee'] = abs($gpc['fee']);
                }

                // 实际金额
                $amount = $this->getCredit($gpc['credit'], $gpc['fee']);

                $update = [
                    'credit3' => $user['credit3'] - $amount,
                    'version' => $user['version'] + 1
                ];
                if ($gpc['credit'] < 0 && $update['credit3'] < 0) {
                    throw new BusinessException(ErrorCode::PARAMS_INVALID, self::NAME . '不足');
                }
                $count = User::where('id', $user['id'])
//                    ->where('version', $user['version'])
                    ->update($update);

                logger()->error('USER VERSION: '. $user['version']);

                if ($count == 0) {
                    throw new BusinessException(ErrorCode::VERSION_UPDATE);
                }


                $data = [
                    'user_id' => $user['id'],
                    'foreign_id' => $gpc['foreign_id'] ?? 0,
                    'pay_time' => $gpc['pay_time'] ?? date('Y-m-d H:i:s'),
                    'original_amount' => $gpc['credit'],
                    'amount' => $amount,
                    'fee' => -$gpc['fee'],
                    'record_no' => $this->generateRecordNo(),
                    'remark' => $gpc['remark'] ?? '',
                    'type' => $gpc['type'],
                    'sub_type' => $gpc['sub_type'] ?? '',
                    'balance' => $update['credit3'],
                ];
                return Credit3::create($data);
            });

        } catch (\Exception $e){
            logger()->error($e->getMessage());
            logger()->error($e->getLine());
            logger()->error($e->getFile());
        }
        ApplicationContext::getContainer()
            ->get(Manager::class)
            ->destroy([$data['user_id']],User::class);

        return $data;
    }
    /**
	 * 增加/减少
	 * @param $gpc
	 * @return mixed
	 */
	public function setCredit($gpc)
	{
		if (round($gpc['credit'], 2) == 0) {
			return null;
		}
		$data =  Db::transaction(function () use ($gpc) {
			if (is_numeric($gpc['user'])) {
				$user_id = $gpc['user'];
			} elseif (isset($gpc['user']['id']) && is_numeric($gpc['user']['id']) && $gpc['user']['id']) {
				$user_id = $gpc['user']['id'];
			} else {
				throw new BusinessException();
			}
			$user = User::findOrFail($user_id);
			if (empty($user)) {
				return null;
			}
			if (!isset($gpc['fee'])) {
				$gpc['fee'] = 0;
			} else {
				$gpc['fee'] = abs($gpc['fee']);
			}

			// 实际金额
			$amount = $this->getCredit($gpc['credit'], $gpc['fee']);

			$update = [
				'credit3' => $user['credit3'] + $amount,
				'version' => $user['version'] + 1
			];
			if ($gpc['credit'] < 0  && $update['credit3'] < 0) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,self::NAME.'不足');
			}
			$count = User::where('id', $user['id'])
				->where('version', $user['version'])
				->update($update);

			if ($count == 0) {
				throw new BusinessException(ErrorCode::VERSION_UPDATE);
			}



			$data = [
				'user_id'        => $user['id'],
				'foreign_id'     => $gpc['foreign_id'] ?? 0,
				'pay_time'       => $gpc['pay_time'] ?? date('Y-m-d H:i:s'),
				'original_amount'=> $gpc['credit'],
				'amount'         => $amount,
				'fee'            => -$gpc['fee'],
				'record_no'      => $this->generateRecordNo(),
				'remark'         => $gpc['remark'] ?? '',
				'type'           => $gpc['type'],
				'sub_type'       => $gpc['sub_type'] ?? '',
				'balance'        => $update['credit3'],
			];
			return Credit3::create($data);
		});

		ApplicationContext::getContainer()
			->get(Manager::class)
			->destroy([$data['user_id']],User::class);

		return $data;
	}

	/**
	 * @return string
	 */
	public function generateRecordNo()
	{
		return 'C1'.time().mt_rand(100000, 999999);
	}

	/**
	 * @param $gpc
	 * @return mixed
	 */
	public function index($gpc,$transformAmount = false)
	{
		$data = $this->query($gpc)
			->selectRaw('credit3.record_no,credit3.original_amount,credit3.amount,credit3.type,credit3.balance,credit3.remark,credit3.created_at,credit3.pay_time,users.name,users.realname')
			->orderBy('credit3.id', 'desc')
			->paginate((int)$gpc['limit']);
		if ($transformAmount === true)
		{
			$minusAmountType = [7, 6];
			foreach ($data as $key => $value)
			{
				if (in_array($value->type, $minusAmountType))
				{
					$data[$key]->amount = -$value->amount;
				}
			}
			// if (count($data) > 0)
			// {
			// 	$data = array_map(function($arr) use ($minusAmountType) {
			// 		if (in_array($arr['type'], $minusAmountType))
			// 		{
			// 			$arr['amount'] = -$arr['amount'];
			// 		}
			// 		return $arr;
			// 	},$data);
			// }
		}
		return $data;
	}

	/**
	 * @param $data
	 * @return
	 */
	public function query($data)
	{
		$query = Credit3::leftJoin('users', 'users.id','=','credit3.user_id');
		if (!empty($data['search'])) {
			$search = $data['search'].'%';
			$query = $query->where(function ($query)use($search) {
				$query->where('users.name','like',$search)
					->orWhere('users.realname', 'like', $search)
					->orWhere('credit3.record_no', 'like', $search);
			});
		}

		if (isset($data['type']) && ($data['type'] != '') && !is_null($data['type'])) {
			$query = $query->where('credit3.type', $data['type']);
		}

		if (isset($data['start_end']) && !empty($data['start_end'])) {
			if (!isset($data['start_end'][0]) || !isset($data['start_end'][1])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'时间格式错误');
			}
			$query = $query->whereBetween('credit3.created_at', [$data['start_end'][0], $data['start_end'][1]]);
		}

		if (!Auth::isAdmin()) {
			$query = $query->where('credit3.user_id', Auth::id());
		}
		return $query;
	}



	/**
	 * 获取与用户当前余额可直接相加减的金额
	 * @param $total_amount
	 * @param $fee
	 * @return float|int
	 */
	private function getCredit($total_amount, $fee)
	{
		if (abs($total_amount) < $fee) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'手续费不能大于申请数目');
		}
		if ($total_amount > 0) {
			$total_amount = $total_amount - $fee;
		} else {
			$total_amount = $total_amount + $fee;
		}
		return $total_amount;
	}

    public function convert2Coupon(array $gpc)
    {
        $user = Auth::user();
        $amount = $gpc['amount'];
        if ($user['credit3'] <= $amount)
        {
            throw new BusinessException(ErrorCode::PARAMS_INVALID,trans('error.user_exchange.balance.insufficient'));
        }
        $unredeemableAmount = $this->getUnredeemableAmount();
        $redeemableAmount = $this->redeemableAmount();


        $filterAmount = $user['credit3']-$unredeemableAmount-$redeemableAmount['redeemed_amount'];

        if ($amount > $filterAmount)
        {
            throw new BusinessException(ErrorCode::PARAMS_INVALID,sprintf("Cannot exhchange more than %s because of redeem time regulation",$filterAmount));
        }


        try {
            Db::beginTransaction();

            // filter if amount is redeemed
            $filterAmountFlag = $user['credit3']-$redeemableAmount['redeemable_amount'];
            if ($filterAmountFlag-$amount > 0)
            {
            	$redeemCounter = $filterAmountFlag-$amount;
            	$query = $this->query(['type' => 5])
            				->selectRaw('credit3.id,credit3.amount,credit3.redeem_amount')
            				->whereRaw("TIMESTAMPDIFF(DAY,credit3.created_at,NOW()) >= 14")
            				->get();
            	foreach ($query as $key => $value)
            	{
            		$i = $value->amount-$value->redeem_amount;
            		if ($redeemCounter == $i)
            		{
						Credit3::where('id',$value->id)->update(['redeem_amount' => $value->amount]);
						break;
            		} else
            		{
            			if (round($redeemCounter,2) > round($i,2))
            			{
							Credit3::where('id',$value->id)->update(['redeem_amount' => $value->amount]);
							$redeemCounter = round($redeemCounter,2) - round($i,2);
            			} else
            			{
							Credit3::where('id',$value->id)->update(['redeem_amount' => round($i,2)-round($redeemCounter,2)]);
							// $redeemCounter = round($redeemCounter,2) - round($i,2);
							break;
            			}
            		}
            	}
            }


            $remarkCredit3 = trans('remark.exchange.brand_coupon_to_zone_coupon_in_credit3',['amount' => $amount]);

            $info = [
                'user' => $user,
                'credit' => $amount,
                'type' => Credit3Logic::CONVERT_ZONE_COUPON,
                'remark' => $remarkCredit3,
                'sub_type' => '',
            ];
            $credit3 = $this->deductCredit($info);

            $remarkCredit2 = trans('remark.exchange.brand_coupon_to_zone_coupon_in_credit2',['amount' => $amount]);

            $infoCredit2 = [
                'user' => $user,
                'credit' => $amount,
                'type' => Credit2Logic::BRAND_COUPON_TO_ZONE_COUPON,
                'remark' => $remarkCredit2,
                'sub_type' => '',
            ];
            $credit2 = make(Credit2Logic::class)->setCredit($infoCredit2);
            Db::commit();
        } catch (Exception $e)
        {
            Db::rollBack();
            throw new BusinessException(ErrorCode::PARAMS_INVALID,"Error occured when converting coupon");
        }
        return [
            'credit3_log' => $credit3,
            'credit2_log' => $credit2,
        ];
    }

    public function getUnredeemableAmount($gpc = [])
    {
    	$filter = [
    		'type' => 5,
    		'min_redeem_days' => 14
    	];
		$data = $this->query($filter)
			->selectRaw('SUM(credit3.amount) AS amount,SUM(credit3.redeem_amount) AS redeem_amount')
			->where('credit3.is_redeemable',1)
			->whereRaw("TIMESTAMPDIFF(DAY,credit3.created_at,NOW()) <= {$filter['min_redeem_days']}")
			->first();
		return $data->amount;
    }
    public function redeemableAmount($gpc = [])
    {
    	$filter = [
    		'type' => 5,
    	];
		$data = $this->query($filter)
			->selectRaw('SUM(credit3.amount) AS amount,SUM(credit3.redeem_amount) AS redeem_amount')
			->where('credit3.is_redeemable',1)
			->first();
		return [
			'amount' => $data->amount,
			'redeemed_amount' => $data->redeem_amount,
			'redeemable_amount' => $data->amount-$data->redeem_amount
		];
    }

}
