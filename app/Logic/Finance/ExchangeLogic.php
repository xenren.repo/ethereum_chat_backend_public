<?php
declare(strict_types=1);

namespace App\Logic\Finance;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Lib\Fr167\Url;
use App\Logic\Logic;
use App\Model\Credit3;
use App\Model\User;
use App\Logic\Finance\Credit3Logic;
use Hyperf\DbConnection\Db;

class ExchangeLogic extends Logic
{
    public function exchange($gpc)
    {
        logger()->error('[TEST ONLY EXCHANGE LOGIC]');

        $user = Auth::user();


        $user = User::where('id', '=', $user['id'])->first();

        logger()->error('[TEST ONLY EXCHANGE LOGIC]: User Credit3: '.$user['id']);
        logger()->error('[TEST ONLY EXCHANGE LOGIC]: User Credit3: '.$user['credit3']);

        $amount = $gpc['amount'];
        if ($user['credit3'] <= $amount)
        {
            throw new BusinessException(ErrorCode::PARAMS_INVALID,trans('error.user_exchange.balance.insufficient'));
        }

        $unredeemableAmount = make(Credit3Logic::class)->getUnredeemableAmount();
        $redeemableAmount = make(Credit3Logic::class)->redeemableAmount();


        $filterAmount = $user['credit3']-$unredeemableAmount-$redeemableAmount['redeemed_amount'];

        logger()->error('[TEST ONLY EXCHANGE LOGIC]: User unredeemableAmount: '.$unredeemableAmount);
        logger()->error('[TEST ONLY EXCHANGE LOGIC]: User Redeemable Amount: ');
        logger()->error(json_encode($redeemableAmount));

        if ($amount > $filterAmount) 
        {
            throw new BusinessException(ErrorCode::PARAMS_INVALID,sprintf("Cannot exhchange more than %s because of redeem time regulation",$filterAmount));
        }

        try {
            
            Db::beginTransaction();

            // filter if amount is redeemed
            $filterAmountFlag = $user['credit3']-$redeemableAmount['redeemable_amount'];
            if ($filterAmountFlag-$amount > 0) 
            {
                $redeemCounter = $filterAmountFlag-$amount;
                $query = Credit3::where('type' ,'=', 5)
                            ->selectRaw('credit3.id,credit3.amount,credit3.redeem_amount')
                            ->whereRaw("TIMESTAMPDIFF(DAY,credit3.created_at,NOW()) >= 14")
                            ->get();
                foreach ($query as $key => $value) 
                {
                    $i = $value->amount-$value->redeem_amount;
                    if ($redeemCounter == $i)
                    {
                        Credit3::where('id',$value->id)->update(['redeem_amount' => $value->amount]);
                        break;
                    } else
                    {
                        if (round($redeemCounter,2) > round($i,2)) 
                        {
                            Credit3::where('id',$value->id)->update(['redeem_amount' => $value->amount]);
                            $redeemCounter = round($redeemCounter,2) - round($i,2);
                        } else
                        {
                            Credit3::where('id',$value->id)->update(['redeem_amount' => round($i,2)-round($redeemCounter,2)]);
                            // $redeemCounter = round($redeemCounter,2) - round($i,2);
                            break;
                        }
                    }
                }
            }

            $remark = trans('remark.exchange.voucher_to_fr_rmb',['amount' => $amount]);



            $this->remoteExchange($user['name'],$amount,$remark);

            $info = [
                'user' => $user,
                'credit' => $amount,
                'type' => Credit3Logic::CONVERT_CASH_COUPON,
                'remark' => trans('remark.brand_voucher_converted',['amount' => $amount]),
                'sub_type' => '',
            ];
            $data = make(Credit3Logic::class)->deductCredit($info);
            Db::commit();
        } catch (\Exception $e)
        {
            Db::rollBack();
            logger()->error('CONVERT TO FR167 RMB EXCEPTION [START]');
            logger()->error($e->getMessage());
            logger()->error($e->getLine());
            logger()->error($e->getFile());
            logger()->error(format_throwable($e->getThrowable()));
            logger()->error('CONVERT TO FR167 RMB EXCEPTION [END]');
            throw new BusinessException(ErrorCode::PARAMS_INVALID,"Error occured when converting coupon");
        }
        return $data;
    }

    private function remoteExchange($name, $amount, $remark)
    {
        try {
            $url = 'https://fr167.com/api/ecomm/gainrmb';
            logger()->error('url is now');
            logger()->error($url);
            $data = [
                'ecommerce_token' => 'H@>tI°n~{I$QHtWz',
                'amount' => $amount,
                'remark' => $remark,
                'username' => $name
            ];
            $res = curl()->post($url, $data);
            if (isset($res['code']) && $res['code'] == '1') {
                logger()->error('CODE CODE 1');
                return true;
            } else {
                logger()->error('EXCEPTION 1');
                throw new BusinessException(ErrorCode::PARAMS_INVALID, $res['message'] ?? '');
            }
        }catch (\Exception $e){
            logger()->error('EXCEPTION 2');
            logger()->error(json_encode($e->getMessage()));
            throw new BusinessException(ErrorCode::PARAMS_INVALID,$res['message'] ?? '');
        }
    }

}
