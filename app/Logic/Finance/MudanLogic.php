<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Finance;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\Credit1;
use App\Model\Credit4;
use App\Model\User;
use Hyperf\DbConnection\Db;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;

class MudanLogic extends Logic
{

	const NAME = '牡丹专区券';

	const SYSTEM = 0; // 系统设置

	const BUY = 1; // 购买

	const REFUND = 2; // 退款

	const ORDER_PROFIT = 3; // 奖励

	const OTHER = 9; // 其他

	/**
	 * 增加/减少
	 * @param $gpc
	 * @return mixed
	 */
	public function setCredit($gpc)
	{
		if (round($gpc['credit'], 2) == 0) {
			return null;
		}
		$data = Db::transaction(function () use ($gpc) {
			if (is_numeric($gpc['user'])) {
				$user_id = $gpc['user'];
			} elseif (isset($gpc['user']['id']) && is_numeric($gpc['user']['id']) && $gpc['user']['id']) {
				$user_id = $gpc['user']['id'];
			} else {
				throw new BusinessException();
			}
			$user = User::findOrFail($user_id);
			if (empty($user)) {
				return null;
			}
			if (!isset($gpc['fee'])) {
				$gpc['fee'] = 0;
			} else {
				$gpc['fee'] = abs($gpc['fee']);
			}

			$usr = User::where('id', $user['id'])->first();

			// 实际金额
			$amount = $this->getCredit($gpc['credit'], $gpc['fee']);

			$update = [
				'mudan_coin' => $usr->mudan_coin - $gpc['total_products'],
			];

			$count = User::where('id', $user['id'])
				->where('version', $user['version'])
				->update($update);

			if ($count == 0) {
				throw new BusinessException(ErrorCode::VERSION_UPDATE);
			}

			$data = [
				'user_id'        => $user['id'],
				'foreign_id'     => $gpc['foreign_id'] ?? 0,
				'pay_time'       => $gpc['pay_time'] ?? date('Y-m-d H:i:s'),
				'original_amount'=> $gpc['credit'],
				'amount'         => $amount,
				'fee'            => -$gpc['fee'],
				'record_no'      => $this->generateRecordNo(),
				'remark'         => $gpc['remark'] ?? '',
				'type'           => $gpc['type'] ?? Credit4::TYPE_SPENT_MD_COIN,
				'sub_type'       => $gpc['sub_type'] ?? '',
				'balance'        => $update['mudan_coin'],
			];
			$c4 = Credit4::create($data);
			return $c4;
		});

		ApplicationContext::getContainer()
			->get(Manager::class)
			->destroy([$data['user_id']],User::class);

		return $data;
	}

	/**
	 * @return string
	 */
	public function generateRecordNo()
	{
		return 'C1'.time().mt_rand(100000, 999999);
	}

	/**
	 * @param $gpc
	 * @return mixed
	 */
	public function index($gpc)
	{
		$data = $this->query($gpc)
			->selectRaw('credit1.record_no,credit1.original_amount,credit1.amount,credit1.type,credit1.balance,credit1.remark,credit1.created_at,credit1.pay_time,users.name,users.realname')
			->orderBy('credit1.id', 'desc')
			->paginate((int)$gpc['limit']);
		return $data;
	}

	/**
	 * @param $data
	 * @return
	 */
	public function query($data)
	{
		$query = Credit1::leftJoin('users', 'users.id','=','credit1.user_id');
		if (!empty($data['search'])) {
			$search = $data['search'].'%';
			$query = $query->where(function ($query)use($search) {
				$query->where('users.name','like',$search)
					->orWhere('users.realname', 'like', $search)
					->orWhere('credit1.record_no', 'like', $search);
			});
		}

		if (isset($data['type']) && ($data['type'] != '') && !is_null($data['type'])) {
			$query = $query->where('credit1.type', $data['type']);
		}

		if (isset($data['start_end']) && !empty($data['start_end'])) {
			if (!isset($data['start_end'][0]) || !isset($data['start_end'][1])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'时间格式错误');
			}
			$query = $query->whereBetween('credit1.created_at', [$data['start_end'][0], $data['start_end'][1]]);
		}

		if (!Auth::isAdmin()) {
			$query = $query->where('credit1.user_id', Auth::id());
		}
		return $query;
	}



	/**
	 * 获取与用户当前余额可直接相加减的金额
	 * @param $total_amount
	 * @param $fee
	 * @return float|int
	 */
	private function getCredit($total_amount, $fee)
	{
		if (abs($total_amount) < $fee) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'手续费不能大于申请数目');
		}
		if ($total_amount > 0) {
			$total_amount = $total_amount - $fee;
		} else {
			$total_amount = $total_amount + $fee;
		}
		return $total_amount;
	}


}
