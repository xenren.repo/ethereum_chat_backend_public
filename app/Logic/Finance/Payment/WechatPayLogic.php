<?php

declare(strict_types=1);

namespace App\Logic\Finance\Payment;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Lib\Payment\WeChatPay\Config;
use App\Lib\Payment\WeChatPay\Handler;
use App\Logic\Order\OrderLogic;
use App\Logic\Order\PayLogic;
use App\Model\Order;
use App\Model\OrderGoods;
use App\Model\ThirdPay;
use Carbon\Carbon;

class WechatPayLogic
{
    protected $config;

    /*
    * Init config
    * can add another options use:
    * $this->config->setOptions($array)
    * (can take a look options in config class, method `getDefaultOptions`)
    */
    public function __construct()
    {
        $this->getConfig();
    }

    public function webPay($body, $orderNo, $amount): string
    {
        $weChat = Handler::Mweb($this->config);
        try {
            return $weChat->getMwebUrl($body, $orderNo, $amount);
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Problem happens when trying do payment, please try again');
        }
    }

    public function appPay(string $body, string $orderNo, float $amount, array $ext = ['attach' => '']): array
    {
        if (! $this->config && empty($this->config)) {
            $this->getConfig();
        }
        $wechat = Handler::App($this->config);
        try {
            $ip = $this->getIp();
            $amt = $this->toCentFormat($amount);
            $prepay_id = $wechat->getPrepayId($body, $orderNo, $amt, $ip, $ext);
            return $wechat->getPackage($prepay_id);
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Failed to do request');
        }
    }

    public function rawPay($order, $paymentData): array
    {
        try {
            $body = 'Qia Payment';
            $order_goods = OrderGoods::query()->leftJoin('goods', 'goods.id', '=', 'order_goods.goods_id')
                ->where('order_id', $order['id'])
                ->select('goods.id as good_id', 'goods.title as good_title', 'goods.description as desc')
                ->get();
            if ($order_goods) {
                $getTitle = $order_goods->pluck('good_title')->toArray();
                $getTitle = implode(',', $getTitle);
                $body = substr($getTitle, 0, 200); // as wechat recomendation...
            }
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Failed to do request');
        }
        return [
            'body' => $body,
            'total_fee' => $order['total_amount'],
            'spbill_create_ip' => $this->getIp(),
            'fee_type' => 'CNY',
            'out_trade_no' => $paymentData['system_no'],
            'notify_url' => $this->getNotifyUrl(),
        ];
    }

    public function getConfig()
    {
        // Default config here
        $appId = env('WECHAT_APP_ID');
        $appSecret = env('WECHAT_APP_SECRET');
        $mchId = env('WECHAT_MCH_ID');
        $apiKey = env('WECHAT_API_KEY'); // leave it like this for now

        $host = env('HOST');

        $notifyUrl = $host . '/notify';
        $refundNotifyUrl = $host . '/refundnotify';

        $base_path = BASE_PATH . '/config/cert';

        if (! file_exists($base_path . '/apiclient_cert.pem') || ! file_exists($base_path . '/apiclient_key.pem')) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '证书未准备');
        }

        $config = new Config();
        $config->setAppId($appId);
        $config->setAppSecret($appSecret);
        $config->setMchId($mchId);
        $config->setApiKey($apiKey);
        $config->setNotifyUrl($notifyUrl);
        $config->setRefundNotifyUrl($refundNotifyUrl);
        $config->setOptions([
            'ssl_cert_path' => $base_path . '/apiclient_cert.pem',
            'ssl_key_path' => $base_path . '/apiclient_key.pem',
        ]);
        $this->config = $config->getOptions();
    }

    public function getIp()
    {
        $ip = ip();
        if (in_array($ip, [
            '127.0.0.1',
        ])) {
            return env('TEST_IP') ?? '127.0.0.1';
        }
        return $ip;
    }

    public function toCentFormat($amount)
    {
        $amt = (string) $amount;
        if (! function_exists('bcmul')) {
            return (int) ($amt * 100);
        }
        return bcmul($amt, '100');
    }

    public function getNotifyUrl()
    {
        $host = env('WECHAT_REGISTERED_HOST') ?? env('HOST') ?? 'https://shop.1688qia.com';
        return  $host . '/notify/wechat';
    }

    public function checkOrder($gpc, $isSystem = false)
    {
        $data = ThirdPay::query()->where('system_no', $gpc['sn'])->first();
        if (! $data) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, 'No order found');
        }
        if (! $this->config && empty($this->config)) {
            $this->getConfig();
        }
        try {
            $handler = new Handler($this->config);
            return $handler->queryOrderByOutTradeNo($gpc['sn']);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function checkWechatPayStatus()
    {
        $timer = 10;
        $selectWechatPay = ThirdPay::query()->where([
            'pay_type' => OrderLogic::WECHAT,
        ])->whereIn('status', [
            OrderLogic::UN_PAY,
        ])->limit(10)->get()->toArray();
        if (count($selectWechatPay) <= 0) {
            return true;
        }
        $failedOrderIds = [];
        $failedPayIds = [];
        $out_time = Carbon::now()->subMinutes($timer);
        foreach ($selectWechatPay as $item) {
            $response = $this->checkOrder(['sn' => $item['system_no']], true);
            if (is_null($response)) {
                $failedOrderIds[] = $item['order_id'];
                $failedPayIds[] = $item['id'];
            } else {
                if ($this->checkRequiredResponse($response) && $this->responseStat($response)) {
                    switch (strtolower($response['trade_state'])) {
                        case 'success':
                            $order = Order::find($item['order_id']);
                            make(PayLogic::class)->proccessWechatToApps($response, $order);
                            break;
                        case 'notpay':
                            if ($out_time->diffInMinutes($item['created_at'], true) > $timer) {
                                $failedOrderIds[] = $item['order_id'];
                                $failedPayIds[] = $item['id'];
                            }
                            break;
                        case 'closed':
                            $failedOrderIds[] = $item['order_id'];
                            $failedPayIds[] = $item['id'];
                            break;
                        default:
                            logger()->error($response['trade_state']);
                            break;
                    }
                }
            }
        }
        if (count($failedPayIds) > 0) {
            make(PayLogic::class)->closePay($failedPayIds, $failedOrderIds);
        }
    }

    public function checkRequiredResponse($response)
    {
        if (! isset($response['return_code']) || ! isset($response['result_code']) || ! isset($response['trade_state'])) {
            return false;
        }
        return true;
    }

    public function responseStat($response)
    {
        if (strtolower($response['return_code']) != 'success') {
            return false;
        }
        if (strtolower($response['result_code']) != 'success') {
            return false;
        }
        return true;
    }
}
