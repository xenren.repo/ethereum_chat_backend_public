<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Finance;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Exception\ParamsException;
use App\Kernel\Auth\Auth;
use App\Kernel\Common\Common;
use App\Logic\Business\BusinessLogic;
use App\Logic\Logic;
use App\Model\Recharge;
use Hyperf\DbConnection\Db;

class RechargeLogic extends Logic
{

	const CANCEL = -1;  //取消

	const AUDITING = 0;  //审核中

	const RECHARGE_SUCCESS = 1;  // 充值成功

	const RECHARGE_FAIL = 2; // 充值失败

	/**
	 * @param $gpc
	 * @return null
	 */
	public function recharge($gpc)
	{
		$this->checkTime();

		$cny_to_credit2_ratio = BusinessLogic::getCnyToCredit2Ratio();
		if (round($gpc['cny_amount'] * $cny_to_credit2_ratio, 2) != round($gpc['amount'], 2)) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'专区券兑换比例可能发生变化,请重新打开,变化');
		}

		$user = Auth::user();

		$auding_exist = Recharge::where('user_id', $user['id'])
			->where('status', self::AUDITING)
			->value('id');

		if ($auding_exist) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'存在未审核的充值记录,请等待审核完成或取消已有记录,方可再次提交');
		}

		$data = [
			'verify_code' => $gpc['hash'] ?? null,
			'type' => 0,
			'bank' => $gpc['bank'],
			'amount'  => $gpc['amount'],
			'cny_amount'  => $gpc['cny_amount'],
			'reality'  => $gpc['amount'],
			'bank_no' => $gpc['bank_no'],
			'remark' => $user['name'],
			'sender' => $gpc['sender'],
			'bank_mobile' => $gpc['bank_mobile'],
			'bank_holder' => $gpc['bank_holder'],
			'created_place' => $gpc['created_place'],
			'credit' => json_encode($gpc['credit']),
			'user_id' => $user['id'],
			'recharge_no' => $this->generateRecordNo(),
			'status'  => self::AUDITING,
			'charge_fee' => 0
		];

		return Recharge::create($data);
	}

	public function setStatus($gpc)
	{

		$query = new Recharge();

		$user = Auth::user();

		if (!Auth::isAdmin()) {
			if ($gpc['status'] != self::CANCEL) {
				throw new BusinessException(ErrorCode::PERMISSION_DENY);
			}
			$query = $query->where('user_id', $user['id']);
		}

		$recharge = $query->findOrFail($gpc['id']);

		if ($recharge['status'] == $gpc['status']) {
			return true;
		}

		if ($recharge['status'] == self::RECHARGE_SUCCESS) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'已成功的充值对应金额已发生,不能被修改状态');
		}

		Db::transaction(function () use ($gpc, $user, $recharge) {
			$update = [
				'status' => $gpc['status'],
				'remark' => $gpc['remark'] ?? '',
				'version'=> $recharge['version'] + 1
			];
			Recharge::where('id', $gpc['id'])->where('version', $recharge['version'])->update($update);
			if ($gpc['status'] == self::RECHARGE_SUCCESS) {
				$params = [
					'remark' => $recharge['recharge_no'].'充值到账',
					'credit' => $recharge['amount'],
					'type'   => Credit2Logic::RECHARGE,
					'user'   => $recharge['user_id'],
					'foreign_id' => $recharge['id']
				];
				make(Credit2Logic::class)->setCredit($params);
			}
		});
		return true;
	}


	/**
	 * @return string
	 */
	public function generateRecordNo()
	{
		return 'RC'.time().mt_rand(100000, 999999);
	}
	/**
	 * @param $gpc
	 * @return mixed
	 */
	public function index($gpc)
	{
		$data = $this->query($gpc)
			->selectRaw('recharge.*,users.name,users.realname')
			->orderBy('recharge.id', 'desc')
			->paginate((int)$gpc['limit']);
		foreach ($data['data'] as $k => $v) {
			$data['data'][$k]['credit'] = json_decode($v['credit'],true);
		}
		return $data;
	}

	/**
	 * @param $data
	 * @return
	 */
	public function query($data)
	{
		$query = Recharge::leftJoin('users', 'users.id','=','recharge.user_id');
		if (!empty($data['search'])) {
			$search = $data['search'].'%';
			$query = $query->where(function ($query)use($search) {
				$query->where('users.name','like',$search)
					->orWhere('users.realname', 'like', $search)
					->orWhere('recharge.recharge_no', 'like', $search);
			});
		}

		if (isset($data['type']) && ($data['type'] != '') && !is_null($data['type'])) {
			$query = $query->where('recharge.type', $data['type']);
		}

		if (isset($data['start_end']) && !empty($data['start_end'])) {
			if (!isset($data['start_end'][0]) || !isset($data['start_end'][1])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'时间格式错误');
			}
			$query = $query->whereBetween('recharge.created_at', [$data['start_end'][0], $data['start_end'][1]]);
		}

		if (!Auth::isAdmin()) {
			$query = $query->where('recharge.user_id', Auth::id());
		}
		return $query;
	}


	public function checkTime()
	{
		$time = date('H:i:s');
		$start_end = Common::getSetting('business.recharge_time', ['09:00:00','22:00:00']);
		if ($time > $start_end[1] || $time < $start_end[0]) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'请在'.$start_end[0].'- '.$start_end[1].' 提交');
		}
	}



}
