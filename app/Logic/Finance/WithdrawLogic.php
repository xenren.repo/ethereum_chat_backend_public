<?php

declare(strict_types=1);

namespace App\Logic\Finance;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Admin\AdminLogic;
use App\Logic\Logic;
use App\Model\Admin;
use App\Model\User;
use App\Model\Withdraw;

class WithdrawLogic extends Logic
{
    /**
     * @param $gpc
     * @return mixed
     */
    public function index($gpc)
    {
        $admin = Admin::where('id', '=', Auth::id())->first();
        if ($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) {
            $data = Withdraw::leftJoin('admin', 'admin.id', '=', 'withdraw.user_id')
                ->where('user_id', '=', Auth::id())
                ->select('withdraw.id', 'withdraw.type' , 'admin.name', 'admin.credit1', 'withdraw.amount', 'withdraw.status', 'withdraw.approved_by', 'invoice', 'realname', 'img_url', 'withdraw.created_at', 'withdraw.bank_no', 'withdraw.bank_account_no', 'withdraw.bank_name', 'withdraw.bank_holder');
        } else {
            $data = Withdraw::leftJoin('admin', 'admin.id', '=', 'withdraw.user_id')
                ->select('withdraw.id', 'withdraw.type' ,  'admin.name', 'admin.credit1', 'withdraw.amount', 'withdraw.status', 'withdraw.approved_by', 'invoice', 'realname', 'img_url', 'withdraw.created_at', 'withdraw.bank_no', 'withdraw.bank_account_no', 'withdraw.bank_name', 'withdraw.bank_holder');
        }
        if (isset($gpc['start_end']) && ! empty($gpc['start_end'])) {
            if (! isset($data['gpc'][0]) || ! isset($data['gpc'][1])) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, '时间格式错误');
            }
            $data = $data->whereBetween('withdraw.created_at', [$gpc['start_end'][0], $gpc['start_end'][1]]);
        }
        return $data->paginate($gpc['limit']);
        //$data = Withdraw::all();
    }

    public function approveWithdraw($gpc)
    {
        $admin = Admin::where('id', '=', Auth::id())
            ->where('group_id', '=', AdminLogic::GROUP_ID_ADMIN)
            ->first();
        if (! $admin) {
            throw new BusinessException(ErrorCode::PERMISSION_DENY, 'permission deny');
        }

        $withdraw_data = withdraw::where('id', $gpc['id'])->first();
        if ($withdraw_data->status == 1) {
            throw new BusinessException(ErrorCode::SYSTEM, '已经批准');
        }
        $feePercent = 0.01; //1%
        $totalPayment = $withdraw_data->amount;
        $totalFee = $totalPayment * $feePercent;
        $withdrawerGet = $totalPayment - $totalFee;

        $withdraw = Withdraw::where('id', $gpc['id'])->update([
            'status' => 1,
            'approved_by' => Auth::id(),
            'img_url' => $gpc['img_url'],
            'amount' => $withdrawerGet,
        ]);

        $seller = Admin::where('id', $withdraw_data->user_id)->first();

        Withdraw::create([
            'user_id' => $seller->id,
            'amount' => -$totalFee,
            'approved_by' => $admin->id,
            'status' => 1,
            'invoice' => $withdraw_data->invoice,
            'img_url' => $withdraw_data->img_url,
            'bank_name' => $withdraw_data->bank_name,
            'bank_account_no' => $withdraw_data->bank_account_no,
            'bank_no' => $withdraw_data->bank_no,
            'bank_holder' => $withdraw_data->bank_holder,
            'created_at' => $withdraw_data->created_at,
            'updated_at' => $withdraw_data->created_at,
        ]);

        //		$seller = Admin::where('id', $withdraw_data->user_id)->first();

        $update_amount = $seller->credit1 - $withdraw_data->amount;

        if ($update_amount < 0) {
            throw new BusinessException(ErrorCode::SYSTEM, '负数');
        }
        Admin::where('id', $withdraw_data->user_id)->update([
            'credit1' => $update_amount,
        ]);

        if ($withdraw) {
            return [
                'status' => 'approved',
                'id' => $gpc['id'],
            ];
        }
    }

    public function withdraw($gpc)
    {
        $seller = Admin::where('id', '=', Auth::id())
            ->where('group_id', '=', AdminLogic::GROUP_ID_SELLER)
            ->first();
        if (! $seller) {
            throw new BusinessException(ErrorCode::PERMISSION_DENY, 'permission deny');
        }

        $credit1 = $seller->credit1;
        $seller_credit1 = $credit1 - $gpc['amount'];
        if ($seller_credit1 < 0) {
            throw new BusinessException(ErrorCode::SYSTEM, '余额不足');
        }

        $no_inv = $this->generateNo();
        $data = [
            'user_id' => Auth::id(),
            'amount' => $gpc['amount'],
            'status' => 0,
            'invoice' => $no_inv,
            'img_url' => '',
            'bank_name' => $gpc['bank_name'],
            'bank_no' => $gpc['bank_no'],
            'bank_account_no' => $gpc['bank_account_no'],
            'bank_holder' => $gpc['bank_holder'],
        ];

        Withdraw::create($data);

        return [
            'status' => 'success',
        ];
    }

    public function uploadImage($gpc)
    {
        $withdraw_data = withdraw::where('id', $gpc['id'])->first();

        $withdraw = Withdraw::where('id', $gpc['id'])->update([
            'img_url' => 1,
            'approved_by' => Auth::id(),
        ]);
    }

    public function generateNo()
    {
        return 'WT' . time() . mt_rand(10, 50);
    }

    public function generateRecordNo()
    {
        return 'C1' . time() . mt_rand(100000, 999999);
    }
}
