<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Goods;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Common\Helper;
use App\Logic\Logic;
use App\Kernel\Auth\Auth;
use App\Model\GoodsCategory;
use App\Model\MulGoodsCategory;

class GoodsCategoryLogic extends Logic
{

	/**
	 * @return bool|mixed|string
	 */
	public function pcIndex()
	{
		$query = GoodsCategory::selectRaw('id,name,pid,image_url,sort,type,status')
				->orderBy('sort', 'desc');

			if(Auth::isSeller()) { 
				$data= $query->whereNotIn('name',["牡丹专区", "专区", "自有商品","限时秒杀" ])->get()->toArray();
			}
			else { 
				$data= $query->get()->toArray();
			}
			
			foreach ($data as $k => $v) {
				foreach ($v as $key => $vo) {
					if (is_numeric($vo)) {
						$data[$k][$key] = (string)$vo;
					}
				}
			}
			$data = Helper::sortLevel($data);
			return $data;
		// return cache()->remember('goods_category_pc_all', function () {	
		// });

	}

	/**
	 * @return bool|mixed|string
	 */
	public function index()
	{
//		return cache()->remember('goods_category_all', function () {
			$data = GoodsCategory::where('status', 0)
				->selectRaw('id,name,pid,image_url,sort,type,status')
                ->where('name', '!=', '专区')
				->orderBy('sort', 'desc')->get()->toArray();
			foreach ($data as $k => $v) {
				foreach ($v as $key => $vo) {
					if (is_numeric($vo)) {
						$data[$k][$key] = (string)$vo;
					}
				}
			}
			$data = Helper::sortLevel($data);
			return $data;
//		});
	}

	/**
	 * @param int $type
	 * @return bool|mixed|string
	 */
	public function platJson()
	{
		return cache()->remember('goods_category_plat_json', function () {
			$data = GoodsCategory::where('status', 0)
				->selectRaw('id,name,pid,image_url,sort,type,status')->get()->toArray();
			foreach ($data as $k => $v) {
				foreach ($v as $key => $vo) {
					if (is_numeric($vo)) {
						$data[$k][$key] = (string)$vo;
					}
				}
			}
			return $data;
		},86400);
	}

	/**
	 * @param $id
	 * @return bool|mixed|string
	 */
	public function detail($id)
	{
		return
            cache()->remember('goods_category_detail_'.$id, function () use ($id) {
			$data = GoodsCategory::findOrFail($id)->toArray();
			foreach ($data as $k => $v) {
				if (is_numeric($v)) {
					$data[$k] = (string)$v;
				}
			}
			return $data;
		});
	}

	/**
	 * @param $gpc
	 * @return string
	 */
	public function store($gpc)
	{
		$this->validate($gpc);
		$res = GoodsCategory::create($gpc);
		$this->clearCache();
		return $res;
	}

	/**
	 * @param $gpc
	 * @return string
	 */
	public function update($gpc)
	{
		$this->validate($gpc);
		$goods_category = GoodsCategory::findOrFail($gpc['id']);
		$res = $goods_category->update($gpc);
		$this->clearCache();
		return $res;
	}


	/**
	 * @param $gpc
	 * @return string
	 */
	public function delete($gpc)
	{
		foreach ($gpc['id'] as $id) {
			$exist = GoodsCategory::where('pid', $id)->value('id');
			if ($exist) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'存在子级分类,请先删除子级分类');
			}
			$exist = MulGoodsCategory::where('cid', $id)->value('id');
			if ($exist) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'分类下存在产品,不能删除');
			}
		}
		$res = GoodsCategory::whereIn('id', $gpc['id'])->delete();
		$this->clearCache();
		return $res;
	}

	/**
	 * @param $data
	 */
	public function validate($data)
	{
		if (isset($data['pid']) && $data['pid']) {
			if (isset($data['id']) && $data['id'] == $data['pid']) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'父级不能是自己');
			}
			$parent = GoodsCategory::find($data['pid']);
			if (empty($parent)) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'分类不存在');
			}
			if ($parent['pid']) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'只支持到二级分类');
			}
		}
	}

	/**
	 * @param array $ids
	 */
	public static function checkExistCategory(array $ids)
	{
		if ($ids) {
			$count = GoodsCategory::whereIn('id', $ids)->count();
			if (count($ids) > $count) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'分类不存在');
			}
		}
	}

	/**
	 * 缓存清除
	 */
	public function clearCache()
	{
		cache()->clearPrefix('goods_category');
	}

	/**
	 * 获取分类id,包含子级的id,最多支持包含自身层级总共三级
	 * @param $id
	 * @return bool|mixed|string
	 */
	public function getChildIds($id)
	{
		if (empty($id) || !is_numeric($id)) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'分类参数错误');
		}
//		return cache()->remember('goods_category.child_ids_'.$id, function () use ($id) {
			$level1_ids = GoodsCategory::where('pid', $id)->pluck('id')->toArray();
			$res = [$id];
			if ($level1_ids) {
				$res = array_merge($res, $level1_ids);
				$level2_ids = GoodsCategory::whereIn('pid', $level1_ids)->pluck('id')->toArray();
				if ($level2_ids) {
					$res = array_merge($res, $level2_ids);
				}
			}
			return $res;
//		});
	}
}
