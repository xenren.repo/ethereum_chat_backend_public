<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Goods;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Kernel\Common\Helper;
use App\Logic\Admin\AdminLogic;
use App\Logic\Logic;
use App\Logic\Order\OrderLogic;
use App\Model\Admin;
use App\Model\Goods;
use App\Model\GoodsComment;
use App\Model\GoodsCommentReply;
use App\Model\Order;
use App\Model\OrderGoods;
use App\Model\User;
use Hyperf\DbConnection\Db;

class GoodsCommentLogic extends Logic
{

	/**
	 * @param $gpc
	 * @return mixed
	 */
	public function index($gpc)
	{
		$data = $this->query($gpc)
			->selectRaw('orders.order_no,orders.user_id,goods_comments.*')
			->orderBy('goods_comments.id', 'desc');

        $admin = Admin::where('id', '=', Auth::id())->first();
				if ($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) 
				{
						$goodsIds  = Goods::where('created_by', '=', Auth::id())->pluck('id');
            $orders = OrderGoods::whereIn('goods_id', $goodsIds)->pluck('order_id');
            $data = $data->whereIn('orders.id', $orders);
        }

		$data = $data->paginate((int)$gpc['limit'])->toArray();
		foreach ($data['data'] as $k => $v) {
			$data['data'][$k]['images'] = json_decode($v['images'], true);
		}
		return $data;
	}

	/**
	 * @param $data
	 * @return
	 */
	public function query($data)
	{
		$query = GoodsComment::leftJoin('orders', 'goods_comments.order_id','=','orders.id');

		if (!empty($data['search'])) {
			$search = '%' . $data['search'] . '%';
			$query  = $query->where('orders.order_no', 'like', $search);
		}

		if (isset($data['status']) && ($data['status'] != '') && !is_null($data['status'])) {
			$query = $query->where('goods_comments.status', $data['status']);
		}

		if (isset($data['start_end']) && !empty($data['start_end'])) {
			if (!isset($data['start_end'][0]) || !isset($data['start_end'][1])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'时间格式错误');
			}
			$query = $query->whereBetween('goods_comments.created_at', [$data['start_end'][0], $data['start_end'][1]]);
		}

		return $query;
	}

	/**
	 * @param $gpc
	 * @return bool
	 */
	public function setStatus($gpc)
	{
		if (!is_array($gpc['id'])) {
			$gpc['id'] = [$gpc['id']];
		}
		foreach ($gpc['id'] as $id) {
			$goods_comments = GoodsComment::findOrFail($id);
			if ($gpc['status'] == $goods_comments['status']) {
				continue;
			}
			$goods_comments->update(['status'=>$gpc['status']]);
		}
		return true;
	}

	/**
	 * @param $gpc
	 * @throws BusinessException
	 */
	public function comment($gpc)
	{
		$query = Order::where('id', $gpc['order_id']);
		if (!Auth::isAdmin()) {
			$query = $query->where('user_id', Auth::id());
		}
		$order = $query->firstOrFail();
		$user = User::findOrFailFromCache($order['user_id']);
		if ($order['status'] == OrderLogic::FINISH) {
			$order_goods = OrderGoods::where('order_id', $order['id'])
				->where('id', $gpc['order_goods_id'])
				->first();
			if (empty($order_goods)) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'无权评价该产品');
			}
			if ($order_goods['is_comment']) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'已经评价过该产品');
			}
			$data = [
				'order_id' => $order['id'],
				'order_goods_id' => $gpc['order_goods_id'],
				'goods_id' => $order_goods['goods_id'],
				'nickname' => Helper::maskStr($user['name'], 1, 1),
				'score' => $gpc['score'],
				'content' => $gpc['content'],
				'pid' => $gpc['pid'] ?? 0,
				'images' => json_encode($gpc['images']),
			];
			// 评价默认通过审核
			$data['status'] = 1;
			Db::transaction(function () use ($data,$order) {
				GoodsComment::insert($data);
				OrderGoods::where('id', $data['order_goods_id'])->update(['is_comment'=>1]);
				$un_comment_order_goods =  OrderGoods::where('order_id', $order['id'])
					->where('is_comment', 0)
					->value('id');
				if (empty($order['is_comment']) && empty($un_comment_order_goods)) {
					Order::where('id', $order['id'])->update(['is_comment'=>1]);
				}
			});
		} else {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'该订单还不能进行评价');
		}
	}

	/**
	 * @param $gpc
	 * @return mixed
	 */
	public function getComment($gpc)
	{
		$field = 'id,order_id,goods_id,pid,nickname,avatar,content,images,sort,type,created_at';
		$query = GoodsComment::where('goods_id', $gpc['goods_id'])
			->where('status', 1)
			->selectRaw($field)
			->orderBy('sort', 'desc')
			->paginate((int)$gpc['limit']);
		$goods_comment_ids = $query->pluck('id');
		$commentReply = GoodsCommentReply::query()
			->leftJoin('admin','goods_comment_replies.user_id','=','admin.id')
			->whereIn('goods_comment_id',$goods_comment_ids)
			->selectRaw('goods_comment_id,content,goods_comment_replies.img,name,realname,goods_comment_replies.created_at')
			->get();
		$data = $query->toArray();

		foreach ($data['data'] as $k => $v) 
		{
			$data['data'][$k]['images'] = json_decode($v['images'], true);
			$item = $commentReply->where('goods_comment_id',$v['id']);
			$reply = $item->first();
			if (!is_null($reply) && !empty($reply))
			{
				$reply = [
					'goods_comment_id' => $reply['goods_comment_id'],
					'content' => $reply['content'],
					'nickname' => $reply['realname'],
					'created_at' => $reply['created_at'],
					'img' => (!is_null($reply['img']))?json_decode($reply['img'], true):null,
				];
				$data['data'][$k]['replies'] = $reply;
			} else
			{
				$data['data'][$k]['replies'] = null;
			}
		}
		return $data;
	}

	/**
	 * @param $gpc
	 * @return bool
	 */
	public function reply($gpc)
	{
		$userId = Auth::id();
		if (!is_array($gpc['id'])) {
			$gpc['id'] = [$gpc['id']];
		}
		$checkIsExist = GoodsCommentReply::query()->whereIn('goods_comment_id',$gpc['id'])->count();
		if ($checkIsExist > 0) 
		{
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'已有人回复该留言');
		}
		Db::transaction(function() use($userId,$gpc) {
			foreach ($gpc['id'] as $id) 
			{
				GoodsCommentReply::insert([
					'goods_comment_id' => $id,
					'user_id' => $userId,
					'img' => json_encode($gpc['img']),
					'content' => $gpc['content'],
				]);
			}	
		});
		return true;
	}

}
