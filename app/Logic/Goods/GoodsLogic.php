<?php

declare(strict_types=1);

namespace App\Logic\Goods;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Kernel\Common\Helper;
use App\Lib\Goods\DingDanXia;
use App\Lib\Goods\HaoDanKu;
use App\Logic\Admin\AdminLogic;
use App\Logic\Logic;
use App\Logic\Order\OrderLogic;
use App\Model\Admin;
use App\Model\Goods;
use App\Model\GoodsOption;
use App\Model\GoodsParam;
use App\Model\GoodsSpec;
use App\Model\GoodsSpecItem;
use App\Model\MulGoodsCategory;
use App\Model\Order;
use App\Model\OrderGoods;
use App\Model\Setting;
use App\Model\User;
use Hyperf\DbConnection\Db;

class GoodsLogic extends Logic
{
    /**
     * 列表.
     * @param $gpc
     * @return
     */
    public function index($gpc)
    {
        $admin = Admin::where('id', '=', Auth::id())->first();
        $data = $this->query($gpc)->selectRaw('goods.id,title,images,price,sale_num,is_recommend,is_new,ship_free,stock,on_sale,original_price,is_hot,source_from')
            ->orderBy('sort', 'desc')
            ->orderBy('id', 'desc');

        $data->leftJoin('admin', 'goods.created_by', '=', 'admin.id');
        $data->addSelect(Db::raw("IFNULL(admin.name,'admin') as seller_name"));

        if ($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) {
            $data = $data->where('created_by', '=', $admin['id']);
        }

        $data = $data->paginate((int) $gpc['limit'])
            ->toArray();
        foreach ($data['data'] as $k => $v) {
            $data['data'][$k]['images'] = json_decode($v['images'], true);
        }
        return $data;
    }

    /**
     * @param $gpc
     * @return mixed
     */
    public function search($gpc)
    {
        $data = $this->query($gpc)
            ->selectRaw('id,title,images,price, price as np, sale_num,is_recommend,is_new,ship_free,stock,on_sale,original_price')
            ->orderBy('id', 'desc')
            ->paginate((int) $gpc['limit'])->toArray();
        foreach ($data['data'] as $k => $v) {
            $data['data'][$k]['images'] = json_decode($v['images'], true);
        }
        return $data;
    }

    /**
     * @param $data
     * @return Goods
     */
    public function query($data)
    {
        $query = new Goods();
        if (! empty($data['search'])) {
            $search = '%' . $data['search'] . '%';
            $query = $query->where('title', 'like', $search);
        }
        if (Auth::isLogin() && Auth::isAdmin()) {
            if (isset($data['on_sale']) && ($data['on_sale'] != '') && ! is_null($data['on_sale'])) {
                $query = $query->where('on_sale', $data['on_sale']);
            }
        } else {
            $query = $query->where('on_sale', 1);
        }
        if (isset($data['is_recommend']) && ($data['is_recommend'] != '') && ! is_null($data['is_recommend'])) {
            $query = $query->where('is_recommend', $data['is_recommend']);
        }
        if (isset($data['is_new']) && ($data['is_new'] != '') && ! is_null($data['is_new'])) {
            $query = $query->where('is_new', $data['is_new']);
        }
        if (isset($data['is_hot']) && ($data['is_hot'] != '') && ! is_null($data['is_hot'])) {
            $query = $query->where('is_hot', $data['is_hot']);
        }
        if (isset($data['ship_free']) && ($data['ship_free'] != '') && ! is_null($data['ship_free'])) {
            $query = $query->where('ship_free', $data['ship_free']);
        }
        if (! empty($data['cid'])) {
            $ids = make(GoodsCategoryLogic::class)->getChildIds($data['cid']);
            $goods_ids = MulGoodsCategory::whereIn('cid', $ids)->pluck('goods_id')->toArray();
            if (empty($goods_ids)) {
                $goods_ids = [-1];
            } else {
                $goods_ids = array_unique($goods_ids);
            }
            $query = $query->whereIn('id', $goods_ids);
        }

        if (isset($data['sort']) && $data['sort']) {
            switch ($data['sort']) {
                case 4:   // 按销量排序
                    $query = $query->orderBy('sale_num', 'desc');
                    break;
                case 1:   // 价格低到高
                    $query = $query->orderBy('price', 'asc');
                    break;
                case 2:   // 价格高到低
                    $query = $query->orderBy('price', 'desc');
                    break;
            }
        }

        if (isset($data['price_min']) && ($data['price_min'] != '') && ! is_null($data['price_min'])) {
            $query = $query->where('price', '>=', $data['price_min']);
        }

        if (isset($data['price_max']) && ($data['price_max'] != '') && ! is_null($data['price_max'])) {
            $query = $query->where('price', '<=', $data['price_max']);
        }

        if (isset($data['filter_creator_by'])) {
            switch ($data['filter_creator_by']) {
                case '1':
                    $query = $query->where('created_by', '=', '0');
                    break;
                case '2':
                    $query = $query->where('created_by', '>', '0');
                    break;
            }
        }

        return $query;
    }

    /**
     * @param $id
     * @param null|mixed $userName
     * @return
     */
    public function show($id, $userName = null)
    {
        if (empty($id) || ! is_numeric($id)) {
            throw new BusinessException();
        }
        //		return cache()->remember('goods_goods_id_' . $id, function () use ($id,$userName) {
        $field = 'id,title,description,receipt_required,images,price,item_endprice,created_by,min_item_endprice,content,original_price,cost_price,sort,sale_num,stock,on_sale,type,is_recommend,is_new,ship_free,unit,weight,cupon_url,item_url,commission,is_commission,is_hot,source_from,version,item_id,express_cost,total_limit, reward_mode';
        $data = Goods::where('id', $id)->selectRaw($field)->firstOrFail()->toArray();
        if ($data['created_by'] !== 0) {
            $seller = Admin::where('id', $data['created_by'])->firstOrFail()->toArray();
            $data['seller_name'] = $seller['name'];
        }
        foreach ($data as $k => $v) {
            if (is_numeric($v)) {
                $data[$k] = (string) $v;
            }
        }
        $data['images'] = json_decode($data['images'], true);

        $data['category'] = MulGoodsCategory::where('goods_id', $id)->pluck('cid')->toArray();
        $category_info = [];
        if ($data['category']) {
            $category_logic = make(GoodsCategoryLogic::class);
            foreach ($data['category'] as $cid) {
                $cate_info = $category_logic->detail($cid);
                if ($cate_info) {
                    $category_info[] = [
                        'id' => $cate_info['id'],
                        'name' => $cate_info['name'],
                    ];
                } else {
                    throw new BusinessException(ErrorCode::PARAMS_INVALID, '分类id缺失');
                }
            }
        }
        $data['category_info'] = $category_info;
        $goodOptions = GoodsOption::where('goods_id', $id)
            ->where('price', '>', 0)
            ->selectRaw('id,title,goods_id,images,price,specs,stock,sort,original_price,cost_price,goods_sn,unit,weight,version')
            ->orderBy('sort', 'desc')
            ->get()->toArray();
        $data['options']['options'] = $goodOptions;
        foreach ($data['options']['options'] as $k => $v) {
            $data['options']['options'][$k]['specs'] = json_decode($v['specs'], true);
            if (empty($data['options']['options'][$k]['images'])) {
                $data['options']['options'][$k]['images'] = $data['images'][0] ?? '';
            }
        }
        $res = [];
        if (! empty($goodOptions)) {
            $res = GoodsSpec::where('goods_id', $id)->selectRaw('id,title,goods_id,sort')
                ->orderBy('sort', 'desc')
                ->get()->toArray();
        }
        $data['options']['spec'] = $res;
        foreach ($data['options']['spec'] as $k => $v) {
            $data['options']['spec'][$k]['spec_item'] = GoodsSpecItem::where('spec_id', $v['id'])
                ->selectRaw('id,title,spec_id,sort')
                ->orderBy('sort', 'desc')
                ->get()->toArray();
        }
        $data['params'] = GoodsParam::where('goods_id', $id)->selectRaw('id,title,value,sort')
            ->orderBy('sort', 'desc')
            ->get()
            ->toArray();
        $canBuyMoreThenOne = true;
        foreach ($data['category_info'] as $k => $v) {
            if ($v['id'] == 412 || $v['id'] == 413) {
                $canBuyMoreThenOne = false;
            }
        }
        $data['canBuyMoreThenOne'] = $canBuyMoreThenOne;
        $data['can_purchase_this_good'] = true; //for category id 412 and 413
        if (! is_null($userName)) {
            $user = User::where('name', '=', $userName)->first();
            $orders = Order::where('user_id', '=', $user->id)->get();
            $orderLogic = new OrderLogic();
            $arr = [];
            foreach ($orders as $k => $v) {
                $orderId = $v->id;
                $order['order_goods'] = OrderGoods::where('order_id', $orderId)->get();
                foreach ($order['order_goods'] as $k1 => $v1) {
                    $category = MulGoodsCategory::where('goods_id', $v1['goods_id'])->first();
                    $categoryId = is_null($category) ? 0 : $category['cid'];
                    $order['order_goods'][$k1]['good_category'] = $categoryId;
                    if ($categoryId == 412 || $categoryId == 413) {
                        foreach ($data['category_info'] as $k2 => $v2) {
                            if ($v2['id'] == 412 || $v2['id'] == 413) {
                                $data['can_purchase_this_good'] = false;
                            }
                        }
                    }
                }
                $orders[$k]['detail'] = $order;
            }
            $data['orders'] = $orders;
        }

        return $data;
        //		});
    }

    /**
     * @param $data
     * @return string
     */
    public function store($data)
    {
        $data['express_cost'] = $data['express_cost'] ?? 0.00;

        $options = $data['options'] ?? [];
        $params = $data['params'] ?? [];
        $category = $data['category'] ?? [];

        if (isset($options['options']) && $options['options']) {
            $stock = 0;
            foreach ($options['options'] as $v) {
                $stock += $v['stock'] ?? 0;
            }
            $data['stock'] = $stock;
        }

        if (is_array($data['images'])) {
            $data['images'] = json_encode($data['images']);
        }

        if ($data['ship_free']) {
            $data['express_cost'] = 0;
        }

        $admin = Admin::where('id', '=', Auth::id())->first();
        $createdBy = 0;
        if ($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) {
            $createdBy = Auth::id();
        }

        return Db::transaction(function () use ($data, $options, $params, $category,$createdBy) {
            $update = [
                'title' => $data['title'],
                'description' => $data['description'] ?? '',
                'images' => $data['images'],
                'price' => $data['price'],
                'content' => $data['content'],
                'original_price' => $data['original_price'],
                'cost_price' => $data['cost_price'],
                'express_cost' => $data['express_cost'],
                'sort' => $data['sort'],
                'sale_num' => $data['sale_num'],
                'stock' => $data['stock'],
                'on_sale' => $data['on_sale'],
                'type' => $data['type'],
                'is_recommend' => $data['is_recommend'],
                'is_commission' => $data['is_commission'],
                'is_new' => $data['is_new'],
                'is_hot' => $data['is_hot'],
                'ship_free' => $data['ship_free'],
                'unit' => $data['unit'],
                'source_from' => $data['source_from'],
                'item_url' => $data['item_url'] ?? '',
                'cupon_url' => $data['cupon_url'] ?? '',
                'weight' => $data['weight'] ?: 0,
                'detail_update' => $data['detail_update'] ?? 0,
                'total_limit' => $data['total_limit'] ?? 0,
                'receipt_required' => $data['receipt_required'] ?? 0,
                'reward_mode' => $data['reward_mode'] ?: 0,
                'created_by' => $createdBy,
            ];

            $goods = Goods::create($update)->toArray();
            $options['category'] = $data['category'];
            $this->dealGoodsOptions($options, $goods);

            $this->dealGoodsParams($params, $goods);

            $this->dealGoodsCategory($category, $goods);

            return $goods;
        });
    }

    public function inCategory($categories)
    {
        $cat = Setting::where('name', 'like', '%categories_price_%')->get()->pluck('key')->toArray();
        $inArr = false;
        foreach ($categories as $k => $v) {
            if (in_array($v, $cat)) {
                $inArr = true;
            }
        }
        return $inArr;
    }

    /**
     * @param $goods
     */
    public function dealGoodsParams(array $params, $goods)
    {
        $params_ids = [-1];

        foreach ($params as $k => $v) {
            $params[$k]['goods_id'] = $goods['id'];
            $update = [
                'title' => $v['title'],
                'goods_id' => $goods['id'],
                'value' => $v['value'],
                'sort' => $v['sort'],
            ];
            if (isset($v['id']) && $v['id']) {
                GoodsParam::where('id', $v['id'])->update($update);
            } else {
                $v['id'] = GoodsParam::insertGetId($update);
            }
            $params_ids[] = $v['id'];
        }
        GoodsParam::where('goods_id', $goods['id'])->whereNotIn('id', $params_ids)->delete();
    }

    /**
     * @param $data
     * @return string
     */
    public function update($data)
    {
//        echo var_dump('$data[\'price\']');
//        echo var_dump($data['price']);
        if (! isset($data['id']) || empty($data['id'])) {
            throw new BusinessException();
        }
        $data['express_cost'] = $data['express_cost'] ?? 0.00;
        $goods = Goods::findOrFail($data['id']);
        $data['version'] = $goods['version'] + 1;

        $options = $data['options'] ?? [];
        $params = $data['params'] ?? [];
        $category = $data['category'] ?? [];

        unset($data['category'], $data['options'], $data['params']);

        if (isset($options['options']) && $options['options']) {
            $stock = 0;
            foreach ($options['options'] as $v) {
                $stock += $v['stock'] ?? 0;
            }
            $data['stock'] = $stock;
        }

        if (is_array($data['images'])) {
            $data['images'] = json_encode($data['images']);
        }
        if ($data['ship_free']) {
            $data['express_cost'] = 0;
        }
        //		echo var_dump('$data[\'price\']');
        //		echo var_dump($data['price']);
        return Db::transaction(function () use ($data, $options, $params, $category) {
            $update = [
                'title' => $data['title'],
                'description' => $data['description'],
                'images' => $data['images'],
                'price' => $data['price'],
                'content' => $data['content'],
                'original_price' => $data['original_price'],
                'cost_price' => $data['cost_price'],
                'express_cost' => $data['express_cost'],
                'sort' => $data['sort'],
                'sale_num' => $data['sale_num'],
                'stock' => $data['stock'],
                'on_sale' => $data['on_sale'],
                'type' => $data['type'],
                'is_recommend' => $data['is_recommend'],
                'is_new' => $data['is_new'],
                'is_hot' => $data['is_hot'],
                'is_commission' => $data['is_commission'],
                'ship_free' => $data['ship_free'],
                'unit' => $data['unit'],
                'source_from' => $data['source_from'],
                'total_limit' => $data['total_limit'],
                'weight' => $data['weight'] ?: 0,
                'item_endprice' => $data['item_endprice'],
                'min_item_endprice' => $data['min_item_endprice'],
                'receipt_required' => $data['receipt_required'],
                'reward_mode' => $data['reward_mode'] ?: 0,
            ];

            $goods = Goods::findOrFail($data['id']);
            $goods->update($update);
            $goods = $goods->toArray();
            $options['category'] = $category;
            $this->dealGoodsOptions($options, $goods);

            $this->dealGoodsParams($params, $goods);

            $this->dealGoodsCategory($category, $goods);

            $this->clearCache($data['id']);

            return $goods;
        });
    }

    /**
     * @param $data
     * @return string
     */
    public function delete($data)
    {
        return Db::transaction(function () use ($data) {
            $count = 0;
            foreach ($data['id'] as $id) {
                $goods = Goods::find($id);
                if (empty($goods)) {
                    continue;
                }
                Goods::where('id', $id)->delete();
                MulGoodsCategory::where('goods_id', $id)->delete();
                GoodsOption::where('goods_id', $id)->delete();
                $spec_ids = GoodsSpec::where('goods_id', $id)->pluck('id')->toArray();
                foreach ($spec_ids as $spec_id) {
                    GoodsSpecItem::where('spec_id', $spec_id)->delete();
                }
                GoodsSpec::where('goods_id', $id)->delete();
                GoodsParam::where('goods_id', $id)->delete();
                $this->clearCache($id);
                ++$count;
            }
            return $count;
        });
    }

    /**
     * @param $gpc
     */
    public function setStatus($gpc)
    {
        Db::transaction(function () use ($gpc) {
            foreach ($gpc['id'] as $id) {
                Goods::where('id', $id)->update([$gpc['type'] => $gpc['value']]);
                $this->clearCache($id);
            }
        });
    }

    /**
     * 缓存清除.
     * @param int $id
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function clearCache($id = 0)
    {
        if ($id && is_numeric($id)) {
            cache()->delete('goods_goods_id_' . $id);
        } else {
            cache()->clearPrefix('goods');
        }
        cache()->delete('goods_today_recommend');
        cache()->delete('goods_hot_goods');
    }

    /**
     * @param $goods_id
     * @param int $option_id
     * @return mixed
     */
    public function getGoodsStock($goods_id, $option_id = 0)
    {
        if (empty($goods_id) || ! is_numeric($goods_id)) {
            throw new BusinessException();
        }
        if (empty($option_id)) {
            $stock = Goods::where('id', $goods_id)->value('stock');
        } else {
            $stock = GoodsOption::where('id', $option_id)->where('goods_id', $goods_id)->value('stock');
        }
        return $stock ?? 0;
    }

    /**
     * @param $goods_id
     * @param int $option_id
     * @param int $num 扣减数量可为正,可为负,为负的场景为关闭订单时,库存恢复
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function updateStocks($goods_id, $option_id = 0, $num = 1)
    {
        if ($goods_id && $num && is_numeric($goods_id) && is_numeric($num)) {
            if (empty($option_id)) {
                $goods = $this->show($goods_id);
                $query = Goods::where('id', $goods_id)->where('version', $goods['version']);
            } else {
                $goods = $this->getOptionInfo($goods_id, $option_id);
                $query = GoodsOption::where('id', $option_id)->where('version', $goods['version']);
            }
            $less = $goods['stock'] - $num;
            if ($less < 0) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, '库存不足');
            }
            $update = [
                'stock' => $less,
                'version' => $goods['version'] + 1,
            ];
            $count = $query->update($update);
            if ($count == 0) {
                throw new BusinessException(ErrorCode::VERSION_UPDATE);
            }
            $this->clearCache($goods_id);
            return null;
        }
        throw new BusinessException();
    }

    /**
     * @return mixed
     */
    public function todayRecommend()
    {
        return cache()->remember('goods_today_recommend', function () {
            $data = Goods::where('is_recommend', 1)->where('on_sale', 1)
                ->selectRaw('id,title,images,price,sale_num,is_recommend,is_new,ship_free,stock,on_sale')
                ->limit(8)
                ->orderBy('sort', 'desc')
                ->get()
                ->toArray();
            foreach ($data as $k => $v) {
                $data[$k]['images'] = json_decode($v['images'], true);
            }
            return $data;
        });
    }

    /**
     * @return mixed
     */
    public function hotGoods()
    {
        return cache()->remember('goods_hot_goods', function () {
            $data = Goods::where('is_hot', 1)->where('on_sale', 1)->selectRaw('id,title,images,price,sale_num,is_recommend,is_new,ship_free,stock,on_sale')->limit(8)->orderBy('sort', 'desc')->get()->toArray();
            foreach ($data as $k => $v) {
                $data[$k]['images'] = json_decode($v['images'], true);
            }
            return $data;
        });
    }

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function pullRecommendGoods()
    {
        $data = make(HaoDanKu::class)->todayRecommend();
        if (isset($data['item_info'])) {
            Db::transaction(function () use ($data) {
                Goods::whereNotNull('item_id')->where('is_recommend', 1)->delete();
                foreach ($data['item_info'] as $v) {
                    $this->dealGoodsInfo($v, true);
                }
                cache()->delete('goods_today_recommend');
            });
            return count($data['item_info']);
        }
        return 0;
    }

    /**
     * @param $goods
     * @param int $is_recommend
     * @return array
     */
    public function parseGoodsInfo($goods, $is_recommend = 0)
    {
        $content_str = '';
        if (isset($goods['taobao_image'])) {
            $content = explode(',', $goods['taobao_image']);
            foreach ($content as $item) {
                $content_str .= '<p><img src="' . $item . '" style="max-width:100%;"><br></p>';
            }
        } elseif (isset($goods['itempic_copy'])) {
            $content_str = '<p><img src="' . 'http://img.haodanku.com/' . $goods['itempic_copy'] . '" style="max-width:100%;"><br></p>';
        }

        return [
            'title' => $goods['itemtitle'],
            'description' => $goods['itemdesc'],
            'images' => json_encode([$goods['itempic']]),
            'content' => $content_str,
            'price' => $goods['itemendprice'],
            'sale_num' => $goods['itemsale'],
            'is_recommend' => $is_recommend,
            'is_new' => 0,
            'is_hot' => 0,
            'ship_free' => 1,
            'original_price' => $goods['itemprice'],
            'cost_price' => $goods['itemendprice'],
            'cupon_url' => $goods['couponurl'],
            'item_url' => 'https://item.taobao.com/item.htm?ft=t&id=' . $goods['itemid'],
            'coupon_money' => $goods['couponmoney'],
            'item_endprice' => $goods['itemendprice'],
            'item_id' => $goods['itemid'],
            'stock_type' => 2,
            'stock' => 1000,
            'unit' => '',
            'on_sale' => 1,
            'type' => 1,
            'source_from' => 1,  // 淘宝来源
        ];
    }

    /**
     * @param $goods
     * @param int $is_recommend
     * @return mixed
     */
    public function dealGoodsInfo($goods, $is_recommend = 0)
    {
        $goods = $this->parseGoodsInfo($goods, $is_recommend);
        $exist = Goods::where('item_id', $goods['item_id'])->value('id');
        if (empty($exist)) {
            return Goods::insert($goods);
        }
        return $exist;
    }

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function syncGoods()
    {
        $category = make(GoodsCategoryLogic::class)->platJson();
        $key = 'sync_goods_cate_id_';
        $handanku = make(HaoDanKu::class);
        $count = 0;
        foreach ($category as $v) {
            if ($v['type'] == 1 || $v['pid'] == 0) {
                continue;
            }

            if (cache()->get($key . $v['id'])) {
                continue;
            }
            if (Helper::isProd()) {
                $limit = 50;
            } else {
                $limit = 10;
            }
            $data = $handanku->search($v['name'], 1, $limit, ['cid' => $v['pid']]);

            if (isset($data['data']) && $data['data']) {
                logger()->info('同步:' . $v['name'] . ' 分类id' . $v['id'] . '  数量:' . count($data['data']));
                foreach ($data['data'] as $vo) {
                    $goods_id = $this->dealGoodsInfo($vo);
                    $mul_goods_category = [];
                    $cids = MulGoodsCategory::where('goods_id', $goods_id)->pluck('cid')->toArray();
                    if (! in_array($v['id'], $cids)) {
                        $mul_goods_category[] = [
                            'cid' => $v['id'],
                            'goods_id' => $goods_id,
                        ];
                    }
                    if (! in_array($v['pid'], $cids)) {
                        $mul_goods_category[] = [
                            'cid' => $v['pid'],
                            'goods_id' => $goods_id,
                        ];
                    }
                    if ($mul_goods_category) {
                        MulGoodsCategory::insert($mul_goods_category);
                    }
                    ++$count;
                }
            }
            cache()->set($key . $v['id'], 1, 14400);
        }
        return $count;
    }

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function disableGoods()
    {
        $res = make(HaoDanKu::class)->getDisabledGoods();
        $item_ids = [];
        foreach ($res as $v) {
            $item_ids[] = $v['itemid'];
        }
        $count = 0;
        $item_ids = array_filter($item_ids);
        if ($item_ids) {
            $ids = Goods::whereIn('item_id', $item_ids)->pluck('id');
            $count = $this->delete(['id' => $ids]);
            logger()->info('失效商品下架:' . $count);
            logger()->info(json_encode($item_ids));
        }
        return $count;
    }

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @return array
     */
    public function parseGoodsDetail(array $goods)
    {
        logger()->info('ITEM ID $goods[\'item_id\']');
        logger()->info($goods['item_id']);
        return $this->show($goods['id']);
        //		cache()->set($key, 1, 3600);
    }

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @return array
     */
    public function parseGoodsLatestDetail(array $goods)
    {
        logger()->info('ITEM ID $goods[\'item_id\']');
        logger()->info($goods['item_id']);
        if (empty($goods['item_id'])) {
            return $goods;
        }
        $key = 'goods_taobao_goods_detail_v3_' . $goods['item_id'];
        if (cache()->get($key)) {
            return $goods;
        }
        $dingdanxia = make(DingDanXia::class);
        $data = $dingdanxia->goodsDetail($goods['item_id']);

        $goods['price'] = $dingdanxia->getGoodsPrice($data);
        if (strpos($goods['price'], '-') !== false) {
            $price = explode('-', $goods['price']);
            $goods['price'] = $price[1];
        }
        logger()->info('GOOD PRICE');
        logger()->info($goods['price']);

        logger()->info('========== TYPE 2 ===========');
        logger()->info(($goods['id']));
        $goodDetail = MulGoodsCategory::where('goods_id', '=', $goods['id'])->get();
        $cat = [];
        foreach ($goodDetail as $ck => $cv) {
            array_push($cat, $cv->cid);
        }

        $inCategory = self::inCategory($cat);

        if ($inCategory) {
            $cost = self::getCategoryCost($cat);
            $orgPriceMarkup = $cost->original_price;
            $costPriceMarkup = $cost->cost_price;

            $goods['original_price'] = floor($goods['price'] * $orgPriceMarkup);
            $goods['cost_price'] = $goods['price'];
            $goods['price'] = floor($goods['price'] * (float) $costPriceMarkup);
        }

        $goods['stock'] = $dingdanxia->getGoodsStock($data);

        $goods['options'] = $dingdanxia->getOptionInfo($data);

        $goods['images'] = $dingdanxia->getGoodsImages($data);
        $goods['content'] = $dingdanxia->getGoodsMobileContent($data);
        $this->update($goods);
        return $this->show($goods['id']);
        //		cache()->set($key, 1, 3600);
    }

    public function getCategoryCost($cat)
    {
        $price = Setting::whereIn('key', $cat)->orderBy('key', 'desc')->first();
        return json_decode($price->value);
    }

    /**
     * 返回用户级别的产品,屏蔽一些用户不需要看到的字段.
     * @param $id
     * @param null|mixed $username
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @return array
     */
    public function userGoodsDetail($id, $username = null)
    {
        $goods = $this->show($id, $username);
        $goods = $this->parseGoodsDetail($goods);
        unset($goods['cost_price'], $goods['category'], $goods['item_url'], $goods['cupon_url'], $goods['commission']);

        foreach ($goods['options']['options'] as $k => $option) {
            unset($goods['options']['options'][$k]['cost_price']);
        }
        return $goods;
    }

    /**
     * 返回用户级别的产品,屏蔽一些用户不需要看到的字段.
     * @param $id
     * @param null|mixed $username
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @return array
     */
    public function userGoodsDetailForPrice($id, $username = null)
    {
        $goods = $this->show($id, $username);
        $goods = $this->parseGoodsLatestDetail($goods);
        unset($goods['cost_price'], $goods['category'], $goods['item_url'], $goods['cupon_url'], $goods['commission']);

        foreach ($goods['options']['options'] as $k => $option) {
            unset($goods['options']['options'][$k]['cost_price']);
        }
        return $goods;
    }

    /**
     * @param $goods_id
     * @param $option_id
     * @return array
     */
    public function getOptionInfo($goods_id, $option_id)
    {
        if (empty($goods_id) || ! is_numeric($goods_id)) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '产品id错误');
        }
        if (empty($option_id) || ! is_numeric($option_id)) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '规格id错误');
        }
        $goods = $this->show($goods_id);
        if (isset($goods['options']['options']) && is_array($goods['options']['options'])) {
            foreach ($goods['options']['options'] as $option) {
                if ($option['id'] == $option_id) {
                    if (empty($option['images'])) {
                        $option['images'] = $goods['images'];
                    }
                    return $option;
                }
            }
        }
        return [];
    }

    /**
     * @param $gpc
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @return string
     */
    public function fetchGoods($gpc)
    {
        $error_url = [];
        $dingdanxia = make(DingDanXia::class);
        $haodanku = make(HaoDanKu::class);
        foreach ($gpc['url'] as $url) {
            $item_id = Helper::parseTaobaoId($url);
            $exist = Goods::where('item_id', $item_id)->value('id');
            if ($exist) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, '产品' . $url . '已存在');
            }

            try {
                $result = parallel([
                    'dingdanxia_goods' => function () use ($item_id,$dingdanxia) {
                        return $dingdanxia->goodsDetail($item_id);
                    },
                    'haodanku_goods' => function () use ($item_id,$haodanku) {
                        return $haodanku->detail($item_id);
                    },
                ]);
            } catch (\Exception $e) {
                $error_url[] = $url;
                continue;
            }

            logger()->info('========== TYPE 3 ===========');
//            $good =  Goods::where('item_id', $item_id)->first();
//            logger()->info(($good->id));
//            $goodDetail = MulGoodsCategory::where('goods_id', '=', $good->id)->get();
//            $cat = array();
//            foreach ($goodDetail as $ck => $cv){
//                array_push($cat, $cv->cid);
//            }
//            logger()->info(json_encode($cat));

            $dingdanxia_goods = $result['dingdanxia_goods'];
            $haodanku_goods = $result['haodanku_goods'];
            $goods['price'] = $dingdanxia->getGoodsPrice($dingdanxia_goods);
            if (strpos($goods['price'], '-') !== false) {
                $price = explode('-', $goods['price']);
                $goods['price'] = $price[1];
            }
            $goods['item_id'] = $item_id;
            $goods['cupon_url'] = $haodanku_goods['couponurl'] ?? '';
            $goods['item_url'] = $url;
            $goods['category'] = $gpc['category'];
            $goods['is_hot'] = $gpc['is_hot'];
            $goods['is_new'] = 0;
            $goods['is_recommend'] = $gpc['is_recommend'];
            $goods['on_sale'] = $gpc['on_sale'];
            $goods['sort'] = $gpc['sort'];
            $goods['content'] = $dingdanxia->getGoodsMobileContent($dingdanxia_goods);
            $goods['title'] = $dingdanxia->getGoodsTitle($dingdanxia_goods);
            $goods['images'] = $dingdanxia->getGoodsImages($dingdanxia_goods);

            $inCategory = self::inCategory($gpc['category']);
            //			echo var_dump(';) ;) ;) ');
            //			echo var_dump($inCategory);

            if (in_array(7, $gpc['category'])) {
                $orgPriceMarkup = Setting::where('name', '=', 'original_price')->first();
                $costPriceMarkup = Setting::Where('key', '=', 'cost_price')->first();

                $goods['cost_price'] = floor($goods['price']);
                $goods['original_price'] = floor($goods['price'] * $orgPriceMarkup->value);
                $goods['price'] = floor($goods['price'] * $costPriceMarkup->value);
            } else {
                $goods['cost_price'] = $goods['price'];
                $goods['original_price'] = $goods['price'];
            }

            $goods['type'] = 1;
            $goods['source_from'] = 1;
            $goods['ship_free'] = 1;
            $goods['sale_num'] = 0;
            $goods['unit'] = '';
            $goods['description'] = '';
            $goods['weight'] = 0;
            $goods['is_commission'] = 0;
            $goods['stock'] = $dingdanxia->getGoodsStock($dingdanxia_goods);
            $goods['options'] = $dingdanxia->getOptionInfo($dingdanxia_goods);

            try {
                $this->store($goods);
            } catch (\Exception $e) {
                $error_url[] = $url;
                continue;
            }
        }
        $error_num = count($error_url);
        $total_num = count($gpc['url']);
        $success_num = $total_num - $error_num;
        return "成功{$success_num}件产品,失败{$error_num}件产品";
    }

    public function updateGoods()
    {
        $key = 'update_goods_haodanku';
        $min_id = cache()->get($key);
        if (is_null($min_id)) {
            $min_id = 1;
        } elseif ($min_id == 0) {
            return null;
        }
        $response = make(HaoDanKu::class)->getUpdateGoods($min_id);
        $item_ids = [];
        $update_items = [];
        $count = 0;
        if (isset($response['data']) && $response['data'] && is_array($response['data'])) {
            foreach ($response['data'] as $v) {
                $update_items[$v['itemid']] = $v;
                $item_ids[] = $v['itemid'];
            }
            $need_update_goods = Goods::whereIn('item_id', $item_ids)
                ->selectRaw('id,item_id,price,cupon_url')
                ->get()
                ->toArray();
            foreach ($need_update_goods as $goods) {
                if ($goods['price'] == $update_items[$goods['item_id']]['itemprice'] && $goods['cupon_url'] == $update_items[$goods['item_id']]['couponurl']) {
                    continue;
                }
                $update = [
                    'cupon_url' => $update_items[$goods['item_id']]['couponurl'] ?? '',
                    'price' => $update_items[$goods['item_id']]['itemprice'],
                    'item_endprice' => $update_items[$goods['item_id']]['itemendprice'],
                ];
                $effect = Goods::where('id', $goods['id'])->update($update);
                $count += $effect;
            }
        }
        cache()->set($key, $response['min_id'] ?? 0, 3600);
        return $count;
    }

    public function updateSaleNum($order)
    {
        $order_goods = OrderGoods::where('order_id', $order['id'])->get()->toArray();
        foreach ($order_goods as $v) {
            $goods = Goods::findFromCache($v['goods_id']);
            if ($goods) {
                $goods->sale_num = $goods->sale_num + $v['num'];
                $goods->real_sale_num = $goods->real_sale_num + $v['num'];
                $goods->save();
            }
        }
    }

    /**
     * @param $goods
     */
    private function dealGoodsCategory(array $category, $goods)
    {
        $exist_cid = MulGoodsCategory::where('goods_id', $goods['id'])->pluck('cid')->toArray();
        $all_category_ids = [];
        $data = [];
        foreach ($category as $item) {
            $all_category_ids[] = $item;
            if (in_array($item, $exist_cid)) {
                continue;
            }
            $data[] = [
                'goods_id' => $goods['id'],
                'cid' => $item,
            ];
        }
        make(GoodsCategoryLogic::class)->checkExistCategory($all_category_ids);
        MulGoodsCategory::where('goods_id', $goods['id'])->whereNotIn('cid', $all_category_ids)->delete();
        if ($data) {
            MulGoodsCategory::insert($data);
        }
    }

    /**
     * @param $goods
     */
    private function dealGoodsOptions(array $options, $goods)
    {
        $exist_spec = GoodsSpec::where('goods_id', $goods['id'])->pluck('id')->toArray();

        $spec_ids = [-1]; // 规格分类项id,本次新增&编辑
        $spec_item_ids = [-1]; // 规格分类子项id,本次新增&编辑
        $options_ids = [-1]; // 规格详细项id,本次新增&编辑

        $spec_items_info = []; // 规格详细项
        if (isset($options['spec']) && is_array($options['spec']) && $options['spec']) {
            $spec_item_num = [];
            foreach ($options['spec'] as $spec) {
                $spec_item_num[] = count($spec['spec_item']);
                $update = [
                    'title' => $spec['title'],
                    'sort' => $spec['sort'],
                    'goods_id' => $goods['id'],
                ];
                if (isset($spec['id']) && $spec['id']) {
                    GoodsSpec::where('id', $spec['id'])->update($update);
                } else {
                    $spec['id'] = GoodsSpec::insertGetId($update);
                }
                $spec_ids[] = $spec['id'];
                foreach ($spec['spec_item'] as $spec_item) {
                    $update = [
                        'title' => $spec_item['title'],
                        'sort' => $spec_item['sort'],
                        'spec_id' => $spec['id'],
                    ];
                    if (isset($spec_item['id']) && $spec_item['id']) {
                        GoodsSpecItem::where('id', $spec_item['id'])->update($update);
                    } else {
                        $spec_item['id'] = GoodsSpecItem::insertGetId($update);
                    }
                    $spec_item_ids[] = $spec_item['id'];
                    $spec_items_info[$spec['title'] . '_' . $spec_item['title']] = $spec_item['id'];
                }
            }
            $total_options_num = 1;
            foreach ($spec_item_num as $num) {
                $total_options_num *= $num;
            }
//            if (!isset($options['options']) || count($options['options']) != $total_options_num) {
//                throw new BusinessException(ErrorCode::PARAMS_INVALID,'请先刷新规格项,确认规格信息再提交');
//            }
        }

        if ($exist_spec) {
            GoodsSpec::whereIn('id', $exist_spec)->whereNotIn('id', $spec_ids)->delete();
            GoodsSpecItem::whereIn('spec_id', $exist_spec)->whereNotIn('id', $spec_item_ids)->delete();
        }

        if (isset($options['options']) && is_array($options['options']) && $options['options']) {
            foreach ($options['options'] as $k => $v) {
                $options['options'][$k]['goods_id'] = $goods['id'];
            }
            foreach ($options['options'] as $v) {
                $v_item = $v['specs'];
                if (empty($v_item) || ! is_array($v_item)) {
                    throw new BusinessException(ErrorCode::PARAMS_INVALID, '规格描述错误');
                }
                foreach ($v_item as $replace_item) {
                    $replace_item['key'] = $replace_item['key'] ?? '';
                    $replace_item['name'] = $replace_item['name'] ?? '';
                    if (! isset($spec_items_info[$replace_item['key'] . '_' . $replace_item['name']])) {
                        throw new BusinessException(ErrorCode::PARAMS_INVALID, '规格组合' . $replace_item['key'] . '_' . $replace_item['name'] . '无法匹配id,请确认已刷新规格项再提交');
                    }
                }

                logger()->info('========== TYPE 1 ===========');
                logger()->info(($goods['id']));
                logger()->info(($options['category']));
                $inCategory = self::inCategory($options['category']);
                /*
                 * Change price to x2 only if the good have the category id 7 (箱包)
                 * */
                if ($inCategory) {
                    $orgPriceMarkup = Setting::where('key', '=', 'original_price')->first();
                    $costPriceMarkup = Setting::Where('key', '=', 'cost_price')->first();
                    $update = [
                        'title' => $v['title'],
                        'goods_id' => $v['goods_id'],
                        'images' => $v['images'],
                        'cost_price' => $v['cost_price'],
                        'original_price' => $v['cost_price'] * $orgPriceMarkup->value,
                        'price' => $v['cost_price'] * $costPriceMarkup->valuex,
                        'stock' => $v['stock'],
                        'sort' => $v['sort'],
                        'specs' => json_encode($v['specs'], JSON_UNESCAPED_UNICODE),
                    ];
                } else {
                    $update = [
                        'title' => $v['title'],
                        'goods_id' => $v['goods_id'],
                        'images' => $v['images'],
                        'price' => $v['price'],
                        'original_price' => $v['original_price'],
                        'cost_price' => $v['cost_price'],
                        'stock' => $v['stock'],
                        'sort' => $v['sort'],
                        'specs' => json_encode($v['specs'], JSON_UNESCAPED_UNICODE),
                    ];
                }

                if (isset($v['id']) && $v['id']) {
                    GoodsOption::where('id', $v['id'])->update($update);
                } else {
                    $v['id'] = GoodsOption::insertGetId($update);
                }
                $options_ids[] = $v['id'];
            }
        }
        GoodsOption::where('goods_id', $goods['id'])->whereNotIn('id', $options_ids)->delete();
    }
}
