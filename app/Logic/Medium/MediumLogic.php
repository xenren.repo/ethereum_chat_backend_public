<?php
namespace App\Logic\Medium;


use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\Medium;

/**
 * @package App\Models\Logic\Goods
 */
class MediumLogic extends Logic
{

	/**
	 * 列表
	 * @param $gpc
	 * @return
	 */
	public function index($gpc)
	{
		$data = $this->query($gpc)
			->selectRaw('id,name,author,img,description,url,pid,cid,sort,status,type,up,down,read_num,created_at')
			->orderBy('sort','desc')
			->orderBy('id','desc')
			->paginate($gpc['limit'])->toArray();
		foreach ($data['data'] as $k => $v) {
			$data['data'][$k]['img'] = json_decode($data['data'][$k]['img'],true);
			$data['data'][$k]['detail_url'] = env('HOST').'/mobile.html#/medium_show?id='.$v['id'];
		}
		return $data;
	}


	/**
	 * @param $data
	 * @return Medium
	 */
	public function query($data)
	{
		$query = new Medium();
		if (!empty($data['search'])) {
			$search = $data['search'].'%';
			$query = $query->where('name','like',$search);
		}

		if (Auth::isAdmin()) {
			if (isset($data['status']) && ($data['status'] != '') && !is_null($data['status'])) {
				$query = $query->where('status',$data['status']);
			}
		} else {
			$query = $query->where('status',0);
		}

		return $query;
	}


	/**
	 * @param $id
	 * @return
	 */
	public function show($id)
	{
		if (Auth::isAdmin()) {
			$data = cache()->remember('medium_admin_medium_id_'.$id,function()use($id){
				$res = Medium::findOrFail($id);
				$res['img'] = json_decode($res['img']);
				return $res;
			});
		} else {
			$data = cache()->remember('medium.user_medium_id_'.$id,function()use($id){
				$res = Medium::where('status',0)->findOrFail($id);
				$res['img'] = json_decode($res['img']);
				return $res;
			});
		}
		return $data;

	}

	/**
	 * @param $gpc
	 * @return string
	 */
	public function store($gpc)
	{
		$gpc['type'] = 1; // 默认type为图文类型
		$data = $this->validate($gpc);
		$data['img'] = json_encode($data['img']);
		$res = Medium::create($data);
		$res['img'] = json_decode($res['img']);
		$this->clearCache();
		return $res;
	}


	/**
	 * @param $gpc
	 * @return string
	 */
	public function update($gpc)
	{
		$data = $this->validate($gpc);
		$data['img'] = json_encode($data['img']);
		$data['type'] = 1; // 默认type为图文类型
		$medium = Medium::findOrFail($data['id']);
		$res = $medium->update($data);
		$this->clearCache();
		return $res;
	}


	/**
	 * @param $gpc
	 * @return string
	 */
	public function delete($gpc)
	{
		if (!is_array($gpc['id'])) {
			$gpc['id'] = [$gpc['id']];
		}
		$res = Medium::whereIn('id',$gpc['id'])->delete();
		$this->clearCache();
		return $res;
	}


	/**
	 * 缓存清除
	 */
	public function clearCache()
	{
		cache()->clearPrefix('medium');
	}


	/**
	 * @param $data
	 * @return
	 */
	public function validate($data)
	{
		$data['pid'] = 0;
		$data['cid'] = 0;


		if (!isset($data['sort']) || empty($data['sort'])) {
			$data['sort'] = 0;
		}

		if (!isset($data['up']) || empty($data['up'])) {
			$data['up'] = 0;
		}

		if (!isset($data['down']) || empty($data['down'])) {
			$data['down'] = 0;
		}

		if (!isset($data['read_num']) || empty($data['read_num'])) {
			$data['read_num'] = 0;
		}

		if (!isset($data['type']) || empty($data['type'])) {
			$data['type'] = 1;
		}

		return $data;
	}




}