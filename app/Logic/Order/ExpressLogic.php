<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Order;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Lib\Express\Express;
use App\Logic\Goods\GoodsLogic;
use App\Logic\Logic;
use App\Model\OrderPackage;
use App\Model\UserCart;

class ExpressLogic extends Logic
{

	/**
	 * @param array $goods_data
	 * @param array $user_info
	 * @return int
	 */
	public function countExpress(array $goods_data, array $user_info)
	{
		$express = 0;
		foreach ($goods_data as $v) {
			$goods = make(GoodsLogic::class)->show($v['goods_id']);
			$express += $goods['express_cost'] ?? 0;
		}
		return $express;
	}

	public function getNameById(string $express_id)
	{
		$data = $this->getExpressList();
		return $data[$express_id] ?? '其他';
	}

	public function getExpressList()
	{
		return cache()->remember('express_list', function () {
			$res = file_get_contents(BASE_PATH.'/app/Lib/Express/express.json');
			$res = json_decode($res, true);
			$data = array_flip($res);
			return $data;
		});
	}

	/**
	 * 获取包裹物流轨迹
	 * @param $id
	 * @return bool|mixed|string
	 */
	public function getPackageExpress($order_package)
	{
		if (is_null($order_package)) 
		{
			$order_package = [
				"express_id"=> "ZTO",
				"express_sn"=> "75311558225855"
			];
		}
		// return $order_package;
		return cache()->remember('package_trace_'.json_encode($order_package), function () use ($order_package) {
			return make(Express::class)->getTrace($order_package);
		}, 21600);
	}


}
