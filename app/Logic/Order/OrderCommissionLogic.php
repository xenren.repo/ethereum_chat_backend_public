<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Order;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Business\BusinessLogic;
use App\Logic\Goods\GoodsLogic;
use App\Logic\Logic;
use App\Model\Order;
use App\Model\UserCart;

class OrderCommissionLogic extends Logic
{

//	/**
//	 * @param $order_id
//	 * @return null
//	 * @throws \Swoft\Db\Exception\DbException
//	 */
//	public function updateOrderCommission($order_id)
//	{
//		$order_goods = Curd::table('order_goods')->where('order_id', $order_id)->_get();
//		$this->updateOrderCommissionByOrderGoods($order_id, $order_goods);
//		return null;
//	}
//
	/**
	 * @param $order
	 * @param $order_goods
	 */
	public function updateOrderCommissionByOrderGoods(Order $order, $order_goods)
	{
		$commission = [
			'type' => 1,
			'value'=> [
				'1' => 0,
				'2' => 0,
				'3' => 0,
			]
		];
		foreach ($order_goods as $v) {
			if (!is_array($v['commission'])) {
				$v['commission'] = json_decode($v['commission'], true);
			}
			$value = $v['commission']['value'] ?? [];
			$commission['value']['1'] += $value['1'] ?? 0;
			$commission['value']['2'] += $value['2'] ?? 0;
			$commission['value']['3'] += $value['3'] ?? 0;
		}
		$order['commission'] = json_encode($commission);
		$order->save();
	}

	/**
	 * @param array $goods_info
	 * @return array|mixed
	 */
	public function getGoodsCommissionInfo(array $goods_info)
	{
		$rules = [
			'goods_id'    => 'required|integer|min:0',
			'option_id'    => 'sometimes|integer|min:0',
			'total'    => 'required|numeric|min:0',
			'num'        => 'required|integer|min:0|max:100000',
		];
		validate($goods_info, $rules);
		$goods = make(GoodsLogic::class)->show($goods_info['goods_id']);

		$commission = [];

		// TODO 是否开启分润判断

		// 检测该产品是否参与分润
		if ($goods['is_commission']) {
			$goods['commission'] = json_decode($goods['commission'], true);

			if ($goods['commission']) {
				$commission = $goods['commission'];
			} else {
				$commission =  BusinessLogic::defaultCommission();
			}
		}

		if (!isset($commission['value']) || !is_array($commission['value'])) {
			return [];
		}

		//TODO 过滤开启分润等级

		// TODO 按比例的产品分润.全部必须被转换为按金额的,这样可兼容多产品不同分润方案的处理
		foreach ($commission['value'] as $k => $v) {
			if ($commission['type'] == 2) {  // 1:金额  2:比例
				$v = $goods_info['total']*$v;
			}
			$commission['value'][$k] = round($v, 2);
		}
		$commission['type'] = 1;
		return $commission;
	}

}
