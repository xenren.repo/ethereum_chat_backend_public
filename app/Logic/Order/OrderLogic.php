<?php

declare(strict_types=1);

namespace App\Logic\Order;

use App\Constants\ErrorCode;
use App\Event\CloseOrderEvent;
use App\Event\FinishOrderEvent;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Kernel\Auth\AuthManager;
use App\Kernel\Common\Common;
use App\Lib\Area\Area;
use App\Lib\Other\Excel;
use App\Logic\Admin\AdminAuthLogic;
use App\Logic\Admin\AdminLogic;
use App\Logic\Finance\Credit1Logic;
use App\Logic\Goods\GoodsLogic;
use App\Logic\Logic;
use App\Logic\User\UserAuthLogic;
use App\Logic\User\UserRelationLogic;
use App\Model\Admin;
use App\Model\Credit1;
use App\Model\Goods;
use App\Model\GoodsCategory;
use App\Model\MulGoodsCategory;
use App\Model\Order;
use App\Model\OrderGoods;
use App\Model\OrderPackage;
use App\Model\OrderReceipt;
use App\Model\User;
use App\Model\UserAddress;
use App\Model\UserCart;
use App\Service\Model\Order\OrderService;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Psr\EventDispatcher\EventDispatcherInterface;

class OrderLogic extends Logic
{
    const CLOSE = -1; // 关闭

    const UN_PAY = 0;  // 待付款

    const HAS_PAY = 1;  // 已付款/待发货

    const HAS_DELIVER = 2;  // 已发货/待收货

    const FINISH = 3;  // 完成

    const REFUNDING = 4;  // 维权中

    const REFUNDING_IN_PROGRESS = 5;  // withdraw request made

//    const WITHDRAWED = 6;  // withdraw request made

    // 支付方式
    const SYSTEM = 0;  // 系统支付

    const WECHAT = 1;  // 微信支付

    const ALIPAY = 2;  // 支付宝支付

    const BALANCE = 3;  // 余额支付

    const SPECIAL_PAY = 4;  // 专区券支付

    const CREDIT3_PAY = 5;  // 专区券支付

    // 订单类型
    const MALL_TYPE = 0;  // 综合订单

    const SPECIAL_TYPE = 1;  // 专区券订单

    const SELLER_PAID_TYPE = 6;

    const JINQIA_PAID_TYPE = 7;

    /**
     * @Inject
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param $gpc
     */
    public function store($gpc)
    {
        $info = $this->storeValidate($gpc);
        $this->checkCanStoreOrder($info);
        return Db::transaction(function () use ($info,$gpc) {
            $user = Auth::user();
            $orderNo = $this->generateNo();
            $order_info = [
                'order_no' => $orderNo,
                'user_id' => $user['id'],
                'pid' => $user['pid'],
                'type' => $info['type'],
                'total_amount' => $info['total_amount'],
                'original_total' => $info['total_amount'],
                'express_amount' => $info['express_amount'],
                'goods_amount' => $info['goods_amount'],
                'remark' => $gpc['remark'] ?? '',
                'receiver' => $gpc['user_info']['receiver'],
                'receiver_mobile' => $gpc['user_info']['receiver_mobile'],
                'province' => $gpc['user_info']['province'],
                'city' => $gpc['user_info']['city'],
                'area' => $gpc['user_info']['area'],
                'address' => $gpc['user_info']['address'],
                'reward_price' => $info['reward_price'] ?? 0,
                'reward_mode' => $info['reward_mode'],
            ];

            $order = Order::create($order_info);
            // $order = Order::create([
            // 	'order_no' => $orderNo,
            // 	'user_id'  => $user['id'],
            // 	'pid'      => $user['pid'],
            // 	'type'      => $info['type'],
            // 	'total_amount' => $info['total_amount'],
            // 	'original_total' => $info['total_amount'],
            // 	'express_amount' => $info['express_amount'],
            // 	'goods_amount' => $info['goods_amount'],
            // 	'remark' => $gpc['remark'] ?? '',
            // 	'receiver' => $gpc['user_info']['receiver'],
            // 	'receiver_mobile' => $gpc['user_info']['receiver_mobile'],
            // 	'province' => $gpc['user_info']['province'],
            // 	'city' => $gpc['user_info']['city'],
            // 	'area' => $gpc['user_info']['area'],
            // 	'address' => $gpc['user_info']['address'],
            // ]);

            // return $order;
            $cart_ids = [];
            foreach ($info['order_goods'] as $k => $v) {
                $info['order_goods'][$k]['order_id'] = $order['id'];
                if (isset($v['cart_id'])) {
                    if ($v['cart_id']) {
                        $cart_ids[] = $v['cart_id'];
                    }
                    unset($info['order_goods'][$k]['cart_id']);
                }
                make(GoodsLogic::class)->updateStocks($v['goods_id'], $v['option_id'], $v['num']);
            }
            OrderGoods::insert($info['order_goods']);
            if ($cart_ids) {
                UserCart::whereIn('id', $cart_ids)->where('user_id', $user['id'])->delete();
                make(UserCartLogic::class)->clearCache($user['id']);
            }

            OrderReceipt::create([
                'company_name' => isset($gpc['receipt']) && isset($gpc['receipt']['companyName']) ? $gpc['receipt']['companyName'] : '',
                'company_code' => isset($gpc['receipt']) && isset($gpc['receipt']['companyCode']) ? $gpc['receipt']['companyCode'] : '',
                'company_phone' => isset($gpc['receipt']) && isset($gpc['receipt']['companyPhone']) ? $gpc['receipt']['companyPhone'] : '',
                'name' => isset($gpc['receipt']) && isset($gpc['receipt']['name']) ? $gpc['receipt']['name'] : '',
                'order_no' => $orderNo,
                'receipt_required' => isset($gpc['receipt']) && isset($gpc['receipt']['receiptRequired']) ? $gpc['receipt']['receiptRequired'] : '',
                'type' => isset($gpc['receipt']) && isset($gpc['receipt']['type']) ? $gpc['receipt']['type'] : false,
            ]);
            make(OrderCommissionLogic::class)->updateOrderCommissionByOrderGoods($order, $info['order_goods']);
            return $this->userDetail($order['id']);
        });
    }

    /**
     * @param $info
     */
    public function checkCanStoreOrder($info)
    {
        $data = [];
        // 同种产品数量综合统计
        foreach ($info['order_goods'] as $v) {
            if (! isset($data[$v['goods_id']])) {
                $data[$v['goods_id']] = 0;
            }
            $data[$v['goods_id']] += $v['num'];
        }
        foreach ($data as $key => $vo) {
            $goods = make(GoodsLogic::class)->show($key);
            if (! empty($goods['total_limit'])) {
                $num = OrderGoods::leftJoin('orders', 'orders.id' . '=' . 'order_goods.order_id')
                    ->where('orders.user_id', Auth::id())
                    ->where('orders.status', '-1', '<>')
                    ->where('order_goods.goods_id', $key)
                    ->_sum('num');
                if (($num + $vo) > $goods['total_limit']) {
                    throw new BusinessException(ErrorCode::PARAMS_INVALID, '产品' . $goods['title'] . '最大限购数量为' . $goods['total_limit']);
                }
            }
        }
    }

    /**
     * 预计算订单信息.
     * @param $gpc
     * @return array
     */
    public function preCountOrder($gpc)
    {
        return $this->storeValidate($gpc, true);
    }

    /**
     * @param $gpc
     * @param bool $is_pre 是否预下单,预下单不对比总额
     * @return array
     */
    public function storeValidate($gpc, $is_pre = false)
    {
        // 产品金额,订单总额,其他优惠信息等金额性验证
        $goods_amount = 0;   // 产品总额
        $order = [];  // 订单基础信息
        $goods_logic = make(GoodsLogic::class);
        $mall_goods_count = 0;  //综合订单产品数量  ,包含默认,淘宝
        $special_goods_count = 0; // 专区产品数量
        logger()->info($gpc['goods']);
        $receipt_required = false;
        $reward_price_before = 0;
        $reward_price = 0;
        foreach ($gpc['goods'] as $v) {
            $goods = $goods_logic->show($v['goods_id']);
            if (empty($goods['on_sale'])) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, '产品:' . $goods['title'] . '已下架');
            }
            if ($goods['receipt_required'] == 1) {
                $receipt_required = true;
            }
            if (in_array($goods['source_from'], [0, 1])) {
                ++$mall_goods_count;
            } elseif (in_array($goods['source_from'], [2])) {
                ++$special_goods_count;
            } else {
                throw new  BusinessException(ErrorCode::PARAMS_INVALID, '不允许的产品类型出现');
            }
            $order_goods = [
                'goods_id' => $v['goods_id'],
                'item_endprice' => $goods['item_endprice'] ?? $goods['price'],
                'title' => $goods['title'],
                'cupon_url' => $goods['cupon_url'],
                'item_url' => $goods['item_url'],
                'type' => $goods['type'],
                'unit' => $goods['unit'],
                'images' => $goods['images'][0] ?? '',
                'num' => $v['num'],
                'cart_id' => $v['cart_id'] ?? 0,
            ];
            if ($v['option_id']) {
                $option = $goods_logic->getOptionInfo($v['goods_id'], $v['option_id']);
                if (empty($option)) {
                    throw new  BusinessException(ErrorCode::PARAMS_INVALID, '规格无法匹配产品');
                }
                $order_goods['price'] = $option['price'];
                $order_goods['original_price'] = $option['original_price'];
                $order_goods['cost_price'] = $option['cost_price'];
                $order_goods['weight'] = $option['weight'] ?? '0';
                $order_goods['option_id'] = $option['id'];
                $order_goods['option_title'] = $option['title'];
                if ($option['images']) {
                    $order_goods['images'] = $option['images'];
                }
            } else {
                $order_goods['price'] = $goods['price'];
                $order_goods['original_price'] = $goods['original_price'];
                $order_goods['cost_price'] = $goods['cost_price'];
                $order_goods['weight'] = $goods['weight'] ?? '0';
                $order_goods['option_id'] = 0;
                $order_goods['option_title'] = '';
            }

            if (($goods['item_endprice'] != null && $goods['item_endprice'] > 0) && $goods['min_item_endprice'] > 0 && $v['num'] >= $goods['min_item_endprice'] && $goods['reward_mode'] == 0) {
                if ($goods['item_endprice'] != round($v['price'], 2)) {
                    throw new  BusinessException(ErrorCode::PARAMS_INVALID, 'Failed');
                }
                $order_goods['price'] = $goods['item_endprice'];
                $order_goods['reward_price'] = $goods['item_endprice'];
            } elseif ((($goods['item_endprice'] != null && $goods['item_endprice'] > 0) && $goods['reward_mode'] == 1)) {
                $order_goods['price'] = $goods['price'];
                $order_goods['reward_price'] = $goods['item_endprice'];
            } else {
                $order_goods['reward_price'] = $goods['item_endprice'];
                if ($order_goods['price'] != round($v['price'], 2)) {
                    throw new  BusinessException(ErrorCode::PARAMS_INVALID, '产品单价错误');
                }
            }

            $reward_price = 0;
            if ($goods['reward_mode'] == 1) {
                $reward_price = round($order_goods['item_endprice'] * $order_goods['num'], 2);
                $order['reward_mode'] = 1;
            } else {
                //$reward_price = round($order_goods['price'] * $order_goods['num'], 2);
                $order['reward_mode'] = 0;
            }

            $order_goods['total'] = round($order_goods['price'] * $order_goods['num'], 2);
            if ($order_goods['total'] != round($v['total'], 2)) {
                throw new  BusinessException(ErrorCode::PARAMS_INVALID, '产品单项总额错误');
            }
            $reward_price_before += $reward_price;
            $goods_amount += $order_goods['total'];
            $order_goods['commission'] = make(OrderCommissionLogic::class)->getGoodsCommissionInfo($v);
            $order_goods['commission'] = json_encode($order_goods['commission']);
            $order['order_goods'][] = $order_goods;
        }
        if (round($goods_amount, 2) != round($gpc['goods_amount'], 2)) {
            throw new  BusinessException(ErrorCode::PARAMS_INVALID, '产品总额发生错误');
        }

        // 专区产品订单不能出现其他类型订单
        if ($mall_goods_count > 0 && $special_goods_count == 0) {
            $order['type'] = OrderLogic::MALL_TYPE;
        } elseif ($mall_goods_count == 0 && $special_goods_count > 0) {
            $order['type'] = OrderLogic::SPECIAL_TYPE;
        } else {
            throw new  BusinessException(ErrorCode::PARAMS_INVALID, '专区产品需单独下单');
        }

        $express_amount = make(ExpressLogic::class)->countExpress($gpc['goods'], $gpc['user_info']);

        $total_amount = $express_amount + $goods_amount;
        $total_reward_price = $express_amount + $reward_price_before;
        if (round($total_amount, 2) != round($gpc['total_amount'], 2) && ! $is_pre) {
            throw new  BusinessException(ErrorCode::PARAMS_INVALID, '总金额计算错误');
        }

        $order['total_amount'] = $total_amount;
        $order['reward_price'] = $total_reward_price;
        $order['express_amount'] = $express_amount;
        $order['goods_amount'] = $goods_amount;
        $order['receipt_required'] = $receipt_required;
        return $order;
    }

    /**
     * @param $gpc
     * @return mixed
     */
    public function index($gpc)
    {
        $data = $this->query($gpc)
            ->selectRaw('orders.id')
            ->orderBy('orders.id', 'desc')
            ->paginate((int) $gpc['limit'])
            ->toArray();
        foreach ($data['data'] as $k => $v) {
            $data['data'][$k] = $this->userDetail($v['id']);
        }
        return $data;
    }

    /**
     * @param $gpc
     * @param null|mixed $userId
     * @return mixed
     */
    public function pcIndex($gpc)
    {
        $userId = Auth::id();
        $data = $this->query($gpc)
            ->selectRaw('orders.id,orders.order_no,users.name,users.realname')
            ->orderBy('orders.id', 'desc');
        $admin = Admin::where('id', '=', $userId)->first();
        if ($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) {
            $goodsIds = Goods::where('created_by', '=', $userId)->pluck('id');
            $orders = OrderGoods::whereIn('goods_id', $goodsIds)->pluck('order_id');
            $data = $data->whereIn('orders.id', $orders);
        }

        $data = $data->paginate((int) $gpc['limit'])->toArray();
        foreach ($data['data'] as $k => $v) {
            $data['data'][$k] = $this->detail($v['id']);
            $data['data'][$k]['name'] = $v['name'];
            $data['data'][$k]['realname'] = $v['realname'];
        }
        return $data;
    }

    /**
     * @param $data
     * @return
     */
    public function query($data)
    {
        $query = Order::leftJoin('users', 'users.id', '=', 'orders.user_id');
        if (! empty($data['search'])) {
            $search = $data['search'] . '%';
            $query = $query->where(function ($query) use ($search) {
                $query->where('users.name', 'like', $search)
                    ->orWhere('users.realname', 'like', $search)
                    ->orWhere('orders.order_no', 'like', $search);
            });
        }
        if (isset($data['status']) && ($data['status'] != '') && ! is_null($data['status'])) {
            if (! is_array($data['status'])) {
                $data['status'] = [$data['status']];
            }
            $query = $query->whereIn('orders.status', $data['status']);
        }
        if (isset($data['type']) && ($data['type'] != '') && ! is_null($data['type']) && $data['type'] != 0) {
            $query = $query->where('orders.type', $data['type']);
        }

        if (isset($data['refunding']) && ($data['refunding'] != '') && ! is_null($data['refunding'])) {
            if ($data['refunding'] == 0) {
                $query = $query->where('orders.refund_id', 0);
            } elseif ($data['refunding'] == 1) {
                $query = $query->where('orders.refund_id', '>', 0);
            }
        }

        if (isset($data['start_end']) && ! empty($data['start_end'])) {
            echo var_dump($data['start_end'][0]);
            if (! isset($data['start_end'][0]) || ! isset($data['start_end'][1])) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, '时间格式错误');
            }
//            $query = $query->whereBetween('orders.created_at', [Carbon::parse($data['start_end'][0])->startOfDay(), Carbon::parse($data['start_end'][1])->endOfDay()]);
            $query = $query->where('orders.created_at', '>=', Carbon::parse($data['start_end'][0])->startOfDay())->where('orders.created_at', '<=', Carbon::parse($data['start_end'][1])->endOfDay());
        }

        if (isset($data['p']) && ($data['p'] != '') && ! is_null($data['p'])) {
            $query = $query->join('order_goods', 'orders.id', '=', 'order_goods.order_id');
            $query = $query->where('order_goods.title', 'LIKE', '%' . $data['p'] . '%');
        }

        if (isset($data['filter_goods_owner'])) {
            switch ($data['filter_goods_owner']) {
                case '1':
                    $goodsIds = Goods::where('created_by', '=', '0')->pluck('id');
                    $orders = OrderGoods::whereIn('goods_id', $goodsIds)->pluck('order_id');
                    $query = $query->whereIn('orders.id', $orders);
                    break;
                case '2':
                    $goodsIds = Goods::where('created_by', '>', '0')->pluck('id');
                    $orders = OrderGoods::whereIn('goods_id', $goodsIds)->pluck('order_id');
                    $query = $query->whereIn('orders.id', $orders);
                    break;
            }
        }

        if (! Auth::isAdmin()) {
            $query = $query->where('orders.user_id', Auth::id());
        }

        return $query;
    }

    public function generateNo()
    {
        return 'SN' . time() . mt_rand(100000, 999999);
    }

    /**
     * 结束订单.
     * @param $order
     */
    public function finishOrder($order)
    {
        Db::transaction(function () use ($order) {
            $order_update = [
                'status' => self::FINISH,
                'finished_time' => Carbon::now(),
                'version' => $order['version'] + 1,
            ];
            $count = Order::where('id', $order['id'])->where('version', $order['version'])->update($order_update);
            if ($count == 0) {
                throw new BusinessException(ErrorCode::VERSION_UPDATE);
            }
            $this->shareProfit($order);
            // $this->addSellerCredit1($order);
            make(GoodsLogic::class)->updateSaleNum($order);
        });
    }

    public function addSellerCredit1($order): void
    {
        make(Credit1Logic::class)->sellerAddCredit1($order);
    }

    /**
     * 订单收益分润.
     * @param $order
     */
    public function shareProfit($order)
    {
        Db::transaction(function () use ($order) {
            if ($order['type'] == self::SPECIAL_TYPE) {
                return true;
            }
            $parents = make(UserRelationLogic::class)->getParentIds($order['user_id'], 2);
            $logic = make(Credit1Logic::class);
            $commission = json_decode($order['commission'], true);
            $commission = $commission['value'] ?? [];
            foreach ($parents as $k => $parent) {
                $parent = User::findFromCache($parent)->toArray();
                if (empty($parent)) {
                    continue;
                }
                $share_profit = $commission[$k + 1] ?? 0;
                if ($share_profit <= 0) {
                    continue;
                }
                $exist = Credit1::where('user_id', $parent['id'])
                    ->where('foreign_id', $order['id'])
                    ->where('type', Credit1Logic::ORDER_PROFIT)->value('id');
                if (empty($exist)) {
                    $gpc = [
                        'remark' => '订单' . $order['order_no'] . '第' . ($k + 1) . '代推荐收益',
                        'user' => $parent['id'],
                        'credit' => $share_profit,
                        'type' => Credit1Logic::ORDER_PROFIT,
                        'foreign_id' => $order['id'],
                    ];
                    $logic->setCredit($gpc);
                }
            }
        });
    }

    public function reduce_to_id($item)
    {
        return $item['cid'];
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function detail($order_id)
    {
        if (empty($order_id)) {
            throw new BusinessException();
        }

        $query = Order::where('id', $order_id);
        if (! Auth::isAdmin()) {
            $user_id = Auth::id();
            $query = $query->where('user_id', $user_id);
        }
        $order = $query->firstOrFail()->toArray();
        $order['order_goods'] = OrderGoods::where('order_id', $order['id'])->get()->toArray();
        $order['goods_count'] = count($order['order_goods']);
        $order['total_num'] = 0;
        $showRefund = true;
        foreach ($order['order_goods'] as $k => $v) {
            $check_good = Goods::where('id', $v['goods_id'])->first();
            $category = MulGoodsCategory::where('goods_id', $v['goods_id'])->first();
            $categoryIds = MulGoodsCategory::where('goods_id', $v['goods_id'])->pluck('cid');
            $categoryId = is_null($category) ? 0 : $category['cid'];
            $categoryName = GoodsCategory::whereIn('id', $categoryIds)->where('name', '=', '牡丹专区券')->first();
            $categoryName = isset($categoryName->name) ? $categoryName->name : '';
            $order['total_num'] += $v['num'];
            $order['order_goods'][$k]['good_category'] = $categoryId;
            if (isset($check_good->reward_mode) && $check_good->reward_mode == 1) {
                $order['order_goods'][$k]['is_reward'] = 'reward_mode';
            } else {
                $order['order_goods'][$k]['is_reward'] = 'simple_mode';
            }
            $order['order_goods'][$k]['good_category_name'] = $categoryName;
            $order['order_goods'][$k]['created_by'] = $check_good['created_by'];
            $order['order_goods'][$k]['is_mudan_product'] = ! isset($categoryName) || $categoryName == '' ? false : true;
            if ($showRefund == true && $categoryId == 392) {
                $showRefund = false;
            }
        }
        $order['address_text'] = make(Area::class)->getAddressText($order['province'], $order['city'], $order['area']);
        $order['order_packages'] = OrderPackage::where('order_id', $order['id'])->get()->toArray();
        $order['allow_refund'] = $showRefund;

        //		echo var_dump($order['order_no']);
        $order['receipt'] = OrderReceipt::where('order_no', '=', $order['order_no'])->first();
        return $order;
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function userDetail($order_id)
    {
        $order = $this->detail($order_id);
        unset($order['commission'], $order['total_commission'], $order['deleted_at'], $order['version']);

        foreach ($order['order_goods'] as $k => $v) {
            unset($order['order_goods'][$k]['cost_price'], $order['order_goods'][$k]['commission'], $order['order_goods'][$k]['total_commission'], $order['order_goods'][$k]['deleted_at']);
        }
        return $order;
    }

    /**
     * @param $order
     * @param string $trans_id
     * @param int $pay_type
     * @return bool
     */
    public function setPay($order, $trans_id = '', $pay_type = 0)
    {
        if (! in_array($pay_type, [0, 1, 2, 3, 4, 5])) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '支付类型错误');
        }
        if (empty($trans_id)) {
            $trans_id = time() . mt_rand(1000000, 9999999);
        }

        $count = Db::transaction(function () use ($order,$trans_id,$pay_type) {
            $update = [
                'status' => self::HAS_PAY,
                'trans_id' => $trans_id,
                'pay_time' => date('Y-m-d H:i:s'),
                'pay_type' => $pay_type,
                'version' => $order['version'] + 1,
            ];
            return Order::where('id', $order['id'])
                ->where('version', $order['version'])
                ->update($update);
        });
        if ($count == 0) {
            throw new BusinessException(ErrorCode::VERSION_UPDATE);
        }
        $order = Order::find($order['id']); //renew $order
        $this->addSellerCredit1($order);
        return true;
    }

    /**
     * @param $order_id
     * @return bool
     */
    public function systemPay($order_id)
    {
        $order = Order::findOrFail($order_id);
        if ($order['status'] != self::UN_PAY) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '该订单状态不能再支付');
        }
        $this->setPay($order);
        return true;
    }

    /**
     * @param $ids
     * @param mixed $isSystem
     * @return bool
     */
    public function close($ids, $isSystem = false)
    {
        if (! is_array($ids)) {
            $ids = [$ids];
        }
        foreach ($ids as $order_id) {
            Db::transaction(function () use ($order_id,$isSystem) {
                $query = Order::where('id', $order_id);
                if (! $isSystem) {
                    if (! Auth::isAdmin()) {
                        $query = $query->where('user_id', Auth::id());
                    }
                }
                $order = $query->firstOrFail();
                if ($order['status'] != self::UN_PAY) {
                    throw new BusinessException(ErrorCode::PARAMS_INVALID, '只有未付款的订单才能直接关闭');
                }
                $update = [
                    'status' => self::CLOSE,
                    'close_time' => date('Y-m-d H:i:s'),
                    'version' => (int) $order['version'] + 1,
                ];
                $count = Order::where('id', $order['id']);
                if (! $isSystem) {
                    $count->where('version', $order['version']);
                }
                $count->update($update);
                if ($count == 0 && ! $isSystem) {
                    logger()->error('HMM');
                    throw new BusinessException(ErrorCode::VERSION_UPDATE);
                }
                $this->eventDispatcher->dispatch(new CloseOrderEvent($order));
            });
        }
        return true;
    }

    /**
     * @param $order_id
     * @param $amount
     */
    public function setTotalAmount($order_id, $amount)
    {
        if (empty($order_id) || ! is_numeric($order_id)) {
            throw new BusinessException();
        }
        if (! is_numeric($amount) || $amount < 0) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '订单金额修改错误');
        }
        $order = Order::findOrFail($order_id);
        if ($order['status'] != self::UN_PAY) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '只能待付款订单才能修改订单总额');
        }
        $update = [
            'total_amount' => $amount,
            'platform_remark' => $order['platform_remark'] . '|' . '管理员修改价格为' . $amount,
            'version' => $order['version'] + 1,
        ];
        $count = Order::where('id', $order['id'])
            ->where('version', $order['version'])
            ->update($update);
        if ($count == 0) {
            throw new BusinessException(ErrorCode::VERSION_UPDATE);
        }
    }

    /**
     * @param $order_id
     * @param $express_amount
     * @return bool
     */
    public function setExpressCost($order_id, $express_amount)
    {
        if (empty($order_id) || ! is_numeric($order_id)) {
            throw new BusinessException();
        }
        if (! is_numeric($express_amount) || $express_amount < 0) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '运费金额错误');
        }
        $order = Order::findOrFail($order_id);
        if ($order['status'] != self::UN_PAY) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '只能待付款订单才能修改运费');
        }
        if ($order['express_amount'] == round($express_amount, 2)) {
            return true;
        }
        $update = [
            'express_amount' => $express_amount,
            'platform_remark' => $order['platform_remark'] . '|' . '管理员修改运费为' . $express_amount,
            'version' => $order['version'] + 1,
            'total_amount' => $order['total_amount'] - $order['express_amount'] + $express_amount,
        ];
        $count = Order::where('id', $order['id'])
            ->where('version', $order['version'])
            ->update($update);
        if ($count == 0) {
            throw new BusinessException(ErrorCode::VERSION_UPDATE);
        }
    }

    /**
     * @param $gpc
     */
    public function setAddress($gpc)
    {
        if (empty($gpc['id']) || ! is_numeric($gpc['id'])) {
            throw new BusinessException();
        }

        $order = Order::findOrFail($gpc['id']);

        $update = [
            'receiver' => $gpc['receiver'],
            'receiver_mobile' => $gpc['receiver_mobile'],
            'province' => $gpc['province'],
            'city' => $gpc['city'],
            'area' => $gpc['area'],
            'address' => $gpc['address'],
        ];

        Order::where('id', $order['id'])->update($update);
    }

    /**
     * @param $gpc
     * @return bool
     */
    public function deliver($gpc)
    {
        return make(OrderPackageLogic::class)->deliver($gpc);
    }

    public function confirm($order_id)
    {
        $query = Order::where('id', $order_id);
        if (! Auth::isAdmin()) {
            $query = $query->where('user_id', Auth::id());
        }
        $order = $query->firstOrFail();

        if (! in_array($order['status'], [self::HAS_PAY, self::HAS_DELIVER])) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '订单状态错误');
        }
        $this->eventDispatcher->dispatch(new FinishOrderEvent($order));
    }

    public function autoShutdown()
    {
        $auto_shutdown_minutes = (int) Common::getSetting('mall.auto_shutdown_minutes', 30);
        $out_time = Carbon::now()->subMinutes($auto_shutdown_minutes)->format('Y-m-d H:i:s');
        $orders = Order::where('status', self::UN_PAY)
            ->where('created_at', $out_time, '<')
            ->select('id,version,status')
            ->get()->toArray();
        $count = 0;
        foreach ($orders as $order) {
            $effect = Order::where('id', $order['id'])
                ->where('version', $order['version'])
                ->update(['status' => self::CLOSE]);
            $count += $effect;
        }
        return $count;
    }

    public function autoFinish()
    {
        //维权中的订单不会被自动确认收货
        $days = (int) Common::getSetting('business.auto_finish', 14);
        $out_time = Carbon::now()->subDays($days)->format('Y-m-d H:i:s');
        $orders = Order::where('status', self::HAS_DELIVER)
            ->where('refund_id', 0)
            ->where('deliver_time', $out_time, '<')
            ->get()->toArray();
        $count = 0;
        foreach ($orders as $order) {
            try {
                $this->finishOrder($order);
                ++$count;
            } catch (\Exception $e) {
                logger()->info('结束订单:' . $order['order_no'] . '失败');
            }
        }
        return $count;
    }

    /**
     * @param $order_id
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function recoverGoodsStock($order_id)
    {
        $order = $this->detail($order_id);
        foreach ($order['order_goods'] as $order_goods) {
            make(GoodsLogic::class)->updateStocks(
                $order_goods['goods_id'],
                $order_goods['option_id'],
                -$order_goods['num']
            );
        }
    }

    /**
     * @param $gpc
     * @return null|\Psr\Http\Message\ResponseInterface
     */
    public function excel($gpc)
    {
        $base_data  = $this->query($gpc)->pluck('orders.id');

        $userId = Auth::id();
        if ($userId == null || $userId == '' ) { 
            throw new BusinessException(ErrorCode::AUTH_FAIL, 'Credential mismatch');
        }
        $admin = Admin::where('id', '=', $userId)->first();
        if ($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) {
            $goodsIds = Goods::where('created_by', '=', $userId)->pluck('id');
            $orders = OrderGoods::whereIn('goods_id', $goodsIds)->pluck('order_id')->toArray();
            $base_data_array = $base_data->toArray() ; 

            $base_data = array_intersect($base_data_array , $orders); 

        }
        print_r($base_data);
        $data = [];
        foreach ($base_data as $order) {
            $v = $this->detail($order);
            $goods = '';
            foreach ($v['order_goods'] as $goods) {
                $goods = $goods['title'] . ' ' . $goods['option_title'];
            }
            if (isset($v['order_goods']) && is_array($v['order_goods']) && count($v['order_goods'] > 1)) {
                $goods .= '等';
            }
            try {
                $user = User::findOrFailFromCache($v['user_id']);
            } catch (\Exception $e) {
                $user['name'] = '';
            }
            switch ($v['status']) {
                case -1:
                    $status = '关闭';
                    break;
                case 0:
                    $status = '待付款';
                    break;
                case 1:
                    $status = '待发货';
                    break;
                case 2:
                    $status = '待收货';
                    break;
                case 3:
                    $status = '已完成';
                    break;
                default:
                    $status = '未知状态';
                    break;
            }
            $data[] = [
                'order_no' => $v['order_no'],
                'name' => $user['name'],
                'total_amount' => $v['total_amount'],
                'goods' => $goods,
                'status' => $status,
                'receiver' => $v['receiver'],
                'receiver_mobile' => $v['receiver_mobile'],
                'address' => Area::getAddressText($v['province'], $v['city'], $v['area']) . $v['address'],
                'refund_amount' => $v['refund_amount'],
                'created_at' => $v['created_at'],
            ];
        }
        $filename = '订单导出' . date('YmdHis');
        $header = [
            '订单编号', '会员', '订单金额', '产品', '状态', '收件人', '收件人联系方式', '地址', '退款金额', '创建时间',
        ];
        return make(Excel::class)->csvOutput($filename, $header, $data);
    }

    /**
     * @param $gpc
     * @return null|\Psr\Http\Message\ResponseInterface
     */

    public function excelPending($gpc)
    {
        $base_data  = $this->query($gpc)->pluck('orders.id');

        $userId = Auth::id();
        if ($userId == null || $userId == '' ) { 
            throw new BusinessException(ErrorCode::AUTH_FAIL, 'Credential mismatch');
        }
        $admin = Admin::where('id', '=', $userId)->first();
        if ($admin['group_id'] == AdminLogic::GROUP_ID_SELLER) {
            $goodsIds = Goods::where('created_by', '=', $userId)->pluck('id');
            $orders = OrderGoods::whereIn('goods_id', $goodsIds)->pluck('order_id')->toArray();
            $base_data_array = $base_data->toArray() ; 

            $base_data = array_intersect($base_data_array , $orders); 

        }
        print_r($base_data);
        $data = [];
        foreach ($base_data as $order) {
            $v = $this->detail($order);
            $goods = '';
            foreach ($v['order_goods'] as $goods) {
                $goods = $goods['title'] . ' ' . $goods['option_title'];
            }
            if (isset($v['order_goods']) && is_array($v['order_goods']) && count($v['order_goods'] > 1)) {
                $goods .= '等';
            }
            try {
                $user = User::findOrFailFromCache($v['user_id']);
            } catch (\Exception $e) {
                $user['name'] = '';
            }
            switch ($v['status']) {
                case -1:
                    $status = '关闭';
                    break;
                case 0:
                    $status = '待付款';
                    break;
                case 1:
                    $status = '待发货';
                    break;
                case 2:
                    $status = '待收货';
                    break;
                case 3:
                    $status = '已完成';
                    break;
                default:
                    $status = '未知状态';
                    break;
            }
            $data[] = [
                'order_no' => $v['order_no'],
                'goods' => $goods,
            ];
        }
        $filename = '订单导出' . date('YmdHis');
        $header = [
            '订单编号', '产品', '快递类型编号', '快递代码'
        ];
        return make(Excel::class)->csvOutput($filename, $header, $data);
    }

    public function importExcelPending($gpc) { 
        $userId = Auth::id();
        if ($userId == null || $userId == '' ) { 
            throw new BusinessException(ErrorCode::AUTH_FAIL, 'Credential mismatch');
        }
        
        $file = $gpc['file'];
        $path = BASE_PATH.'/public/upload/excel';	
        
		if (!is_dir($path)) {
			mkdir($path,0777,true);
        }
        // prepare file 
        $file_name = time().'csv'.'.'.'csv';
        $final_path = $path."/".$file_name; 
        $file->moveTo($final_path);
        $uploaded_file = fopen($final_path,'r'); 

        while( (!feof($uploaded_file))) {
            Db::transaction(function () use ($uploaded_file) {
                if (! $uploaded_file[0]) {              
                    $csv = fgetcsv($uploaded_file);  
                   $this->batchImportCourier($csv); 
                           
                } 
            });
            
            // skip the header 
            
        }
        fclose($uploaded_file);

      

    }

    public function batchImportCourier($array) { 
        if( isset($array[0]) && isset($array[2]) && isset($array[3])  ) { 
        
            $order = Order::where('order_no', $array[0])->first(); 
            if ($order != null ){ 
                $express_id = $array[2]; 
                $serial_number= $array[3]; 
                $custom_code = $array[4]; 
    
    
                $data = [
                    'id' => $order->id, 
                    'package_type' => 0 , 
                    'express_id' => $express_id, 
                    'express_sn' => $serial_number, 
                    'custom_code' => $custom_code, 
                    'order_goods_id' => []
                ]; 
    
                make(OrderPackageLogic::class)->deliver($data);
            }
           
            }
    }
    

    /**
	 * 获取扩展
	 * @param $type
	 * @return string
	 */
	public function getExt($type)
	{
		return explode('/', $type)[1] ?? '';
	}


    public function orderHistory($gpc)
    {
        $seller = Admin::query()->where([
            'name' => $gpc['name'],
            'group_id' => AdminLogic::GROUP_ID_SELLER,
        ])->first();
        if ($seller && password_verify($gpc['password'], $seller['password'])) {
            make(AuthManager::class)->internalLogin($seller, AdminAuthLogic::AUTH_TYPE);
            $filter = $gpc['filter'];
            return $this->pcIndex($filter, $seller['id']);
        }
        throw new BusinessException(ErrorCode::AUTH_FAIL, 'Credential mismatch');
    }

    public function quickOrder(array $gpc)
    {
        $user = make(UserAuthLogic::class)->attempt((array) $gpc);
        // if (! password_verify($gpc['password'], $user['password'])) {
        //     throw new BusinessException(ErrorCode::AUTH_FAIL);
        // }
        if (is_null($gpc['address'])) {
            $address = $this->getDefaultAddress($user['id']);
            if (! $address) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, "You don't have default address, please input it");
            }
            $gpc['address'] = [
                'receiver' => $address['name'],
                'receiver_mobile' => $address['mobile'],
                'province' => $address['province'],
                'city' => $address['city'],
                'area' => $address['area'],
                'address' => $address['address'],
            ];
        }
        $orderService = new OrderService();
        $payType = $gpc['pay_type'] ?? 'alipay-later';
        return $orderService->quickOrder($gpc, $user, $gpc['goods'], $gpc['address'], $payType);
    }

    public function getDefaultAddress($userId)
    {
        return UserAddress::query()->where([
            'user_id' => $userId,
            'is_default' => 1,
        ])->first();
    }

    public function autoFinishBySellerRank(array $rankLists = []): int
    {
        $count = 0;
        foreach ($rankLists as $rank => $releasedAt) {
            $out_time = Carbon::now()->subMinutes($releasedAt)->format('Y-m-d H:i:s');
            $orders = Order::leftJoin('order_goods', 'orders.id', '=', 'order_goods.order_id')
                ->leftJoin('goods', 'order_goods.goods_id', '=', 'goods.id')
                ->leftJoin('seller', 'goods.created_by', '=', 'seller.admin_id')
                ->where('orders.status', self::HAS_DELIVER)
                ->where('seller.rank_id', $rank)
                ->where('orders.refund_id', 0)
                ->where('deliver_time', '<=', $out_time)
                ->select([
                    'orders.*',
                ])
                ->distinct()
                ->get()->toArray();
            if (count($orders) > 0) {
                foreach ($orders as $order) {
                    try {
                        $this->finishOrder($order);
                        ++$count;
                    } catch (\Exception $e) {
                        logger()->error($e->getMessage());
                    }
                }
            }
        }
        return $count;
    }
}
