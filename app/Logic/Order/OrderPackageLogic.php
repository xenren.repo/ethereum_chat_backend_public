<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Order;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use App\Model\Order;
use App\Model\OrderGoods;
use App\Model\OrderPackage;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use App\Lib\Express\Express;

class OrderPackageLogic extends Logic
{


	/**
	 * @param $gpc
	 * @return bool
	 */
	public function deliver($gpc)
	{
		
		$order = Order::findOrFail($gpc['id']);
		if (!in_array($order['status'], [OrderLogic::HAS_PAY,OrderLogic::HAS_DELIVER])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'订单状态错误');
		}
		if (!empty($gpc['order_goods_id'])) {
			$goods_count = count($gpc['order_goods_id']);
			$count = OrderGoods::where('order_id', $gpc['id'])
				->whereIn('id', $gpc['order_goods_id'])
				->count();
			if ($goods_count != $count) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'订单产品id参数错误');
			}
		} else {
			$count = OrderGoods::where('order_id', $gpc['id'])->count();
		}
		$express = make(ExpressLogic::class)->getNameById($gpc['express_id']);
		$data = [
			'package_no' => $this->generateNo(),
			'order_id'   => $gpc['id'],
			'express'   => $express,
			'express_sn'   => $gpc['express_sn'],
			'express_id'   => $gpc['express_id'],
			'goods_num'  => $count,
			'receiver'   => $order['receiver'],
			'receiver_mobile'   => $order['receiver_mobile'],
			'address'   => $order['address'],
			'province'   => $order['province'],
			'city'   => $order['city'],
			'area'   => $order['area'],
		];
		Db::transaction(function () use ($data,$order,$gpc) {
			$package_id = OrderPackage::insertGetId($data);

			$query = OrderGoods::where('order_id', $order['id']);
			if ($gpc['order_goods_id']) {
				$query = $query->whereIn('id', $gpc['order_goods_id']);
			}
			$query->update(['package_id' => $package_id]);

			$order_update = [
				'package_num' => $order['package_num'] + 1,
				'version' => $order['version'] + 1,
			];

			if (empty($order['deliver_time'])) {
				$order_update['deliver_time'] = Carbon::now();
			}
			if ($gpc['package_type'] == 0) {
				$order_update['status'] = OrderLogic::HAS_DELIVER;
			} else {
				$exist = OrderGoods::where('order_id', $order['id'])
					->where('package_id', 0)
					->value('id');
				if ($exist) {
					$order_update['status'] = OrderLogic::HAS_PAY;
				} else {
					$order_update['status'] = OrderLogic::HAS_DELIVER;
				}
			}
			Order::where('id', $order['id'])->update($order_update);
		});
		return true;
	}


	/**
	 * @return string
	 */
	public function generateNo()
	{
		return 'OP' . time() . mt_rand(100000, 999999);
	}


	public function getPackageExpress($id)
	{
		$order_package = OrderPackage::where('id',$id)->selectRaw('express_id,express_sn')->firstOrFail()->toArray();
		return make(ExpressLogic::class)->getPackageExpress($order_package);
	}

	public function getDevTracer($gpc)
	{
		return make(Express::class)->getTrace($gpc);
	}
}
