<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Order;


use App\Constants\ErrorCode;
use App\Event\RefundOrderEvent;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Lib\Area\Area;
use App\Logic\Logic;
use App\Model\Order;
use App\Model\OrderGoods;
use App\Model\OrderPackage;
use App\Model\OrderRefund;
use Carbon\Carbon;
use App\Logic\Finance\Credit1Logic;
use Hyperf\DbConnection\Db;

class OrderRefundLogic extends Logic
{

	const REFUND_AND_RETURN = 1;  // 退款&退货

	const ONLY_REFUND = 2; // 仅退款

	const CHANGE = 3;  // 换货

	//退款处理方式

	const RESULT_REFUSE = 1; // 驳回申请

	const RESULT_PASS = 2; // 通过申请(需客户寄回商品)

	const RESULT_REFUND = 3; // 原路退款

	const RESULT_MANUAL_REFUND = 4; // 手动退款

	const RESULT_RECEIVE = 5; // 确认发货(确认收到退回商品或者直接重新发货)

	/**
	 * @param array $gpc
	 * @return bool
	 */
	public function refundApply(array $gpc)
	{
		if (isset($gpc['refund_credit']) && is_array($gpc['refund_credit'])) {
			$gpc['refund_credit'] = json_encode($gpc['refund_credit']);
		}
		$user_id = Auth::id();

		$order = Order::where('id', $gpc['id'])->where('user_id', $user_id)->firstOrFail();

		$this->check($gpc, $order);

		// 换货时退款金额为0
		if ($gpc['apply_type'] == self::CHANGE) {
			$gpc['refund_amount'] = 0;
		}
		$data = [
			'refund_no' => $this->generateNo(),
			'user_id'   => $user_id,
			'order_id'  => $order['id'],
			'refund_amount' => $gpc['refund_amount'],
			'refund_credit' => $gpc['refund_credit'],
			'apply_type' => $gpc['apply_type'],
			'reason' => $gpc['reason'],
		];
		Db::transaction(function () use ($order,$data) {
			$refund_id = OrderRefund::insertGetId($data);
			Order::where('id', $order['id'])->update(['refund_id'=>$refund_id]);
		});
		return true;
	}

	/**
	 * 维权撤销
	 * @param $refund_id
	 */
	public function cancelRefundApply($refund_id)
	{
		if (empty($refund_id) || !is_numeric($refund_id)) {
			throw new BusinessException();
		}
		$query = OrderRefund::where('id', $refund_id);
		if (!Auth::isAdmin()) {
			$user_id = Auth::id();
			$query = $query->where('user_id', $user_id);
		}
		$order_refund = $query->firstOrFail();
		if ($order_refund['status'] == 1) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'无法撤销该记录');
		}
		Db::transaction(function () use ($order_refund) {
			Order::where('id', $order_refund['order_id'])->update(['refund_id' => 0]);
			OrderRefund::where('id', $order_refund['id'])->update(['status' => 1,'remark' => '该维权记录已被撤销']);
		});
	}

	/**
	 * 维权记录
	 * @param $refund_id
	 * @return
	 */
	public function refundDetail($refund_id)
	{
		if (empty($refund_id) || !is_numeric($refund_id)) {
			throw new BusinessException();
		}
		$query = OrderRefund::where('id', $refund_id);
		if (!Auth::isAdmin()) {
			$user_id = Auth::id();
			$query = $query->where('user_id', $user_id);
		}
		$order_refund = $query->firstOrFail();
		if ($order_refund['refund_credit']) {
			$order_refund['refund_credit'] = json_decode($order_refund['refund_credit'], true);
		}
		$order_refund['address_text'] = make(Area::class)->getAddressText($order_refund['province'], $order_refund['city'], $order_refund['area']).$order_refund['address'];
		return $order_refund;
	}


	public function generateNo()
	{
		return 'RF' . time() . mt_rand(100000, 999999);
	}

	/**
	 * @param $gpc
	 * @param $order
	 */
	private function check($gpc, $order)
	{
		$un_deal_id = OrderRefund::where('order_id', $order['id'])
			->where('status', 0)
			->value('id');
		if ($un_deal_id) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'已存在未处理的申请,不能重复提交申请');
		}

		if (($order['total_amount'] - $this->getPartRefund($order)) < $gpc['refund_amount']) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'退款金额超过限制');
		}
		// 未付款
		if ($order['status'] == OrderLogic::UN_PAY) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'未付款订单不能发起退款');
		} elseif ($order['status'] == OrderLogic::HAS_PAY) {
			// 仅能选择 仅退款
			if ($gpc['apply_type'] != self::ONLY_REFUND) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'未发货订单只能申请直接退款');
			}
		} elseif ($order['status'] == OrderLogic::HAS_DELIVER) {
			// 仅能选择 仅退款
			if (!in_array($gpc['apply_type'], [1,2,3])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'申请退款方式错误');
			}
		} else {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'该订单状态不支持申请退款');
		}
	}

	/**
	 * 获取订单正在申请或已经完成的部分退款金额
	 * @param $order
	 * @return int
	 */
	public function getPartRefund($order)
	{
		return 0;
	}

	/**
	 * @param $gpc
	 * @return bool
	 */
	public function dealRefund($gpc)
	{
		if ($gpc['refund_result'] == self::RESULT_PASS) {
			if (empty($gpc['receiver']) || empty($gpc['receiver_mobile']) || empty($gpc['province']) || empty
				($gpc['city']) || empty($gpc['area']) || empty($gpc['address'])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'退货信息必须完善');
			}
		}
		$order_refund = OrderRefund::findOrFail($gpc['refund_id']);
		if ($order_refund['refund_result'] == $gpc['refund_result'] && $gpc['refund_result'] != self::RESULT_PASS) {
			return true;
		}
		$order = Order::findOrFail($order_refund['order_id']);
		$this->checkDeal($order_refund, $gpc);
		$update = [
			'refund_result' => $gpc['refund_result'],
			'status'        => 1,
			'version'       => $order_refund['version'] + 1
		];
		if ($gpc['refund_result'] == self::RESULT_PASS) {
			$update['receiver'] = $gpc['receiver'];
			$update['receiver_mobile'] = $gpc['receiver_mobile'];
			$update['province'] = $gpc['province'];
			$update['city'] = $gpc['city'];
			$update['area'] = $gpc['area'];
			$update['address'] = $gpc['address'];
		}
		Db::transaction(function () use ($order_refund, $update,$order) {
			$count = OrderRefund::where('id', $order_refund['id'])
				->where('version', $order_refund['version'])
				->update($update);

			if ($count == 0) {
				throw new BusinessException(ErrorCode::VERSION_UPDATE);
			}

			if ($update['refund_result'] == self::RESULT_REFUND) {
				dispatch(new RefundOrderEvent($order,$order_refund));
			}

			$order_update = [];
			if ($update['refund_result'] != self::RESULT_PASS) {
				// 重置订单当前维权状态
				$order_update =['refund_id'=>0];
			}

			if (in_array($update['refund_result'], [self::RESULT_REFUND, self::RESULT_MANUAL_REFUND])) {
				$order_update['refund_amount'] = $order_refund['refund_amount'];
				$order_update['status'] = OrderLogic::FINISH;
				make(Credit1Logic::class)->refundFreezeSeller($order_refund['order_id']); 

			}

			if ($order_update) {
				Order::where('id', $order_refund['order_id'])->update($order_update);
			}
		});
		// TODO 站内信息通知
		return true;
	}


	/**
	 * @param $order_refund
	 * @param $gpc
	 */
	private function checkDeal($order_refund, $gpc)
	{
		if ($order_refund['refund_result'] == self::RESULT_REFUND) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'已经提交退款的状态不能被修改');
		}

		// 仅退款
		if ($order_refund['apply_type'] == self::ONLY_REFUND) {
			if (!in_array($gpc['refund_result'], [self::RESULT_REFUND, self::RESULT_REFUSE, self::RESULT_MANUAL_REFUND])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'不允许的处理结果状态');
			}
		} elseif ($order_refund['apply_type'] == self::CHANGE) {  // 换货
			if (!in_array($gpc['refund_result'], [self::RESULT_PASS, self::RESULT_REFUSE, self::RESULT_RECEIVE])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'不允许的处理结果状态');
			}
		} elseif ($order_refund['apply_type'] == self::REFUND_AND_RETURN) {    // 退款&退货
			if (!in_array($gpc['refund_result'], [self::RESULT_PASS, self::RESULT_REFUSE, self::RESULT_REFUND,self::RESULT_MANUAL_REFUND])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'不允许的处理结果状态');
			}
		} else {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'未知类型错误');
		}
	}
}
