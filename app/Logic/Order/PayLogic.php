<?php

declare(strict_types=1);

namespace App\Logic\Order;

use App\Constants\ErrorCode;
use App\Event\PayOrderEvent;
use App\Event\SpendingRewardCalculatorEvent;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Finance\AlipayLaterLogic;
use App\Logic\Finance\AlipayLogic;
use App\Logic\Finance\Credit1Logic;
use App\Logic\Finance\Credit2Logic;
use App\Logic\Finance\Credit3Logic;
use App\Logic\Finance\MudanLogic;
use App\Logic\Finance\Payment\WechatPayLogic;
use App\Logic\Logic;
use App\Model\Order;
use App\Model\ThirdPay;
use App\Model\User;
use App\Traits\Payable;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Psr\EventDispatcher\EventDispatcherInterface;

class PayLogic extends Logic
{
    use Payable;

    const CLOSE = -1;

    const UN_PAY = 0;

    const HAS_PAY = 1;

    const PART_REFUND = 2;

    const REFUND = 3;

    const PAY_LATER = 4;

    /**
     * @Inject
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function doesContainRewardMode($goods)
    {
        $contain = true;
        foreach ($goods as $k => $v) {
            if ($v['is_reward'] != 'reward_mode') {
                $contain = false;
            }
        }
        return $contain;
    }

    /**
     * @param mixed $gpc
     * @return array
     */
    public function getAllowList($gpc)
    {
        $order = make(OrderLogic::class)->detail($gpc['order_id']);
        $allMudanGoods = true;
        $totalCCoinRequired = 0;
        foreach ($order['order_goods'] as $k => $v) {
            if (! $v['is_mudan_product']) {
                $allMudanGoods = false;
            }
            if ($v['is_reward'] == 'reward_mode') {
                $price = $v['price'];
                $endPrice = $v['item_endprice'];
                $ccoin = ($price - $endPrice) * 10;
                $totalCCoinRequired += $ccoin;
            }
        }
        $this->totalCCoinRequired = $totalCCoinRequired;
        if ($allMudanGoods) {
            return $this->getAllowedListOnly(['mudan']);
        }
        $containRewardMode = $this->doesContainRewardMode($order['order_goods']);
        if ($containRewardMode) {
            return $this->getAllowedListExcept([
                'mudan',
            ]);
            // or can use this
            // return $this->getAllowedListOnly([
            //     'alipay',
            //     'wechat',
            //     'balance',
            //     'credit3',
            //     'reward_mode',
            //     'total_c_coin_required',
            //     'alipay-later',
            //     'alipay-qr'
            // ]);
        }
        return $this->getAllowedListExcept(['mudan', 'reward_mode', 'credit3', 'balance']);
    }

    public function deductUserCCoin($coin, $userId)
    {
        $url = 'http://localhost:8000/api/deduct-user-c-coin/' . $userId;

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => ['amount' => $coin],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);
        echo var_dump($response);
    }

    public function updateOrderAmonut($amount, $userId, $orderId)
    {
        Order::where('id', '=', $orderId)->update([
            'total_amount' => $amount,
            'original_total' => $amount,
        ]);
    }

    public function jinqiaPay($gpc)
    {
        $orderLogic = new OrderLogic();
        $amount = $gpc['amount'];
        $jinqiaUserId = $gpc['user_id'] ?? '';
        $user = User::where('name', '=', 'fong7890')->first();

        $order_info = [
            'order_no' => $orderLogic->generateNo(),
            'user_id' => $user['id'],
            'pid' => $user['pid'],
            'type' => OrderLogic::JINQIA_PAID_TYPE,
            'total_amount' => $amount,
            'original_total' => $amount,
            'express_amount' => $amount,
            'goods_amount' => $amount,
            'remark' => 'JINQIA ORDER FOR USER ID: ' . $jinqiaUserId,
            'receiver' => $user->id,
            'receiver_mobile' => $user->mobile,
            'province' => $user->province,
            'city' => $user->city,
            'area' => $user->area,
            'address' => $user->address,
            'reward_price' => 0,
            'reward_mode' => 0,
        ];
        $order = Order::create($order_info);

        return $this->alipayLaterJinQia($order, $user, $gpc['cart']);
    }
    
    /**
     * @param $gpc
     * @return bool
     */
    public function pay($gpc)
    {
        // TODO 检测支付方式是否允许
        if (! Auth::isAdmin() && $gpc['pay_type'] == 'system') {
            throw new BusinessException(ErrorCode::PERMISSION_DENY);
        }

        $order = make(OrderLogic::class)->detail($gpc['order_id']);
        if ($order['status'] != 0) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '该订单状态无法支付');
        }
        if (isset($gpc['reward_amount']) && $gpc['reward_amount'] > 0) {
            $order['total_amount'] = $gpc['amount'];
        }
        if ($order['total_amount'] != round($gpc['amount'], 2)) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '订单金额错误');
        }
        if ($order['type'] == OrderLogic::SPECIAL_TYPE && $gpc['pay_type'] != 'special_pay') {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '专区订单只能使用专区券支付');
        }
        logger()->info(json_encode($order));
        $allMudanGoods = true;
        $giveAward = true;
        foreach ($order['order_goods'] as $k => $v) {
            if (! $v['is_mudan_product']) {
                $allMudanGoods = false;
            }
            if ($v['created_by'] != 0) {
                $giveAward = false;
            }
        }

        $user_id = Auth::id();
        if (isset($gpc['reward_amount']) && $gpc['reward_amount'] > 0) {
            self::deductUserCCoin($gpc['reward_amount'], $user_id);
        }

        if ($allMudanGoods && $gpc['pay_type'] != 'mudan') {
            throw new BusinessException(ErrorCode::PERMISSION_DENY, '仅仅能使用牡丹专区券购买');
        }

        return Db::transaction(function () use ($order,$gpc, $allMudanGoods, $user_id, $giveAward) {
            switch ($gpc['pay_type']) {
                case 'balance':
                    $pay_result = $this->balancePay($order, $gpc['pay_password'] ?? '');
                    make(OrderLogic::class)->setPay($order, $pay_result['record_no'], OrderLogic::BALANCE);
                    if ($gpc['reward_amount'] > 0) {
                        self::updateOrderAmonut($gpc['amount'], $user_id, $order['id']);
                    }
                    $this->eventDispatcher->dispatch(new PayOrderEvent($order));
                    break;
                case 'special_pay':
                        $pay_result = $this->specialPay($order, $gpc['pay_password'] ?? '');
                        make(OrderLogic::class)->setPay($order, $pay_result['record_no'], OrderLogic::SPECIAL_PAY);
                        $this->eventDispatcher->dispatch(new PayOrderEvent($order));
                break;
                case 'credit3':
                        $pay_result = $this->credit3Pay($order, $gpc['pay_password'] ?? '');
                        make(OrderLogic::class)->setPay($order, $pay_result['record_no'], OrderLogic::CREDIT3_PAY);
                        $this->eventDispatcher->dispatch(new PayOrderEvent($order));
                break;
                case 'alipay':
                    $pay_result = $this->alipay($order);
                break;
                case 'wechat':
                $pay_result = $this->wechat($order);
                break;
                case 'alipay-later':
                    $user = Auth::user();
                    $pay_result = $this->alipayLater($order, $user);
                break;
                case 'alipay-qr':
                    $user = Auth::user();
                    $pay_result = $this->alipayQr($order, $user);
                break;
                case 'mudan':
                        $user = User::where('id', '=', $order['user_id'])->first();
                        if ($user->mudan_coin < $order['total_num']) {
                            throw new BusinessException(ErrorCode::PERMISSION_DENY, '牡丹专区券不足');
                        }
                        if ($allMudanGoods == false) {
                            throw new BusinessException(ErrorCode::PERMISSION_DENY, '仅仅能使用牡丹专区券购买');
                        }
                        $pay_result = $this->balanceMudanPay($order, $order['total_num']);
                        make(OrderLogic::class)->setPay($order, $pay_result['record_no'], OrderLogic::BALANCE);
                        if ($gpc['reward_amount'] > 0) {
                            self::updateOrderAmonut($gpc['amount'], $user_id, $order['id']);
                        }
                        $this->eventDispatcher->dispatch(new PayOrderEvent($order));
                break;
                default:
                    throw new BusinessException(ErrorCode::PARAMS_INVALID, '不支持的支付方式');
            }
            if ($giveAward) {
                // event dispatcher for 1% percent reward
                $user = Auth::user();
                $this->eventDispatcher->dispatch(new SpendingRewardCalculatorEvent($user, $order));
            }
            return $pay_result;
        });
    }

    /**
     * @param $order
     * @param $order_refund
     */
    public function refund($order, $order_refund)
    {
        // TODO 检查订单存在退款且总共退款总额不能超过订单支付总金额
        switch ($order['pay_type']) {
            case 0:
                break;
            case 1: // 微信退款
                break;
            case 2: // 支付宝退款
                break;
            case 3: // 余额退款
                $gpc = [
                    'remark' => '订单:' . $order['order_no'] . '退款',
                    'user' => $order['user_id'],
                    'credit' => $order_refund['refund_amount'],
                    'foreign_id' => $order_refund['id'],
                ];
                    $gpc['type'] = Credit1Logic::REFUND;
                    make(Credit1Logic::class)->setCredit($gpc);

                break;
            case OrderLogic::SPECIAL_PAY: // 专区券退款
                throw new BusinessException(ErrorCode::PARAMS_INVALID, '专区券产品目前不支持退款');
                break;
            case OrderLogic::CREDIT3_PAY:
                $gpc = [
                    'remark' => '订单:' . $order['order_no'] . '退款',
                    'user' => $order['user_id'],
                    'credit' => $order_refund['refund_amount'],
                    'foreign_id' => $order_refund['id'],
                ];
                $gpc['type'] = Credit3Logic::REFUND;
                logger()->error('[refund credit3 called]');
                logger()->error(json_encode($gpc));
                make(Credit3Logic::class)->setCredit($gpc);
                break;
            default:
                break;
        }
    }

    public function payLaterHandler($service, $no, $cart='')
    {
        if (strtolower($service) === 'alipay') {
            $data = ThirdPay::query()->where([
                'system_no' => $no,
            ])->first()->toArray();
            if (! in_array($data['status'], [self::UN_PAY, self::PAY_LATER])) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Transaction failed');
            }

            $order = Order::find($data['order_id']);
            if($order->type == 7){
                return make(AlipayLaterLogic::class)->webJinQiaPay($data, $cart);
            } else {
                return make(AlipayLaterLogic::class)->webPay($data);
            }
        }

        throw new BusinessException(ErrorCode::SYSTEM);
    }

    /**
     * @return string
     */
    public function generateNo()
    {
        return  'PY' . time() . mt_rand(1000000, 9999999);
    }

    /**
     * @param $gpc
     */
    public function alipayNotify($gpc)
    {
        make(AlipayLogic::class)->verify($gpc);
        return $this->alipayNotifyLogic($gpc);
    }

    public function alipayNotifyNew($gpc)
    {
        make(AlipayLaterLogic::class)->newVerify($gpc);
        return $this->alipayNotifyLogic($gpc);
    }

    public function alipayNotifyLogic($gpc)
    {
        $rules = [
            'trade_status' => 'required',
            'total_amount' => 'required|numeric',
            'out_trade_no' => 'required',
            'trade_no' => 'required',
            'gmt_payment' => 'required',
            'app_id' => 'required',
            'buyer_logon_id' => 'required',
        ];
        validate($gpc, $rules);
        if ($gpc['trade_status'] != 'TRADE_SUCCESS') {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '支付状态错误');
        }

        $third_pay = ThirdPay::where('system_no', $gpc['out_trade_no'])
            ->where('pay_type', OrderLogic::ALIPAY)
            ->first();
        if (empty($third_pay)) {
            logger()->error($gpc);
            logger()->error('三方支付记录未找到');
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '三方支付记录未找到');
        }
        if ($third_pay['money'] != round($gpc['total_amount'], 2)) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '金额错误');
        }
        $order = Order::find($third_pay['order_id']);
        Db::transaction(function () use ($gpc,$order) {
            make(OrderLogic::class)->setPay($order, $gpc['trade_no'], OrderLogic::ALIPAY);
            $update = [
                'status' => self::HAS_PAY,
                'pay_time' => $gpc['gmt_payment'],
                'trans_no' => $gpc['trade_no'],
                'mch_id' => $gpc['app_id'],
                'unionid' => $gpc['buyer_logon_id'],
            ];
            ThirdPay::where('system_no', $gpc['out_trade_no'])->update($update);
            try {
                $this->eventDispatcher->dispatch(new PayOrderEvent($order));
            } catch (\Exception $e) {
                logger()->error('订单支付监听错误:' . $order['order_no']);
            }
        });
    }

    public function credit3Pay($order, string $param)
    {
        if ($order['total_amount'] < 0) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '订单金额错误');
        }
        //        UserLogic::checkPayPassword($pay_password);
        $remark = '订单:' . $order['order_no'] . '支付';
        $gpc = [
            'user' => $order['user_id'],
            'credit' => -$order['total_amount'],
            'type' => Credit3Logic::ORDER,
            'foreign_id' => $order['id'],
            'remark' => $remark,
            'order_goods' => $order['order_goods'],
        ];

        return make(Credit3Logic::class)->setCredit($gpc);
    }

    /**
     * Assumse format sent by wechat is json or array, if content type is xml so you need to convert gpc first xD.
     */
    public function notifyWechat(array $gpc): string
    {
        if (isset($gpc['return_code'])) {
            if (strtolower($gpc['return_code']) != strtolower('SUCCESS')) {
                logger()->error('return_code not success');
                return 'ok';
            }
        } else {
            logger()->error('return_code not success');
            return 'ok';
        }
        if (isset($gpc['result_code'])) {
            if (strtolower($gpc['result_code']) != strtolower('SUCCESS')) {
                logger()->error('result_code not success');
                return 'ok';
            }
        } else {
            logger()->error('result_code not success');
            return 'ok';
        }

        $third_pay = ThirdPay::where('system_no', $gpc['out_trade_no'])
            ->where('pay_type', OrderLogic::WECHAT)
            ->first();
        if (empty($third_pay)) {
            logger()->error($gpc);
            logger()->error('三方支付记录未找到');
            return 'OK';
        }
        if ($third_pay['money'] != make(WechatPayLogic::class)->toCentFormat($gpc['total_amount'])) {
            return 'OK';
        }
        $order = Order::find($third_pay['order_id']);
        if ($order['status'] == self::HAS_PAY) {
            return 'OK';
        }
        $this->proccessWechatToApps($gpc, $order);
        return 'OK';
    }

    public function proccessWechatToApps($gpc, $order): void
    {
        Db::transaction(function () use ($gpc,$order) {
            logger()->info('this wechat is called');
            make(OrderLogic::class)->setPay($order, $gpc['transaction_id'], OrderLogic::WECHAT);
            $update = [
                'status' => self::HAS_PAY,
                'pay_time' => $gpc['time_end'],
                'trans_no' => $gpc['transaction_id'],
                'mch_id' => $gpc['appid'],
                'unionid' => $gpc['openid'],
            ];
            ThirdPay::where('system_no', $gpc['out_trade_no'])->update($update);
            try {
                $this->eventDispatcher->dispatch(new PayOrderEvent($order));
            } catch (\Exception $e) {
                logger()->error('订单支付监听错误:' . $order['order_no']);
            }
        });
    }

    /**
     * @param $order
     * @param null|mixed $user
     * @param null|mixed $pc
     * @return string
     */
    public function alipayQr($order, $user = null, $pc = null)
    {
        if (is_null($user)) {
            $user = Auth::user();
        }
        $data = [
            'system_no' => $this->generateNo(),
            'order_id' => $order['id'],
            'money' => $order['total_amount'],
            'real_money' => $order['total_amount'],
            'user_id' => $user['id'],
            'pay_type' => OrderLogic::ALIPAY,
        ];
        $id = ThirdPay::insertGetId($data);
        if (empty($id)) {
            throw new BusinessException(ErrorCode::SYSTEM);
        }

        if (is_null($pc)) {
            $returnUrl = env('HOST') . '/mobile.html#/index/home';
        } else {
            $returnUrl = env('CLIENT_HOST') . '/pc.html#/finance/pay';
        }
        return make(AlipayLaterLogic::class)->qrPay($data, $returnUrl);
    }

    public function closePay($payIds, $orderIds)
    {
        Db::transaction(function () use ($payIds,$orderIds) {
            ThirdPay::whereIn('id', $payIds)->update([
                'status' => PayLogic::CLOSE,
            ]);
        });
    }

    /**
     * @param $order
     * @param $pay_password
     * @param mixed $totalNum
     */
    private function balanceMudanPay($order, $totalNum)
    {
        if ($order['total_amount'] < 0) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '订单金额错误');
        }

        $remark = '订单:' . $order['order_no'] . '支付';
        $gpc = [
            'user' => $order['user_id'],
            'credit' => -$order['total_amount'],
            'type' => Credit1Logic::BUY,
            'remark' => $remark,
            'total_products' => $totalNum,
        ];
        return make(MudanLogic::class)->setCredit($gpc);
    }

    /**
     * @param $order
     * @param $pay_password
     */
    private function balancePay($order, $pay_password)
    {
        if ($order['total_amount'] < 0) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '订单金额错误');
        }

        //TODO 支付密码验证
        //UserLogic::checkPayPassword($pay_password);
        $remark = '订单:' . $order['order_no'] . '支付';
        $gpc = [
            'user' => $order['user_id'],
            'credit' => -$order['total_amount'],
            'type' => Credit1Logic::BUY,
            'foreign_id' => $order['id'],
            'remark' => $remark,
            'order_goods' => $order['order_goods'],
        ];
        return make(Credit1Logic::class)->setCredit($gpc);
    }

    /**
     * @param $order
     * @param $pay_password
     */
    private function specialPay($order, $pay_password)
    {
        if ($order['total_amount'] < 0) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '订单金额错误');
        }
        if ($order['type'] != OrderLogic::SPECIAL_TYPE) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '专区券支付只允许支付专区券订单');
        }
        //        UserLogic::checkPayPassword($pay_password);
        $remark = '订单:' . $order['order_no'] . '支付';
        $gpc = [
            'user' => $order['user_id'],
            'credit' => -$order['total_amount'],
            'type' => Credit2Logic::ORDER,
            'foreign_id' => $order['id'],
            'remark' => $remark,
            'order_good' => $order['order_goods'],
        ];

        return make(Credit2Logic::class)->setCredit($gpc);
    }

    /**
     * @param $order
     * @return string
     */
    private function alipay($order)
    {
        $user = Auth::user();
        $data = [
            'system_no' => $this->generateNo(),
            'order_id' => $order['id'],
            'money' => $order['total_amount'],
            'real_money' => $order['total_amount'],
            'user_id' => $user['id'],
            'pay_type' => OrderLogic::ALIPAY,
        ];
        $id = ThirdPay::insertGetId($data);
        if (empty($id)) {
            throw new BusinessException(ErrorCode::SYSTEM);
        }

        return make(AlipayLogic::class)->pay($data);
    }

    private function alipayLater($order, $user)
    {
        $checkIsExist = ThirdPay::query()->where([
            'order_id' => $order['id'],
            'pay_type' => OrderLogic::ALIPAY,
        ])->first();
        if ($checkIsExist) {
            $data = $checkIsExist->toArray();
            $systemNo = $data['system_no'];
        } else {
            $systemNo = $this->generateNo();
            $data = [
                'system_no' => $systemNo,
                'order_id' => $order['id'],
                'money' => $order['total_amount'],
                'real_money' => $order['total_amount'],
                'user_id' => $user['id'],
                'pay_type' => OrderLogic::ALIPAY,
                'status' => self::PAY_LATER,
            ];

            $id = ThirdPay::insertGetId($data);
            if (empty($id)) {
                throw new BusinessException(ErrorCode::SYSTEM);
            }
        }
        $service = 'alipay';
        return sprintf(
            '%s/help-pay/later/%s/%s',
            env('HOST'),
            $service,
            $systemNo
        );
    }

    private function alipayLaterJinQia($order, $user, $cart)
    {
        $checkIsExist = ThirdPay::query()->where([
            'order_id' => $order['id'],
            'pay_type' => OrderLogic::ALIPAY,
        ])->first();
        if ($checkIsExist) {
            $data = $checkIsExist->toArray();
            $systemNo = $data['system_no'];
        } else {
            $systemNo = $this->generateNo();
            $data = [
                'system_no' => $systemNo,
                'order_id' => $order['id'],
                'money' => $order['total_amount'],
                'real_money' => $order['total_amount'],
                'user_id' => $user['id'],
                'pay_type' => OrderLogic::ALIPAY,
                'status' => self::PAY_LATER,
            ];

            $id = ThirdPay::insertGetId($data);
            if (empty($id)) {
                throw new BusinessException(ErrorCode::SYSTEM);
            }
        }
        $service = 'alipay';
        return sprintf(
            '%s/help-pay/later/%s/%s/%s',
            env('HOST'),
            $service,
            $systemNo,
            $cart
        );
    }

    /**
     * @param $order
     * @return string
     */
    private function wechat($order)
    {
        $user = Auth::user();
        $orderNo = $this->generateNo();
        $data = [
            'system_no' => $orderNo,
            'order_id' => $order['id'],
            'money' => $order['total_amount'],
            'real_money' => $order['total_amount'],
            'user_id' => $user['id'],
            'pay_type' => OrderLogic::WECHAT,
        ];
        $id = ThirdPay::insertGetId($data);
        if (empty($id)) {
            throw new BusinessException(ErrorCode::SYSTEM);
        }
        $response = make(WechatPayLogic::class)->rawPay($order, $data);
        return json_encode($response);
    }
}
