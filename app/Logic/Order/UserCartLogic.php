<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\Order;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Goods\GoodsLogic;
use App\Logic\Logic;
use App\Model\UserCart;

class UserCartLogic extends Logic
{


	/**
	 * @param $gpc
	 * @return bool|mixed|string
	 */
	public function index($gpc)
	{
		$user_id = Auth::id();
		$data = cache()->remember('goods_cart_user_'.$user_id, function () use ($user_id) {
			$data = UserCart::where('user_id', $user_id)
				->selectRaw('id,goods_id,images,title,num,option_id,price,total,option_name')
				->get()->toArray();
			return $data;
		},86400);
		// $goods_logic = make(GoodsLogic::class);
		// foreach ($data as $k => $v) {
		// 	if ($v['option_id']) {
		// 		$info = $goods_logic->getOptionInfo($v['goods_id'], $v['option_id']);
		// 	} else {
		// 		$info = $goods_logic->show($v['goods_id']);
		// 	}
		// 	$data[$k]['price'] = $info['price'];
		// 	$data[$k]['total'] = round($info['price']*$v['num'], 2);
		// }
		return $data;
	}

	/**
	 * @param $gpc
	 * @return string
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function store($gpc)
	{
		$cart = UserCart::where('user_id', Auth::id())
			->where('goods_id', $gpc['goods_id'])
			->where('option_id', $gpc['option_id'])
			->first();

		if ($cart) {
			$gpc['num'] = $cart['num'] + $gpc['num'];
			$info = $this->dealParams($gpc);
			$cart->update($info);
			$res = $cart['id'];
		} else {
			$info = $this->dealParams($gpc);
			$res = UserCart::insertGetId($info);
		}

		$this->clearCache($info['user_id']);
		return $res;
	}

	/**
	 * 获取产品总价价格
	 * @param $info
	 * @param $num
	 * @return mixed
	 */
	private function getGoodsTotalPrice($info, $num)
	{
		return $info['price']*$num;
	}

	/**
	 * @param $gpc
	 * @return string
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function update($gpc)
	{
		$user_id = Auth::id();
		$user_cart = UserCart::where('id',$gpc['id'])->where('user_id',$user_id)->firstOrFail()->toArray();
		$user_cart['num'] = $gpc['num'];
		$info = $this->dealParams($user_cart);
		$res = UserCart::where('id', $gpc['id'])->update($info);
		$this->clearCache($user_id);
		return $res;
	}

	/**
	 * @param $gpc
	 * @return array
	 */
	private function dealParams($gpc)
	{
		if ($gpc['num'] <= 0) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'数量不能小于0');
		}
		$goods = make(GoodsLogic::class)->show($gpc['goods_id']);
		$info = [];
		if (isset($gpc['option_id']) && $gpc['option_id']) {
			foreach ($goods['options']['options'] as $v) {
				if ($v['id'] == $gpc['option_id']) {
					if ($gpc['num'] > $v['stock']) {
						throw new BusinessException(ErrorCode::PARAMS_INVALID,'库存不足');
					}
					$info = [
						'goods_id' => $goods['id'],
						'title' => $goods['title'],
						'num' => $gpc['num'],
						'option_id' => $v['id'],
						'price' =>$v['price'],
						'option_name' =>$v['title'],
					];
					$info['total'] = $this->getGoodsTotalPrice($info, $gpc['num']);
					if (!empty($v['images'])) {
						$info['images'] = $v['images'];
					} else {
						$info['images'] = '';
					}
				}
			}
			if (empty($info)) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'规格信息错误');
			}
		} else {
			if ($gpc['num'] > $goods['stock']) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'库存不足');
			}
			$info = [
				'goods_id' => $goods['id'],
				'title' => $goods['title'],
				'num' => $gpc['num'],
				'option_id' => 0,
				'price' => $goods['price']
			];
			$info['images'] = $goods['images'][0];
			$info['total'] = $this->getGoodsTotalPrice($info, $gpc['num']);
		}
		$info['user_id'] = Auth::id();
		return $info;
	}

	/**
	 * @param $gpc
	 * @return string
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function delete($gpc)
	{
		if (!is_array($gpc['id'])) {
			$gpc['id'] = [$gpc['id']];
		}
		$user_id = Auth::id();
		$res = UserCart::where('user_id', $user_id)->whereIn('id', $gpc['id'])->delete();
		$this->clearCache($user_id);
		return $res;
	}

	/**
	 * 购物车清空
	 * @return mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function flush()
	{
		$user_id = Auth::id();
		$res = UserCart::where('user_id', $user_id)->delete();
		$this->clearCache($user_id);
		return $res;
	}

	/**
	 * 缓存清除
	 * @param int $user_id
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function clearCache($user_id = 0)
	{
		if ($user_id) {
			cache()->clearPrefix('goods_cart');
		} else {
			cache()->delete('goods_cart_user_'.$user_id);
		}
	}


}
