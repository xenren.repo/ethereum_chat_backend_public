<?php

declare(strict_types=1);

namespace App\Logic\Seller;

use App\Logic\Logic;
use App\Model\Setting;

class RankSettingLogic extends Logic
{
    const SETTING_KEY_NAME = 'seller_rank_setting';

    const KEY_RANK = 'rank';

    const KEY_AUTO_CONFIRM_ORDER = 'auto_confirm_order';

    const KEY_AUTO_RELEASE_PAYMENT = 'auto_release_payment';

    /**
     * get Setting of seller ranks.
     * HARDDDDDD FOR MEEE :(.
     * @param array|int $rank
     */
    public function getSettings($rank, ?string $keyName = null)
    {
        $data = [];
        if (! is_null($keyName)) {
            $keyName = strtolower($keyName);
        }
        foreach ($this->getLists() as $item) {
            if (isset($item[self::KEY_RANK])) {
                if (is_array($rank)) {
                    if (in_array($item[self::KEY_RANK], $rank)) {
                        if (isset($item[$keyName])) {
                            $data[$item[self::KEY_RANK]] = $item[$keyName];
                        } else {
                            $data[$item[self::KEY_RANK]] = $item;
                        }
                    }
                } else {
                    if ($item[self::KEY_RANK] === $rank) {
                        $data = $item;
                        if (isset($item[$keyName])) {
                            $data = $item[$keyName];
                        }
                    }
                }
            }
        }
        return $data;
    }

    public function setSettings(int $rank, string $keyName, ?string $keyValue): bool
    {
        if (! $keyValue || is_null($keyValue) || empty($keyValue)) {
            $keyValue = null;
        }
        $settings = [];
        $isExist = false;
        $keyName = strtolower($keyName);
        foreach ($this->getLists() as $key => $value) {
            if (isset($value[self::KEY_RANK]) && $value[self::KEY_RANK] === $rank) {
                $value[$keyName] = $keyValue;
                $isExist = true;
            }
            $settings[] = $value;
        }
        if (! $isExist) {
            $settings[] = [
                self::KEY_RANK => $rank,
                $keyName => $keyValue,
            ];
        }
        $query = Setting::query()->where('key', self::SETTING_KEY_NAME);
        if ($query->first()) {
            $result = $query->update([
                'value' => json_encode($settings),
            ]);
        } else {
            $result = Setting::create([
                'name' => self::SETTING_KEY_NAME,
                'key' => self::SETTING_KEY_NAME,
                'value' => json_encode($settings),
            ]);
        }
        return (bool) $result;
    }

    public function getLists()
    {
        $data = Setting::query()->where([
            'key' => self::SETTING_KEY_NAME,
        ])->first();
        if (! $data) {
            Setting::create([
                'name' => self::SETTING_KEY_NAME,
                'key' => self::SETTING_KEY_NAME,
                'value' => json_encode($this->getDefaultLists()),
            ]);
            return $this->getDefaultLists();
        }
        return json_decode($data['value'], true);
    }

    /**
     * Note: all time based setting must be in minutes, to make easier to maintain.
     */
    public function getDefaultLists(): array
    {
        return [
            [
                self::KEY_RANK => 1,
                self::KEY_AUTO_CONFIRM_ORDER => (60 * 48),
                self::KEY_AUTO_RELEASE_PAYMENT => (60 * 48),
            ],
            [
                self::KEY_RANK => 2,
                self::KEY_AUTO_CONFIRM_ORDER => (60 * 48),
                self::KEY_AUTO_RELEASE_PAYMENT => (60 * 48),
            ],
            [
                self::KEY_RANK => 3,
                self::KEY_AUTO_CONFIRM_ORDER => (60 * 2),
                self::KEY_AUTO_RELEASE_PAYMENT => (60 * 1),
            ],
            [
                self::KEY_RANK => 4,
                self::KEY_AUTO_CONFIRM_ORDER => (60 * 2),
                self::KEY_AUTO_RELEASE_PAYMENT => (60 * 1),
            ],
        ];
    }
}
