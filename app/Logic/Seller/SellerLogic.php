<?php

declare(strict_types=1);

namespace App\Logic\Seller;

use App\Exception\BusinessException;
use App\Logic\Admin\AdminLogic;
use App\Model\Admin;
use App\Model\Seller;
use App\Model\SellerReject;
use App\Model\SellerReport;
use Hyperf\DbConnection\Db;

class SellerLogic
{
    public function list($gpc)
    {
        $query = $this->baseQuery();
        // $query->selectRaw('*');
        $query->select([
            'admin.id',
            'admin.name',
            'admin.mobile',
            'admin.group_id',
            'admin.status',
            'seller.approval_photo',
            'seller.company_info',
            'seller.company_picture',
            'seller.company_no',
        ]);
        $query->default([0, 2]); // approved and not yet approved
        // $query->orderBy('admin.id', 'desc');
        $query->orderBy('admin.status', 'desc');
        return $query->paginate((int) $gpc['limit'] ?? 10)->toArray();
    }

    public function changeStatus($gpc)
    {
        return Db::transaction(function ($query) use ($gpc) {
            $user = Admin::find($gpc['id']);
            $stat = 2;
            if ($gpc['status'] == 0) {
                $stat = 0;
            } elseif ($gpc['status'] == 1) {
                $stat = 1;
            }
            $user->update(['status' => $stat]);
            return $user;
        });
    }

    public function reportList($gpc)
    {
        $query = SellerReport::query();
        return $query->leftJoin('admin', 'seller_reports.seller_id', '=', 'admin.id')
            ->leftJoin('users', 'seller_reports.user_id', '=', 'users.id')
            ->orderBy('seller_reports.created_at', 'DESC')
            ->select('seller_reports.message', 'users.name as user', 'admin.name as seller', 'seller_reports.created_at')
            ->paginate((int) $gpc['limit'] ?? 25)->toArray();
    }

    public function updateReject($gpc)
    {
        $transaction = Db::transaction(function () use ($gpc) {
            foreach ($gpc['data'] as $array) {
                $reject = SellerReject::where('id', '=', $array['reject_id'])->first();

                switch ($reject['action']) {
            case AdminLogic::REJECT_PHOTO:
              $data = ['approval_photo' => $array['approval_photo']];
            break;
            case AdminLogic::REJECT_COMPANY_NO:
              $data = ['company_no' => $array['company_no']];
            break;
            case AdminLogic::REJECT_COMPANY_INFO:
              $data = ['company_info' => $array['company_info']];
            break;
            case AdminLogic::REJECT_COMPANY_PHOTO:
              $data = ['company_picture' => $array['company_picture']];
            break;
            default:
                      throw new BusinessException();
          }
                $seller = Seller::where('admin_id', '=', $array['seller_id'])
                    ->update($data);

                $reject_update = SellerReject::find($array['reject_id'])
                    ->update(['done' => true]);
            }
        });
    }

    private function baseQuery()
    {
        $query = Admin::query();
        $query->leftJoin('seller', 'admin.id', '=', 'seller.admin_id');
        $query->where('group_id', '=', AdminLogic::GROUP_ID_SELLER);
        return $query;
    }
}
