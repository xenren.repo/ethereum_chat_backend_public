<?php


namespace App\Logic\Seller;


use App\Model\Seller;
use App\Model\User;
use App\Model\Admin;
use Hyperf\DbConnection\Db;
use App\Exception\BusinessException;
use App\Constants\ErrorCode;
use App\Logic\Admin\AdminLogic;

class SellerRegisterLogic
{
    public function register($gpc)
    {
        $username = $gpc['name'];
        $password = $gpc['password'];

        if ($gpc['exist_user'] == true) 
        {
            $user = $this->isBuyerUserExist($username,$password);
            $adminUser = $this->isAdminUserExist($username,$password);
            if (!$user)
            {
                throw new BusinessException(ErrorCode::PARAMS_INVALID,'User not exist');
            }
            if ($adminUser) 
            {
                throw new BusinessException(ErrorCode::PARAMS_INVALID,'You have register before');
            }
            $getUser = $this->checkBuyerUserCredentials($username,$password);
            return $this->registerExistingUser($gpc,$getUser->toArray());
        } else
        {
            $user = $this->isBuyerUserExist($username,$password);
            $admin = $this->isAdminUserExist($username,$password);
            if ($user || $admin)
            {
                throw new BusinessException(ErrorCode::PARAMS_INVALID,'User exist');
            }
            return $this->registerNewUser($gpc);
        }
    }
    private function registerNewUser($gpc)
    {
        return Db::transaction(function () use ($gpc) 
        {
            $user = Admin::create([
                'name' => $gpc['name'],
                'realname' => $gpc['realname'],
                'password' => password_hash($gpc['password'],PASSWORD_BCRYPT),
                'mobile' => $gpc['phone'],
                'group_id' => AdminLogic::GROUP_ID_SELLER,
                'status' => AdminLogic::DEFAULT_STATUS
            ]);
            $seller = Seller::create([
                'admin_id' => $user->id,
                'approval_photo' => $gpc['photo'],
                'company_info' => $gpc['company']['info'],
                'company_picture' => $gpc['company']['photo'],
                'company_no' => $gpc['company']['no'],
            ]);
            return ['message' => 'Success register as seller, wait admin confirmation'];
        });
    }
    private function registerExistingUser($gpc,$user)
    {
        return Db::transaction(function () use ($gpc,$user) 
        {
            $user = Admin::create([
                'name' => $user['name'],
                'realname' => $user['realname'],
                'password' =>$user['password'],
                'mobile' => $gpc['phone'],
                'group_id' => AdminLogic::GROUP_ID_SELLER,
                'status' => AdminLogic::DEFAULT_STATUS
            ]);
            $seller = Seller::create([
                'admin_id' => $user->id,
                'approval_photo' => $gpc['photo'],
                'company_info' => $gpc['company']['info'],
                'company_picture' => $gpc['company']['photo'],
                'company_no' => $gpc['company']['no'],
                'new_user' => false
            ]);
            return ['message' => 'Success register as seller, wait admin confirmation'];
        });
    }
    private function isAdminUserExist($username,$password)
    {
        $user = Admin::query()->where(['name' => $username])->first();
        if ($user)
        {
            return true;
        }
        return false;
    }
    private function isBuyerUserExist($username,$password)
    {
        $buyerUser = User::query()->where(['name' => $username])->first();
        if ($buyerUser)
        {
            return true;
        }
        return false;
    }
    private function checkBuyerUserCredentials($username,$password)
    {        
        $user = User::query()->where(['name' => $username])->first();
        if ($user && password_verify($password,$user->password))
        {
            return $user;
        }
        return false;
    }
    private function createSeller(array $data)
    {
        $user = $this->createUser($data);

    }

    public function SellerVerifPhone ($gpc) {
        $url = 'https://test.fr167.com/api/guest/seller/send-mobile-verification';
        //$url = 'http://localhost:8000/api/guest/seller/send-mobile-verification';
        //$url = 'https://fr167.com/api/guest/seller/send-mobile-verification';
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('phone' => $gpc['phone']),
        ));

        $response = curl_exec($curl);
        
        curl_close($curl);

        $result = json_decode($response,true);
        return $result; 

    }
}