<?php

declare(strict_types=1);

namespace App\Logic\Seller;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use App\Model\Setting;
use App\Traits\Arrayable;

class TopUpLogic extends Logic
{
    use Arrayable;

    const SETTING_KEY_NAME = 'seller_topup_lists';

    public function getTopUpLists(int $currentRank = 0)
    {
        foreach ($this->getLists() as $item) {
            if ((int) $item['current_rank'] === $currentRank) {
                return $item['lists'];
            }
        }
        throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Lists not found');
    }

    public function deleteTopUpValue(int $currentRank, int $destinationRank)
    {
        $lists = $this->getLists();
        $data = [];
        foreach ($lists as $item) {
            $newItem = [];
            $newItem = $item;
            if ((int) $item['current_rank'] === $currentRank) {
                foreach ($item['lists'] as $key => $values) {
                    if ((int) $values['rank_id'] === $destinationRank) {
                        unset($newItem['lists'][$key]);
                    }
                }
            }
            $data[] = $newItem;
        }
        $setting = Setting::query()->where([
            'key' => self::SETTING_KEY_NAME,
        ])->first();
        if (! $setting) {
            Setting::create([
                'name' => self::SETTING_KEY_NAME,
                'key' => self::SETTING_KEY_NAME,
                'value' => json_encode($data),
            ]);
        } else {
            $setting->update([
                'value' => json_encode($data),
            ]);
        }
        return $this->getLists();
    }

    public function setTopUpValue(int $currentRank, int $destinationRank, array $values)
    {
        if (! $this->isRequiredKeysExist($values, ['rank_id', 'message', 'amount'])) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Required Data is missing');
            return null; // it will be used by command
        }
        $lists = $this->getLists();
        $data = [];
        $currentRankExist = false;
        foreach ($lists as $item) {
            if ((int) $item['current_rank'] === $currentRank) {
                $isDestinationRankExist = false;
                $temporaryItem = [];
                foreach ($item['lists'] as $lists) {
                    if ((int) $lists['rank_id'] === $destinationRank) {
                        $lists = $values;
                        $isDestinationRankExist = true;
                    }
                    $temporaryItem[] = $lists;
                }
                if (! $isDestinationRankExist) {
                    $temporaryItem[] = $values;
                }
                $currentRankExist = true;
                $item['lists'] = $temporaryItem;
            }
            $data[] = $item;
        }
        if (! $currentRankExist) {
            $data[] = [
                'current_rank' => $currentRank,
                'lists' => [
                    $this->setRankValue($destinationRank, $data['amount'], $data['message']),
                ],
            ];
        }
        $setting = Setting::query()->where([
            'key' => self::SETTING_KEY_NAME,
        ])->first();
        if (! $setting) {
            Setting::create([
                'name' => self::SETTING_KEY_NAME,
                'key' => self::SETTING_KEY_NAME,
                'value' => json_encode($data),
            ]);
        } else {
            $setting->update([
                'value' => json_encode($data),
            ]);
        }
        return $this->getLists();
    }

    public function getLists()
    {
        $data = Setting::query()->where([
            'key' => self::SETTING_KEY_NAME,
        ])->first();
        if (! $data) {
            Setting::create([
                'name' => self::SETTING_KEY_NAME,
                'key' => self::SETTING_KEY_NAME,
                'value' => json_encode($this->getDefaultLists()),
            ]);
            return $this->getDefaultLists();
        }
        return json_decode($data['value'], true);
    }

    public function getDefaultLists()
    {
        return [
            [
                'current_rank' => 0,
                'lists' => [
                    $this->setRankValue(1, 2000),
                    $this->setRankValue(2, 1000),
                    $this->setRankValue(3, 2000),
                    $this->setRankValue(4, 1000),
                ],
            ],
            [
                'current_rank' => 1,
                'lists' => [
                    $this->setRankValue(2, 1000),
                    $this->setRankValue(3, 2000),
                    $this->setRankValue(4, 1000),
                ],
            ],
            [
                'current_rank' => 2,
                'lists' => [
                    $this->setRankValue(1, -1, 'downgrade'),
                    $this->setRankValue(2, 0, '已达相同等级'),
                    $this->setRankValue(3, 2000),
                    $this->setRankValue(4, 1000),
                ],
            ],
            [
                'current_rank' => 3,
                'lists' => [
                    $this->setRankValue(4, 1000),
                ],
            ],
        ];
    }

    public function setRankValue($rankId, $amount, $message = '')
    {
        return [
            'rank_id' => $rankId,
            'amount' => $amount,
            'message' => $message,
        ];
    }
}
