<?php
declare(strict_types=1);

namespace App\Logic\Storage;


use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\UploadFileLog;
use Carbon\Carbon;

class UploadFileLogic extends Logic
{
    public function upload($file,$path,$fileName = null)
    {
        if (!file_exists(BASE_PATH.'/'.$path))
        {
            mkdir(BASE_PATH . '/' .$path,0755,true);
//            mkdir(BASE_PATH . '/' .$path);
        }
        $fullNameWithPath = $path.'/'.$fileName.'.'.$file->getExtension();
        logger()->error("FULL PATH: ".$fullNameWithPath);
        logger()->error("BASE_PATH : ".BASE_PATH);
        $file->moveTo(BASE_PATH.'/'.$fullNameWithPath);
        if ($file->isMoved())
        {
            if (chmod(BASE_PATH.'/'.$fullNameWithPath,0644))
            {
                return [
                    'path' => $path,
                    'file_name' => $fileName,
                    'ext' => $file->getExtension(),
                    'original_name' => $file->getClientFileName(),
                    'full_path' => $fullNameWithPath
                ];
            }
        }
        return [
            'failed' => $file->getClientFileName()
        ];
    }

    public function uploadHandler($gpc)
    {
        $userId = Auth::id();
        $time   = Carbon::now();
        $path = $this->setPath($userId,$time);
        $res = [];
        foreach ($gpc->getUploadedFiles()['files'] AS $file)
        {
            if ($file->isValid())
            {
                $hashFileName = uniqid($time->format('His'));
                $upload = $this->upload($file,$path,$hashFileName);
                $res[] = $upload;
            }
        }
        $actualResponses = [];
        if (count($res) > 0)
        {
            foreach ($res as $item)
            {
                if (!isset($item['failed']))
                {
                    $item['user_id'] = $userId;
                    $item['must_deleted_at'] = $time->addDays(7)->toDateTimeString(); // uncomment if want to upload to prod server
//                    $item['must_deleted_at'] = $time->addMinutes(2)->toDateTimeString(); // comment this if want to upload to prod server
                    $tempOrigName = $item['original_name'];
                    $insert = UploadFileLog::insert($item);
                    if ($insert)
                    {
                        $actualResponses['success'][] = $tempOrigName;
                    } else
                    {
                        $actualResponses['failed'][] = $tempOrigName;
                    }
                } else
                {
                    $actualResponses['failed'][] = $item['failed'];
                }
            }
        }
        return $actualResponses;
    }

    private function setPath($userId,$time)
    {
        $composer = [
            'resources',
            'moments',
            $userId,
            $time->format('Ymd')
        ];
        return sprintf("%s",implode('/',$composer));
    }

    public function index(array $gpc)
    {
        $data = $this->query($gpc)
                    ->selectRaw('upload_file_logs.id,upload_file_logs.user_id,upload_file_logs.file_name,upload_file_logs.ext,upload_file_logs.original_name,upload_file_logs.is_shareable AS shareable_link,created_at')
                    ->orderBy('id','desc')
                    ->paginate((int) $gpc['limit'])
                    ->toArray();
        if ($data && isset($data['data']) && count($data['data']) > 0)
        {
            $data = $this->transformOutput($data);
        }
        return $data;
    }

    private function query(array $gpc)
    {
        $query = UploadFileLog::query();
        $query = $query->where('user_id',Auth::id());
        if (isset($gpc['filter']) && !is_null($gpc['filter']))
        {
            $filter = $gpc['filter'];
            if (isset($gpc['type']) && !is_null($gpc['type']))
            {
                // supported type for now just 'ext' :D
                switch ($gpc['type'])
                {
                    case "ext":
                        $filter = explode(',',$filter);
                        $query->whereIn('ext',"%".$filter."%");
                        break;
                };
            } else
            {
                $query->where('original_name',"%".$filter."%");
            }
        }
        return $query;
    }

    private function transformOutput($responses)
    {
        $shareableLink = 'share/moment';
        $data = $responses['data'];
        foreach ($data AS $key => $value)
        {
            $userId = $data[$key]['user_id'];
            $fileName = $data[$key]['file_name'];
            unset(
                $data[$key]['user_id'],
                $data[$key]['file_name']
            );
            if ($data[$key]['shareable_link'] == 0)
            {
                $data[$key]['shareable_link'] = NULL;
            } else
            {
                $data[$key]['shareable_link'] = implode('/',[$shareableLink,$userId,$fileName]);
            }
        }
        $responses['data'] = $data;
        return $responses;
    }
}