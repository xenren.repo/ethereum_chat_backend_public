<?php

declare(strict_types=1);

namespace App\Logic\System;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use App\Logic\Seller\RankSettingLogic;
use App\Logic\Seller\TopUpLogic;
use App\Model\Setting;

class CheatSheetLogic extends Logic
{
    public function getPayment()
    {
        $paymentSettings = Setting::query()->where('key', 'pay')->first();
        if ($paymentSettings) {
            return array_map(function ($data) {
                return [
                    'key' => $data['key'],
                    'value' => (bool) $data['value'],
                ];
            }, json_decode($paymentSettings['value'], true));
        }
        return [
            [
                'key' => 'alipay',
                'value' => false,
            ],
            [
                'key' => 'wechat',
                'value' => false,
            ],
        ];
    }

    public function setPaymentSetting($gpc)
    {
        $payment = $gpc['pay_type'];
        $paymentValue = $gpc['value'];
        $settings = [];
        $isExist = false;
        foreach ($this->getPayment() as $key => $value) {
            if (strtolower($value['key']) === strtolower($payment)) {
                $value['value'] = (bool) $paymentValue;
                $isExist = true;
            }
            $settings[] = $value;
        }
        if (! $isExist) {
            $settings[] = [
                'key' => $payment,
                'value' => $paymentValue,
            ];
        }
        try {
            $update = Setting::query()->where('key', 'pay')->update([
                'value' => json_encode($settings),
            ]);
        } catch (\Exception $e) {
            logger()->error($e);
            throw new BusinessException(ErrorCode::PARAMS_INVALID, "Failed to update {$payment} status");
        }
        cache()->clearPrefix('setting');
        return $update;
    }

    public function getSellerRank()
    {
        return make(RankSettingLogic::class)->getLists();
    }

    public function setSellerRankSetting($gpc)
    {
        return make(RankSettingLogic::class)->setSettings((int) $gpc['rank'], (string) $gpc['column'], (string) $gpc['value']);
    }

    public function getSellerTopup($gpc)
    {
        return make(TopUpLogic::class)->getLists();
    }

    public function setSellerTopup($gpc)
    {
        try {
            $update = make(TopUpLogic::class)->setTopUpValue((int) $gpc['current_rank'], (int) $gpc['rank_id'], [
                'rank_id' => (int) $gpc['rank_id'],
                'message' => $gpc['message'] ?? '',
                'amount' => (int) $gpc['amount'],
            ]);
            if (! $update) {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Failed to save');
            }
        } catch (\Exception $e) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Failed to save');
        }
        return true;
    }

    public function deleteSellerTopup($gpc)
    {
        try {
            $delete = make(TopUpLogic::class)->deleteTopUpValue((int) $gpc['current_rank'], (int) $gpc['rank_id']);
        } catch (\Exception $th) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, 'Failed to delete');
        }
        return true;
    }
}
