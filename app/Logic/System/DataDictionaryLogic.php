<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\System;


use App\Logic\Logic;
use App\Model\DataDictionary;

class DataDictionaryLogic extends Logic
{


	/**
	 * @param $module
	 * @param $key_name
	 * @return bool|mixed|string
	 */
	public function getJson($module, $key_name)
	{
		return cache()->remember('dictionary_'.$module.'_'.$key_name,function()use($module,$key_name){
			$query = make(DataDictionary::class);
			if ($module && $key_name) {
				$query = $query->where('module',$module)
					->where('key_name',$key_name);
			}
			$data = $query->selectRaw('module,key_name,key_id as id,key_value as name')->get()->toArray();
			foreach ($data as $k => $v) {
				$data[$k]['id'] = (string)$v['id'];
			}
			return $data;
		});
	}


}
