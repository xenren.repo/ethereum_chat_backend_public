<?php

declare(strict_types=1);


namespace App\Logic\System;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\LoginLog;
use App\Model\Setting;
use Hyperf\DbConnection\Db;

class LoginLogLogic extends Logic
{




	public function log($info)
	{
		return LoginLog::insert($info);
	}

}
