<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\System;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Kernel\Common\Helper;
use App\Logic\Admin\AdminLogic;
use App\Logic\Logic;
use App\Model\Admin;
use App\Model\Module;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;

class ModuleLogic extends Logic
{


	/**
	 * @param $data
	 * @return mixed
	 */
	public function store($data)
	{
		$res = Module::create($data);

		return $res;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function update($data)
	{
		$module = Module::findOrFailFromCache($data['id']);
		$res = $module->update($data);

		return $res;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function delete($data)
	{
		$exist = Module::whereIn('pid',$data['id'])->value('id');
		if ($exist) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'存在子级的菜单无法直接删除');
		}

		$res = Module::whereIn('id',$data['id'])->delete();

		ApplicationContext::getContainer()
			->get(Manager::class)
			->destroy($data['id'],Module::class);

		return $res;
	}


	/**
	 * 获取树形菜单
	 * @param $client_type
	 * @return array
	 */
	public function getModuleTree($client_type)
	{
		$gpc = [
			'user_id' => Auth::id(),
			'client_type' => $client_type
		];
		$permissions_ids = make(UserPermissionLogic::class)->getPermission($gpc);
		if (!empty($permissions_ids)) {
			$data = Module::whereIn('id',$permissions_ids)
				->selectRaw('id,path,name,icon,rel,pid,is_hidden,type,description,permission,client_type,sort')
				->orderBy('sort','desc')
				->get()->toArray();
			$res = Helper::sortLevel($data);
		}
		return $res ?? [];
	}


	/**
	 * 获取授权的菜单级授权列表
	 * @param int $client_type
	 * @return mixed
	 */
	public function getAuthModules($client_type = 0)
	{
		$gpc = [
			'user_id' => Auth::id(),
			'client_type' => $client_type
		];
		$admin = Admin::where('id', '=', Auth::id())->first();
        $rolePermission = [];
		if($admin['group_id'] == AdminLogic::GROUP_ID_ADMIN || $admin['group_id'] == AdminLogic::GROUP_ID_SUB_ADMIN) { //
            $rolePermission = [0,1];
        }
		if($admin['group_id'] == AdminLogic::GROUP_ID_SELLER && $admin['status'] == 0) {
            $rolePermission = [1];
        }
		$permissions_ids = make(UserPermissionLogic::class)->getPermission($gpc);
		if (!empty($permissions_ids)) {
			$menus = Module::whereIn('id',$permissions_ids)
				->selectRaw('id,path,name,icon,rel,pid,type,description,is_hidden')
				->where('type',1)
				->whereIn('role_permission',$rolePermission)
				->orderBy('sort','desc')
				->get()->toArray();

			$permissions = Module::whereIn('id',$permissions_ids)
				->selectRaw('id,permission,pid')
				->where('type',2)
                ->whereIn('role_permission',$rolePermission)
                ->get()->toArray();

			foreach ($menus as $k => $menu) {
				foreach ($permissions as $permission) {
					if ($permission['pid'] === $menu['id']) {
						$menus[$k]['permissions'][] = $permission['permission'];
					}
				}
			}
			$res = Helper::sortLevel($menus);
		}
		return $res ?? [];
	}



}
