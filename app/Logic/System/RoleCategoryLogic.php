<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\System;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use App\Model\Role;
use App\Model\RoleCategory;

class RoleCategoryLogic extends Logic
{


	public function json($gpc)
	{
		return cache()->remember('role_role_category_json',function()use($gpc){
			return $this->query($gpc)
				->selectRaw('id,name,pid')
				->get()->toArray();
		},14400);
	}

	public function clearCache()
	{
		cache()->clearPrefix('role_category_');
	}

	/**
	 * @param $gpc
	 * @return Role
	 */
	public function query($gpc)
	{
		$query = new RoleCategory();
		if (!empty($gpc['search'])) {
			$search = $gpc['search'].'%';
			$query = $query->where('name','like',$search);
		}
		return $query;
	}

}
