<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\System;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use App\Model\Role;
use App\Model\UserRole;
use Hyperf\Cache\Annotation\Cacheable;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;

class RoleLogic extends Logic
{


	public function index($gpc)
	{
		$data = $this->query($gpc)
			->selectRaw('id,name,pid')
			->paginate($gpc['limit'])->toArray();
		foreach ($data['data'] as $k => $v) {
			$data['data'][$k]['pid_name'] = '';
		}
		return $data;
	}


	public function json($gpc)
	{
		return cache()->remember('role_json',function()use($gpc){
			return $this->query($gpc)
				->selectRaw('id,name,pid')
				->get()->toArray();
		},14400);
	}

	public function clearCache()
	{
		cache()->clearPrefix('role_');
	}

	/**
	 * @param $gpc
	 * @return Role
	 */
	public function query($gpc)
	{
		$query = new Role();
		if (!empty($gpc['search'])) {
			$search = $gpc['search'].'%';
			$query = $query->where('name','like',$search);
		}
		return $query;
	}


	/**
	 * @param $data
	 * @return mixed
	 */
	public function store($data)
	{
		$res = Role::create($data);
		$this->clearCache();
		return $res;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function update($data)
	{
		$res = Role::findOrFailFromCache($data['id'])->update($data);
		$this->clearCache();
		return $res;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function delete($data)
	{
		$exist = UserRole::whereIn('role_id',$data['id'])->value('id');
		if ($exist) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'存在归属该角色成员,无法删除');
		}

		$res = Role::whereIn('id',$data['id'])->delete();

		ApplicationContext::getContainer()
			->get(Manager::class)
			->destroy($data['id'],Role::class);
		$this->clearCache();
		return $res;
	}


	public function getAllRolesId()
	{
		return cache()->remember('role_all_ids',function(){
			return Role::pluck('id')->toArray();
		},86400);
	}



	public function getRoleName($id)
	{
		$role = Role::findOrFailFromCache($id);
		return $role['name'] ?? '';
	}

}
