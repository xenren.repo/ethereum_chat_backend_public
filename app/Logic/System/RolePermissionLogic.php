<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\System;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\Role;
use App\Model\RolePermission;
use Hyperf\DbConnection\Db;

class RolePermissionLogic extends Logic
{


	/**
	 * 设置角色权限
	 * @param $gpc
	 */
	public function set($gpc)
	{
		$this->checkPermission($gpc);
		$role = Role::findOrFailFromCache($gpc['role_id']);
		$exists_permissions = $this->getPermission($gpc);
		$need_add = [];
		foreach ($gpc['permissions'] as $permission) {
			if (!in_array($permission, $exists_permissions)) {
				$need_add[] = $permission;
			}
		}
		$need_remove = array_diff($exists_permissions,$gpc['permissions']);
		Db::transaction(function()use($role,$need_add,$need_remove){
			$add_info = [];
			if ($need_remove) {
				RolePermission::where('role_id',$role['id'])
					->whereIn('permission_id',$need_remove)
					->delete();
			}
			foreach ($need_add as $item) {
				$add_info[] = [
					'role_id' => $role['id'],
					'permission_id' => $item,
				];
			}
			if (!empty($add_info)) {
				RolePermission::insert($add_info);
			}
			$this->clearCache();
			// 角色权限变化时,同时也会引起用户权限变化
			make(UserPermissionLogic::class)->clearCache();
		});
	}


	/**
	 * 获取角色当前权限
	 * @param $gpc
	 * @return array
	 */
	public function getPermission($gpc)
	{
		if (is_array($gpc['role_id'])) {
			$key = md5(json_encode($gpc['role_id']));
		} else {
			$key = $gpc['role_id'];
		}
		return cache()->remember('role_permission_'.$gpc['client_type'].'_'.$key,function()use($gpc){
			$query = RolePermission::leftJoin('module','module.id','=','role_permission.permission_id');
			if (is_array($gpc['role_id'])) {
				$query = $query->whereIn('role_permission.role_id',$gpc['role_id']);
			} else {
				$query = $query->where('role_permission.role_id',$gpc['role_id']);
			}
			$permissions = $query->where('module.client_type',$gpc['client_type'])
				->pluck('role_permission.permission_id')->toArray();
			return array_unique($permissions);
		});
	}


	/**
	 * 清理角色相关缓存
	 */
	public function clearCache()
	{
		cache()->clearPrefix('role_permission');
	}


	/**
	 * 检测当前设置权限者自身权限是否超出授权范围
	 * @param $gpc
	 * @throws BusinessException
	 */
	protected function checkPermission($gpc)
	{
		$gpc['user_id'] = Auth::id();
		$user_permissions = make(UserPermissionLogic::class)->getPermission($gpc);
		$diff = array_diff($gpc['permissions'],$user_permissions);

		if (!empty($diff)) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'无法设置超出权限范围的授权');
		}
	}
}
