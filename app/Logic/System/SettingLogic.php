<?php

declare(strict_types=1);

namespace App\Logic\System;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\Setting;
use Hyperf\DbConnection\Db;

class SettingLogic extends Logic
{
    /**
     * @return bool|void
     */
    public function set(array $data, bool $isSystem = false)
    {
        Db::transaction(function () use ($data,$isSystem) {
            $allow_type = [1];
            if (! $isSystem) {
                if (Auth::isSuperAdmin()) {
                    $allow_type = [0, 1];
                } else {
                    $allow_type = [1];
                }
            }
            foreach ($data as $k => $v) {
                if (! is_array($v)) {
                    $v = json_decode($v, true);
                }
                $gpc = [];
                foreach ($v['value'] as $key => $vo) {
                    if ($vo === null) {
                        $v['value'][$key] = '';
                    } elseif (is_array($v['value'])) {
                        foreach ($v['value'] as $key3 => $v3) {
                            if ($v3['value'] === null) {
                                $v['value'][$key3]['value'] = '';
                            }
                            if ($v3['description'] === null) {
                                $v['value'][$key3]['description'] = '';
                            }
                            $gpc[$v3['key']] = $v['value'][$key3]['value'];
                        }
                    }
                }
                //TODO 输入参数验证
                //				$rules = Setting::where('key',$v['key'])->value('validation');
                //				if ($rules) {
                //					$rules = json_decode($rules,true);
                //					validate($gpc,$rules);
                //				}
                Setting::where('key', $v['key'])
                    ->whereIn('type', $allow_type)
                    ->update(['value' => json_encode($v['value'], JSON_UNESCAPED_UNICODE)]);
            }
            cache()->clearPrefix('setting');
        });
        return null;
    }

    /**
     * 系统设置键值列表.
     * @param int $type
     * @return array
     */
    public function index($type = 1)
    {
        if (is_null($type) || $type == '') {
            $type = 1;
        }
        if (Auth::isSuperAdmin()) {
            $allow_type = [0, 1];
            $is_hidden = [0, 1];
        } else {
            $allow_type = [1];
            $is_hidden = [0];
        }
        if (! in_array($type, $allow_type)) {
            throw new BusinessException(ErrorCode::PERMISSION_DENY);
        }
        $res = Setting::where('type', $type)
            ->whereIn('is_hidden', [0])
            ->orderBy('sort', 'desc')
            ->get()
            ->toArray();
        $info = [];
        foreach ($res as $v) {
            $info[] = [
                'name' => $v['name'],
                'key' => $v['key'],
                'value' => json_decode($v['value'], true),
            ];
        }
        return $info;
    }

    public static function getSetting($key, $default_value = null)
    {
        if (empty($key)) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '键值错误');
        }
        $keys = explode('.', $key);
        $cache_key = 'setting_' . $keys[0];
        $value = cache()->remember($cache_key, function () use ($keys,$default_value,$key) {
            $value = Setting::where('key', $keys[0])->value('value');
            if (! empty($value)) {
                $value = json_decode($value, true);
                foreach ($value as $k => $v) {
                    if (! isset($v['value']) || ! isset($v['key'])) {
                        throw  new BusinessException(ErrorCode::PARAMS_INVALID, $key . '值参数设置错误');
                    }
                    unset($value[$k]['name'], $value[$k]['description'], $value[$k]['value_type']);
                }
            } else {
                $value = [];
            }
            return $value;
        }, 86400);
        $res = $default_value;
        if (! empty($value)) {
            foreach ($value as $v) {
                // 整个键值数据返回
                if (count($keys) === 1) {
                    $res[$v['key']] = $value;
                    break;
                }
                // 返回单个键值
                if ($keys[1] == $v['key']) {
                    $res = $v['value'];
                    break;
                }
            }
        } else {
            $res = $default_value;
        }
        return $res;
    }
}
