<?php

declare(strict_types=1);


namespace App\Logic\System;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Kernel\Auth\AuthManager;
use App\Kernel\Common\Common;
use App\Kernel\Common\Helper;
use App\Lib\Sms\Message\CodeMessage;
use App\Logic\Admin\AdminAuthLogic;
use App\Logic\Logic;
use App\Logic\User\UserAuthLogic;
use Overtrue\EasySms\EasySms;

class SmsLogic extends Logic
{


	public function getSms($gpc)
	{
		if (!isset($gpc['type']) || empty($gpc['type'])) {
			throw new BusinessException();
		}

		$type = Common::getSetting('sms.allow_type');
		if (empty($type)) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'验证码获取类型未定义');
		}

		$type = explode(',',$type);

		if (!is_array($type) || !in_array($gpc['type'], $type)) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'验证码定义类型错误');
		}

		$config = config('sms');
		$easySms = new EasySms($config);
		$mobile = $this->getMobile($gpc);
		$msg = new CodeMessage($gpc['type'],$mobile);
		if (Helper::isProd()) {
			$easySms->send($mobile, $msg);
		} else {
			// 模拟验证码入缓存
			$msg->getData();
		}
		return null;
	}


	private function getMobile($gpc)
	{
		switch ($gpc['type']) {
			case 'reset_pay_password':
				$user_type = '';
				try {
					make(AuthManager::class)->parseData(UserAuthLogic::AUTH_TYPE);
					$user_type = 'user';
				} catch (\Exception $e) {

				}
				if (empty($user_type)) {
					try {
						make(AuthManager::class)->parseData(AdminAuthLogic::AUTH_TYPE);
						$user_type = 'admin';
					} catch (\Exception $e) {

					}
				}
				if ($user_type) {
					$user = Auth::user();
					$gpc['mobile'] = $user['mobile'] ?? '';
					if (empty($gpc['mobile'])) {
						throw new BusinessException(ErrorCode::WITHOUT_PHONE);
					}
				}
				break;
			default:
				break;
		}
		if (empty($gpc['mobile'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'手机号码获取错误');
		}
		if (!Helper::isMobile($gpc['mobile'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'手机号码格式错误');
		}
		return $gpc['mobile'];
	}
}
