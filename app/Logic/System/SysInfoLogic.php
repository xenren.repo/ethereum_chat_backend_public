<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\System;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use App\Model\SysInfo;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;

class SysInfoLogic extends Logic
{

	const ENABLE_STATUS = 0;
	const DISABLE_STATUS = 1;


	public function index($gpc)
	{
		$data = $this->query($gpc)
			->paginate((int)$gpc['limit'])
			->toArray();
		return $data;
	}


	/**
	 * @param $gpc
	 * @return SysInfo
	 */
	public function query($gpc)
	{
		$query = new SysInfo();
		if (!empty($gpc['search'])) {
			$search = $gpc['search'].'%';
			$query = $query->where(function ($query)use($search) {
				$query->where('title','like',$search);
			});
		}
		return $query;
	}


	/**
	 * @param $data
	 * @return mixed
	 */
	public function store($data)
	{
		$res = SysInfo::create($data);
		return $res;
	}


	/**
	 * @param $id
	 * @return mixed
	 */
	public function show($id)
	{
		if (empty($id) || !is_numeric($id)) {
			throw new BusinessException();
		}
		return SysInfo::findOrFailFromCache($id);
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function update($data)
	{
		$admin = SysInfo::findOrFailFromCache($data['id']);

		$admin->update($data);

		return null;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function delete($data)
	{

		$res = SysInfo::whereIn('id',$data['id'])->delete();

		ApplicationContext::getContainer()
			->get(Manager::class)
			->destroy($data['id'],SysInfo::class);


		return $res;
	}


	/**
	 * 获取最新公告
	 * @return mixed
	 */
	public function getLatestInfo()
	{
		return cache()->remember('sys_info_latest_info',function(){
			$id = SysInfo::orderBy('id','desc')->value('id');
			if ($id) {
				try {
					$data = $this->show(($id));
				} catch (\Exception $e) {

				}
			}
			return $data ?? '';
		});
	}
}
