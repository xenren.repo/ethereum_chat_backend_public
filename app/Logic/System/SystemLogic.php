<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\System;

use App\Logic\Logic;


class SystemLogic extends Logic
{


	public function clearCache()
	{
		//TODO 各缓存封装方法集中此处清除,不准使用flushDB,后果非常严重
		cache()->clearPrefix('admin');

		// 配置
		cache()->clearPrefix('setting');

		// 角色分组
		cache()->clearPrefix('role_category');

		// 用户角色
		cache()->clearPrefix('user_role');
		// 用户权限
		cache()->clearPrefix('user_permission');
		// 角色权限
		cache()->clearPrefix('role_permission');
		// 菜单
		cache()->clearPrefix('module');

		cache()->clearPrefix('users');

		cache()->clearPrefix('dictionary');

		cache()->clearPrefix('goods');

		cache()->clearPrefix('goods_category');

		cache()->clearPrefix('credit_goods');

		cache()->clearPrefix('user_banks');

		cache()->clearPrefix('sys_info');

		cache()->clearPrefix('finance');

		cache()->clearPrefix('third_ad');

		cache()->clearPrefix('ad');

		cache()->clearPrefix('goods_cart');

		cache()->clearPrefix('user_im');

		cache()->delete('express_list');
	}

}
