<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\System;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Logic\Logic;
use Hyperf\Utils\Str;


class UploadLogic extends Logic
{

	/**
	 * 多图片上传
	 * @param $files
	 * @return array
	 */
	public function uploadImages($files)
	{
		$image_config = config('upload')['images'];
		$allow_types = $image_config['ext'];
		$size_limit = $image_config['limit']*1024;
		$mul = true;
		if (!is_array($files)) {
			$files = [$files];
			$mul = false;
		}
		foreach ($files as $k => $file) {
			$file = $file->toArray();
			if (!isset($file['error']) || $file['error'] != 0) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'文件上传错误');
			}
			if (!in_array($this->getExt($file['type']), $allow_types)) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'图片类型不合法');
			}
			if ($file['size'] > $size_limit) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'图片超出规定尺寸');
			}
			$files[$k] = $file;
		}
		$images = [];
		foreach ($files as $file) {
			$images[] = $this->uploadImage($file);
		}
		if (!$mul) {
			$images = $images[0];
		}
		return $images;
	}



	/**
	 * 单图片上传
	 * @param $file
	 * @return string
	 */
	private function uploadImage($file)
	{
		$file_store_type = config('upload')['file_store_type'];
		$path = BASE_PATH.'/public/upload/images';
		$plus_path = '';
		if ($file_store_type === 'day') {
			$day = date('Y-m-d');
			$plus_path = $plus_path.'/'.$day;
		} else if ($file_store_type === 'month') {
			$month = date('Y-m');
			$plus_path = $plus_path.'/'.$month;
		}
		$path .= $plus_path;
		if (!is_dir($path)) {
			mkdir($path,0777,true);
		}
		$file_name = time().Str::random(16).'.'.$this->getExt($file['type']);
		move_uploaded_file($file['tmp_file'],$path.'/'.$file_name);
		$plus_path = $plus_path ? ($plus_path.'/'):'';
		$plus_path = ltrim($plus_path,'/');
		return env('HOST').'/upload/images/'.$plus_path.$file_name;
	}



	/**
	 * 获取扩展
	 * @param $type
	 * @return string
	 */
	public function getExt($type)
	{
		return explode('/', $type)[1] ?? '';
	}

}
