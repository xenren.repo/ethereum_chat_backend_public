<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\System;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\Admin;
use App\Model\Module;
use App\Model\Role;
use App\Model\UserPermission;
use Hyperf\Cache\Annotation\Cacheable;
use Hyperf\DbConnection\Db;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;

class UserPermissionLogic extends Logic
{


	/**
	 * @param $gpc
	 * @return mixed
	 */
	public function set($gpc)
	{
		$this->checkPermission($gpc['permissions'],$gpc['client_type']);

		$role_permissions = $this->getUserRolePermission($gpc['user_id'],$gpc['client_type']);

		$need_remove = array_diff($role_permissions,$gpc['permissions']);
		$need_add = array_diff($gpc['permissions'],$role_permissions);

		Db::transaction(function()use($need_remove,$need_add,$gpc){
			$info = [];

			$client_type_ids = Module::where('client_type',$gpc['client_type'])->pluck('id')->toArray();

			if (empty($client_type_ids)) {
				return;
			}
			$exist_custom_permissions = $this->getCustomPermission($gpc['user_id'],$gpc['client_type']);

			$query = UserPermission::where('user_id',$gpc['user_id'])
				->whereIn('permission_id',$client_type_ids)
				->where('type',1);
			if ($need_add) {
				$query = $query->whereNotIn('permission_id',$need_add);
			}
			$query->delete();

			// 移除不在本次移除范围的移除权限
			$remove_query = UserPermission::where('user_id',$gpc['user_id'])
				->whereIn('permission_id',$client_type_ids)
				->where('type',2);
			if ($need_remove) {
				$remove_query = $remove_query->whereNotIn('permission_id',$need_remove);
			}
			$remove_query->delete();

			foreach ($need_add as $item) {
				if (in_array($item, $exist_custom_permissions['plus'])) {
					continue;
				}
				$info[] = [
					'user_id' => $gpc['user_id'],
					'permission_id' => $item,
					'type' => 1
				];
			}

			// 自定义移除的权限
			foreach ($need_remove as $item) {
				if (in_array($item, $exist_custom_permissions['remove'])) {
					continue;
				}

				$info[] = [
					'user_id' => $gpc['user_id'],
					'permission_id' => $item,
					'type' => 2
				];
			}

			if ($info) {
				UserPermission::insert($info);
			}

			// 权限检测那边为合并pc端与手机端的权限,需一同清除
			$this->clearCache($gpc['user_id']);
		});
		return true;
	}



	/**
	 * 检测当前设置权限者自身权限是否超出授权范围
	 * @param array $permissions
	 * @param $client_type
	 * @throws BusinessException
	 */
	protected function checkPermission(array $permissions,$client_type)
	{
		$gpc = [
			'user_id' => Auth::id(),
			'client_type' => $client_type
		];
		$user_permissions = make(UserPermissionLogic::class)->getPermission($gpc);
		$diff = array_diff($permissions,$user_permissions);
		if (!empty($diff)) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'无法设置超出权限范围的授权');
		}
	}


	/**
	 * 获取用户当前权限
	 * @param $gpc
	 * @return array
	 */
	public function getPermission($gpc)
	{
		if (empty($gpc['user_id'])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'用户参数错误');
		}
//		return cache()->remember('user_permission_'.$gpc['client_type'].'_'.$gpc['user_id'],function()use($gpc){
		    $admin = Admin::where('id', '=', Auth::id())->first();
			if (Auth::isSuperAdmin($gpc['user_id']) || $admin['group_id'] == 2 ) {
					$permissions = Module::where('client_type',$gpc['client_type'])->pluck('id')->toArray();
			} elseif ($admin['group_id'] == 2) {
				// group seller, i think
					$permissions = Module::where('client_type',$gpc['client_type'])->where('role_permission',1)->pluck('id')->toArray();
			} else {
				// 获取原生角色的权限
				$permissions = $this->getUserRolePermission($gpc['user_id'],$gpc['client_type']);

				// 获取用户自定义的权限
				$change_permission = $this->getCustomPermission($gpc['user_id'], $gpc['client_type']);

				foreach ($permissions as $k => $v) {
					if (in_array($v, $change_permission['remove'])) {
						unset($permissions[$k]);
					}
				}

				if ($change_permission['plus']) {
					$permissions = array_merge($permissions,$change_permission['plus']);
				}
			}
			return array_values(array_unique($permissions));
//		},86400);
	}



	/**
	 * 获取用户所属角色原生的权限(非最终权限)
	 * @param $user_id
	 * @param $client_type
	 * @return array
	 */
	private function getUserRolePermission($user_id,$client_type)
	{
		$roles = make(UserRoleLogic::class)->getRolesId($user_id);
		$gpc = [
			'role_id' => $roles,
			'client_type' => $client_type
		];
		return make(RolePermissionLogic::class)->getPermission($gpc);
	}


	/**
	 * 获取自定义的权限
	 * @param $user_id
	 * @param $client_type
	 * @return mixed
	 */
	private function getCustomPermission($user_id, $client_type)
	{
		// 获取用户自定义的权限
		$change_permissions = UserPermission::leftJoin('module','module.id','=','user_permission.permission_id')
			->where('user_permission.user_id',$user_id)
			->where('module.client_type',$client_type)
			->selectRaw('user_permission.type,user_permission.permission_id')
			->get();

		$res['plus'] = [];  // 额外添加的权限
		$res['remove'] = [];  // 额外扣除的权限
		foreach ($change_permissions as $change_permission) {
			if ($change_permission['type'] == 1) {
				$res['plus'][] = $change_permission['permission_id'];
			} else {
				$res['remove'][] = $change_permission['permission_id'];
			}
		}
		return $res;
	}



	public function clearCache($user_id = 0)
	{
		if (empty($user_id)) {
			cache()->clearPrefix('user_permission');
		} else {
			cache()->delete('user_permission_0_'.$user_id);
			cache()->delete('user_permission_1_'.$user_id);
			cache()->delete('user_permission_all_'.$user_id);
		}
	}


	/**
	 * 获取用户拥有的权限节点(含手机端,PC端)
	 * @param int $user_id
	 * @return mixed
	 */
	public function getAllNodePermissions($user_id = 0)
	{
		if (empty($user_id)) {
			$user_id = Auth::id();
		}

		return cache()->remember('user_permission_all_' . $user_id, function () use ($user_id) {

			$gpc = [
				'user_id' => $user_id,
				'client_type' => 0
			];
			// pc端权限
			$pc_permissions = make(UserPermissionLogic::class)->getPermission($gpc);

			$gpc['client_type'] = 1;
			// 手机端权限
			$mobile_permissions = make(UserPermissionLogic::class)->getPermission($gpc);

			$permissions = array_merge($pc_permissions, $mobile_permissions);
			$permissions = array_unique($permissions);
			$node_permissions = [];
			if ($permissions) {
				$node_permissions = Module::whereIn('id', $permissions)
					->whereNotNull('permission')
					->where('permission','<>','')
					->pluck('permission')->toArray();
			}
			return $node_permissions;
		});
	}

}
