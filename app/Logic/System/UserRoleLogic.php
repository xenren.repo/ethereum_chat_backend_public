<?php

declare(strict_types=1);


namespace App\Logic\System;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\UserRole;
use Hyperf\DbConnection\Db;

class UserRoleLogic extends Logic
{


	public function clearCache($user_id = 0)
	{
		if (empty($user_id)) {
			cache()->clearPrefix('user_role');
		} else {
			cache()->delete('user_role_info_'.$user_id);
		}

	}


	/**
	 * 设置用户角色
	 * @param $gpc
	 * @internal param $user_id
	 */
	public function set($gpc)
	{

		// 检查角色是否合法
		$this->checkRoles($gpc['role_id']);

		$exists_roles = $this->getRolesId($gpc['user_id']);
		$need_remove = array_diff($exists_roles,$gpc['role_id']);
		$need_add = array_diff($gpc['role_id'],$exists_roles);
		$add_info = [];
		foreach ($need_add as $item) {
			$add_info[] = [
				'user_id' => $gpc['user_id'],
				'role_id' => $item,
			];
		}
		Db::transaction(function()use($gpc,$need_remove,$add_info){

			if (!empty($need_remove)) {
				UserRole::where('user_id',$gpc['user_id'])
					->whereIn('role_id',$need_remove)
					->delete();
			}

			if (!empty($add_info)) {
				UserRole::insert($add_info);
			}
			$this->clearCache($gpc['user_id']);
		});
	}

	/**
	 * 检测角色id是否合法
	 * @param $roles
	 * @throws BusinessException
	 */
	protected function checkRoles($roles)
	{
		$res = false;
		if (!empty($roles)) {
			$all_roles = make(RoleLogic::class)->getAllRolesId();
			if (is_array($roles)) {
				$count = count($roles);
				$available = array_intersect($roles,$all_roles);
				$res = (count($available) === $count) ? true : false;
			} else {
				$res = in_array($roles,$all_roles);
			}
		}
		if (!$res) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'角色参数不合法');
		}
	}

	/**
	 * 获取用户角色ID
	 * @param $user_id
	 * @return array
	 */
	public function getRolesId($user_id = 0)
	{
		if (empty($user_id)) {
			$user_id = Auth::id();
		}
		return cache()->remember('user_role_info_'.$user_id,function()use($user_id){
			return UserRole::where('user_id',$user_id)->pluck('role_id')->toArray();
		});
	}





}
