<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\User;


use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Lib\Area\Area;
use App\Logic\Logic;
use App\Model\User;
use App\Model\UserAddress;
use Hyperf\DbConnection\Db;

class UserAddressLogic extends Logic
{




	/**
	 * @param array $data
	 * @param int $user_id
	 * @return mixed
	 */
	public function store(array $data, $user_id = 0)
	{
		if (empty($user_id) && !Auth::isAdmin()) {
			$user_id = Auth::id();
		}else {
			throw new BusinessException();
		}
		$data['user_id'] = $user_id;

		return Db::transaction(function () use ($data) {
			if ($data['is_default'] == 1) {
				UserAddress::where('user_id', $data['user_id'])->update(['is_default' => 0]);
			}
			$bank =  UserAddress::create($data);
			cache()->delete('user_address_user_id_'.$data['user_id']);
			return $bank;
		});
	}

	/**
	 * @param array $data
	 * @param int $user_id
	 * @return mixed
	 */
	public function update(array $data, $user_id = 0)
	{
		if (empty($user_id) && !Auth::isAdmin()) {
			$user_id = Auth::id();
		} else {
			throw new BusinessException();
		}
		$data['user_id'] = $user_id;

		return Db::transaction(function () use ($data) {
			if ($data['is_default'] == 1) {
				UserAddress::where('user_id', $data['user_id'])->update(['is_default' => 0]);
			}
			$address =  UserAddress::where('id', $data['id'])
				->where('user_id', $data['user_id'])
				->update($data);
			cache()->delete('user_address_user_id_'.$data['user_id']);
			return $address;
		});
	}

	/**
	 * @param $id
	 * @param int $user_id
	 * @return mixed
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function delete($id, $user_id = 0)
	{
		if (empty($user_id) && !Auth::isAdmin()) {
			$user_id = Auth::id();
		}else {
			throw new BusinessException();
		}
		$res = UserAddress::whereIn('id', $id)
			->where('user_id', $user_id)
			->delete();
		cache()->delete('user_address_user_id_'.$user_id);
		return $res;
	}

	/**
	 * @param $user_id
	 * @return bool|mixed|string
	 */
	public function getList($user_id = 0)
	{
		if (empty($user_id)) {
			$user_id = Auth::id();
		}
		return cache()->remember('user_address_user_id_'.$user_id, function () use ($user_id) {
			$address = UserAddress::where('user_id', $user_id)
				->selectRaw('id,name,mobile,province,city,area,address,code,tag,is_default')
				->orderBy('is_default', 'desc')
				->orderBy('id', 'desc')
				->get()->toArray();
			foreach ($address as $k => $v) {
				$address[$k]['address_text'] = Area::getAddressText($v['province'],$v['city'],$v['area']);
			}
			return $address;
		},86400);
	}

	/**
	 * @param int $user_id
	 * @return array
	 */
	public function getDefaultAddress($user_id = 0)
	{
		if (empty($user_id)) {
			$user_id = Auth::id();
		}
		$address = $this->getList($user_id);
		return $address[0] ?? [];
	}



}
