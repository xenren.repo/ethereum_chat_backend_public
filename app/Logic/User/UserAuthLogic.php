<?php

declare(strict_types=1);

namespace App\Logic\User;

use App\Constants\ErrorCode;
use App\Event\UserRegisteredEvent;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Kernel\Auth\AuthManager;
use App\Kernel\Common\Helper;
use App\Lib\Sms\Sms;
use App\Logic\Logic;
use App\Model\Setting;
use App\Model\User;
use App\Model\UserDevice;
use App\Model\UserIm;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;
use Psr\EventDispatcher\EventDispatcherInterface;

class UserAuthLogic extends Logic
{
    const AUTH_TYPE = 'auth_user';

    protected $response = [];

    protected $gpc;

    /**
     * @Inject
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param mixed $gpc
     */
    public function setGpc($gpc): void
    {
        $this->gpc = $gpc;
    }

    public function login(array $gpc)
    {
        $user = $this->attempt($gpc);
        $auth_info = make(AuthManager::class)->getToken($user['id'], self::AUTH_TYPE);
        $auth_info['info'] = make(UserLogic::class)->getUserInfo($user['id']);
        $auth_info['im'] = make(UserImLogic::class)->getImToken($user);
        $auth_info['info']['credit1_enabled'] = (int) Setting::where('key', '=', 'credit1_enabled')->pluck('value')[0];
        $auth_info['info']['show_check_version'] = (int) Setting::where('key', '=', 'show_app_version')->pluck('value')[0];
        $auth_info['info']['level_name'] = self::getLevelName($auth_info['info']['level']);
        $this->authorized($user, $gpc);
        make(UserImLogic::class)->addUplineAsFriend($user);
        return $auth_info;
    }

    public function autoLogin(array $gpc)
    {
        $user = $this->attemptAutoLogin($gpc);
        $auth_info = make(AuthManager::class)->getToken($user['id'], self::AUTH_TYPE);
        $auth_info['info'] = make(UserLogic::class)->getUserInfo($user['id']);
        $auth_info['im'] = make(UserImLogic::class)->getImToken($user);
        $auth_info['info']['credit1_enabled'] = (int) Setting::where('key', '=', 'credit1_enabled')->pluck('value')[0];
        $auth_info['info']['show_check_version'] = (int) Setting::where('key', '=', 'show_app_version')->pluck('value')[0];
        $auth_info['info']['level_name'] = self::getLevelName($auth_info['info']['level']);
        $this->authorized($user, $gpc);
        return $auth_info;
    }

    public function getLevelName($level)
    {
        switch ($level) {
            case 0:
                $levelName = '会员';
                break;
            case 1:
                $levelName = 'VIP1';
                break;
            case 2:
                $levelName = 'VIP2';
                break;
            case 3:
                $levelName = 'VIP3';
                break;
            default:
                $levelName = '';
        }
        return $levelName;
    }

    /**
     * 登入成功后处理操作.
     * @param $user
     * @param $gpc
     */
    public function authorized($user, $gpc)
    {
        if (isset($gpc['device_no'], $gpc['device_type']) && ! empty($gpc['device_no']) && ! empty($gpc['device_type'])) {
            switch ($gpc['device_type']) {
                case 'android':
                    $type = 1;
                    break;
                case 'ios':
                    $type = 2;
                    break;
                default:
                    throw new BusinessException(ErrorCode::PARAMS_INVALID);
            }
            $device = UserDevice::where('user_id', $user['id'])
                ->where('device_no', $gpc['device_no'])
                ->where('device_type', $type)
                ->first();
            if (empty($device)) {
                $insert = [
                    'user_id' => $user['id'],
                    'device_no' => $gpc['device_no'],
                    'device_type' => $type,
                    'last_login' => Carbon::now(),
                ];
                UserDevice::insert($insert);
            } else {
                $update = [
                    'last_login' => Carbon::now(),
                ];
                $device->update($update);
            }
        }
    }

    public function logout()
    {
        return make(AuthManager::class)->logout();
    }

    /**
     * 验证账号密码
     * @param $data
     * @return null|\Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object
     */
    public function attempt(array $data)
    {
        $user = User::query()->where('name', $data['name'])
            ->orWhere('mobile', $data['name'])
            ->orderBy('id')
            ->first();

        if (empty($user) || ! password_verify($data['password'], $user['password'])) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '账号或密码错误');
        }
        // 检测是否其他限制无法登入
        $this->checkLogin($user);
        return $user;
    }

    /**
     * 验证账号密码
     * @param $data
     * @return null|\Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object
     */
    public function attemptAutoLogin(array $data)
    {
        $user = User::query()->where('name', $data['name'])
            ->orWhere('mobile', $data['name'])
            ->orderBy('id')
            ->first();

        if (empty($user)) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '账号或密码错误');
        }
        // 检测是否其他限制无法登入
//        $this->checkLogin($user);
        return $user;
    }

    /**
     * 登入前用户业务逻辑.
     * @param $user
     * @throws BusinessException
     */
    public function checkLogin($user)
    {
        if ($user['status'] != 0) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '该账号异常,请联系管理员');
        }
    }

    public function register($gpc)
    {
        if (empty($gpc['mobile']) && Helper::isMobile($gpc['name'])) {
            $gpc['mobile'] = $gpc['name'];
        }
        Sms::check($gpc['code'], 'register', $gpc['mobile']);
        $gpc['pid'] = 0;
        if (isset($gpc['agent']) && $gpc['agent']) {
            $parent = User::where('name', $gpc['agent'])->first();
            if ($parent) {
                $gpc['pid'] = $parent['id'];
                unset($gpc['agent']);
            } else {
                throw new BusinessException(ErrorCode::PARAMS_INVALID, '推荐人不存在');
            }
        }

        $gpc['password'] = password_hash($gpc['password'], PASSWORD_BCRYPT);

        return Db::transaction(function () use ($gpc) {
            $user = User::create($gpc);
            make(UserRelationLogic::class)->add($user);
            Sms::clear('register', $gpc['mobile']);

            $this->eventDispatcher->dispatch(new UserRegisteredEvent($user));

            unset($user['password']);
            return $user;
        });
    }

    /**
     * 修改密码
     * @param $gpc
     * @return bool
     */
    public function changePassword($gpc)
    {
        $user = Auth::user();
        if (! password_verify($gpc['old_password'], $user['password'])) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID, '旧密码错误');
        }
        $user['password'] = password_hash($gpc['password'], PASSWORD_BCRYPT);
        return $user->save();
    }

    /**
     * 重置密码
     * @param $gpc
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function resetPassword($gpc)
    {
        Sms::check($gpc['code'], 'reset_password', $gpc['name']);
        $user = User::where('name', $gpc['name'])->firstOrFail();
        $user->password = password_hash($gpc['password'], PASSWORD_BCRYPT);
        $user->save();
        ApplicationContext::getContainer()
            ->get(Manager::class)
            ->destroy([$user['id']], User::class);
        Sms::clear('reset_password', $gpc['name']);
        return null;
    }

    /**
     * @param $gpc
     * @return string
     */
    public function addUserByApi($gpc)
    {
        logger()->info(json_encode($gpc));
        if (isset($gpc['introducer']) && $gpc['introducer']) {
            $pid = User::where('name', $gpc['introducer'])->value('id');
            if (empty($pid)) {
                $pid = 0;
            }
        } else {
            $pid = 0;
        }
        if (isset($gpc['password']) && strlen($gpc['password']) < 20) {
            $gpc['password'] = password_hash($gpc['password'], PASSWORD_BCRYPT);
        }

        $checkUser = make(UserLogic::class)->checkUserByName($gpc['username']);
        if (! $checkUser) {
            $data = [
                'name' => $gpc['username'],
                'pid' => $pid,
                'password' => $gpc['password'] ?? '',
                'mobile' => $gpc['mobile'] ?? '',
            ];
            $user = User::create($data)->toArray();
        } else {
            $data = [
                'pid' => $pid,
                'password' => $gpc['password'] ?? '',
                'mobile' => $gpc['mobile'] ?? '',
            ];
            $checkUser->update($data);
            $user = $checkUser->toArray();
        }

        $usr = User::where('name', '=', $gpc['username'])->first();
        $userIm = UserIm::where('user_id', '=', $usr->id)->first();
        $data = [
            'user_id' => $usr->id,
            'accid' => $gpc['accid'],
            'token' => $gpc['token'],
        ];
        if (is_null($userIm)) {
            UserIm::create($data);
        } else {
            $userIm->update($data);
        }
        // 索引父级关系
        $logic = new UserRelationLogic();
        $logic->add($user);
        return true;
    }

    public function clearUserIm($gpc = null)
    {
        if (! is_null($gpc)) {
            $this->setGpc($gpc);
        }

        $delete = UserIm::where('accid', $this->gpc['username'])->delete();
        if ($delete) {
            $this->response['delete']['user_im'] = true;
        } else {
            $this->response['delete']['user_im'] = false;
        }
        return $this;
    }

    public function clearCache($gpc = null)
    {
        if (! is_null($gpc)) {
            $this->setGpc($gpc);
        }
        $user = User::where('name', $this->gpc['username'])->first()->toArray();
        if ($user) {
            $delete = cache()->delete('user_im_' . $user['id']);
            if ($delete) {
                $this->response['delete']['user_im_cache'] = true;
                return $this;
            }
        }
        $this->response['delete']['user_im_cache'] = false;
        return $this;
    }

    public function getResponse()
    {
        logger()->error('[CLEAR-CACHE-LOGGER]');
        logger()->error(json_encode($this->response));
        return $this->response;
    }
}
