<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\User;


use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\UserImFavorite;

class UserImFavoriteLogic extends Logic
{


	public function index($gpc)
	{
		$data = $this->query($gpc)
			->selectRaw('user_im_favorite.id,name AS accid,uuid,type,user_im_favorite.created_at')
			->orderBy('id','desc')
			->paginate((int) $gpc['limit'])
			->toArray();
		return $data;
	}



	/**
	 * @param $gpc
	 * @return UserImFavorite
	 */
	public function query($gpc)
	{
		$query = new UserImFavorite();
		$query = $query->where('user_id',Auth::id())->leftJoin('users','user_im_favorite.user_id','=','users.id');
		if (isset($gpc['filter']) && !is_null($gpc['filter']))
        {
            $filter = $gpc['filter'];
            if ($filter !== 'all')
            {
                $explodeFilter = explode(',',$filter);
                $query->whereIn('type',$explodeFilter);
            }
        }
		return $query;
	}


	/**
	 * @param $gpc
	 * @return mixed
	 */
	public function store($gpc)
	{
		$data = [
			'user_id' => Auth::id(),
            'uuid' => $gpc['uuid'],
            'type' => $gpc['type']
		];
		$res = UserImFavorite::create($data);
		return $res;
	}




	/**
	 * @param $data
	 * @return string
	 */
	public function delete($data)
	{
		$res = UserImFavorite::whereIn('id',$data['id'])->delete();
		return $res;
	}

    public function deleteByUuid(array $data)
    {
        $res = UserImFavorite::whereIn('uuid',$data['uuid'])->delete();
        return $res;
    }

}
