<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\User;


use App\Kernel\Auth\Auth;
use App\Logic\Logic;
use App\Model\UserImFavorite;
use App\Model\UserImFavoriteRaw;

class UserImFavoriteRawLogic extends Logic
{


	public function index($gpc)
	{
		$data = $this->query($gpc)
			->selectRaw('user_im_favorite_raw.id,body AS body,type,user_im_favorite_raw.created_at')
			->orderBy('id','desc')
			->paginate((int) $gpc['limit'])
			->toArray();
		return $data;
	}



	/**
	 * @param $gpc
	 * @return UserImFavorite
	 */
	public function query($gpc)
	{
		$query = new UserImFavoriteRaw();
		$query = $query->where('user_id',Auth::id());
		if (isset($gpc['filter']) && !is_null($gpc['filter']))
        {
            $filter = $gpc['filter'];
            if ($filter !== 'all')
            {
                $explodeFilter = explode(',',$filter);
                $query->whereIn('type',$explodeFilter);
            }
        }
		if (isset($gpc['search']) && !is_null($gpc['search']))
        {
            $search = $gpc['search'];
			$query->like('body','%'.$search.'%');
        }
		return $query;
	}


	/**
	 * @param $gpc
	 * @return mixed
	 */
	public function store($gpc)
	{
		$data = [
			'user_id' => Auth::id(),
            'body' => $gpc['body'],
            'type' => $gpc['type']
		];
		$res = UserImFavoriteRaw::create($data);
		return $res;
	}




	/**
	 * @param $data
	 * @return string
	 */
	public function delete($data)
	{
		$res = UserImFavoriteRaw::whereIn('id',$data['id'])->delete();
		return $res;
	}
}
