<?php

declare(strict_types=1);

namespace App\Logic\User;

use App\Constants\ErrorCode;
use App\Event\UserLoginEvent;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Lib\IM\ServerAPI;
use App\Logic\Logic;
use App\Model\User;
use App\Model\UserIm;
use Hyperf\Di\Annotation\Inject;
use Psr\EventDispatcher\EventDispatcherInterface;

class UserImLogic extends Logic
{
    /**
     * @Inject
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param null $user
     * @return mixed
     */
    public function getImToken($user = null)
    {
        if (empty($user)) {
            $user = Auth::user();
        }
        $this->clearCache($user['id']); //clear cache
        //		return cache()->remember('user_im_'.$user['id'],function()use($user){
        $user_im = UserIm::where('user_id', $user['id'])->delete();
        //			if (empty($user_im)) {
        $res = $this->createUserId($user['name']);
        $data = [
            'user_id' => $user['id'],
            'accid' => $res['accid'],
            'token' => $res['token'],
        ];
        $user_im = UserIm::create($data);
        //			}
        return [
            'accid' => $user_im['accid'],
            'token' => $user_im['token'],
        ];
        //		},14400);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getImAccid($name)
    {
        if (empty($name)) {
            throw new BusinessException(ErrorCode::PARAMS_INVALID);
        }
        $user = User::where('name', $name)->orWhere('mobile', $name)->firstOrFail();
        $info = $this->getImToken($user);
        return $info['accid'];
    }

    public function clearCache($user_id = 0)
    {
        if (empty($user_id)) {
            cache()->clearPrefix('user_im_');
        } else {
            cache()->clearPrefix('user_im_' . $user_id);
        }
    }

    public function createUserId($accid)
    {
        $im = $this->getNetes();
        $res = $im->createUserId($accid);
        if (isset($res['code'])) {
            if ($res['code'] == 200) {
                return $res['info'];
            }
            if ($res['code'] == 414 && $res['desc'] == 'already register') {
                $res = $this->updateToken($accid);
                return $res['info'];
            }
        }

        $this->logError($accid, $res);

        logger()->error('创建im账号出现错误:' . json_encode($res));
        throw new BusinessException(ErrorCode::PARAMS_INVALID, '请求im发生错误');
    }

    public function updateToken($accid)
    {
        $res = $this->getNetes()->updateUserToken($accid);
        if (isset($res['code'])) {
            if ($res['code'] == 200) {
                return $res;
            }
        }

        $this->logError($accid, $res);

        logger()->error('更新im token出现错误:' . json_encode($res));
        throw new BusinessException(ErrorCode::PARAMS_INVALID, '请求im发生错误');
    }

    public function resetToken($user = null)
    {
        if (empty($user)) {
            $user = Auth::user();
        }
        $user_im = $this->getUserIm($user);
        if (empty($user_im)) {
            return $this->getImToken($user['name']);
        }
        $res = $this->getNetes()->updateUserToken($user_im['accid']);
        $user_im['token'] = $res['info']['token'];
        $user_im->save();
        $this->clearCache($user['id']);
        return $res['info']['token'];
    }

    public function getUserIm($user)
    {
        return UserIm::where('user_id', $user['id'])->orderBy('id', 'desc')->first();
    }

    public function addUplineAsFriend($downlineUser)
    {
        $downlineImUser = UserIm::where('user_id', $downlineUser['id'])->first();
        $uplineImUser = UserIm::where('user_id', $downlineUser['pid'])->first();
        if ($downlineImUser && $uplineImUser) {
            $this->getNetes()->addFriend($downlineImUser['accid'], $uplineImUser['accid']);
        }
    }

    public function getNetes()
    {
        return make(ServerAPI::class, [env('IM_APP_ID'), env('IM_APP_SECRET')]);
    }

    private function logError($accid, $res)
    {
        logger()->error('logError Called');
        $user = User::where('name', $accid)->first();
        $this->eventDispatcher->dispatch(new UserLoginEvent($user, 2, $res));
    }
}
