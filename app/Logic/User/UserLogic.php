<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\User;


use App\Constants\ErrorCode;
use App\Event\UserRegisteredEvent;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Logic\Finance\Credit1Logic;
use App\Logic\Finance\Credit2Logic;
use App\Logic\Logic;
use App\Model\Role;
use App\Model\SellerReport;
use App\Model\User;
use App\Model\UserIm;
use Hyperf\DbConnection\Db;
use Hyperf\ModelCache\Manager;
use Hyperf\Utils\ApplicationContext;
use Psr\EventDispatcher\EventDispatcherInterface;

class UserLogic extends Logic
{



	public function getUserInfo($user_id = 0)
	{
		if (empty($user_id) && !Auth::isAdmin()) {
			$user_id = Auth::id();
		}
		$user = User::findOrFailFromCache($user_id);
		$reward = $this->reward_point($user_id) ; 

		$user->refresh();
		return  [
			'id' => $user['id'],
			'name' => $user['name'],
			'mobile' => $user['mobile'],
			'nickname' => $user['nickname'],
			'realname' => $user['realname'],
			'avatar' => $user['avatar'],
			'gender' => $user['gender'].'',
			'level' => $user['level'],
			'credit1' => $user['credit1'],
            'credit2' => $user['credit2'],
			'credit3' => $user['credit3'],
			'reward_mode' => $reward,  
            'mudan' => (int)$user['mudan_coin'],
			'auth_status' => $user['auth_status'],
			'status' => $user['status'],
			'created_at' => $user['created_at'],
			'has_sign' => make(UserSignLogic::class)->checkHasSign($user['id'])
		];
	}

	public function reward_point ($user_id) {
	    try {
            $url = 'https://test.fr167.com//api/get-user-c-coin/';
            //$url = 'https://fr167.com/api/get-user-c-coin/';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . $user_id);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);

            $json = json_decode($output, true);
            if ($json['status'] == true) {
                return $json['c_coin'];
            }
        } catch (\Exception $e){
	        return 0;
        }
	}

	public function index($gpc)
	{
		$data = $this->query($gpc)
			->selectRaw('id,name,email,level,mobile,pid,realname,nickname,gender,avatar,province,city,area,address,credit1,credit2,auth_status,status,remark,created_at')
			->orderBy('id','desc')
			->paginate((int)$gpc['limit'])
			->toArray();
		foreach ($data['data'] as $k => $v) {
			if ($v['pid']) {
				$data['data'][$k]['agent'] = User::where('id', $v['pid'])->value('name');
			} else {
				$data['data'][$k]['agent'] = '';
			}
		}
		return $data;
	}



	/**
	 * @param $gpc
	 * @return Role
	 */
	public function query($gpc)
	{
		$query = new User();
		if (!empty($gpc['search'])) {
			$search = $gpc['search'].'%';
			$query = $query->where(function ($query)use($search) {
				$query->where('name','like',$search)
					->orWhere('mobile', 'like', $search)
					->orWhere('realname', 'like', $search);
			});
		}
		if (isset($gpc['status']) && ($gpc['status'] != '') && !is_null($gpc['status'])) {
			if (!is_array($gpc['status'])) {
				$gpc['status'] = [$gpc['status']];
			}
			$query = $query->whereIn('status', $gpc['status']);
		}
		if (isset($gpc['level']) && ($gpc['level'] != '') && !is_null($gpc['level'])) {
			$query = $query->where('level', $gpc['level']);
		}

		if (isset($gpc['start_end']) && !empty($gpc['start_end'])) {
			if (!isset($gpc['start_end'][0]) || !isset($gpc['start_end'][1])) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'时间格式错误');
			}
			$query = $query->whereBetween('created_at', [$gpc['start_end'][0], $gpc['start_end'][1]]);
		}
		return $query;
	}


	/**
	 * @param $data
	 * @return mixed
	 * @throws \Exception
	 */
	public function store($data)
	{
		$data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
		$data['pid'] = 0;
		if (isset($data['agent']) && $data['agent']) {
			$pid = User::where('name', $data['agent'])->value('id');
			if (!$pid) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'推荐人不存在');
			}
			$data['pid'] = $pid;
		}

		try {
			$res = Db::transaction(function () use ($data) {
				$user = User::create($data);
				$relation_logic = new UserRelationLogic();
				$relation_logic->add($user);
				make(EventDispatcherInterface::class)->dispatch(new UserRegisteredEvent($user,'add'));
				return $user;
			});
		} catch (\Exception $e) {
			if (strpos($e->getMessage(), 'Duplicate entry') !== false) {
				throw new BusinessException(ErrorCode::PARAMS_INVALID,'账号已存在');
			} else {
				throw $e;
			}
		}
		unset($res['password']);
		return $res;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function update($data)
	{
		$user = User::findOrFailFromCache($data['id']);
		if (isset($data['password']) && $data['password']) {
			$data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
		}
		$user->update($data);
		return null;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function delete($data)
	{
		// TODO 删除限制

		Db::transaction(function () use ($data) {
			foreach ($data['id'] as $v) {
				$user = User::findOrFail($v);
				$user->name = $user->name.'_'.time();
				$user->save();
				$user->delete();
			}
		});
		return null;
	}


	/**
	 * @param $data
	 * @return string
	 */
	public function setStatus($data)
	{
		$user = User::findOrFailFromCache($data['id']);
		$user['status'] = $data['status'];
		$user->save();
		return null;
	}

	/**
	 * @param $gpc
	 */
	public function setCredit($gpc)
	{
		switch ($gpc['account']) {
			case 1:
				$logic = make(Credit1Logic::class);
				$key = 'credit1';
				break;
			case 2:
				$logic =  make(Credit2Logic::class);
				$key = 'credit2';
				break;
			default:
				throw new BusinessException();
		}
		$user = User::findOrFailFromCache($gpc['id']);

		if ($gpc['change'] == 1) {
			$money = $gpc['money'] - $user[$key];
		} else {
			$money = $gpc['money'];
		}
		$params = [
			'credit' => $money,
			'user'   => $user,
			'remark' => '管理员设置',
			'type'   => 0
		];
		$logic->setCredit($params);
	}



	/**
	 * @param $gpc
	 */
	public function setInfo($gpc)
	{
		$user = Auth::user();
		$user->update($gpc);
	}

	public function checkUserByName($name)
    {
        $user = User::where('name',$name)->first();
        return $user;
	}
	
	public function reportSeller($gpc) { 
		$user_id = Auth::user()->id; 
		$data = [
			'seller_id' => $gpc['seller_id'],
			'message' => $gpc['message'], 
			'user_id' => $user_id
		];
		$create = SellerReport::create($data);
		
		if($create) { 
			return [
				'status'=>'success'
			];
		}
	}
}
