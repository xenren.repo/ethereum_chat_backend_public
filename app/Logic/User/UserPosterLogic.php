<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\User;


use App\Kernel\Auth\Auth;
use App\Kernel\Common\Common;
use App\Kernel\Common\Helper;
use App\Lib\Other\Images;
use App\Logic\Logic;
use App\Model\UserImFavorite;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;

class UserPosterLogic extends Logic
{

	public function getPoster()
	{
		$user = Auth::user();
		if (empty($user['old_id'])) {
			$url = 'https://fr167.com/api/my-qr-code';
			$query = [
				'username' => $user['name']
			];
			$res = curl()->get($url,$query);

			if (isset($res['status']) && $res['status'] == 'success') {
				$user['old_id'] = $res['display_id'];
				$user->save();
				$user->refresh();
			}
		}
		return $this->generatePoster($user);
	}



	/**
	 * @param array $user
	 * @return string
	 * @throws \Exception
	 */
	public function generatePoster($user)
	{
		$qrcode = $this->generateQrcode($user);
		$images = new Images();
		$path = $images->blend($qrcode,$user['id']);
		return Common::host().'/poster/'.$path;
	}


	/**
	 * 生成用户推广二维码
	 * @param $user
	 * @return string
	 */
	public function generateQrcode($user)
	{
		$options = new QROptions([
			'version'    => 7,
			'quietzoneSize' => 4
		]);
		$qr = new QRCode($options);
		if (!file_exists(BASE_PATH . '/resources/qrcode')) {
			mkdir(BASE_PATH . '/resources/qrcode',0775);
		}
		$path = BASE_PATH.'/resources/qrcode/user_'.$user['id'].'.png';
		$qr->render($this->getRecommendUrl($user),$path);
		return $path;
	}


	/**
	 * @param $user
	 * @return string
	 */
	public function getRecommendUrl($user)
	{
		return 'https://fr167.com/guest/register?introducer_id='.$user['old_id'];
	}

}
