<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\User;


use App\Exception\BusinessException;
use App\Kernel\Common\Helper;
use App\Logic\Logic;
use App\Model\User;
use App\Model\UserRelation;

class UserRelationLogic extends Logic
{


	/**
	 * 重置所有用户上下级关系
	 */
	public function rebuildRelation()
	{
		UserRelation::where('id','>','0')->delete();

		$users = User::selectRaw('id,pid')->orderBy('id')->get()->toArray();

		foreach ($users as $user) {
			$parents = Helper::getParents($users,$user['id']);
			array_shift($parents);
			if ($parents) {
				$info = [];
				foreach ($parents as $k => $parent) {
					$info[] = [
						'user_id' => $user['id'],
						'pid'     => $parent['id'],
						'level'   => $k+1,
					];
				}
				if ($info) {
					UserRelation::insert($info);
				}
			}
		}
	}


	/**
	 * 会员新增父级关系处理
	 * @param array $user
	 */
	public function add($user)
	{
		if ($user['pid']) {
			$parents = UserRelation::where('user_id',$user['pid'])
				->selectRaw('pid,level')
				->get()->toArray();

			$info[] = [
				'user_id' => $user['id'],
				'pid'     => $user['pid'],
				'level'   => 1
			];

			if ($parents) {
				foreach ($parents as $parent) {
					$info[] = [
						'user_id' => $user['id'],
						'pid'     => $parent['pid'],
						'level'   => $parent['level'] + 1
					];
				}
			}
			UserRelation::insert($info);
		}
	}


	/**
	 * 获取无限子级ID
	 * @param $user_id
	 * @param int $level_type 子级层级  0: 所有 -1:所有非1级用户
	 * @param int $level 子级会员等级
	 * @return array
	 */
	public function getChildrenIds($user_id,$level_type = 0,$level = null)
	{
		if (empty($user_id) || !is_numeric($user_id) || !is_numeric($level_type)) {
			throw new BusinessException();
		}
		$query = UserRelation::leftJoin('users','users.id','=','user_relation.user_id')
			->where('user_relation.pid',$user_id)
			->whereNull('users.deleted_at');
		if ($level_type == -1) {
			$query = $query->where('user_relation.level',1,'>');
		} else  if ($level_type > 0) {
			$query = $query->where('user_relation.level',$level_type);
		}
		if ($level !== null) {
			$query = $query->where('users.level',$level);
		}
		return $query->where('user_relation.user_id','<>',$user_id)
			->orderBy('user_relation.level')
			->pluck('user_relation.user_id');
	}

	/**
	 * 获取无限父级ID
	 * @param $user_id
	 * @param int $limit
	 * @return array
	 */
	public function getParentIds($user_id,$limit = 0)
	{
		if ($user_id) {
			$query = UserRelation::where('user_id',$user_id);
			if ($limit != 0) {
				$query = $query->limit($limit);
			}
			$pid = $query->orderBy('level')->pluck('pid');
		}
		return $pid ?? [];
	}

}
