<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Logic\User;


use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\Auth;
use App\Kernel\Common\Common;
use App\Kernel\Common\Helper;
use App\Logic\Logic;
use App\Model\User;
use App\Model\UserImFavorite;
use App\Model\UserSign;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Psr\EventDispatcher\EventDispatcherInterface;

class UserSignLogic extends Logic
{

	const DEFAULT_SIGN_TYPE = 0;  // 正常签到
	const PLUS_SIGN_TYPE = 1;     // 补签


	/**
	 * @param int $user_id
	 * @param string $date
	 * @param int $type
	 * @return bool
	 */
	public function sign($user_id = 0, $date = '', $type = 0)
	{
		if (empty($user_id)) {
			$user_id = Auth::id();
		}

		if (empty($date)) {
			$date = date('Y-m-d');
		}

		if (!in_array($type, [0, 1])) {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,'签到类型错误');
		}
		$exist = UserSign::where('user_id',$user_id)->where('sign_date',$date)->value('id');

		if (!$exist) {
			try {
				$data = [
					'user_id'    => $user_id,
					'sign_date'  => $date,
					'sign_type'       => $type
				];
				$message = Db::transaction(function()use($data){
					UserSign::create($data);
					return $this->remoteSign($data['user_id'],$data['sign_date']);
				});
			} catch (\Exception $e) {
				if (strpos($e->getMessage(), 'error=Duplicate entry') !== false) {
					throw new BusinessException(ErrorCode::PARAMS_INVALID,'不能重复签到');
				}
				logger()->error($e->getMessage());
				throw new BusinessException(ErrorCode::PARAMS_INVALID,$e->getMessage());
			}
		}
		return $message ?? '';
	}


	/**
	 * 检测某天是否签到
	 * @param int $user_id
	 * @param string $date
	 * @return bool
	 */
	public function checkHasSign($user_id = 0, $date = '')
	{
		if (empty($user_id)) {
			$user_id =  Auth::id();
		}

		if (empty($date)) {
			$date = date('Y-m-d');
		}

		$exist = UserSign::where('user_id',$user_id)->where('sign_date',$date)->value('id');

		return $exist ? 1 : 0;
	}


	public function remoteSign($user_id, $date)
	{
		$url = 'https://fr167.com/api/check-in-individual';
		$user = User::findFromCache($user_id);

		$data = [
			'server_token' => 'Dhjj5-7042m@N52190Hctas#',
			'check_in_date' => $date,
			'username'   => $user['name']
		];
		$res = curl()->post($url,$data);
		if (isset($res['status']) && $res['status'] == 'success') {
			return $res['message'];
		} else {
			throw new BusinessException(ErrorCode::PARAMS_INVALID,$res['message'] ?? '');
		}
	}
}
