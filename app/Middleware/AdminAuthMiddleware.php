<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Middleware;

use App\Kernel\Auth\Auth;
use App\Kernel\Auth\AuthManager;
use App\Logic\Admin\AdminAuthLogic;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class AdminAuthMiddleware implements MiddlewareInterface
{

	/**
	 * Process an incoming server request and return a response, optionally delegating
	 * response creation to a handler.
	 * @param \Psr\Http\Message\ServerRequestInterface $request
	 * @param \Psr\Http\Server\RequestHandlerInterface $handler
	 * @return \Psr\Http\Message\ResponseInterface
	 * @throws \Throwable
	 */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
	    make(AuthManager::class)->parseData(AdminAuthLogic::AUTH_TYPE);
	    $admin = Auth::user();
	    make(AdminAuthLogic::class)->checkLogin($admin);
	    $response = $handler->handle($request);
	    return $response;
    }
}