<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Middleware;

use App\Exception\BusinessException;
use App\Kernel\Common\Common;
use Hyperf\Utils\Context;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class CorsMiddleware implements MiddlewareInterface
{


	/**
	 * @param ServerRequestInterface $request
	 * @param RequestHandlerInterface $handler
	 * @return ResponseInterface
	 * @throws BusinessException
	 */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
	    $response = Context::get(ResponseInterface::class);
	    if ('OPTIONS' === $request->getMethod()) {
		    return $this->configResponse($request,$response);
	    }
	    $response = $this->configResponse($request,$response);
	    Context::set(ResponseInterface::class, $response);
	    $response = $handler->handle($request);
	    return $response;
    }


	/**
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @return ResponseInterface
	 */
	private function configResponse(ServerRequestInterface $request, ResponseInterface $response)
	{
		$origin =  $request->getHeaders()['origin'][0] ?? '';
		$allow_origin = Common::getSetting('system.allow_origin');
		if (!empty($allow_origin)) {
			$allow_origin = explode(',',$allow_origin);
		}
		$allow_origin = $allow_origin ? $allow_origin : [];
		$is_allow_all = (array_search('*',$allow_origin) !== false);
		if(in_array($origin, $allow_origin) || $is_allow_all){
			if ($is_allow_all) {
				$origin = '*';
			} else {
				/*当允许访问域名为 * 时,Access-Control-Allow-Credentials 若为true,请求将会失败 */
				$response = $response->withHeader('Access-Control-Allow-Credentials','true');
			}
		}
		return $response->withHeader('Access-Control-Allow-Origin',$origin)
			->withHeader('Access-Control-Allow-Headers','Origin, Content-Type, Cookie, Accept,Authorization, x-xsrf-token, authorization')
			->withHeader('Access-Control-Allow-Methods','GET, POST, PATCH, PUT, OPTIONS');
	}
}
