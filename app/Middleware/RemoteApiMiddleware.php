<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Middleware;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Common\Common;
use App\Kernel\Common\Helper;
use Hyperf\Contract\TranslatorInterface;
use Hyperf\Translation\Translator;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class RemoteApiMiddleware implements MiddlewareInterface
{


	/**
	 * @param ServerRequestInterface $request
	 * @param RequestHandlerInterface $handler
	 * @return ResponseInterface
	 * @throws BusinessException
	 */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
	    if (Helper::isProd()) {
	    	$ip = ip();
		    if (env('REMOTE_SYSTEM_IP') != $ip) {
			   // throw new BusinessException(ErrorCode::PERMISSION_DENY);
		    }
	    }

	    $response = $handler->handle($request);
	    return $response;
    }


}
