<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Middleware;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class SubmitThrottleMiddleware implements MiddlewareInterface
{


	/**
	 * 提交频率限制
	 * @param ServerRequestInterface $request
	 * @param RequestHandlerInterface $handler
	 * @return ResponseInterface
	 * @throws BusinessException
	 */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
		$expire = $this->getThrottleSeconds($request->getUri()->getPath());
		$key = 'throttle_'.$this->getFingerPrint($request);
		$add_key = cache()->addKey($key,'throttle');
	    if (!$add_key) {
		    throw new BusinessException(ErrorCode::THROTTLE_SUBMIT);
	    }
	    cache()->expire($key,$expire);
	    $response = $handler->handle($request);
	    return $response;
    }


	/**
	 * 获取访问指纹
	 * @param ServerRequestInterface $request
	 * @return string
	 */
	public function getFingerPrint(ServerRequestInterface $request)
	{
		$cookies = $request->getCookieParams() ? implode(',',$request->getCookieParams()):'';
		$ip = ip();
		return sha1(implode('|', [$request->fullUrl(),$ip,$cookies]));
	}


	/**
	 * 因中间件无法传递参数,暂时用此方式解决限制秒数非默认的路由处理
	 * @param $path
	 * @return int|mixed
	 */
	public function getThrottleSeconds($path)
	{
		$data = [];
		return $data[$path] ?? 2;
	}
}