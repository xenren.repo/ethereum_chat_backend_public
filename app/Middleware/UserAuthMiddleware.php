<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Middleware;

use App\Kernel\Auth\Auth;
use App\Kernel\Auth\AuthManager;
use App\Logic\Admin\AdminAuthLogic;
use App\Logic\User\UserAuthLogic;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class UserAuthMiddleware implements MiddlewareInterface
{

	/**
	 * 用户端认证中间件
	 * @param ServerRequestInterface $request
	 * @param RequestHandlerInterface $handler
	 * @return ResponseInterface
	 * @throws \Throwable
	 */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
	    make(AuthManager::class)->parseData(UserAuthLogic::AUTH_TYPE);
	    $user = Auth::user();
	    make(UserAuthLogic::class)->checkLogin($user);
	    $response = $handler->handle($request);
	    return $response;
    }
}