<?php

declare(strict_types=1);

namespace App\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property int $gender
 * @property string $mobile
 * @property int $status
 * @property string $realname
 * @property string $remark
 * @property float $credit1
 * @property bool $is_online
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $last_online_at
 */
class Admin extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'email', 'password', 'gender', 'mobile', 'status', 'realname', 'remark', 'credit1', 'deleted_at', 'created_at', 'updated_at', 'group_id', 'is_online', 'last_online_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'gender' => 'integer', 'status' => 'integer', 'credit1' => 'float', 'created_at' => 'datetime', 'updated_at' => 'datetime', 'is_online' => 'boolean', 'updated_at' => 'datetime'];

    public function scopeDefault($query, $status = [2])
    {
        return $query->whereIn('status', $status);
    }

    public function reseller()
    {
        return $this->hasOne(Seller::class, 'admin_id', 'id');
    }
}
