<?php

declare(strict_types=1);

namespace App\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $foreign_id
 * @property int $type
 * @property string $sub_type
 * @property string $record_no
 * @property float $amount
 * @property float $original_amount
 * @property float $fee
 * @property float $balance
 * @property string $pay_time
 * @property string $remark
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Credit1 extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'credit1';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'foreign_id',
        'type',
        'sub_type',
        'record_no',
        'amount',
        'original_amount',
        'fee',
        'balance',
        'pay_time',
        'remark',
        'deleted_at',
        'created_at',
        'updated_at',
        'status',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'foreign_id' => 'integer',
        'type' => 'integer',
        'amount' => 'float',
        'original_amount' => 'float',
        'fee' => 'float',
        'balance' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function seller()
    {
        return $this->hasOne(Seller::class, 'admin_id', 'user_id');
    }

    public function order()
    {
        return $this->hasOne(Order::class, 'id', 'foreign_id');
    }
}
