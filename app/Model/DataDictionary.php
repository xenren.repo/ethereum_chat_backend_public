<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $module 
 * @property string $key_name 
 * @property int $key_id 
 * @property string $key_value 
 * @property string $remark 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class DataDictionary extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'data_dictionary';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'module', 'key_name', 'key_id', 'key_value', 'remark', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'key_id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}