<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $uniqueid 
 * @property int $type 
 * @property string $message 
 * @property int $status 
 * @property string $url 
 * @property string $request_data 
 * @property string $original_object 
 * @property string $error_line 
 * @property string $error_message 
 * @property string $error_raw 
 * @property string $response_data 
 */
class FrRequestStatus extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fr_request_status';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'uniqueid', 'type', 'message', 'status', 'url', 'request_data', 'original_object', 
                            'error_line','error_message','error_raw', 'response_data', 'created_at','updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer', 
        'uniqueid' => 'string', 
        'status' => 'integer', 
        'type' => 'integer', 
        'message' => 'string',
        'url' => 'string',
        'request_data' => 'string',
        'original_object' => 'string',
        'error_line' => 'string',
        'error_message' => 'string',
        'error_raw' => 'string',
        'response_data' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}