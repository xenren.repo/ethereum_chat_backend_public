<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $title 
 * @property string $description 
 * @property string $images 
 * @property string $content 
 * @property float $price 
 * @property float $original_price 
 * @property float $cost_price 
 * @property float $express_cost 
 * @property int $sale_num 
 * @property int $real_sale_num 
 * @property int $version 
 * @property int $stock 
 * @property int $sort 
 * @property int $on_sale 
 * @property int $type 
 * @property int $is_recommend 
 * @property int $is_new 
 * @property int $is_hot 
 * @property int $ship_free 
 * @property int $is_commission 
 * @property string $commission 
 * @property int $source_from 
 * @property int $detail_update 
 * @property int $stock_type 
 * @property int $weight 
 * @property string $unit 
 * @property string $item_id 
 * @property string $goods_sn 
 * @property string $item_url 
 * @property string $cupon_url
 * @property float $item_endprice
 * @property int $min_item_endprice
 * @property float $coupon_money
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class Goods extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'goods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'title', 'description', 'images', 'content', 'price', 'original_price', 'cost_price', 'express_cost', 'sale_num', 'real_sale_num', 'version', 'stock', 'sort', 'on_sale', 'type', 'is_recommend', 'is_new', 'is_hot', 'ship_free', 'is_commission', 'commission', 'source_from', 'detail_update', 'stock_type', 'weight', 'unit', 'item_id', 'goods_sn', 'item_url', 'cupon_url', 'item_endprice', 'min_item_endprice', 'coupon_money', 'deleted_at', 'created_at', 'updated_at', 'receipt_required', 'reward_mode', 'created_by'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'price' => 'float', 'original_price' => 'float', 'cost_price' => 'float', 'express_cost' => 'float', 'sale_num' => 'integer', 'real_sale_num' => 'integer', 'version' => 'integer', 'stock' => 'integer', 'sort' => 'integer', 'on_sale' => 'integer', 'type' => 'integer', 'is_recommend' => 'integer', 'is_new' => 'integer', 'is_hot' => 'integer', 'ship_free' => 'integer', 'is_commission' => 'integer', 'source_from' => 'integer', 'detail_update' => 'integer', 'stock_type' => 'integer', 'weight' => 'integer', 'item_endprice' => 'float', 'min_item_endprice' => 'integer', 'coupon_money' => 'float', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}