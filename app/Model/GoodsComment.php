<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property int $order_id 
 * @property int $goods_id 
 * @property int $order_goods_id 
 * @property int $pid 
 * @property string $nickname 
 * @property string $avatar 
 * @property float $score 
 * @property string $content 
 * @property string $images 
 * @property int $sort 
 * @property int $type 
 * @property int $status 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class GoodsComment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'goods_comments';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'order_id', 'goods_id', 'order_goods_id', 'pid', 'nickname', 'avatar', 'score', 'content', 'images', 'sort', 'type', 'status', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'order_id' => 'integer', 'goods_id' => 'integer', 'order_goods_id' => 'integer', 'pid' => 'integer', 'score' => 'float', 'sort' => 'integer', 'type' => 'integer', 'status' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}