<?php

declare (strict_types=1);
namespace App\Model;

/**
 */
class GoodsCommentReply extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'goods_comment_replies';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'goods_comment_id',
        'pid',
        'user_id',
        'content',
        'img'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}