<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $title 
 * @property int $goods_id 
 * @property string $images 
 * @property float $price 
 * @property float $original_price 
 * @property float $cost_price 
 * @property string $specs 
 * @property int $version 
 * @property int $stock 
 * @property int $sort 
 * @property int $sale_num 
 * @property int $real_sale_num 
 * @property int $weight 
 * @property string $unit 
 * @property string $goods_sn 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class GoodsOption extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'goods_options';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'title', 'goods_id', 'images', 'price', 'original_price', 'cost_price', 'specs', 'version', 'stock', 'sort', 'sale_num', 'real_sale_num', 'weight', 'unit', 'goods_sn', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'goods_id' => 'integer', 'price' => 'float', 'original_price' => 'float', 'cost_price' => 'float', 'version' => 'integer', 'stock' => 'integer', 'sort' => 'integer', 'sale_num' => 'integer', 'real_sale_num' => 'integer', 'weight' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}