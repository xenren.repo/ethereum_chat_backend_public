<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $name 
 * @property string $author 
 * @property string $description 
 * @property string $img 
 * @property string $url 
 * @property int $pid 
 * @property int $cid 
 * @property int $up 
 * @property int $down 
 * @property int $read_num 
 * @property int $sort 
 * @property string $content 
 * @property int $status 
 * @property int $type 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class Medium extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'medium';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'author', 'description', 'img', 'url', 'pid', 'cid', 'up', 'down', 'read_num', 'sort', 'content', 'status', 'type', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'pid' => 'integer', 'cid' => 'integer', 'up' => 'integer', 'down' => 'integer', 'read_num' => 'integer', 'sort' => 'integer', 'status' => 'integer', 'type' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}