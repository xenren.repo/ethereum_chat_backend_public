<?php

declare (strict_types=1);
namespace App\Model;

use Hyperf\Database\Model\SoftDeletes;

/**
 * @property int $id 
 * @property string $name 
 * @property string $permission 
 * @property string $icon 
 * @property string $description 
 * @property string $path 
 * @property string $rel 
 * @property int $pid 
 * @property int $sort 
 * @property int $type 
 * @property int $client_type 
 * @property int $is_hidden 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class Module extends Model
{
	use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'module';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id',
        'name',
        'permission',
        'icon',
        'description',
        'path',
        'rel',
        'pid',
        'sort',
        'type',
        'client_type',
        'is_hidden',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'pid' => 'integer', 'sort' => 'integer', 'type' => 'integer', 'client_type' => 'integer', 'is_hidden' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}