<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $order_no 
 * @property string $trans_id 
 * @property int $package_num 
 * @property int $user_id 
 * @property int $pid 
 * @property float $total_amount 
 * @property float $original_total 
 * @property float $express_amount 
 * @property float $off_amount 
 * @property float $goods_amount 
 * @property int $pay_type 
 * @property int $type 
 * @property int $status 
 * @property int $is_comment 
 * @property string $commission 
 * @property float $total_commission 
 * @property string $remark 
 * @property string $platform_remark 
 * @property string $close_time 
 * @property string $pay_time 
 * @property string $deliver_time 
 * @property string $finished_time 
 * @property int $version 
 * @property string $receiver 
 * @property string $receiver_mobile 
 * @property string $province 
 * @property string $city 
 * @property string $area 
 * @property string $address 
 * @property int $refund_id 
 * @property float $refund_amount 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class Order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'order_no',
        'trans_id',
        'package_num',
        'user_id',
        'pid',
        'total_amount',
        'original_total',
        'express_amount',
        'off_amount',
        'goods_amount',
        'pay_type',
        'type',
        'status',
        'is_comment',
        'commission',
        'total_commission',
        'remark',
        'platform_remark',
        'close_time',
        'pay_time',
        'deliver_time',
        'finished_time',
        'version',
        'receiver',
        'receiver_mobile',
        'province',
        'city',
        'area',
        'address',
        'refund_id',
        'refund_amount',
        'deleted_at',
        'reward_price',
        'reward_mode',
        'created_at',
        'updated_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    const status_seller_refunded = 4;

    protected $casts = ['id' => 'integer', 'package_num' => 'integer', 'user_id' => 'integer', 'pid' => 'integer', 'total_amount' => 'float', 'original_total' => 'float', 'express_amount' => 'float', 'off_amount' => 'float', 'goods_amount' => 'float', 'pay_type' => 'integer', 'type' => 'integer', 'status' => 'integer', 'is_comment' => 'integer', 'total_commission' => 'float', 'version' => 'integer', 'refund_id' => 'integer', 'refund_amount' => 'float', 'created_at' => 'datetime', 'updated_at' => 'datetime'];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function admin()
    {
        return $this->hasOne(Admin::class, 'id', 'user_id');
    }

    public function sellerRefundRequest()
    {
        return $this->hasOne(SellerOrderRefundRequest::class, 'order_id', 'id');
    }
}