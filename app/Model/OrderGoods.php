<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property int $order_id 
 * @property int $goods_id 
 * @property int $option_id 
 * @property int $package_id 
 * @property string $title 
 * @property string $option_title 
 * @property string $images 
 * @property int $num 
 * @property float $price 
 * @property float $original_price 
 * @property float $cost_price 
 * @property float $total 
 * @property string $commission 
 * @property float $total_commission 
 * @property int $source_from 
 * @property string $cupon_url 
 * @property string $item_url 
 * @property float $off_amount 
 * @property int $type 
 * @property int $is_comment 
 * @property float $weight 
 * @property string $unit 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class OrderGoods extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_goods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'order_id', 'goods_id', 'option_id', 'package_id', 'title', 'option_title', 'images', 'num', 'price', 'original_price', 'cost_price', 'total', 'commission', 'total_commission', 'source_from', 'cupon_url', 'item_url', 'off_amount', 'type', 'is_comment', 'weight', 'unit', 'deleted_at', 'created_at', 'updated_at', 'reward_price'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'order_id' => 'integer', 'goods_id' => 'integer', 'option_id' => 'integer', 'package_id' => 'integer', 'num' => 'integer', 'price' => 'float', 'original_price' => 'float', 'cost_price' => 'float', 'total' => 'float', 'total_commission' => 'float', 'source_from' => 'integer', 'off_amount' => 'float', 'type' => 'integer', 'is_comment' => 'integer', 'weight' => 'float', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}