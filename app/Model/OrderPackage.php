<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $package_no 
 * @property int $order_id 
 * @property int $goods_num 
 * @property string $receiver 
 * @property string $receiver_mobile 
 * @property string $province 
 * @property string $city 
 * @property string $area 
 * @property string $address 
 * @property string $code 
 * @property string $express 
 * @property string $express_id 
 * @property string $custom_code 
 * @property string $express_sn 
 * @property string $remark 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class OrderPackage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_package';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'package_no', 'order_id', 'goods_num', 'receiver', 'receiver_mobile', 'province', 'city', 'area', 'address', 'code', 'express', 'express_id', 'custom_code', 'express_sn', 'remark', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'order_id' => 'integer', 'goods_num' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}