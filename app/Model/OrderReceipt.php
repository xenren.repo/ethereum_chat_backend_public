<?php

declare (strict_types=1);

namespace App\Model;

/**
 * @property int $id
 * @property string $company_name
 * @property string $company_code
 * @property string $company_phone
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class OrderReceipt extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_receipts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',
        'company_code',
        'company_phone',
        'name',
        'order_no',
        'receipt_required',
        'type',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_no' => 'string',
        'company_name' => 'string',
        'company_code' => 'string',
        'receipt_required' => 'string',
        'type' => 'string',
        'company_phone' => 'string',
        'name' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}