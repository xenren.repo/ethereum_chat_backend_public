<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $refund_no 
 * @property int $user_id 
 * @property int $order_id 
 * @property float $refund_amount 
 * @property string $reason 
 * @property string $refund_credit 
 * @property int $status 
 * @property string $receiver 
 * @property string $receiver_mobile 
 * @property string $province 
 * @property string $city 
 * @property string $area 
 * @property string $address 
 * @property string $order_goods_ids 
 * @property int $apply_type 
 * @property int $refund_result 
 * @property string $remark 
 * @property int $version 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class OrderRefund extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_refund';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'refund_no', 'user_id', 'order_id', 'refund_amount', 'reason', 'refund_credit', 'status', 'receiver', 'receiver_mobile', 'province', 'city', 'area', 'address', 'order_goods_ids', 'apply_type', 'refund_result', 'remark', 'version', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'user_id' => 'integer', 'order_id' => 'integer', 'refund_amount' => 'float', 'status' => 'integer', 'apply_type' => 'integer', 'refund_result' => 'integer', 'version' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}