<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $recharge_no 
 * @property int $user_id 
 * @property string $trans_id 
 * @property int $verify_code 
 * @property string $bank 
 * @property string $bank_no 
 * @property string $bank_holder 
 * @property string $bank_mobile 
 * @property string $created_place 
 * @property string $sender 
 * @property string $credit 
 * @property int $version 
 * @property int $type 
 * @property int $status 
 * @property float $amount 
 * @property float $cny_amount 
 * @property float $charge_fee 
 * @property float $reality 
 * @property string $remark 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class Recharge extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'recharge';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'recharge_no', 'user_id', 'trans_id', 'verify_code', 'bank', 'bank_no', 'bank_holder', 'bank_mobile', 'created_place', 'sender', 'credit', 'version', 'type', 'status', 'amount', 'cny_amount', 'charge_fee', 'reality', 'remark', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'user_id' => 'integer', 'verify_code' => 'integer', 'version' => 'integer', 'type' => 'integer', 'status' => 'integer', 'amount' => 'float', 'cny_amount' => 'float', 'charge_fee' => 'float', 'reality' => 'float', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}