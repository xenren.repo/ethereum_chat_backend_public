<?php

declare(strict_types=1);

namespace App\Model;

class Seller extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seller';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_id',
        'approval_photo',
        'company_info',
        'company_picture',
        'company_no',
        'rank_id',
        'new_user',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function admin()
    {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }
}
