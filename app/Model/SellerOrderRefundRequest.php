<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $name 
 * @property string $description 
 * @property int $status 
 * @property int $type 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class SellerOrderRefundRequest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seller_order_refund_request';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'admin_id',
        'bank_name',
        'bank_account_no',
        'bank_account_holder_name',
        'img_url',
        'order_id'
    ];

    const STATUS_APPROVED = 1;
    const STATUS_IN_PROGRESS = 0;

}