<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $system_no 
 * @property int $order_id 
 * @property int $charge_id 
 * @property float $money 
 * @property float $refund 
 * @property float $real_money 
 * @property int $status 
 * @property int $user_id 
 * @property int $pay_type 
 * @property string $pay_time 
 * @property string $trans_no 
 * @property int $type 
 * @property string $remark 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $unionid 
 * @property string $mch_id 
 * @property string $device_id 
 */
class ThirdPay extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'third_pay';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'system_no', 'order_id', 'charge_id', 'money', 'refund', 'real_money', 'status', 'user_id', 'pay_type', 'pay_time', 'trans_no', 'type', 'remark', 'deleted_at', 'created_at', 'updated_at', 'unionid', 'mch_id', 'device_id'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'order_id' => 'integer', 'charge_id' => 'integer', 'money' => 'float', 'refund' => 'float', 'real_money' => 'float', 'status' => 'integer', 'user_id' => 'integer', 'pay_type' => 'integer', 'type' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}