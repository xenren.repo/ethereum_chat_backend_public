<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property string $app_id 
 * @property string $open_id 
 * @property int $type 
 * @property int $user_type 
 * @property int $user_id 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class ThirdUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'third_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'app_id', 'open_id', 'type', 'user_type', 'user_id', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'type' => 'integer', 'user_type' => 'integer', 'user_id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}