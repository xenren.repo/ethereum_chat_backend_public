<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property string $path
 * @property string $file_name
 * @property string $ext
 * @property string $full_path
 * @property boolean $is_shareable
 * @property \Carbon\Carbon $must_deleted_at
 * @property \Carbon\Carbon $created_at
 */
class UploadFileLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'upload_file_logs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'path', 'file_name', 'ext', 'full_path', 'is_shareable', 'must_deleted_at', 'created_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'path' => 'string',
        'file_name' => 'string',
        'ext' => 'string',
        'full_path' => 'string',
        'is_shareable' => 'boolean',
        'must_deleted_at' => 'datetime',
        'created_at' => 'datetime',
    ];
}