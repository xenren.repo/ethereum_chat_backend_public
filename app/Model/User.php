<?php

declare (strict_types=1);
namespace App\Model;

use Hyperf\Database\Model\SoftDeletes;
/**
 * @property int $id 
 * @property int $old_id 
 * @property string $name 
 * @property string $email 
 * @property string $password 
 * @property string $pay_password 
 * @property string $coin_password 
 * @property int $pid 
 * @property int $level 
 * @property int $gender 
 * @property string $public_openid 
 * @property string $mini_openid 
 * @property string $unionid 
 * @property string $mobile 
 * @property string $realname 
 * @property string $id_card 
 * @property int $auth_status 
 * @property string $nickname 
 * @property string $avatar 
 * @property string $province 
 * @property string $city 
 * @property string $area 
 * @property string $address 
 * @property float $credit1 
 * @property float $credit2 
 * @property float $credit3 
 * @property float $credit4 
 * @property float $credit5 
 * @property float $credit6 
 * @property float $total_credit1 
 * @property float $total_credit2 
 * @property float $total_credit3 
 * @property float $total_credit4 
 * @property float $total_credit5 
 * @property float $total_credit6 
 * @property string $shop_name 
 * @property string $withdraw_url 
 * @property string $special_id 
 * @property string $relation_id 
 * @property string $tao_user_id 
 * @property string $pocket_url 
 * @property string $remark 
 * @property int $status 
 * @property int $version 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class User extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'old_id', 'name', 'email', 'password', 'pay_password', 'coin_password', 'pid', 'level', 'gender', 'public_openid', 'mini_openid', 'unionid', 'mobile', 'realname', 'id_card', 'auth_status', 'nickname', 'avatar', 'province', 'city', 'area', 'address', 'credit1', 'credit2', 'credit3', 'credit4', 'credit5', 'credit6', 'total_credit1', 'total_credit2', 'total_credit3', 'total_credit4', 'total_credit5', 'total_credit6', 'shop_name', 'withdraw_url', 'special_id', 'relation_id', 'tao_user_id', 'pocket_url', 'remark', 'status', 'version', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'old_id' => 'integer', 'pid' => 'integer', 'level' => 'integer', 'gender' => 'integer', 'auth_status' => 'integer', 'credit1' => 'float', 'credit2' => 'float', 'credit3' => 'float', 'credit4' => 'float', 'credit5' => 'float', 'credit6' => 'float', 'total_credit1' => 'float', 'total_credit2' => 'float', 'total_credit3' => 'float', 'total_credit4' => 'float', 'total_credit5' => 'float', 'total_credit6' => 'float', 'status' => 'integer', 'version' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
    public function ThirdUser()
    {
        return $this->hasMany(ThirdUser::class, 'user_id', 'id');
    }
}