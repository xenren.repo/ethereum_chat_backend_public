<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property int $user_id 
 * @property int $goods_id 
 * @property string $title 
 * @property int $option_id 
 * @property string $option_name 
 * @property int $shop_id 
 * @property string $images 
 * @property int $num 
 * @property float $price 
 * @property float $total 
 * @property int $sort 
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class UserCart extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_cart';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'goods_id', 'title', 'option_id', 'option_name', 'shop_id', 'images', 'num', 'price', 'total', 'sort', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'user_id' => 'integer', 'goods_id' => 'integer', 'option_id' => 'integer', 'shop_id' => 'integer', 'num' => 'integer', 'price' => 'float', 'total' => 'float', 'sort' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}