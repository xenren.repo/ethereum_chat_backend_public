<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id
 * @property string $uuid
 * @property string $type
 * @property int $user_id
 * @property string $deleted_at 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class UserImFavorite extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_im_favorite';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'uuid', 'type', 'user_id', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'uuid' => 'string', 'type' => 'string', 'user_id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}