<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id
 * @property string $body
 * @property string $type
 * @property int $user_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class UserImFavoriteRaw extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_im_favorite_raw';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'body', 'type', 'user_id', 'deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'body' => 'string', 'type' => 'string', 'user_id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}