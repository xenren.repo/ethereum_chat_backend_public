<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property int $user_id 
 * @property float $amount 
 * @property int $approved_by 
 * @property int $status 
 * @property string $invoice 
 * @property string $img_url 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class Withdraw extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'withdraw';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    const type_deposit_refund = 1;

    protected $fillable = [
        'id',
        'user_id',
        'order_id',
        'amount',
        'approved_by',
        'status',
        'invoice',
        'create_at',
        'updated_at',
        'img_url',
        'bank_name',
        'bank_account_no',
        'bank_no',
        'type',
        'bank_holder'
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id'=>'integer', 'user_id'=>'integer', 'amount'=>'float', 'approved_by'=>'integer', 'status'=>'integer', 'invoice'=>'string', 'create_at'=>'datetime', 'updated_at'=>'integer', 'img_url'=>'string'];
}