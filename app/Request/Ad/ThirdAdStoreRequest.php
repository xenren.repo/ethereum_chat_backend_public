<?php

declare(strict_types=1);

namespace App\Request\Ad;

use Hyperf\Validation\Request\FormRequest;

class ThirdAdStoreRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'pid'          => 'required|integer',
			'name'          => 'required|max:100',
			'description'          => 'sometimes|max:1000',
			'img'          => 'required|array',
			'img.*'          => 'required|string|max:500|url',
			'url'          => 'string|max:500',
			'status'           => 'required|integer|in:0,1',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'url' => trans('params.url'),
		];
	}
}
