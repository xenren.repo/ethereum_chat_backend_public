<?php

declare(strict_types=1);

namespace App\Request\Admin;

use Hyperf\Validation\Request\FormRequest;

class AdminStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'name'          => 'required|max:100|unique:admin,name',
		    'password'      => 'required|min:6',
		    'mobile'        => 'sometimes|string|mobile',
		    'realname'      => 'sometimes|string|max:30',
		    'remark'        => 'sometimes|string|max:191',
		    'status'        => 'sometimes|integer|in:0,1',
	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'name' => trans('params.auth.name'),
			'password' =>  trans('params.password'),
		];
	}
}
