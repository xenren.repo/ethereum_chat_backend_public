<?php

declare(strict_types=1);

namespace App\Request\Admin;

use Hyperf\Validation\Request\FormRequest;

class SellerRejectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		   'seller_reject_reason' => 'required|array', 
		   'seller_reject_reason.*.seller_id' => 'required|integer', 
		   'seller_reject_reason.*.action' => 'required|integer|in:1,2,3,4', 
		   'seller_reject_reason.*.message' => 'required|string'
	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [

		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function messages(): array
	{
		return [
			'id.*' => '操作异常'
		];
	}
}
