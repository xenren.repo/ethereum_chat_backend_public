<?php

declare(strict_types=1);

namespace App\Request;

use Hyperf\Validation\Request\FormRequest;

class CommonIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'page' => 'required|integer|min:1|max:65565',
            'limit' => 'required|integer|min:1|max:200',
            'search' => 'sometimes|string|max:200',
            'level' => 'sometimes|integer',
            'filter' => 'sometimes|string',
            'type' => 'sometimes',
            'status' => 'sometimes|integer',
            'start_end' => 'sometimes|array',
            'start_end.*' => 'date',
            'admin_only' => 'sometimes',
            'seller_only' => 'sometimes',
            'searchSeller' => 'sometimes',
            'amount' => 'sometimes|array',
            'amount.from' => 'sometimes',
            'amount.to' => 'sometimes',
        ];
    }

    /**
     * 获取验证错误的自定义属性.
     */
    public function attributes(): array
    {
        return [
        ];
    }
}
