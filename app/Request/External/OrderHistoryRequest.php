<?php

declare(strict_types=1);

namespace App\Request\External;

use App\Request\AbstractRequest;

class OrderHistoryRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'exists:users,name'],
            'password' => ['required', 'string'],

            'filter' => ['required', 'array'],
            'filter.page' => ['required', 'integer', 'min:1'],
            'filter.limit' => ['required', 'integer', 'min:1'],
            'filter.search' => ['sometimes', 'string', 'max:200'],
            'filter.refunding' => ['sometimes', 'integer'],
            'filter.status' => ['sometimes', 'integer'],
            'filter.type' => ['sometimes', 'integer'],
            'filter.start_end' => ['sometimes', 'array'],
            'filter.start_end.*' => ['sometimes', 'date'],
            'filter.p' => ['sometimes'],
        ];
    }
}
