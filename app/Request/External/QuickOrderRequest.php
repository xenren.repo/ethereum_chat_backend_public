<?php

declare(strict_types=1);

namespace App\Request\External;

use App\Request\AbstractRequest;
use Hyperf\Validation\Rule;

class QuickOrderRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'exists:users,name'],
            'password' => ['required', 'string'],

            'goods' => ['required', 'array'],
            'goods.*.id' => ['required', 'numeric', 'exists:goods,id'],
            'goods.*.qty' => ['required', 'integer', 'min:1'],

            'pay_type' => ['sometimes', 'in:alipay-later,alipay-qr'],
            'address' => ['sometimes', 'array'],

            'address.receiver' => $this->validateAddress([
                'string',
                'max:191',
            ]),
            'address.receiver_mobile' => $this->validateAddress([
                'string',
                'numeric',
            ]),
            'address.province' => $this->validateAddress([
                'digits:6',
            ]),
            'address.city' => $this->validateAddress([
                'digits:6',
            ]),
            'address.area' => $this->validateAddress([
                'digits:6',
            ]),
            'address.address' => $this->validateAddress([
                'string',
            ]),
        ];
    }

    public function validateAddress(array $rules = ['string'])
    {
        $address = $this->input('address');
        if (is_null($address)) {
            return 'nullable';
        }
        $required = Rule::requiredIf(function () use ($address) {
            return is_array($address);
        });
        $rules[] = $required;
        return $rules;
    }
}
