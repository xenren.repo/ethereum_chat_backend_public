<?php

declare(strict_types=1);

namespace App\Request\Finance;

use Hyperf\Validation\Request\FormRequest;

class ApproveWithDrawRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'id' => 'required|integer',
			'img_url'=> 'required|string',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'cny_amount' => trans('params.recharge.cny_amount'),
			'credit' => trans('params.recharge.credit'),
			'remark' => trans('params.recharge.remark'),
			'sender' => trans('params.recharge.sender'),
		];
	}
}
