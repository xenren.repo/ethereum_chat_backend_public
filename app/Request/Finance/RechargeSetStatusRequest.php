<?php

declare(strict_types=1);

namespace App\Request\Finance;

use Hyperf\Validation\Request\FormRequest;

class RechargeSetStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'id' => 'required|numeric',
			'status' => 'required|in:-1,0,1,2',
			'remark' => 'string|max:191',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [

		];
	}
}
