<?php

declare(strict_types=1);

namespace App\Request\Finance;

use Hyperf\Validation\Request\FormRequest;

class RechargeStoreRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'amount' => 'required|numeric|min:0.01|max:999999',
			'cny_amount' => 'required|numeric|min:0.01|max:999999',
			'bank' => 'required|string|max:32',
			'bank_no' => 'required|string|max:50',
			'bank_holder' => 'required|string|max:32',
			'bank_mobile' => 'required|string|max:20',
			'created_place' => 'required|string|max:191',
			'credit' => 'required|array',
			'credit.*' => 'required|url',
			'sender' => 'required|string|max:200',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'cny_amount' => trans('params.recharge.cny_amount'),
			'credit' => trans('params.recharge.credit'),
			'remark' => trans('params.recharge.remark'),
			'sender' => trans('params.recharge.sender'),
		];
	}
}
