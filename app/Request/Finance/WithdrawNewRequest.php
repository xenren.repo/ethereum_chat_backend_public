<?php

declare(strict_types=1);

namespace App\Request\Finance;

use Hyperf\Validation\Request\FormRequest;

class WithdrawNewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'amount' => 'required|integer',
            'bank_name' => 'required',
            'bank_no' => 'required',
            'bank_account_no' => 'required',
            'bank_holder' => 'required',
        ];
    }

    /**
     * 获取验证错误的自定义属性.
     */
    public function attributes(): array
    {
        return [
        ];
    }
}
