<?php

declare(strict_types=1);

namespace App\Request\Goods;

use Hyperf\Validation\Request\FormRequest;

class CommentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'order_goods_id' => 'required|integer',
			'order_id' => 'required|integer',
			'score' => 'required|numeric|min:0|max:5',
			'content' => 'required|string|max:255',
			'type' => 'sometimes|integer|in:0,1,2',
			'images' => 'sometimes|array',
			'images.*' => 'required|url',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [

		];
	}
}
