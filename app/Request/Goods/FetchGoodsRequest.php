<?php

declare(strict_types=1);

namespace App\Request\Goods;

use Hyperf\Validation\Request\FormRequest;

class FetchGoodsRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'sort'              => 'required|integer|min:-30000|max:30000',
			'on_sale'         => 'required|integer|in:0,1',
			'is_recommend'    => 'required|integer|in:0,1',
			'is_hot'          => 'required|integer|in:0,1',
			'category'          => 'required|array',
			'category.*'          => 'required|integer',
			'url'          => 'required|array',
			'url.*'          => 'required|url',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'on_sale'         => trans('params.goods.on_sale'),
			'is_commission' => trans('params.goods.is_commission'),
			'type'            => trans('params.goods.type'),
			'is_recommend'    => trans('params.goods.is_recommend'),
			'is_hot'          => trans('params.goods.is_hot'),
			'category'          => trans('params.goods.category'),
		];
	}
}
