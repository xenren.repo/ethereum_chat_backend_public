<?php

declare(strict_types=1);

namespace App\Request\Goods;

use Hyperf\Validation\Request\FormRequest;

class GoodsCategoryStoreRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'name'          => 'required|max:100',
			'image_url'     => 'sometimes|string|url',
			'pid'           => 'required|integer',
			'sort'          => 'required|integer|min:-30000|max:30000',
			'type'          => 'required|in:0,1',
			'status'        => 'required|in:0,1',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [

		];
	}
}
