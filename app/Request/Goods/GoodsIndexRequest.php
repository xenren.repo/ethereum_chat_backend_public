<?php

declare(strict_types=1);

namespace App\Request\Goods;

use Hyperf\Validation\Request\FormRequest;

class GoodsIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'page' => 'required|integer|min:1|max:65565',
            'limit' => 'required|integer|min:1|max:200',
            'search' => 'sometimes|string|max:200',
            'on_sale' => 'sometimes|integer',
            'is_recommend' => 'sometimes|integer',
            'is_hot' => 'sometimes|integer',
            'is_new' => 'sometimes|integer',
            'cid' => 'sometimes|integer',
            'ship_free' => 'sometimes|integer',
            'start_end' => 'sometimes|array',
            'start_end.*' => 'date',
            'filter_creator_by' => 'sometimes',
        ];
    }

    /**
     * 获取验证错误的自定义属性.
     */
    public function attributes(): array
    {
        return [
            'on_sale' => trans('params.goods.on_sale'),
            'is_recommend' => trans('params.goods.is_recommend'),
            'is_new' => trans('params.goods.is_new'),
            'is_hot' => trans('params.goods.is_hot'),
            'ship_free' => trans('params.goods.ship_free'),
        ];
    }
}
