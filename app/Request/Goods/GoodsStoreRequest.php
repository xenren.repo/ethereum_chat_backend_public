<?php

declare(strict_types=1);

namespace App\Request\Goods;

use Hyperf\Validation\Request\FormRequest;

class GoodsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'id' => 'sometimes|integer',
            'title' => 'required|string|max:191',
            'description' => 'sometimes|nullable|string|max:191',
            'images' => 'required|array',
            'images.*' => 'sometimes|string|max:500',
            'price' => 'required|numeric|min:0',
            'original_price' => 'sometimes|numeric|min:0',
            'item_endprice' => 'sometimes|numeric|min:0',
            'min_item_endprice' => 'sometimes|numeric|min:0',
            'express_cost' => 'sometimes|numeric|min:0',
            'cost_price' => 'sometimes|numeric|min:0',
            'content' => 'required|string|max:65000',
            'sale_num' => 'required|integer|min:0',
            'stock' => 'required|integer|min:0',
            'sort' => 'required|integer|min:-30000|max:30000',
            'source_from' => 'required|integer',
            'total_limit' => 'required|integer',
            'on_sale' => 'required|integer|in:0,1',
            'is_commission' => 'required|integer|in:0,1',
            'type' => 'required|integer|in:1,2',
            'is_recommend' => 'required|integer|in:0,1',
            'is_new' => 'required|integer|in:0,1',
            'is_hot' => 'required|integer|in:0,1',
            'ship_free' => 'required|integer|in:0,1',
            'receipt_required' => 'required|integer|in:0,1',
            'unit' => 'sometimes|nullable|string|max:10',
            'weight' => 'sometimes|nullable|numeric|min:0|max:1000000',
            'category' => 'required|array|min:1',
            'category.*' => 'required|integer',
            'options' => 'sometimes|array',
            'reward_mode' => 'sometimes',

            // 规格项
            'options.spec.*.title' => 'required|string|max:191',
            'options.spec.*.sort' => 'required|integer|min:-30000|max:30000',
            'options.spec.*.spec_item' => 'required|array',

            // 规格子项
            'options.spec.*.spec_item.*.title' => 'required|string|max:191',
            'options.spec.*.spec_item.*.sort' => 'required|integer|min:-30000|max:30000',

            // 规格
            'options.options.*.title' => 'required|string|max:191',
            'options.options.*.images' => 'sometimes|string|max:500',
            'options.options.*.price' => 'required|numeric|min:0|max:999999',
            'options.options.*.original_price' => 'required|numeric|min:0|max:999999',
            'options.options.*.cost_price' => 'required|numeric|min:0|max:999999',
            'options.options.*.stock' => 'required|numeric|min:0|max:999999',
            'options.options.*.sort' => 'required|integer|min:-30000|max:30000',
            'options.options.*.specs' => 'required|array',
            'options.options.*.specs.*.key' => 'required|string|max:100',
            'options.options.*.specs.*.name' => 'required|string|max:100',

            'params' => 'sometimes|array',

            'params.*.title' => 'required|string|max:50',
            'params.*.value' => 'required|string|max:50',
            'params.*.sort' => 'required|integer|min:-30000|max:30000',
        ];
    }


    /**
     * 获取验证错误的自定义属性
     */
    public function attributes(): array
    {
        return [
            'original_price' => trans('params.goods.original_price'),
            'express_cost' => trans('params.goods.express_cost'),
            'cost_price' => trans('params.goods.cost_price'),
            'content' => trans('params.goods.content'),
            'sale_num' => trans('params.goods.sale_num'),
            'stock' => trans('params.goods.stock'),
            'source_from' => trans('params.goods.source_from'),
            'on_sale' => trans('params.goods.on_sale'),
            'is_commission' => trans('params.goods.is_commission'),
            'type' => trans('params.goods.type'),
            'is_recommend' => trans('params.goods.is_recommend'),
            'is_new' => trans('params.goods.is_new'),
            'is_hot' => trans('params.goods.is_hot'),
            'ship_free' => trans('params.goods.ship_free'),
            'unit' => trans('params.goods.unit'),
            'weight' => trans('params.goods.weight'),
            'category' => trans('params.goods.category'),
            'options' => trans('params.goods.options'),
            'params' => trans('params.goods.params'),
            'item_endprice' => trans('params.goods.item_endprice'),
            'options.spec.*.title' => trans('params.title'),
            'options.spec.*.spec_item' => trans('params.goods.spec_item'),
            'options.spec.*.spec_item.*.title' => trans('params.title'),
            'options.spec.*.spec_item.*.sort' => trans('params.sort'),
        ];
    }
}
