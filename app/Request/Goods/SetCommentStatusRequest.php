<?php

declare(strict_types=1);

namespace App\Request\Goods;

use Hyperf\Validation\Request\FormRequest;

class SetCommentStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'id' => 'required',
			'status' => 'required|integer|in:0,1,2',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [

		];
	}
}
