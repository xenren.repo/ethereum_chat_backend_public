<?php

declare(strict_types=1);

namespace App\Request\Medium;

use Hyperf\Validation\Request\FormRequest;

class MediumStoreRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'id' => 'sometimes|numeric',
			'name' => 'required|max:191',
			'img' => 'required|array',
			'description' => 'required|max:1000',
			'content' => 'required',
			'type' => 'sometimes|numeric|in:1,2,3',
			'sort' => 'sometimes|numeric',
			'status' => 'required|numeric|in:0,1',
			'read_num' => 'sometimes|numeric|min:0',
			'up' => 'sometimes|numeric|min:0',
			'down' => 'sometimes|numeric|min:0',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [

		];
	}
}
