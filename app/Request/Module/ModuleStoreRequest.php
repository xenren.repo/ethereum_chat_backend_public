<?php

declare(strict_types=1);

namespace App\Request\Module;

use Hyperf\Validation\Request\FormRequest;

class ModuleStoreRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'name'          => 'required|max:100',
			'pid'           => 'required|integer',
			'sort'          => 'required|integer',
			'type'          => 'required|in:1,2',
			'client_type'   => 'required|in:0,1',
			'is_hidden'     => 'required|in:0,1',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'name' => trans('params.name'),
			'pid' =>  trans('params.module.pid'),
			'type' =>  trans('params.module.type'),
			'client_type' =>  trans('params.client_type'),
		];
	}
}
