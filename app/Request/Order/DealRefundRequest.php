<?php

declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class DealRefundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'refund_id'    => 'required|integer',
		    'refund_result'    => 'required|integer|in:1,2,3,4,5',
		    'receiver' => 'sometimes|string|max:50',
		    'receiver_mobile' => 'sometimes|string|mobile',
		    'province' => 'sometimes|size:6',
		    'city' => 'sometimes|size:6',
		    'area' => 'sometimes|size:6',

		    'address' => 'sometimes|string|max:191',

	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
		];
	}
}
