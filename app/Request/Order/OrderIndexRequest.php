<?php

declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class OrderIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'page' => 'required|integer|min:1|max:65565',
            'limit' => 'required|integer|min:1|max:200',
            'search' => 'sometimes|string|max:200',
            'refunding' => 'sometimes|integer',
            'status' => 'sometimes|integer',
            'type' => 'sometimes|integer',
            'start_end' => 'sometimes|array',
            'start_end.*' => 'date',
            'p' => 'sometimes',
            'filter_goods_owner' => 'sometimes',
        ];
    }

    /**
     * 获取验证错误的自定义属性.
     */
    public function attributes(): array
    {
        return [
        ];
    }
}
