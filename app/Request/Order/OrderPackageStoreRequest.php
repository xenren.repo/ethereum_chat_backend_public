<?php

declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class OrderPackageStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'id' => 'required|integer|min:0',
		    'package_type' => 'required|integer|in:0,1',
		    'express_id' => 'required|string|max:20',
		    'express_sn' => 'required|string|max:255',
		    'custom_code' => 'sometimes|string|max:50',
		    'order_goods_id' => 'sometimes|array',
		    'order_goods_id.*' => 'required|integer',
	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'custom_code' => trans('params.order_package.custom_code'),
		];
	}
}
