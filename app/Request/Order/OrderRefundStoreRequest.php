<?php

declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class OrderRefundStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'id'              => 'required|integer|min:0',
		    'refund_credit'   => 'sometimes|array',
		    'refund_credit.*'   => 'required|url',
		    'reason'          => 'sometimes|string|max:191',
		    'refund_amount'   => 'required|numeric|min:0',
		    'apply_type'      => 'required|integer|in:1,2,3',
	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'reason' => trans('params.order_refund.reason'),
			'apply_type' => trans('params.order_refund.apply_type'),
			'refund_credit' => trans('params.order_refund.refund_credit'),
		];
	}
}
