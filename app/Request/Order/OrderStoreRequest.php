<?php

declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class OrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'goods' => 'required|array',
            'user_info' => 'required|array',
            'receipt' => 'sometimes|array',
            'express_amount' => 'required|numeric|min:0',
            'goods_amount' => 'required|numeric|min:0',
            'total_amount' => 'required|numeric|min:0',

            'user_info.receiver' => 'required|string|max:50',
            'user_info.receiver_mobile' => 'required|string|max:30',
            'user_info.province' => 'required|string|max:6',
            'user_info.city' => 'required|string|max:6',
            'user_info.area' => 'required|string|max:6',

            'user_info.address' => 'required|string|max:191',

            'goods.*.goods_id' => 'required|integer|min:0',
            'goods.*.option_id' => 'sometimes|integer|min:0',
            'goods.*.cart_id' => 'sometimes|integer|min:0',
            'goods.*.num' => 'required|integer|min:0|max:100000',
            'goods.*.price' => 'required|numeric|min:0|max:100000',
            'goods.*.total' => 'required|numeric|min:0|max:100000',
        ];
    }

    /**
     * 获取验证错误的自定义属性.
     */
    public function attributes(): array
    {
        return [
            'goods' => trans('params.order.goods'),
            'user_info' => trans('params.order.user_info'),
            'express_amount' => trans('params.order.express_amount'),
            'goods_amount' => trans('params.order.goods_amount'),
            'total_amount' => trans('params.order.total_amount'),
        ];
    }
}
