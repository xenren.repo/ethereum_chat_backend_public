<?php

declare(strict_types=1);

namespace App\Request\Order;

use App\Traits\Payable;
use Hyperf\Validation\Request\FormRequest;

class PayOrderRequest extends FormRequest
{
    use Payable;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'order_id' => 'required|integer|min:0',
            'amount' => 'required|numeric|min:0|max:100000',
            'pay_type' => 'required|string|in:' . $this->getPaymentTypes(),
            'reward_amount' => 'sometimes|integer',
            'reward' => 'sometimes|string',
        ];
    }

    /**
     * 获取验证错误的自定义属性.
     */
    public function attributes(): array
    {
        return [
        ];
    }

    public function getPaymentTypes()
    {
        $lists = (array) $this->baseAllowedLists();
        return implode(',', array_map(function ($item) {
            return $item['name'];
        }, $lists));
    }
}
