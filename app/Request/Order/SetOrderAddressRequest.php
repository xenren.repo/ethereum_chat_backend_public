<?php

declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class SetOrderAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'id' => 'required|integer',
		    'receiver' => 'required|string|max:50',
		    'receiver_mobile' => 'required|string|mobile',
		    'province' => 'required|string|size:6',
		    'city' => 'required|string|size:6',
		    'area' => 'required|string|size:6',

		    'address' => 'required|max:191',

	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
		];
	}
}
