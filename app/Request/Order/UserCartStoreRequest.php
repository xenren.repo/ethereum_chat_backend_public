<?php

declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class UserCartStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'goods_id' => 'required|integer|min:0',
		    'num' => 'required|integer|min:0|max:10000',
		    'option_id' => 'sometimes|integer|min:0',
	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'option_id' => trans('params.user_cart.option_id'),
			'goods_id' => trans('params.goods_id'),
		];
	}
}
