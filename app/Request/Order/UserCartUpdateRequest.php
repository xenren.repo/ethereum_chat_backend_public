<?php

declare(strict_types=1);

namespace App\Request\Order;

use Hyperf\Validation\Request\FormRequest;

class UserCartUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'id' => 'required|integer|min:0',
		    'num' => 'required|integer|min:-10000|max:10000',
	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [

		];
	}
}
