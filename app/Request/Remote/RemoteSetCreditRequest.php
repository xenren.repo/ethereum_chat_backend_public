<?php

declare(strict_types=1);

namespace App\Request\Remote;

use Hyperf\Validation\Request\FormRequest;

class RemoteSetCreditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'name'          => 'required|string|max:50',
		    'amount'      => 'required|numeric|min:-999999|max:999999',
		    'record_no'      => 'required|string|max:50',
		    'remark'      => 'sometimes|max:191',
		    'type'      => 'sometimes|in:1,2,3,4',
	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [

		];
	}
}
