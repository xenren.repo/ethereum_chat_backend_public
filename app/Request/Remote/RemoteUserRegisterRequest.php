<?php

declare(strict_types=1);

namespace App\Request\Remote;

use Hyperf\Validation\Request\FormRequest;

class RemoteUserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'username'          => 'required|string|max:50',
		    'introducer'      => 'sometimes|max:50',
		    'password'      => 'sometimes|string|max:255',
		    'mobile'      => 'sometimes',
		    'accid'      => 'required',
		    'token'      => 'required',
	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [

		];
	}
}
