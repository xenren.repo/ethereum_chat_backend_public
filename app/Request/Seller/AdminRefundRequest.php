<?php

declare(strict_types=1);

namespace App\Request\Seller;

use Hyperf\Validation\Request\FormRequest;

class AdminRefundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|max:191',
            'amount' => 'required',
            'bank_name' => 'required',
            'bank_account_no' => 'required',
            'bank_account_holder_name' => 'required',
        ];
    }
}
