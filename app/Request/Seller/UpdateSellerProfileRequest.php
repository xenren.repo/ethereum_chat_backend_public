<?php

declare(strict_types=1);

namespace App\Request\Seller;

use Hyperf\Validation\Request\FormRequest;

class UpdateSellerProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
		return [
            'data' => 'required|array',
            'data.*.reject_id' => 'required|integer',
            'data.*.approval_photo' => 'sometimes',
            'data.*.seller_id' => 'required',
            'data.*.company_picture' => 'sometimes',
            'data.*.company_info' => 'sometimes',
            'data.*.company_no' => 'sometimes',

        ];
    }
}
