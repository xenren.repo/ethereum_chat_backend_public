<?php

declare(strict_types=1);

namespace App\Request\Seller;

use Hyperf\Validation\Request\FormRequest;

class UpdateSellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
		return [
            'seller_id' => 'required',
            'rank' => 'required',
            'company_info' => 'required',
            'company_no' => 'required',
        ];
    }
}
