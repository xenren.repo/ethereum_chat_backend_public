<?php

declare(strict_types=1);

namespace App\Request\System\CheatSheet;

use Hyperf\Validation\Request\FormRequest;

class SetSellerSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'rank' => ['required'],
            'column' => ['required'],
            'value' => ['sometimes'],
        ];
    }
}
