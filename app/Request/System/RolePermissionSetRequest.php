<?php

declare(strict_types=1);

namespace App\Request\System;

use Hyperf\Validation\Request\FormRequest;

class RolePermissionSetRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'permissions'            => 'array',
			'permissions.*'            => 'integer|distinct',
			'client_type'            => 'required|integer|in:0,1',
			'role_id'            => 'required|integer',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'permissions' => trans('params.permissions'),
		];
	}
}
