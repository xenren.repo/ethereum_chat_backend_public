<?php

declare(strict_types=1);

namespace App\Request\System;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class RoleStoreRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'name'          => [
				'required',
                'max:100',
				Rule::unique('roles')->where(function ($query) {
					$query->whereNull('deleted_at');
				})
			],
			'pid'           => 'required|integer',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'name' => trans('params.name'),
			'pid' =>  trans('params.role.pid'),
		];
	}
}
