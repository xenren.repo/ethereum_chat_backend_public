<?php

declare(strict_types=1);

namespace App\Request\User;

use Hyperf\Validation\Request\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'old_password'  => 'required|max:100',
			'password'      => 'required|min:6',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'old_password' => trans('params.old_password'),
			'password' =>  trans('params.password'),
		];
	}
}
