<?php

declare(strict_types=1);

namespace App\Request\User;

use Hyperf\Validation\Request\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'name'  => 'required|max:100',
			'password'      => 'required|min:6',
			'code'      => 'required|min:4|max:6',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'password' =>  trans('params.password'),
		];
	}
}
