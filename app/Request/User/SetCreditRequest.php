<?php

declare(strict_types=1);

namespace App\Request\User;

use Hyperf\Validation\Request\FormRequest;

class SetCreditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }


	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'account' => 'required|in:1,2',
			'id' => 'required|integer',
			'money' => 'required|numeric|min:-9999999|max:9999999',
			'change' => 'required|in:1,2',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'account' => trans('params.account'),
			'change' => trans('params.change'),
			'money' => trans('params.money'),
		];
	}
}
