<?php

declare(strict_types=1);

namespace App\Request\User;

use App\Traits\UploadFileRule;
use Hyperf\Validation\Request\FormRequest;

class UploadFileRequest extends FormRequest
{
    use UploadFileRule;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'files' => ['required','array'],
            'files.*' => [
                'required',
                'file',
                sprintf("mimes:%s",implode(',',$this->getAllowedMimes())),
                sprintf("max:%s",$this->getMaxUploadedSizeInKB())
            ]
        ];
    }

}
