<?php

declare(strict_types=1);

namespace App\Request\User;

use Hyperf\Validation\Request\FormRequest;

class UserAddressUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'id' => 'required|integer',
		    'name' => 'required|string|max:50',
		    'mobile' => 'required|string|mobile',
		    'province' => 'required|string|size:6',
		    'city' => 'required|string|size:6',
		    'area' => 'required|string|size:6',

		    'address' => 'required|max:191',

		    'tag' => 'sometimes|nullable|string|max:30',
		    'code' => 'sometimes|nullable|string|size:6',
		    'is_default' => 'required|integer|in:0,1',
	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'name' => trans('params.user_address.name'),
			'tag' => trans('params.user_address.tag'),
			'code' => trans('params.user_address.code'),
			'is_default' => trans('params.user_address.is_default'),
		];
	}
}
