<?php

declare(strict_types=1);

namespace App\Request\User;

use Hyperf\Validation\Request\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
	    return [
		    'name'          => 'required|max:50|unique:users,name',
		    'password'      => 'required|min:6',
		    'mobile'      => 'sometimes|string|mobile',
		    'code'      => 'required|min:4|max:6',
		    'agent'      => 'sometimes|string|max:50',
	    ];
    }



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
			'name' => trans('params.auth.name'),
			'password' =>  trans('params.password'),
		];
	}
}
