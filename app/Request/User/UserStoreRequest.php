<?php

declare(strict_types=1);

namespace App\Request\User;

use Hyperf\Validation\Request\FormRequest;

class UserStoreRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 */
	public function rules(): array
	{
		return [
			'name'     => 'required|string|max:32|unique:users,name',
			'password'          => 'required|string|max:6|max:32',
			'mobile'          => 'sometimes|string|mobile',
			'agent'          => 'sometimes|string|max:32',
			'realname'           => 'sometimes|max:32',
			'province'           => 'sometimes|string|size:6',
			'city'           => 'sometimes|string|size:6',
			'area'           => 'sometimes|string|size:6',
			'address'           => 'sometimes|string|max:255',
			'level'          => 'required|integer|min:0|max:100',
			'remark'           => 'sometimes|string|max:255',
			'status'   => 'sometimes|integer|in:0,1',
		];
	}



	/**
	 * 获取验证错误的自定义属性
	 */
	public function attributes(): array
	{
		return [
		];
	}
}
