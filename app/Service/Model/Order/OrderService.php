<?php

declare(strict_types=1);

namespace App\Service\Model\Order;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Auth\AuthManager;
use App\Logic\Order\OrderLogic;
use App\Logic\Order\PayLogic;
use App\Logic\User\UserAuthLogic;
use App\Model\Goods;
use Hyperf\Utils\Context;

class OrderService
{
    public function quickOrder($rawData, $user, $goods, $address, $payType = 'alipay-later')
    {
        self::authenticateUser($user, UserAuthLogic::AUTH_TYPE);
        $goods = self::fetchSelectedGoods($goods);
        $data = [
            'goods' => $goods['goods'],
            'goods_amount' => $goods['goods_amount'],
            'total_amount' => $goods['goods_amount'],
            'express_amount' => 0,
            'user_info' => $address,
        ];
        $order = make(OrderLogic::class)->store($data);
        $payData = [
            'amount' => $order['total_amount'],
            'order_id' => $order['id'],
            'order_no' => $order['order_no'],
            'pay_type' => $payType,
            'type' => $order['type'],
        ];
        $pay_result = make(PayLogic::class)->pay($payData);
        return [
            'order_id' => $order['id'],
            'order_no' => $order['order_no'],
            'link' => $pay_result,
        ];
    }

    public static function authenticateUser($user, $type): void
    {
        $token = make(AuthManager::class)->getToken($user['id'], $type);
        if (! $token && is_null($token['token'])) {
            throw new BusinessException(ErrorCode::AUTH_FAIL);
        }
        $data = make(AuthManager::class)->getJwt()->getParserData($token['token']);
        Context::set(AuthManager::CONTEXT_KEY, $data);
    }

    public static function fetchSelectedGoods($goods)
    {
        $selectedGoods = [];
        foreach ($goods as $item) {
            $selectedGoods[$item['id']] = $item;
        }
        $goodsData = Goods::query()->whereIn('id', array_keys($selectedGoods))->get()->toArray();
        $newGoods = [];
        $totalAmount = 0;
        foreach ($goodsData as $key => $item) {
            $itemId = $item['id'];
            $total = $selectedGoods[$itemId]['qty'] * $item['price'];
            $newGoods[] = [
                'goods_id' => $itemId,
                'option_id' => $selectedGoods[$itemId]['option_id'] ?? '',
                'cart_id' => $selectedGoods[$itemId]['cart_id'] ?? '',
                'num' => (int) $selectedGoods[$itemId]['qty'],
                'price' => $item['price'],
                'total' => $total,
            ];
            $totalAmount += $total;
        }
        return [
            'goods' => $newGoods,
            'goods_amount' => $totalAmount,
        ];
    }
}
