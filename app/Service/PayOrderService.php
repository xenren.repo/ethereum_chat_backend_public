<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\FrRequestStatus;
use App\Model\User;
use Exception;
use Hyperf\AsyncQueue\Annotation\AsyncQueueMessage;
use Hyperf\Di\Annotation\Inject;
use Psr\EventDispatcher\EventDispatcherInterface;
use App\Event\FrRequestStatusEvent;

class PayOrderService
{
	private $order;
	
	private $user;
	/**
	 * @AsyncQueueMessage
	 * @param $order
	 */
	public function notify($order)
	{
		// 需要异步执行的代码逻辑
		$url = env('REMOTE_SYSTEM_URL').'/api/user/spend-evoucher';
		$user = User::findFromCache($order['user_id']);
		if (empty($user)) {
			$message = '订单:'.$order['order_no'].'用户信息不存在,通知失败';
			 $this->writeLogsToDB([
			 	'uniqueid' => $order['order_no'],
			 	'request' => [
			 		'url' => $url
			 	],
			 	'original_object' => json_encode($order)
			 ],1,$message);
			logger()->error($message);
			return;
		}
		$data = [
			'id' => $order['user_id'],
			'username' => $user['name'],
			'amount'   => $order['total_amount'],
			'order_no' => $order['order_no']
		];

		$this->user = $user;
		$this->order = $order;
		try {
			$res = curl()->post($url, $data);
		} catch (Exception $e)
		{
			 $this->writeLogsToDB([
			 	'uniqueid' => $this->order['order_no'],
			 	'request' => [
			 		'url' => env('REMOTE_SYSTEM_URL').'/api/user/spend-evoucher',
			 		'data' => json_encode([
                        'id' => $this->order['user_id'],
                        'username' => $this->user['name'],
                        'amount'   => $this->order['total_amount'],
                        'order_no' => $this->order['order_no']
                    ])
			 	],
			 	'original_object' => json_encode($this->order),
			 	'error' => [
			 		'line' => $e->getLine(),
			 		'message' => $e->getMessage(),
			 	]
			 ],1,"Failed send request to remote host (Internal error before sent)");
			 logger()->error('Failed send data to remote host');
			 return;
		}

		if (isset($res))
        {
            if (!isset($res['code']) || $res['code'] != 0) {
                $message = '订单:'.$order['order_no'].'推送失败, detail:'.json_encode($res);
                $this->writeLogsToDB([
                    'uniqueid' => $this->order['order_no'],
                    'request' => [
                        'url' => env('REMOTE_SYSTEM_URL').'/api/user/spend-evoucher',
                        'data' => json_encode([
                            'id' => $this->order['user_id'],
                            'username' => $this->user['name'],
                            'amount'   => $this->order['total_amount'],
                            'order_no' => $this->order['order_no']
                        ])
                    ],
                    'original_object' => json_encode($this->order),
                    'response' => [
                        'data' => $res
                    ]
                ],1,$message);
                logger()->error($message);
                return;
            }
        }
		logger()->error('订单:'.$order['order_no'].'推送成功');
	}

	function writeLogsToDB($data,$requestType,$message,$status = 0)
    {
        FrRequestStatus::insert([
            'uniqueid'  => $data['uniqueid'],
            'type'      => $requestType,
            'message'   => $message,
            'status' => $status,
            'url'       => $data['request']['url'],
            'request_data' => $data['request']['data'],
            'original_object' => $data['original_object'],
            'error_line' => $data['error']['line']??0,
            'error_message' => $data['error']['message']??'',
            'error_raw' => $data['error']['raw']??'',
            'response_data' => $data['response']['data']??'',
        ]);
    }
}