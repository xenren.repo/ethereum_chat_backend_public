<?php

declare(strict_types=1);

namespace App\Task;

use App\Logic\Order\OrderLogic;
use App\Logic\Seller\RankSettingLogic;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="AutoFinishOrderBySellerRankTask", rule="* * * * *", callback="execute", memo="Auto finish order by seller rank", singleton=true)
 */
class AutoFinishOrderBySellerRankTask
{
    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function execute()
    {
        $out_logger = make(StdoutLoggerInterface::class);
        $out_logger->info('Start auto finish order by seller rank...');
        $rankLists = make(RankSettingLogic::class)->getSettings([1, 2, 3, 4], RankSettingLogic::KEY_AUTO_CONFIRM_ORDER);
        $count = make(OrderLogic::class)->autoFinishBySellerRank($rankLists);
        $out_logger->info("End 'auto finish order by seller rank' task with" . $count . 'order finished');
    }
}
