<?php
declare(strict_types=1);

namespace App\Task;

use App\Logic\Goods\GoodsLogic;
use App\Logic\Order\OrderLogic;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="AutoFinishOrder", rule="25 * * * *", callback="execute", memo="自动确认超时未确认订单",singleton=true)
 */
class AutoFinishOrderTask
{


	/**
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function execute()
	{
		$out_logger = make(StdoutLoggerInterface::class);
		$out_logger->info('执行自动关闭超时未支付订单...');
		$count = make(OrderLogic::class)->autoFinish();
		$out_logger->info('执行自动关闭超时未支付订单'.$count.'单');
	}
}