<?php

declare(strict_types=1);

namespace App\Task;

use App\Logic\Finance\Credit1Logic;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="AutoUnfreeze", rule="* * * * *", callback="execute", memo="自动提取卖家资金", singleton=true)
 */
class AutoUnfreezeTask
{
    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function execute()
    {
        $out_logger = make(StdoutLoggerInterface::class);
        $out_logger->info('自动提取卖家资金...');
        $count = make(Credit1Logic::class)->autoUnfreeze();
        // $count =1 ;
        $out_logger->info('自动提取卖家资金' . $count . '单');
    }
}
