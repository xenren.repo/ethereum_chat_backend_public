<?php
declare(strict_types=1);

namespace App\Task;

use App\Kernel\Common\Helper;
use App\Logic\Goods\GoodsLogic;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="AutoUpdateGoods", rule="*\/50 * * * *", callback="execute", memo="自动更新产品信息",singleton=true)
 */
class AutoUpdateGoodsTask
{


	/**
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function execute()
	{
		$out_logger = make(StdoutLoggerInterface::class);
		$out_logger->info('执行自动更新产品信息...');
		$limit = 0;
		$total_count = 0;

		if (Helper::isProd()) {
			$limit_num = 300;
		} else {
			$limit_num = 20;
		}

		while ($limit < $limit_num) {
			$count = make(GoodsLogic::class)->updateGoods();
			$limit++;
			$total_count += $count;
		}

		$out_logger->info('执行自动更新产品信息'.$total_count.'件');
	}
}