<?php


namespace App\Task;

use App\Model\UploadFileLog;
use Carbon\CarbonImmutable;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="DeleteUploadedFilesTask", rule="0 3 * * *", callback="execute", memo="Change to '0 3 * * * *' Run every morning at 3 AM application time",singleton=true)
 */
class DeleteUploadedFilesTask
{
    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function execute()
    {
        $out_logger = make(StdoutLoggerInterface::class);
        $now = CarbonImmutable::now();
        $findExpiredFiles = UploadFileLog::query()->where('must_deleted_at','<=',$now->toDateTimeString())->get();
        $ids = [];
        if ($findExpiredFiles && count($findExpiredFiles) > 0)
        {
            foreach ($findExpiredFiles->toArray() AS $key => $value)
            {
                $fullPath = BASE_PATH.'/'.$value['full_path'];
                if (!file_exists($fullPath))
                {
                    $ids[] = $value['id'];
                } else
                {
                    if (unlink($fullPath))
                    {
                        $ids[] = $value['id'];
                    }
                }
            }

            if (count($ids) > 0)
            {
                $batchDelete = UploadFileLog::whereIn('id',$ids)->delete();
                if (!$batchDelete)
                {
                    $out_logger->error("CRONTAB #1 [FAILED]: DELETE BATCH UPLOADED FILES");
                    $out_logger->error("LIST OF IDS");
                    $out_logger->error(json_encode($ids));
                } else
                {
                    // change this to info when up to prod server
                    $out_logger->info("CRONTAB #1 [SUCCESS]: DELETE BATCH UPLOADED FILES");
                    $out_logger->info("LIST OF IDS");
                    $out_logger->info(json_encode($ids));
                }
            }
        }

        $out_logger->info("CRONTAB #1 [INFO]: DELETE BATCH UPLOADED FILES");
        $out_logger->info("LIST OF IDS");
        $out_logger->info(json_encode($ids));
    }
}