<?php
declare(strict_types=1);

namespace App\Task;

use App\Logic\Goods\GoodsLogic;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="DisableGoods", rule="15 * * * *", callback="execute", memo="失效产品下架",singleton=true)
 */
class DisableGoodsTask
{


	/**
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function execute()
	{
		$out_logger = make(StdoutLoggerInterface::class);
		$out_logger->info('执行失效产品下架...');
		$count = make(GoodsLogic::class)->disableGoods();
		$out_logger->info('失效产品下架'.$count.'件');
	}
}