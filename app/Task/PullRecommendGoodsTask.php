<?php
declare(strict_types=1);

namespace App\Task;

use App\Logic\Goods\GoodsLogic;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="PullRecommendGoods", rule="0 3 * * *", callback="execute", memo="拉取每日推荐产品",singleton=true)
 */
class PullRecommendGoodsTask
{


	/**
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function execute()
	{
		$out_logger = make(StdoutLoggerInterface::class);
		$out_logger->info('拉取今日推荐产品...');
		$count = make(GoodsLogic::class)->pullRecommendGoods();
		$out_logger->info('拉取今日推荐产品'.$count.'件');
	}
}