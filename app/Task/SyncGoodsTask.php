<?php
declare(strict_types=1);

namespace App\Task;

use App\Logic\Goods\GoodsLogic;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="SyncGoods", rule="15 4 * * *", callback="execute", memo="拉取每日产品",singleton=true)
 */
class SyncGoodsTask
{


	/**
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function execute()
	{
		$out_logger = make(StdoutLoggerInterface::class);
		$out_logger->info('拉取每日产品...');
		$count = make(GoodsLogic::class)->syncGoods();
		$out_logger->info('拉取每日产品'.$count.'件');
	}
}