<?php

declare(strict_types=1);

namespace App\Task;

use App\Logic\Finance\Payment\WechatPayLogic;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Annotation\Crontab;

/**
 * @Crontab(name="WechatPayCheckStatusTask", rule="* * * * *", callback="execute", memo="Check wechatpay status", singleton=true)
 */
class WechatPayCheckStatusTask
{
    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function execute()
    {
        $out_logger = make(StdoutLoggerInterface::class);
        $out_logger->info('Call wechatpay status checker...');
        make(WechatPayLogic::class)->checkWechatPayStatus();
        $out_logger->info('End call wechatpay status checker...');
    }
}
