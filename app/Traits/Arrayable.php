<?php

declare(strict_types=1);

namespace App\Traits;

trait Arrayable
{
    public function isRequiredKeysExist(array $data, array $requiredKeys): bool
    {
        $keys = array_keys($data);
        if ($keys) {
            foreach ($keys as $key) {
                if (! in_array($key, $requiredKeys)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
