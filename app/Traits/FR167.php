<?php


namespace App\Traits;


use App\Kernel\Common\Helper;

trait FR167
{
    private $serverToken;
    private $url;

    /**
     * @return mixed
     */
    public function getServerToken()
    {
        if (Helper::isProd()) {
            $this->url = env('REMOTE_SYSTEM_URL','https://fr167.com');
            $this->serverToken = env('REMOTE_SYSTEM_SERVER_TOKEN','Dhjj5-7042m@N52190Hctas#');
        } else
        {
            $this->serverToken = env('REMOTE_SYSTEM_SERVER_TOKEN','Dhjj5-7042m@N52190Hctas#');
            $this->url = env('REMOTE_SYSTEM_URL','https://test.fr167.com');
        }
        return $this->serverToken;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        if (Helper::isProd()) {
            $this->url = env('REMOTE_SYSTEM_URL','https://fr167.com');
        } else
        {
            $this->url = env('REMOTE_SYSTEM_URL','https://test.fr167.com');
        }
        return $this->url;
    }
}