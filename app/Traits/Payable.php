<?php

declare(strict_types=1);

namespace App\Traits;

use App\Kernel\Common\Common;

trait Payable
{
    protected $allowedLists;

    protected $totalCCoinRequired = 0;

    /**
     * Return all allowed payment lists.
     *
     * @return array
     */
    public function baseAllowedLists()
    {
        $this->allowedLists = [
            [
                'status' => Common::getSetting('pay.alipay', 0),
                'logo' => '',
                'name' => 'alipay',
                'type' => 0,
            ],
            [
                'status' => Common::getSetting('pay.wechat', 0),
                'logo' => '',
                'name' => 'wechat',
                'type' => 0,
            ],
            [
                'status' => 1,
                'logo' => '',
                'name' => 'balance',
                'type' => 0,
            ],
            [
                'status' => 1,
                'logo' => '',
                'name' => 'credit3',
                'type' => 0,
            ],
            [
                'status' => 1,
                'logo' => '',
                'name' => 'reward_mode',
                'type' => 0,
            ],
            [
                'type' => 1,
                'name' => 'total_c_coin_required',
                'value' => $this->totalCCoinRequired,
            ],
            [
                'status' => Common::getSetting('pay.alipay', 0),
                'logo' => '',
                'name' => 'alipay-later',
            ],
            [
                'status' => Common::getSetting('pay.alipay', 0),
                'logo' => '',
                'name' => 'alipay-qr',
            ],
            [
                'status' => 1,
                'logo' => '',
                'name' => 'mudan',
            ],
        ];
        return $this->allowedLists;
    }

    /**
     * filter allowed lists.
     *
     * @param mixed $key
     * @return array
     */
    public function getAllowedListOnly(array $data, $key = 'name')
    {
        $lists = $this->baseAllowedLists();
        $filteredLists = [];
        foreach ($lists as $value) {
            if (isset($value[$key]) && in_array($value[$key], $data)) {
                $filteredLists[] = $value;
            }
        }
        return $filteredLists;
    }

    /**
     * filter allowed lists [except].
     *
     * @param mixed $keyName
     * @return array
     */
    public function getAllowedListExcept(array $data, $keyName = 'name')
    {
        $lists = $this->baseAllowedLists();
        foreach ($lists as $key => $value) {
            if (isset($value[$keyName]) && in_array($value[$keyName], $data)) {
                unset($lists[$key]);
            }
        }
        return array_values($lists);
    }
}
