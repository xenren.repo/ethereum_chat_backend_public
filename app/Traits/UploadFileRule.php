<?php


namespace App\Traits;


trait UploadFileRule
{
    private $mimesOptions;
    private $maxUploadedSize = 20; // In MB
    private $maxUploadedSizeType = 'MB';


    public function getMimesOptions()
    {
        $this->mimesOptions = [
            'images' => ['jpg','jpeg','png','gif'],
            'texts' => ['doc','docx','txt','pdf'],
            'sheets' => ['csv','xls','xlsx'],
            'presentations' => ['pptx','ppt']
        ];
        return $this->mimesOptions;
    }
    public function getAllowedMimes()
    {
        $mimes = array_values($this->getMimesOptions());
        $tmp = [];
        foreach ($mimes AS $val)
        {
            $tmp = array_merge($tmp,$val);
        }
        return $tmp;
    }

    /**
     * @return int
     */
    public function getMaxUploadedSize(): int
    {
        return $this->maxUploadedSize;
    }

    /**
     * @return string
     */
    public function getMaxUploadedSizeType(): string
    {
        return $this->maxUploadedSizeType;
    }
    /**
     * @return int
     */
    public function getMaxUploadedSizeInKB(): int
    {
        return $this->maxUploadedSize*1024;
    }

}