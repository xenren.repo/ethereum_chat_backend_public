<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

return [
    'default' => [
        'handlers' => [
	        [
		        // 'class' => Monolog\Handler\StreamHandler::class,
		        'class' => Monolog\Handler\RotatingFileHandler::class,
		        'constructor' => [
			        'filename' => BASE_PATH . '/runtime/logs/error.log',
			        'level' => Monolog\Logger::ERROR,
		        ],
		        'formatter' => [
			        'class' => Monolog\Formatter\LineFormatter::class,
			        'constructor' => [
				        'format' => null,
				        'dateFormat' => null,
				        'allowInlineLineBreaks' => true,
			        ],
		        ],
	        ],
	        [
		        'class' => Monolog\Handler\RotatingFileHandler::class,
		        'constructor' => [
			        'filename' => BASE_PATH . '/runtime/logs/info.log',
			        'level' => Monolog\Logger::INFO,
		        ],
		        'formatter' => [
			        'class' => Monolog\Formatter\LineFormatter::class,
			        'constructor' => [
				        'format' => null,
				        'dateFormat' => null,
				        'allowInlineLineBreaks' => true,
			        ],
		        ],
	        ],
	        [
		        'class' => Monolog\Handler\RotatingFileHandler::class,
		        'constructor' => [
			        'filename' => BASE_PATH . '/runtime/logs/debug.log',
			        'level' => Monolog\Logger::DEBUG,
		        ],
		        'formatter' => [
			        'class' => Monolog\Formatter\LineFormatter::class,
			        'constructor' => [
				        'format' => null,
				        'dateFormat' => null,
				        'allowInlineLineBreaks' => true,
			        ],
		        ],
	        ],
        ]
    ],
];
