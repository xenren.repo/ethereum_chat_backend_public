<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

return [
	// HTTP 请求的超时时间（秒）
	'timeout' => 5.0,

	// 默认发送配置
	'default' => [
		// 网关调用策略，默认：顺序调用
		'strategy' => \Overtrue\EasySms\Strategies\OrderStrategy::class,

		// 默认可用的发送网关
		'gateways' => [
			env('SMS_GATE_WAY')
		],
	],
	// 可用的网关配置
	'gateways' => [
		'errorlog' => [
			'file' => BASE_PATH.'/runtime/logs/easy-sms.log',
		],
		'aliyun' => [
			'access_key_id' => env('SMS_APP_ID'),
			'access_key_secret' => env('SMS_SECRET'),
			'sign_name' => env('SMS_SIGN_NAME'),
		],
		'wlwx' => [
			'access_key_id' => env('SMS_APP_ID'),
			'access_key_secret' => env('SMS_SECRET'),
			'sign_name' => env('SMS_SIGN_NAME'),
		],
		'chuanglan' => [
			'account' => env('SMS_APP_ID'),
			'password' => env('SMS_SECRET'),

			// 国际短信时必填
			'intel_account' => '',
			'intel_password' => '',

			// \Overtrue\EasySms\Gateways\ChuanglanGateway::CHANNEL_VALIDATE_CODE  => 验证码通道（默认）
			// \Overtrue\EasySms\Gateways\ChuanglanGateway::CHANNEL_PROMOTION_CODE => 会员营销通道
			'channel'  => \Overtrue\EasySms\Gateways\ChuanglanGateway::CHANNEL_VALIDATE_CODE,

			// 会员营销通道 特定参数。创蓝规定：api提交营销短信的时候，需要自己加短信的签名及退订信息
			'sign' => env('SMS_SIGN_NAME'),
			'unsubscribe' => '回TD退订',
		],
	],
];
