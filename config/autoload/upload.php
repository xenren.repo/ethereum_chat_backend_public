<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

return [
		'images' => [
			'ext' => array('gif', 'jpg', 'jpeg', 'png'),
			'limit' => 5000,  // 单位K
			'upload_type' => 1,  // 1:本地服务器  2:OSS
		],
		'file_store_type' => env('UPLOAD_STORE_TYPE','month')  // day 按天分目录   month 按月份目录  其他值不分目录
];
