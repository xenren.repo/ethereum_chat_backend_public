<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->string('name',100)->comment('名称');
	        $table->string('key',60)->unique()->comment('键');
	        $table->string('value',5000)->comment('值');
	        $table->tinyInteger('type')->default(0)->comment('类型 0:系统核心默认  1:应用信息配置');
	        $table->tinyInteger('is_hide')->default(0)->comment('是否隐藏 0:否  1:是');
	        $table->mediumInteger('sort')->default(0)->comment('排序 数字越大越靠前');
	        $table->string('validation',5000)->nullable()->comment('表单验证');
	        $table->string('remark',500)->nullable()->comment('备注');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('setting');
    }
}
