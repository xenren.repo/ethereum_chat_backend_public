<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('name',100)->unique()->comment('账号');
	        $table->string('email',100)->nullable()->unique()->comment('邮箱');
	        $table->string('password',255)->nullable()->comment('密码');
	        $table->string('pay_password',255)->nullable()->comment('支付密码');
	        $table->integer('pid')->default(0)->comment('推荐人id');
	        $table->integer('version')->default(0);
	        $table->tinyInteger('level')->default(0)->comment('会员等级');
	        $table->tinyInteger('gender')->default(0)->comment('性别 0:未知 1:男 2:女');
	        $table->string('mobile',20)->nullable()->index()->comment('手机号码');
	        $table->tinyInteger('status')->default(0)->comment('状态 0:正常 1:禁用');
	        $table->tinyInteger('auth_status')->default(0)->comment('实名认证状态 0:正常 1:禁用');
	        $table->string('nickname',100)->nullable()->index()->comment('昵称');
	        $table->string('realname',100)->nullable()->index()->comment('真实姓名');
	        $table->string('avatar',500)->nullable()->comment('头像');
	        $table->string('province',6)->nullable()->index()->comment('省');
	        $table->string('city',6)->nullable()->index()->comment('市');
	        $table->string('area',6)->nullable()->index()->comment('区');
	        $table->string('address',255)->nullable()->comment('详细地址');
	        $table->decimal('credit1',10,2)->default(0);
	        $table->decimal('credit2',10,2)->default(0);
	        $table->string('remark',500)->nullable()->comment('备注');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
}
