<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateThirdUserTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('third_user', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('app_id',32)->index()->comment('app_id');
	        $table->string('open_id',32)->index()->comment('open_id & union_id');
	        $table->tinyInteger('type')->index()->comment('类型 1:微信');
	        $table->tinyInteger('user_type')->index()->comment('用户类型 1:C端用户  2:B端用户');
	        $table->bigInteger('user_id')->unsigned()->index()->comment('用户id');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();

	        $table->unique(['open_id','app_id','user_type'], 'unique_open_app_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('third_user');
    }
}
