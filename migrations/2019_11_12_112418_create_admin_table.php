<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('admin', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('name',100)->unique()->comment('账号');
	        $table->string('email',100)->nullable()->unique()->comment('邮箱');
	        $table->string('password',255)->comment('密码');
	        $table->tinyInteger('gender')->default(0)->comment('性别 0:未知 1:男 2:女');
	        $table->string('mobile',20)->nullable()->index()->comment('手机号码');
	        $table->tinyInteger('status')->default(0)->comment('状态 0:正常 1:禁用');
	        $table->string('realname',100)->nullable()->index()->comment('真实姓名');
	        $table->string('remark',500)->nullable()->comment('备注');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('admin');
    }
}
