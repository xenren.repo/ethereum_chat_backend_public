<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateRoleCategoryTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('role_category', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('name',100)->comment('名称');
	        $table->string('description',255)->nullable()->comment('描述');
	        $table->bigInteger('pid')->default(0)->comment('父级id');
	        $table->mediumInteger('sort')->default(0)->index()->comment('排序');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('role_category');
    }
}
