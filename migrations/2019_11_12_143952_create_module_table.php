<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateModuleTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('module', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('name',50)->comment('名称');
	        $table->string('permission',50)->nullable()->comment('节点名称');
	        $table->string('icon',50)->nullable()->comment('菜单图标');
	        $table->string('description',255)->nullable()->comment('概述');
	        $table->string('path',255)->nullable()->comment('路由路径');
	        $table->string('rel',255)->nullable()->comment('路由文件');
	        $table->bigInteger('pid')->unsigned()->default(0)->comment('父级ID');
	        $table->mediumInteger('sort')->default(0)->index()->comment('排序');
	        $table->tinyInteger('type')->default(0)->comment('状态 1:菜单  2:操作权限');
	        $table->tinyInteger('client_type')->default(0)->comment('状态 0:PC端  1:APP');
	        $table->tinyInteger('is_hidden')->default(0)->comment('是否隐藏 0:正常  1:隐藏');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('module');
    }
}
