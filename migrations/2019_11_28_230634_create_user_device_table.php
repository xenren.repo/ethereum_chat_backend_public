<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateUserDeviceTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_device', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->tinyInteger('device_type')->comment('类型 1: 安卓 2:IOS');
	        $table->bigInteger('user_id')->unsigned()->index()->comment('用户id');
	        $table->string('device_no',50)->index()->comment('设备编号');
	        $table->timestamp('last_login')->index()->comment('最后登入时间');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();

	        $table->unique(['user_id','device_no','device_type'], 'unique_u_d_d');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_device');
    }
}
