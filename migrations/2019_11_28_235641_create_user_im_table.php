<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateUserImTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_im', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->bigInteger('user_id')->unsigned()->unique()->comment('用户id');
	        $table->string('accid',50)->index()->comment('im账号');
	        $table->string('token',50)->nullable()->comment('token');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_im');
    }
}
