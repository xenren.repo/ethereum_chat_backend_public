<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateDataDictionaryTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_dictionary', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('module',50)->index()->comment('模块');
	        $table->string('key_name',50)->index()->comment('键名');
	        $table->integer('key_id')->index()->comment('数据id');
	        $table->string('key_value',100)->nullable()->comment('数值');
	        $table->string('remark',255)->nullable()->comment('备注');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_dictionary');
    }
}
