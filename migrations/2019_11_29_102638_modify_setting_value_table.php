<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class ModifySettingValueTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('setting', function (Blueprint $table) {
            //
	        $table->longText('value')->nullable()->comment('值')->change();
	        $table->renameColumn('is_hide', 'is_hidden')->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('setting', function (Blueprint $table) {
            //
	        $table->string('value',5000)->comment('值')->change();
	        $table->renameColumn( 'is_hidden','is_hide')->change();
        });
    }
}
