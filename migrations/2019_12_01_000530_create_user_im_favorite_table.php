<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateUserImFavoriteTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_im_favorite', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('body',5000)->comment('消息信息');
	        $table->bigInteger('user_id')->index()->comment('用户id');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_im_favorite');
    }
}
