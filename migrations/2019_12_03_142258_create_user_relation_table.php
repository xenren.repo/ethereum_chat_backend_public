<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateUserRelationTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_relation', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->bigInteger('user_id')->unsigned()->index()->comment('用户id');
	        $table->bigInteger('pid')->unsigned()->index()->comment('父级');
	        $table->smallInteger('level')->index()->comment('层级');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_relation');
    }
}
