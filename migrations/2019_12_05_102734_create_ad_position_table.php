<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateAdPositionTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ad_position', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('name',191)->comment('广告位置');
	        $table->string('description',1000)->nullable()->comment('描述');
	        $table->tinyInteger('status')->comment('状态 0:启用  1: 禁用');
	        $table->tinyInteger('type')->default(0)->comment('广告类型 默认:0');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ad_position');
    }
}
