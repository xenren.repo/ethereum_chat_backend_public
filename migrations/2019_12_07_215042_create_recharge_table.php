<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateRechargeTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('recharge', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('recharge_no',32)->unique()->comment('流水号');
	        $table->integer('user_id')->index()->comment('用户ID');
	        $table->string('trans_id',32)->default('')->index()->comment('第三方平台流水号');
	        $table->integer('verify_code')->nullable()->unique()->comment('哈希值验证');
	        $table->string('bank',32)->default('')->comment('银行名称/支付宝/微信');
	        $table->string('bank_no',100)->default('')->comment('银行账号/支付宝/微信账号');
	        $table->string('bank_holder',32)->default('')->comment('持卡人/账号户名支付宝/微信');
	        $table->string('bank_mobile',32)->default('')->comment('预留手机号码');
	        $table->string('created_place',191)->default('')->comment('开户地址');
	        $table->string('sender',32)->default('')->comment('汇款人');
	        $table->string('credit',1000)->default('')->comment('充值凭证');
	        $table->integer('version')->default(0)->comment('乐观锁');
	        $table->tinyInteger('type')->index()->comment('充值类型');
	        $table->tinyInteger('status')->index()->comment('状态');
	        $table->decimal('amount',10,2)->comment('充值数目');
	        $table->decimal('cny_amount',10,2)->comment('折算金额');
	        $table->decimal('charge_fee',10,2)->comment('手续费');
	        $table->decimal('reality',10,2)->comment('实际到账');
	        $table->string('remark',500)->nullable()->comment('备注');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('recharge');
    }
}
