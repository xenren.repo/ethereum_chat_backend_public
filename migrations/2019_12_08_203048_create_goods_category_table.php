<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateGoodsCategoryTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('goods_category', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('name',191)->comment('名称');
	        $table->bigInteger('pid')->default(0)->index()->comment('父级ID');
	        $table->string('image_url',500)->nullable()->comment('图片');
	        $table->tinyInteger('status')->comment('状态 0:启用  1: 禁用');
	        $table->tinyInteger('type')->comment('类型 0:外部分类 1:自营品牌分类');
	        $table->smallInteger('sort')->index()->default(0)->comment('排序 从大到小排序');
	        $table->string('remark',1000)->nullable()->comment('备注');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('goods_category');
    }
}
