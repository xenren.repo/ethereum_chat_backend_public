<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateGoodsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('goods', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('title',191)->index()->comment('名称');
	        $table->string('description',191)->nullable()->comment('概述');
	        $table->string('images',1000)->nullable()->comment('图片JSON格式,首张图作为缩略图');
	        $table->text('content')->nullable()->comment('详情');
	        $table->decimal('price',10,2)->index()->comment('售价');
	        $table->decimal('original_price',10,2)->nullable()->comment('原价');
	        $table->decimal('cost_price',10,2)->nullable()->comment('成本');
	        $table->decimal('express_cost',10,2)->default(0.00)->comment('运费');
	        $table->integer('sale_num')->default(0)->comment('销量');
	        $table->integer('real_sale_num')->default(0)->comment('实际销量');
	        $table->integer('version')->default(0)->comment('乐观锁版本号');
	        $table->integer('stock')->default(0)->comment('库存');
	        $table->smallInteger('sort')->index()->default(0)->comment('排序 从大到小排序');
	        $table->tinyInteger('on_sale')->index()->comment('是否上架 0:否 1:是');
	        $table->tinyInteger('type')->index()->comment('产品类型  1:实体  2:虚拟 ');
	        $table->tinyInteger('is_recommend')->index()->comment('是否推荐  0: 否   1:是');
	        $table->tinyInteger('is_new')->index()->comment('是否新品  0: 否   1:是');
	        $table->tinyInteger('is_hot')->index()->comment('是否爆款 0否  1是');
	        $table->tinyInteger('ship_free')->index()->comment('是否包邮  0: 否   1:是');
	        $table->tinyInteger('is_commission')->default(0)->comment('是否参与分销');
	        $table->string('commission',500)->nullable()->comment('分销信息');
	        $table->tinyInteger('source_from')->default(0)->comment('来源 0:默认 1:淘宝');
	        $table->tinyInteger('detail_update')->default(0)->comment('更新状态表示');
	        $table->tinyInteger('stock_type')->default(0)->comment('减库存方式 0:拍下减库存 1:付款减库存 2:永不减库存');
	        $table->integer('weight')->nullable()->comment('重量(克)');
	        $table->string('unit')->nullable()->comment('单位');
	        $table->string('item_id',50)->nullable()->index()->comment('宝贝id');
	        $table->string('goods_sn',50)->nullable()->index()->comment('编码');
	        $table->string('item_url',500)->nullable()->comment('宝贝地址');
	        $table->string('cupon_url',500)->nullable()->comment('优惠券地址');
	        $table->decimal('item_endprice',10,2)->default(0)->comment('券后价格');
	        $table->decimal('coupon_money',10,2)->default(0)->comment('优惠券金额');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('goods');
    }
}
