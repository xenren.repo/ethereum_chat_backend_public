<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateGoodsOptionsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('goods_options', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('title',191)->comment('名称');
	        $table->bigInteger('goods_id')->index()->comment('产品id');
	        $table->string('images',500)->nullable()->comment('图片');
	        $table->decimal('price',10,2)->index()->comment('售价');
	        $table->decimal('original_price',10,2)->nullable()->comment('原价');
	        $table->decimal('cost_price',10,2)->nullable()->comment('成本');
	        $table->string('specs')->comment('规格组合');
	        $table->integer('version')->default(0)->comment('乐观锁版本号');
	        $table->integer('stock')->default(0)->comment('库存');
	        $table->smallInteger('sort')->index()->default(0)->comment('排序 从大到小排序');
	        $table->integer('sale_num')->default(0)->comment('销量');
	        $table->integer('real_sale_num')->default(0)->comment('实际销量');
	        $table->integer('weight')->nullable()->comment('重量(克)');
	        $table->string('unit')->nullable()->comment('单位');
	        $table->string('goods_sn',50)->nullable()->index()->comment('编码');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('goods_options');
    }
}
