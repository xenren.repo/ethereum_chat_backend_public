<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateGoodsParamsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('goods_params', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('title',191)->comment('名称');
	        $table->bigInteger('goods_id')->index()->comment('产品id');
	        $table->string('value')->nullable()->comment('编号');
	        $table->smallInteger('sort')->default(0)->comment('排序 从大到小排序');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('goods_params');
    }
}
