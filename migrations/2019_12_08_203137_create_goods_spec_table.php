<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateGoodsSpecTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('goods_spec', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('title',191)->comment('名称');
	        $table->bigInteger('goods_id')->index()->comment('产品id');
	        $table->string('img_url',500)->nullable()->comment('图片');
	        $table->string('value_id',32)->nullable()->comment('编号');
	        $table->smallInteger('sort')->default(0)->comment('排序 从大到小排序');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('goods_spec');
    }
}
