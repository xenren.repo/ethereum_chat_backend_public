<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateGoodsCommentsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('goods_comments', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->bigInteger('order_id')->index()->comment('订单ID');
	        $table->bigInteger('goods_id')->index()->comment('产品ID');
	        $table->bigInteger('order_goods_id')->index()->comment('订单产品ID');
	        $table->bigInteger('pid')->index()->comment('追评记录id');
	        $table->string('nickname',50)->comment('昵称');
	        $table->string('avatar',500)->nullable()->comment('图片');
	        $table->decimal('score',6,1)->comment('评分');
	        $table->string('content',1000)->nullable()->comment('评价内容');
	        $table->string('images',1000)->nullable()->comment('评价图片');
	        $table->smallInteger('sort')->default(0)->comment('排序 从大到小排序');
	        $table->tinyInteger('type')->default(0)->comment('评价类型 0:默认 1:回复 2:追加');
	        $table->tinyInteger('status')->default(0)->comment('状态 0:审核中 1:通过审核 2:未通过审核');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('goods_comments');
    }
}
