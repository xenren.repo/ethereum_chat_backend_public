<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('order_no',32)->unique()->comment('订单编号');
	        $table->string('trans_id',50)->nullable()->unique()->comment('支付流水号');
	        $table->tinyInteger('package_num')->unsigned()->default(0)->comment('包裹数量');
	        $table->bigInteger('user_id')->index();
	        $table->bigInteger('pid')->default(0)->index();
	        $table->decimal('total_amount',10,2)->comment('订单总额');
	        $table->decimal('original_total',10,2)->comment('原始订单总额');
	        $table->decimal('express_amount',10,2)->default(0.00)->comment('邮费');
	        $table->decimal('off_amount',10,2)->default(0.00)->comment('优惠总金额');
	        $table->decimal('goods_amount',10,2)->comment('产品金额');
	        $table->tinyInteger('pay_type')->default(0)->comment('支付方式 0:系统 1:微信 2:支付宝 3:余额');
	        $table->tinyInteger('type')->default(0)->comment('订单类型 0:综合订单  1:专区券订单');
	        $table->tinyInteger('status')->default(0)->comment('订单状态 orders.status');
	        $table->tinyInteger('is_comment')->default(0)->comment('是否已经评价 0"否  1:是');
	        $table->string('commission',500)->nullable()->comment('分销收益信息');
	        $table->decimal('total_commission',10,2)->default(0.00)->comment('产生分润总额');
	        $table->string('remark',255)->nullable()->comment('用户备注');
	        $table->string('platform_remark',255)->nullable()->comment('商家备注');
	        $table->timestamp('close_time')->nullable()->comment('关闭时间');
	        $table->timestamp('pay_time')->nullable()->comment('支付时间');
	        $table->timestamp('deliver_time')->nullable()->comment('发货时间');
	        $table->timestamp('finished_time')->nullable()->comment('订单结束时间');
	        $table->integer('version')->default(0)->comment('乐观锁版本号');
	        $table->string('receiver',50)->nullable()->index()->comment('收件人');
	        $table->string('receiver_mobile',30)->nullable()->index()->comment('收件人联系方式');
	        $table->string('province',6)->nullable()->comment('省');
	        $table->string('city',6)->nullable()->comment('市');
	        $table->string('area',6)->nullable()->comment('区');
	        $table->string('address',255)->nullable()->comment('详细地址');
	        $table->bigInteger('refund_id')->default(0)->index()->comment('退款id');
	        $table->decimal('refund_amount',10,2)->default(0)->comment('退款金额');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
}
