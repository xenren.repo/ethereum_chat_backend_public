<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateOrderGoodsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order_goods', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->bigInteger('order_id')->index();
	        $table->bigInteger('goods_id')->index();
	        $table->bigInteger('option_id')->default(0)->index()->comment('产品规格id');
	        $table->bigInteger('package_id')->default(0)->index()->comment('包裹id');
	        $table->string('title',191)->comment('产品名称');
	        $table->string('option_title',191)->nullable()->comment('规格名称');
	        $table->string('images',500)->nullable()->comment('产品图片');
	        $table->integer('num')->comment('产品数量');
	        $table->decimal('original_price',10,2)->comment('原价');
	        $table->decimal('cost_price',10,2)->comment('成本');
	        $table->decimal('total',10,2)->comment('总价');
	        $table->string('commission',500)->nullable()->comment('分销收益信息');
	        $table->decimal('total_commission',10,2)->default(0.00)->comment('产生分润总额');
	        $table->tinyInteger('source_from')->unsigned()->default(0)->comment('来源 0:默认');
	        $table->string('cupon_url',500)->nullable()->comment('优惠券url');
	        $table->string('item_url',500)->nullable()->comment('产品原url');
	        $table->decimal('off_amount',10,2)->default(0.00)->comment('优惠总金额');
	        $table->tinyInteger('type')->default(0)->comment('预留');
	        $table->tinyInteger('is_comment')->default(0)->comment('是否已经评价 0:否 1:是');
	        $table->decimal('weight',10,2)->default(0.00)->comment('重量(克)');
	        $table->string('unit',10)->nullable()->comment('单位');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order_goods');
    }
}
