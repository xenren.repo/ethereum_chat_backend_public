<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateOrderPackageTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order_package', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('package_no',32)->unique()->comment('包裹编号');
	        $table->bigInteger('order_id')->index();
	        $table->integer('goods_num')->comment('产品数量');
	        $table->string('receiver',50)->nullable()->index()->comment('收件人');
	        $table->string('receiver_mobile',30)->nullable()->index()->comment('收件人联系方式');
	        $table->string('province',6)->nullable()->comment('省');
	        $table->string('city',6)->nullable()->comment('市');
	        $table->string('area',6)->nullable()->comment('区');
            $table->string('address',255)->nullable()->comment('详细地址');
	        $table->string('code',6)->nullable()->comment('邮编');
	        $table->string('express',50)->comment('物流中文名称');
	        $table->string('express_id',50)->comment('物流ID编号');
	        $table->string('custom_code',50)->nullable()->comment('客户编码');
	        $table->string('express_sn',50)->index()->comment('物流单号');
	        $table->string('remark',191)->nullable()->comment('备注');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order_package');
    }
}
