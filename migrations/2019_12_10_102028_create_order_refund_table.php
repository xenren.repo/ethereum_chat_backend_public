<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateOrderRefundTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order_refund', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('refund_no',32)->unique()->comment('编号');
	        $table->bigInteger('user_id')->index();
	        $table->bigInteger('order_id')->index();
	        $table->decimal('refund_amount',10,2)->comment('退款金额');
	        $table->string('reason',191)->nullable()->comment('退款原因');
	        $table->string('refund_credit',2000)->nullable()->comment('退款凭证');
	        $table->tinyInteger('status')->default(0)->index()->comment('状态 0:处理中 1:已处理');
	        $table->string('receiver',50)->nullable()->index()->comment('收件人');
	        $table->string('receiver_mobile',30)->nullable()->index()->comment('收件人联系方式');
	        $table->string('province',6)->nullable()->comment('省');
	        $table->string('city',6)->nullable()->comment('市');
	        $table->string('area',6)->nullable()->comment('区');
	        $table->string('address',255)->nullable()->comment('详细地址');
	        $table->string('order_goods_ids',255)->comment('退款订单产品id, 多个用,隔开');
	        $table->tinyInteger('apply_type')->comment('申请维权方式 1:退款&退货 2:仅退款 3:换货');
	        $table->tinyInteger('refund_result')->comment('维权处理结果 1:驳回申请 2:通过申请 3:原路退款 4:手动退款');
	        $table->string('remark',191)->nullable()->comment('备注');
	        $table->integer('version')->default(0);
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order_refund');
    }
}
