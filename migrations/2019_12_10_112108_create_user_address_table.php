<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_address', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->bigInteger('user_id')->index();
	        $table->string('name',50)->comment('收件人');
	        $table->string('mobile',30)->comment('收件人联系方式');
	        $table->string('province',6)->comment('省');
	        $table->string('city',6)->comment('市');
	        $table->string('area',6)->comment('区');
	        $table->string('address',255)->comment('详细地址');
	        $table->string('code',6)->nullable()->comment('邮政编码');
	        $table->string('tag',30)->nullable()->comment('标签');
	        $table->tinyInteger('is_default')->default(0)->comment('是否默认选择 0:否  1:是');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_address');
    }
}
