<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateUserCartTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_cart', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->bigInteger('user_id')->index();
	        $table->bigInteger('goods_id')->index();
	        $table->string('title',191)->comment('名称');
	        $table->bigInteger('option_id')->default(0)->index()->comment('规格id');
	        $table->string('option_name',191)->default('')->comment('规格名称');
	        $table->bigInteger('shop_id')->default(0)->index();
	        $table->string('images',500)->nullable();
	        $table->integer('num')->comment('产品数量');
	        $table->decimal('price',10,2)->comment('单价');
	        $table->decimal('total',10,2)->comment('总价');
	        $table->smallInteger('sort')->index()->default(0)->comment('排序 从大到小排序');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();

	        $table->unique(['user_id','goods_id','option_id'], 'u_g_o');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_cart');
    }
}
