<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AddOrderGoodsPriceTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('order_goods', function (Blueprint $table) {
	        $table->decimal('price',10,2)->comment('单价')->after('num');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('order_goods', function (Blueprint $table) {
            //
	        $table->dropColumn('price');
        });
    }
}
