<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateCredit3Table extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('credit3', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->integer('user_id')->index()->comment('用户ID');
	        $table->integer('foreign_id')->default(0)->index()->comment('关联ID');
	        $table->tinyInteger('type')->index()->comment('类型');
	        $table->string('sub_type',32)->default('')->index()->comment('子类型');
	        $table->string('record_no',32)->unique()->comment('流水号');
	        $table->decimal('amount',10,2)->comment('实际数目');
	        $table->decimal('original_amount',10,2)->comment('原数目');
	        $table->decimal('fee',10,2)->default(0.00)->comment('手续费');
	        $table->decimal('balance',10,2)->comment('余额');
	        $table->dateTime('pay_time')->comment('入账时间');
	        $table->string('remark',500)->nullable()->comment('备注');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('credit3');
    }
}
