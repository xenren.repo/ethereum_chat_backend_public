<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class FixOrderColumn extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('orders', function (Blueprint $table) {
	        $table->renameColumn('disount_amount', 'off_amount')->change();
	        $table->renameColumn('total_commision', 'total_commission')->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('orders', function (Blueprint $table) {
	        $table->renameColumn('off_amount', 'disount_amount')->change();
	        $table->renameColumn('total_commission', 'total_commision')->change();
        });
    }
}
