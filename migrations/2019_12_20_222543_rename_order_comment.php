<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class RenameOrderComment extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
	    Schema::rename('order_comment', 'goods_comments');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
	    Schema::rename('goods_comments','order_comment');
    }
}
