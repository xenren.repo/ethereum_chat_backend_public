<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateLoginLogTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('login_log', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->bigInteger('user_id')->index();
	        $table->tinyInteger('type')->index()->comment('用户类型 1:管理员 2:用户');
	        $table->bigInteger('ip')->comment('ip');
	        $table->string('remark',500)->default('')->comment('登入其他信息');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('login_log');
    }
}
