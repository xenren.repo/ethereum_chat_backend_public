<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateUserSignTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_sign', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->bigInteger('user_id')->index();
	        $table->date('sign_date')->index();
	        $table->tinyInteger('sign_type')->index()->comment('签到类型 0:正常签到 1:补签');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();

	        $table->unique(['user_id','sign_date'], 'unique_user_date');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_sign');
    }
}
