<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateMediumTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('medium', function (Blueprint $table) {
	        $table->bigIncrements('id');
	        $table->string('name',191)->index()->comment('标题');
	        $table->string('author',191)->nullable()->comment('作者');
	        $table->string('description',1000)->nullable()->comment('概述');
	        $table->string('img',1000)->nullable()->comment('图片JSON格式,首张图作为缩略图');
	        $table->string('url',255)->nullable()->comment('');
	        $table->integer('pid')->default(0)->index()->comment('一级分类');
	        $table->integer('cid')->default(0)->index()->comment('二级分类');
	        $table->integer('up')->default(0)->comment('喜欢人数');
	        $table->integer('down')->default(0)->comment('不喜欢人数');
	        $table->integer('read_num')->default(0)->comment('阅读数');
	        $table->smallInteger('sort')->index()->default(0)->comment('排序 从大到小排序');
	        $table->text('content')->nullable()->comment('内容');
	        $table->tinyInteger('status')->default(0)->index()->comment('状态 0:正常 1:屏蔽');
	        $table->tinyInteger('type')->default(1)->index()->comment('类型 1:文章');
	        $table->timestamp('deleted_at')->nullable()->index();
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('medium');
    }
}