<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateFrRequestStatusTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fr_request_status', function (Blueprint $table) {
			$table->bigIncrements('id');
	        $table->string('uniqueid',191)->index();
	        $table->integer('type');
	        $table->string('message',191);
	        $table->integer('status');
	        $table->string('url',191)->nullable();
	        $table->text('request_data')->nullable();
	        $table->text('original_object')->nullable();
			$table->integer('error_line')->nullable()->default(0);
	        $table->string('error_message',191)->nullable();
	        $table->text('error_raw')->nullable();
	        $table->text('response_data')->nullable();
			
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('medium');
    }
}