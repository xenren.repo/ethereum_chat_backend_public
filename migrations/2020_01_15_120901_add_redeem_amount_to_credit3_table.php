<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AddRedeemAmountToCredit3Table extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('credit3', function (Blueprint $table) {
            $table->decimal('redeem_amount',20,6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('credit3', function (Blueprint $table) {
            $table->dropColumn(['redeem_amount']);
        });
    }
}
