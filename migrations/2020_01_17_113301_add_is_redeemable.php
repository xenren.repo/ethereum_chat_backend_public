<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AddIsRedeemable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('credit3', function (Blueprint $table) {
	        $table->boolean('is_redeemable')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('credit3', function (Blueprint $table) {
	        $table->dropColumn('is_redeemable');
        });
    }
}
