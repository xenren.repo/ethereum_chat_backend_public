<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AddUuidAccidTypeToUserImFavoriteTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('user_im_favorite', function (Blueprint $table) {
            $table->string('uuid')->comment('uuid from netease server');
            $table->string('type')->comment('text,video,audio,image,location,file');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('user_im_favorite', function (Blueprint $table) {
            $table->dropColumn(['uuid','type']);
        });
    }
}
