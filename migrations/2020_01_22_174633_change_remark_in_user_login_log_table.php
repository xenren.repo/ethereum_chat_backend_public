<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class ChangeRemarkInUserLoginLogTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('user_login_log', function (Blueprint $table) {
            $table->text('remark')->default('')->comment('Detail Data')->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('user_login_log', function (Blueprint $table) {
            $table->string('remark',500)->default('')->comment('Detail Data')->change();
        });
    }
}
