<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateUploadFileLogsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('upload_file_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->string('path');
            $table->string('file_name');
            $table->string('ext');
            $table->text('full_path')->nullable(); //is it neccesary?
            $table->string('original_name');
            $table->boolean('is_shareable')->default(true);
            $table->timestamp('must_deleted_at')->nullable()->index();
            $table->timestamp('created_at')->useCurrent()->index();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('upload_file_logs');
    }
}
