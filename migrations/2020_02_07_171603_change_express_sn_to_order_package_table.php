<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class ChangeExpressSnToOrderPackageTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('order_package', function (Blueprint $table) {
            $table->string('express_sn',255)->change()->comment('物流单号');
        });
        Schema::table('order_package', function (Blueprint $table) {
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('order_package');

            if (! $doctrineTable->hasIndex('order_package_express_sn_index')) {
                $table->index('express_sn', 'order_package_express_sn_index');
            }
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('order_package', function (Blueprint $table) {
            $table->string('express_sn',50)->change()->comment('物流单号');
        });
        Schema::table('order_package', function (Blueprint $table) {
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('order_package');

            if (! $doctrineTable->hasIndex('order_package_express_sn_index')) {
                $table->index('express_sn', 'order_package_express_sn_index');
            }
        });
    }
}
