<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class ChangeAddressToOrderRefundTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('order_refund', function (Blueprint $table) {
	        $table->text('address')->nullable()->change()->comment('详细地址');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('order_refund', function (Blueprint $table) {
	        $table->string('address',255)->nullable()->change()->comment('详细地址');
        });
    }
}
