<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class ChangeAddressToOrderPackageTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('order_package', function (Blueprint $table) {
	        $table->text('address')->nullable()->change()->comment('详细地址');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('order_package', function (Blueprint $table) {
	        $table->string('address',255)->nullable()->change()->comment('详细地址');
        });
    }
}
