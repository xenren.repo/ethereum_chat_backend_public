<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateSellerTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('seller', function (Blueprint $table) 
        {
            $table->bigIncrements('id');
            $table->unsignedInteger('admin_id')->nullable();
            $table->string('approval_photo')->nullable();
            $table->string('company_picture')->nullable();
            $table->string('company_info')->nullable();
            $table->string('company_no')->nullable();
            $table->boolean('new_user')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('seller');
    }
}
