<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateGoodsCommentRepliesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('goods_comment_replies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('goods_comment_id')->nullable();
            $table->unsignedBigInteger('pid')->default(0);
            $table->unsignedBigInteger('user_id')->nullable();
            $table->text('content');
	        $table->text('img')->nullable()->comment('Format: JSON stringify');
	        $table->timestamp('created_at')->useCurrent()->index();
	        $table->timestamp('updated_at')->useCurrent();

            $table->foreign('goods_comment_id')->references('id')->on('goods_comments');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('goods_comment_replies');
    }
}
