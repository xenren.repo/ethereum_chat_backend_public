<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateRejectSellerTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('seller_reject', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('seller_id'); 
            $table->integer('action'); 
            $table->string('message'); 
            $table->boolean('done')->default(false); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('seller_reject');
    }
}
