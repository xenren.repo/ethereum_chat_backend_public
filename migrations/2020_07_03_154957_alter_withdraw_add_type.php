<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AlterWithdrawAddType extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('withdraw', function (Blueprint $table) {
            $table->integer('type')->default(0)->comment('0=>  balance: the money he earn from selling, 1=> deposit: the money he deposit');
            $table->bigInteger('order_od')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('', function (Blueprint $table) {
            //
        });
    }
}
