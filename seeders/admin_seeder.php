<?php

declare(strict_types=1);

use Hyperf\Database\Seeders\Seeder;
use Hyperf\DbConnection\Db;

class AdminSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 * @throws Exception
	 */
    public function run()
    {
	    $admin_password = '111111';
	    $admin = [
		    'id'   => 1,
	    	'name' => 'admin',
		    'password' => password_hash($admin_password,PASSWORD_BCRYPT),
	    ];

	    try {
		    Db::table('admin')->insert([$admin]);
		    echo "-------------------------------\n";
		    echo "管理员账号初始化:\n";
		    echo "-------------------------------\n";
		    echo "账号1: {$admin['name']} \n";
		    echo "账号1密码: $admin_password \n";
		    echo "-------------------------------\n";
	    } catch (Exception $e) {
	    	throw $e;
	    }
    }
}
