<?php

declare(strict_types=1);

use Hyperf\Database\Seeders\Seeder;
use Hyperf\DbConnection\Db;

class credit1_seeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 * @throws Exception
	 */
    public function run()
    {
	    $admin_password = '111111';
	    $admin = [
		    'name'   => 1,
	    	'key' => 'credit1_enabled',
		    'value' => 1,
		    'remark' => '',
		    'type' => 1,
		    'is_hidden' => 1,
		    'sort' => 0,
	    ];

	    try {
		    Db::table('setting')->insert([$admin]);
		    echo "-------------------------------\n";
		    echo "管理员账号初始化:\n";
		    echo "-------------------------------\n";
		    echo "账号1: {$admin['name']} \n";
		    echo "账号1密码: $admin_password \n";
		    echo "-------------------------------\n";
	    } catch (Exception $e) {
	    	throw $e;
	    }
    }
}
