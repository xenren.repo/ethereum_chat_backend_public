<?php

declare(strict_types=1);

use App\Model\DataDictionary;
use Hyperf\Database\Seeders\Seeder;

class DictionarySeeder extends Seeder
{


	public function run()
	{
		DataDictionary::where('id','>',0)->delete();

		$this->users();

		$this->orders();

		$this->credit1();

		$this->credit2();

		$this->credit3();

		$this->operateLog();

		$this->goods();

		$this->other();

		$this->orderComment();

		$this->recharge();

		cache()->clearPrefix('dictionary');
	}


	public function goods()
	{
		$data_dictionary = [
			// 订单
			[
				'module' => 'goods',
				'key_name' => 'source_from',
				'key_id' => 0,
				'key_value' => '默认',
			],
			[
				'module' => 'goods',
				'key_name' => 'source_from',
				'key_id' => 1,
				'key_value' => '淘宝',
			],
			[
				'module' => 'goods',
				'key_name' => 'source_from',
				'key_id' => 2,
				'key_value' => '专区券产品',
			],

		];
		DataDictionary::insert($data_dictionary);
	}


	public function orders()
	{
		$data_dictionary = [
			// 订单
			[
				'module' => 'orders',
				'key_name' => 'status',
				'key_id' => 0,
				'key_value' => '待付款',
			],
			[
				'module' => 'orders',
				'key_name' => 'status',
				'key_id' => 1,
				'key_value' => '待发货',
			],
			[
				'module' => 'orders',
				'key_name' => 'status',
				'key_id' => 2,
				'key_value' => '待收货',
			],
			[
				'module' => 'orders',
				'key_name' => 'status',
				'key_id' => 3,
				'key_value' => '已完成',
			],
			[
				'module' => 'orders',
				'key_name' => 'status',
				'key_id' => -1,
				'key_value' => '已关闭',
			],

			// 支付方式
			[
				'module' => 'orders',
				'key_name' => 'pay_type',
				'key_id' => 1,
				'key_value' => '微信支付',
			],
			[
				'module' => 'orders',
				'key_name' => 'pay_type',
				'key_id' => 2,
				'key_value' => '支付宝支付',
			],
			[
				'module' => 'orders',
				'key_name' => 'pay_type',
				'key_id' => 3,
				'key_value' => '余额支付',
			],
			[
				'module' => 'orders',
				'key_name' => 'pay_type',
				'key_id' => 4,
				'key_value' => '专区券支付',
			],
			[
				'module' => 'orders',
				'key_name' => 'pay_type',
				'key_id' => 0,
				'key_value' => '系统支付',
			],

			// 订单类型
			[
				'module' => 'orders',
				'key_name' => 'type',
				'key_id' => 0,
				'key_value' => '综合订单',
			],
			[
				'module' => 'orders',
				'key_name' => 'type',
				'key_id' => 1,
				'key_value' => '专区券订单',
			],


			// 退款处理进度
			[
				'module' => 'refund',
				'key_name' => 'status',
				'key_id' => 0,
				'key_value' => '处理中',
			],
			[
				'module' => 'refund',
				'key_name' => 'status',
				'key_id' => 1,
				'key_value' => '已处理',
			],


			// 申请维权方式
			[
				'module' => 'refund',
				'key_name' => 'apply_type',
				'key_id' => 1,
				'key_value' => '退款&退货',
			],
			[
				'module' => 'refund',
				'key_name' => 'apply_type',
				'key_id' => 2,
				'key_value' => '仅退款',
			],
			[
				'module' => 'refund',
				'key_name' => 'apply_type',
				'key_id' => 3,
				'key_value' => '换货',
			],


			// 维权处理结果
			[
				'module' => 'refund',
				'key_name' => 'refund_result',
				'key_id' => 1,
				'key_value' => '驳回申请',
			],
			[
				'module' => 'refund',
				'key_name' => 'refund_result',
				'key_id' => 2,
				'key_value' => '通过申请(需客户寄回商品)',
			],
			[
				'module' => 'refund',
				'key_name' => 'refund_result',
				'key_id' => 3,
				'key_value' => '原路退款',
			],
			[
				'module' => 'refund',
				'key_name' => 'refund_result',
				'key_id' => 4,
				'key_value' => '手动退款',
			],
			[
				'module' => 'refund',
				'key_name' => 'refund_result',
				'key_id' => 5,
				'key_value' => '确认发货(确认收到退回商品或者直接重新发货)',
			],
		];
		DataDictionary::insert($data_dictionary);
	}


	public function credit1()
	{
		$data_dictionary = [
			[
				'module' => 'finance',
				'key_name' => 'credit1',
				'key_id' => 1,
				'key_value' => '购买商品',
			],
			[
				'module' => 'finance',
				'key_name' => 'credit1',
				'key_id' => 2,
				'key_value' => '退款',
			],
			[
				'module' => 'finance',
				'key_name' => 'credit1',
				'key_id' => 3,
				'key_value' => '分润奖励',
			],
			[
				'module' => 'finance',
				'key_name' => 'credit1',
				'key_id' => 9,
				'key_value' => '其他',
			],
			[
				'module' => 'finance',
				'key_name' => 'credit1',
				'key_id' => 0,
				'key_value' => '系统',
			],
		];
		DataDictionary::insert($data_dictionary);
	}


	public function credit2()
	{
		$data_dictionary = [
			[
				'module' => 'finance',
				'key_name' => 'credit2',
				'key_id' => 1,
				'key_value' => '充值',
			],
			[
				'module' => 'finance',
				'key_name' => 'credit2',
				'key_id' => 2,
				'key_value' => '订单消费',
			],
			[
				'module' => 'finance',
				'key_name' => 'credit2',
				'key_id' => 0,
				'key_value' => '系统',
			],
		];
		DataDictionary::insert($data_dictionary);
	}


	public function credit3()
	{
		$data_dictionary = [
			[
				'module' => 'finance',
				'key_name' => 'credit3',
				'key_id' => 1,
				'key_value' => '消费返佣',
			],
			[
				'module' => 'finance',
				'key_name' => 'credit3',
				'key_id' => 2,
				'key_value' => '签到',
			],
			[
				'module' => 'finance',
				'key_name' => 'credit3',
				'key_id' => 3,
				'key_value' => '品牌转专区',
			],
			[
				'module' => 'finance',
				'key_name' => 'credit3',
				'key_id' => 4,
				'key_value' => '结算',
			],
			[
				'module' => 'finance',
				'key_name' => 'credit3',
				'key_id' => 0,
				'key_value' => '系统',
			],
		];
		DataDictionary::insert($data_dictionary);
	}



	public function operateLog()
	{
		$data = [
			[
				'module' => 'operate_log',
				'key_name' => 'type',
				'key_id' => '1',
				'key_value' => '系统设置更新',
			],
			[
				'module' => 'operate_log',
				'key_name' => 'type',
				'key_id' => '2',
				'key_value' => '管理员操作',
			],
			[
				'module' => 'operate_log',
				'key_name' => 'type',
				'key_id' => '3',
				'key_value' => '菜单操作',
			],
			[
				'module' => 'operate_log',
				'key_name' => 'type',
				'key_id' => '4',
				'key_value' => '用户操作',
			],
			[
				'module' => 'operate_log',
				'key_name' => 'type',
				'key_id' => '5',
				'key_value' => '角色操作',
			],
			[
				'module' => 'operate_log',
				'key_name' => 'type',
				'key_id' => '6',
				'key_value' => '角色权限操作',
			],
			[
				'module' => 'operate_log',
				'key_name' => 'type',
				'key_id' => '7',
				'key_value' => '用户权限操作',
			],
			[
				'module' => 'operate_log',
				'key_name' => 'type',
				'key_id' => '8',
				'key_value' => '订单操作',
			],
			[
				'module' => 'operate_log',
				'key_name' => 'type',
				'key_id' => '9',
				'key_value' => '金额操作',
			],
			[
				'module' => 'operate_log',
				'key_name' => 'type',
				'key_id' => '10',
				'key_value' => '提现操作',
			],
		];

		DataDictionary::insert($data);
	}


	public function users()
	{
		$data_dictionary = [
			// 会员
			[
				'module' => 'member',
				'key_name' => 'member_status',
				'key_id' => 0,
				'key_value' => '正常',
			],
			[
				'module' => 'member',
				'key_name' => 'member_status',
				'key_id' => 1,
				'key_value' => '禁用',
			],
			// 性别-------------------------------
			[
				'module' => 'member',
				'key_name' => 'gender',
				'key_id' => 0,
				'key_value' => '未知',
			],
			[
				'module' => 'member',
				'key_name' => 'gender',
				'key_id' => 1,
				'key_value' => '男',
			],
			[
				'module' => 'member',
				'key_name' => 'gender',
				'key_id' => 2,
				'key_value' => '女',
			],
			// -----------------------------------
			[
				'module' => 'member',
				'key_name' => 'member_auth_status',
				'key_id' => 0,
				'key_value' => '未认证',
			],
			[
				'module' => 'member',
				'key_name' => 'member_auth_status',
				'key_id' => 1,
				'key_value' => '审核中',
			],
			[
				'module' => 'member',
				'key_name' => 'member_auth_status',
				'key_id' => 2,
				'key_value' => '审核失败',
			],
			[
				'module' => 'member',
				'key_name' => 'member_auth_status',
				'key_id' => 3,
				'key_value' => '认证成功',
			],
			// -----------------------------------
			[
				'module' => 'member',
				'key_name' => 'member_level',
				'key_id' => 0,
				'key_value' => '会员',
			],
			[
				'module' => 'member',
				'key_name' => 'member_level',
				'key_id' => 1,
				'key_value' => 'VIP1',
			],
			[
				'module' => 'member',
				'key_name' => 'member_level',
				'key_id' => 2,
				'key_value' => 'VIP2',
			],
			[
				'module' => 'member',
				'key_name' => 'member_level',
				'key_id' => 3,
				'key_value' => 'VIP3',
			],
			// ------------------------------------
			[
				'module' => 'member',
				'key_name' => 'recharge',
				'key_id' => 1,
				'key_value' => '余额',
			],
			[
				'module' => 'member',
				'key_name' => 'recharge',
				'key_id' => 2,
				'key_value' => '专用券',
			],
			[
				'module' => 'member',
				'key_name' => 'recharge',
				'key_id' => 3,
				'key_value' => '品牌券',
			],


		];
		DataDictionary::insert($data_dictionary);
	}


	public function other()
	{
		$data_dictionary = [
			[
				'module' => 'realname_auth',
				'key_name' => 'type',
				'key_id' => 0,
				'key_value' => '身份证',
			],
			[
				'module' => 'realname_auth',
				'key_name' => 'type',
				'key_id' => 1,
				'key_value' => '港澳台同胞证',
			],
			[
				'module' => 'realname_auth',
				'key_name' => 'type',
				'key_id' => 5,
				'key_value' => '其他',
			],
		];
		DataDictionary::insert($data_dictionary);
	}



	public function orderComment()
	{
		$data_dictionary = [

			[
				'module' => 'order_comment',
				'key_name' => 'status',
				'key_id' => 0,
				'key_value' => '审核中',
			],
			[
				'module' => 'order_comment',
				'key_name' => 'status',
				'key_id' => 1,
				'key_value' => '通过审核',
			],
			[
				'module' => 'order_comment',
				'key_name' => 'status',
				'key_id' => 2,
				'key_value' => '未通过审核',
			],
		];
		DataDictionary::insert($data_dictionary);
	}


	public function recharge()
	{
		$data_dictionary = [
			[
				'module' => 'recharge',
				'key_name' => 'status',
				'key_id' => -1,
				'key_value' => '取消',
			],
			[
				'module' => 'recharge',
				'key_name' => 'status',
				'key_id' => 0,
				'key_value' => '审核中',
			],
			[
				'module' => 'recharge',
				'key_name' => 'status',
				'key_id' => 1,
				'key_value' => '成功',
			],
			[
				'module' => 'recharge',
				'key_name' => 'status',
				'key_id' => 2,
				'key_value' => '失败',
			],
		];
		DataDictionary::insert($data_dictionary);
	}
}
