<?php

declare(strict_types=1);

use Hyperf\Database\Seeders\Seeder;
use App\Model\Module;

class ReplyModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $getSetStatusComment = Module::query()->where([
            'permission' => 'order_comment/set_status'
        ])->first();
        $pid = 134;
        if ($getSetStatusComment) 
        {
            $pid = $getSetStatusComment->pid;
            $getSetStatusComment->update(['role_permission' => 1]);
        }
        $isExistBefore = Module::query()->where([
            'permission' => 'order_comment/reply'
        ])->count();
        if ($isExistBefore > 0) 
        {
            Module::insert([
                'name' => '审核',
                'permission' => 'order_comment/reply',
                'pid' => $pid,
                'sort' => 0,
                'type' => 2,
                'client_type' => 0,
                'is_hidden' => 0,
                'operator_id' => 0,
                'role_permission' => 1,
            ]);
        }
    }
}
