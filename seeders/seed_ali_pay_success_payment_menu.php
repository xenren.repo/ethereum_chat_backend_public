<?php

declare(strict_types=1);

use Hyperf\Database\Seeders\Seeder;
use Hyperf\DbConnection\Db;
class SeedAliPaySuccessPaymentMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'AliPay Paid Transactions',
            'permission' => '',
            'icon' => '',
            'description' => '',
            'path' => 'alipayorder',
            'rel' => 'finance/alipay',
            'pid' => 126,
            'sort' => 0,
            'type' => 1,
            'client_type' => 0,
            'is_hidden' => 0,
        ];

        try {
            Db::table('order')->insert([$data]);
        } catch (\Exception $e){

        }
    }
}
