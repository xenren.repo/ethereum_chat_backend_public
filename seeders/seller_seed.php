<?php

declare(strict_types=1);

use Hyperf\Database\Seeders\Seeder;
use Hyperf\DbConnection\Db;
class SellerSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data= [
            'name' => "投诉卖家", 
            'permission'=>'',
            'icon'=>'',
            'description'=>'',
            'path'=>'report-seller',
            'rel' => 'home/report-seller',
            'pid'=>36, 
            'sort'=>0, 
            'type'=>1, 
            'client_type'=>0, 
            'is_hidden'=>0,
            'operator_id'=>0 

        ];
        $data2= [
            'name' => "商家管理面板", 
            'permission'=>'',
            'icon'=>'shezhi',
            'description'=>'',
            'path'=>'/seller',
            'rel' => '',
            'pid'=>0, 
            'sort'=>0, 
            'type'=>1, 
            'client_type'=>0, 
            'is_hidden'=>0,
            'operator_id'=>0 

        ];

        $data3= [
            'name' => "商家申请审核", 
            'permission'=>'',
            'icon'=>'shezhi',
            'description'=>'',
            'path'=>'approval',
            'rel' => 'seller/approval/index',
            'pid'=>0, 
            'sort'=>0, 
            'type'=>1, 
            'client_type'=>0, 
            'is_hidden'=>0,
            'operator_id'=>0 

        ];

        try{
            Db::table('module')->insert([$data,$data2,$data3]);
            echo 'succes';
        }
        catch (Exception $e) {
	    	throw $e;
	    }
        
    }
}
