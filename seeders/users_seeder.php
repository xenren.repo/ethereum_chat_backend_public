<?php

declare(strict_types=1);

use Hyperf\Database\Seeders\Seeder;
use Hyperf\DbConnection\Db;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [];
	    $num = 100;
	    $name = 13500000001;
	    $pid = 0;
	    $password = password_hash('111111',PASSWORD_BCRYPT);
	    for ($i = 0; $i < $num; $i++) {
		    $auth_status = mt_rand(0,3);
		    $temp = [
			    'name'     => $name,
			    'mobile'     => $name,
			    'realname'     => \Hyperf\Utils\Str::random(8),
			    'status'   => 0,
			    'auth_status'  => $auth_status,
			    'level'  => mt_rand(0,1),
			    'credit1'  => mt_rand(0,10000),
			    'credit2'  => mt_rand(0,10000),
			    'password' => $password,
			    'pid'      => $pid,
		    ];
		    $data[] = $temp;
		    $pid++;
		    $name++;
	    }

	    Db::table('users')->insert($data);
    }
}
