<?php

return [
	'system_error' => 'system_error',
	'unset_pay_password' => '支付密码未配置',
	'version_update' => '系统繁忙',
	'permission_deny' => '未授权操作',
	'params_invalid' => 'params invalid',
	'throttle_submit' => '操作过于频繁',
	'auth_fail' => '未登入',
];