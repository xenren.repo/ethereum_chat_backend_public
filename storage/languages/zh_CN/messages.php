<?php

return [
	'system_error' => '系统错误',
	'unset_pay_password' => '支付密码未配置',
	'version_update' => '系统繁忙',
	'permission_deny' => '未授权操作',
	'params_invalid' => '参数错误',
	'throttle_submit' => '操作过于频繁',
	'auth_fail' => '未登入',
	'without_phone' => '手机号码未绑定',


];