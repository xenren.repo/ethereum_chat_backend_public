<?php

return [
	'name' => '名称',
	'nickname' => '名称',
	'sort' => '排序',
	'title' => '名称',

	'is_hidden' => '是否隐藏',
	'auth' => [
		'name' => '账号',
	],
	'password' => '密码',
	'old_password' => '原密码',

	'permissions' => '权限',

	// 菜单
	'module' => [
		'pid'  => '上级菜单',
		'type' => '菜单类型',
	],
	'client_type' => '客户端类型',
	'role'   => [
		'pid'  => '角色分组',
	],
	'url' => '跳转地址',
	'account' => '账户类型',
	'change' => '变动类型',
	'money' => '数目',


	'recharge' => [
		'cny_amount' => '折算CNY',
		'credit' => '凭证',
		'credit.*' => '凭证',
		'remark' => '用户',
		'sender' => '汇款人',
	],
	'goods' => [
		'original_price'    => '原价',
		'express_cost'        => '邮费',
		'cost_price'        => '成本',
		'content'           => '详情',
		'sale_num'        => '销量',
		'stock'             => '库存',
		'source_from'     => '产品来源',
		'on_sale'         => '状态',
		'is_commission' => '是否参与分销',
		'type'            => '类型',
		'is_recommend'    => '是否推荐',
		'is_new'          => '是否新品',
		'is_hot'          => '是否爆款',
		'ship_free'       => '是否包邮',
		'unit'              => '单位',
		'weight'            => '重量',
		'category'          => '分类',
		'options'           => '规格',
		'params'          => '参数',
		'spec_item'          => '规格项',
		'item_endprice'		=> '团购至少是0'
	],
	'goods_id' => '产品ID',
	'user_address' => [
		'name' => '收件人',
		'tag' => '标签',
		'code' => '邮政编码',
		'is_default' => '是否默认',
	],
	'user_cart' => [
		'option_id' => '规格id',
	],
	'order' => [
		'goods' => '产品信息',
		'user_info' => '收货信息',
		'express_amount' => '运费',
		'goods_amount' => '产品总额',
		'total_amount' => '订单总额',
		'order_id' => '订单id'
	],
	'order_package' => [
		'custom_code' => '商家编码',
	],
	'order_refund' => [
		'reason' => '退款原因',
		'refund_amount' => '退款金额',
		'apply_type' => '退款方式',
		'refund_credit' => '退款凭证',
	],
];