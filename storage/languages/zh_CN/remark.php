<?php
declare(strict_types=1);

return [
    'brand_voucher_converted' => '【:amount】品牌券成功转换成现金券',

    'exchange' => [
        'voucher_to_fr_rmb' => '品牌券转换成【:amount】现金券',
        'brand_coupon_to_zone_coupon_in_credit3' => '扣取【:amount】品牌券，转换成专区券',
        'brand_coupon_to_zone_coupon_in_credit2' => '获得【:amount】专区券',
        'refund_one_percent_spent' => "下级申请退款通过，退还1%奖金。"
    ]
];