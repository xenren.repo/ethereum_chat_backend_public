<?php

declare(strict_types=1);

namespace HyperfTest\Cases\User\Auth;

use HyperfTest\HttpTestCase;

/**
 * @internal
 * @coversNothing
 */
class LoginTest extends HttpTestCase
{
    protected $loginUrl = '/user/login';

    public function testLoginSuccess()
    {
        $this->assertTrue(true);
        $data = [
            'name' => 'fong7890',
            'password' => 'Neo12345',
        ];

        $responseStructure = [
            'code',
            'data' => [
                'token',
                'expire',
                'info' => [
                    'id',
                    'name',
                    'mobile',
                    'nickname',
                    'realname',
                    'avatar',
                    'gender',
                    'level',
                    'credit1',
                    'credit2',
                    'credit3',
                    'reward_mode',
                    'mudan',
                    'auth_status',
                    'status',
                    'created_at',
                    'has_sign',
                ],
                'im' => [
                    'accid',
                    'token',
                ],
            ],
        ];

        $response = $this->json($this->loginUrl, $data, [
            'Accept' => 'application/json',
        ]);
        $this->assertArrayStructure($responseStructure, $response);
        $this->assertSame(0, $response['code']);
    }
}
